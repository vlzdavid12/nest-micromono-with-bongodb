import {IsNumber, IsOptional, IsPositive, IsString, Min} from "class-validator";

export class PaginationDtoFilter {

    @IsString()
    @IsOptional()
    search?: string;

    @IsOptional()
    @IsNumber()
    @Min(1)
    @IsPositive()
    limit?: number;

    @IsOptional()
    @IsNumber()
    @Min(0)
    @IsPositive()
    offset?: number;

    @IsOptional()
    @IsString()
    orderBy?: string

}
