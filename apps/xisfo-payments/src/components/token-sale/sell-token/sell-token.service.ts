import {Injectable, NotFoundException} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginationDto } from 'apps/xisfo-gateway/src/commons/paginationDto';
import { Model } from 'mongoose';
import { PaymentRequest } from '../../payment/payment-request/entities/payment-request.entity';
import { CreateSellTokenDto } from './dto/create-sell-token.dto';
import { UpdateSellTokenDto } from './dto/update-sell-token.dto';
import { SellToken } from './entities/sell-token.entity';

@Injectable()
export class SellTokenService {
  constructor(
    @InjectModel(SellToken.name) private sellTokenModel: Model<SellToken>,
    @InjectModel(PaymentRequest.name) private paymentRequestModel: Model<PaymentRequest>,
  ) { }

  async create(createSellTokenDto: CreateSellTokenDto) {
    const client_total = (createSellTokenDto.trm - 200)* createSellTokenDto.amount_tokens;

    const a = await this.paymentRequestModel.create({
      client_platform: createSellTokenDto.client_platform,
      amount: client_total / createSellTokenDto.trm,
      invoice: createSellTokenDto.invoice,
      payment_support: createSellTokenDto.payment_support,
      is_identified: true,
      is_active: true,
      deleted_at: null,
    });
    
    return await this.sellTokenModel.create({
      payment_request: a._id,
      amount_tokens: createSellTokenDto.amount_tokens,
      trm: createSellTokenDto.trm,
      is_active: true,
      deleted_at: null 
    });
  }

  async findAll(paginationDto: PaginationDto) {
    const {limit = 12, offset = 1, orderBy} = paginationDto;
    const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
    try{
      const count = await this.sellTokenModel.count();
      const data = await this.sellTokenModel.find()
      .skip((limit * offset) - limit)
      .limit(limit)
      .sort(order)
      .populate('payment_request')
      .exec();
      return {data, count, limit, offset, orderBy};
    }catch (error){
      throw new NotFoundException();
    }

  }

  async findOne(id: string) {
    return await this.sellTokenModel.findById(id)
    .populate('payment_request')
    .exec();
  }

  async update(id: string, updateSellTokenDto: UpdateSellTokenDto) {
    const sellTokenUpdated = await this.sellTokenModel
    .findByIdAndUpdate(id, updateSellTokenDto);
    return sellTokenUpdated;
  }

  async remove(id: string) {
    return await this.sellTokenModel
    .findByIdAndUpdate(id, {
      deleted_at: new Date,
      is_active: false
    })
    .exec();
  }
}
