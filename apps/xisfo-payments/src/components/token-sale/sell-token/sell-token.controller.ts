import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { SellTokenService } from './sell-token.service';
import { CreateSellTokenDto } from './dto/create-sell-token.dto';
import { UpdateSellTokenDto } from './dto/update-sell-token.dto';
import {PaginationDto} from "../../../commons/paginationDto";

@Controller()
export class SellTokenController {
  constructor(private readonly sellTokenService: SellTokenService) {}

  @MessagePattern('createSellToken')
  create(@Payload() createSellTokenDto: CreateSellTokenDto) {
    return this.sellTokenService.create(createSellTokenDto);
  }

  @MessagePattern('findAllSellToken')
  findAll(paginationDto: PaginationDto) {
    return this.sellTokenService.findAll(paginationDto);
  }

  @MessagePattern('findOneSellToken')
  findOne(@Payload() id: string) {
    return this.sellTokenService.findOne(id);
  }

  @MessagePattern('updateSellToken')
  update(@Payload() updateSellTokenDto: UpdateSellTokenDto) {
    return this.sellTokenService.update(updateSellTokenDto.id, updateSellTokenDto);
  }

  @MessagePattern('removeSellToken')
  remove(@Payload() id: string) {
    return this.sellTokenService.remove(id);
  }
}
