import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, {Document} from 'mongoose';
import { PaymentRequest } from '../../../payment/payment-request/entities/payment-request.entity';

@Schema({ collection: 'sell_tokens', timestamps: true })
export class SellToken extends Document {

  @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'PaymentRequest' })
  payment_request: PaymentRequest;

  @Prop({ required: true })
  amount_tokens: number;

  @Prop({ required: true })
  trm: number;

  @Prop({required: true})
  is_active: boolean;

  @Prop({required: false})
  deleted_at: Date;
}

export const SellTokenSchema = SchemaFactory.createForClass(SellToken);
