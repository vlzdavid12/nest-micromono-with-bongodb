import { Module } from '@nestjs/common';
import { SellTokenService } from './sell-token.service';
import { SellTokenController } from './sell-token.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { SellToken, SellTokenSchema } from './entities/sell-token.entity';
import { PaymentRequest, PaymentRequestSchema } from '../../payment/payment-request/entities/payment-request.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: SellToken.name,
        schema: SellTokenSchema,
      },
      {
        name: PaymentRequest.name,
        schema: PaymentRequestSchema,
      }
    ]),
  ],
  controllers: [SellTokenController],
  providers: [SellTokenService]
})
export class SellTokenModule {}
