import { IsDecimal, IsMongoId, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateSellTokenDto {

    @ApiProperty()
    @IsMongoId()
    @IsNotEmpty()
    client_platform: string;

    @ApiProperty()
    // @IsNumber()
    @IsNotEmpty()
    amount_tokens: number;

    @ApiProperty()
    // @IsNumber()
    @IsNotEmpty()
    trm: number;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    invoice: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    payment_support: string;
}
