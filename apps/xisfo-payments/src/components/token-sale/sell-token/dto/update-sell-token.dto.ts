import { PartialType } from '@nestjs/mapped-types';
import { IsMongoId, IsNotEmpty } from 'class-validator';
import { CreateSellTokenDto } from './create-sell-token.dto';

export class UpdateSellTokenDto extends PartialType(CreateSellTokenDto) {

  @IsMongoId()
  @IsNotEmpty()
  id: string;
}
