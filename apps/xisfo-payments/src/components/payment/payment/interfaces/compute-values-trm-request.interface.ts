export interface ComputeValuesTrmInterface {
   results: ComputeValuesTrm
}

export interface ComputeValuesTrm {
    massive_payment: number;
    trm: number;
}
