export interface ExportPaymentInterface {
 results: ExportPayment
}

export interface ExportPayment {
    massive_payment: number;
    client: string;
    client_value_usd: number;
    subtotal_usd: number;
    trm: number;
    subtotal_cop: number;
    with_holding_tax: number;
    client_pay: number;
}
