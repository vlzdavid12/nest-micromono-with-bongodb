import { ObjectId } from "mongoose";
import { PaymentRequest } from "../../payment-request/interface/payment-request.interface";

export interface PaymentInterface {
    results: Payment
}

export interface Payment {
    _id?: ObjectId;
    client_total: number;
    client_pay: number;
    client_value_usd: number;
    xisfo_commission_general_usd: number;
    trm: number;
    tax: number;
    subtotal_usd:number;
    subtotal_cop:number;
    bank_commission: number;
    platform_commission: number;
    with_holding_tax: number;
    is_immediate: boolean;
    is_paid: boolean;
    in_management: boolean;
    massive_payment: number;
    payment_request: PaymentRequest;
    is_active: boolean;
    deleted_at: Date;
}
