import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { PaymentService } from './payment.service';
import { CreatePaymentDto } from './dto/create-payment.dto';
import { UpdatePaymentDto } from './dto/update-payment.dto';
import { ComputeValuesTrm } from './interfaces/compute-values-trm-request.interface';
import {PaginationDtoFilter} from "../../../commons/paginationDto.filter";
import { PaginationDto } from 'apps/xisfo-payments/src/commons/paginationDto';

@Controller()
export class PaymentController {
  constructor(private readonly paymentService: PaymentService) {}

  @MessagePattern('createPayment')
  create(@Payload() createPaymentDto: CreatePaymentDto) {
    return this.paymentService.create(createPaymentDto);
  }

  @MessagePattern('findAllPayment')
  findAll(@Payload() paginationDtoFilter: PaginationDtoFilter) {
    return this.paymentService.findAll(paginationDtoFilter);
  }

  @MessagePattern('findAllInManagementPayments')
  findAllInManagementPayments(@Payload() paginationDto: PaginationDto) {
    return this.paymentService.findAllInManagementPayments(paginationDto);
  }

  @MessagePattern('paymentsGroupedByGiro')
  paymentsGroupedByGiro(@Payload() paginationDto: PaginationDto) {
    return this.paymentService.paymentsGroupedByGiro(paginationDto);
  }

  @MessagePattern('findPaymentsByGiro')
  findPaymentsByGiro(@Payload() payload: any) {
    return this.paymentService.findPaymentsByGiro(payload.giro, payload.paginationDto);
  }

  @MessagePattern('findOnePayment')
  findOne(@Payload() id: string) {
    return this.paymentService.findOne(id);
  }

  @MessagePattern('updatePayment')
  update(@Payload() updatePaymentDto: UpdatePaymentDto) {
    return this.paymentService.update(updatePaymentDto.id, updatePaymentDto);
  }

  @MessagePattern('removePayment')
  remove(@Payload() id: string) {
    return this.paymentService.remove(id);
  }

  @MessagePattern('computeValuesTrm')
  computeValuesTrm(@Payload() payload: ComputeValuesTrm) {
    return this.paymentService.computeValuesTrm(payload.massive_payment, payload.trm);
  }

  @MessagePattern('closePaymentsManagement')
  closePaymentsManagement() {
    return this.paymentService.closePaymentsManagement();
  }

  @MessagePattern('exportPayment')
  exportPaymentExcel(@Payload() payload: number) {
    return this.paymentService.downloadExcel(payload);
  }
}
