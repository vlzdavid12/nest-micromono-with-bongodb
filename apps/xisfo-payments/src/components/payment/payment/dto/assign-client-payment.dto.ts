import { IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class AssignClientPaymentDto {

  @IsMongoId()
  @IsNotEmpty()
  id: string;

  @IsString()
  @IsNotEmpty()
  client_platform: string;
}
