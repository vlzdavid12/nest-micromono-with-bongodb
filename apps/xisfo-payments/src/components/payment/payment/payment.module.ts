import { Module } from '@nestjs/common';
import { PaymentService } from './payment.service';
import { PaymentController } from './payment.controller';
import { MongooseModule } from '@nestjs/mongoose';
import {PaymentEntity, PaymentSchema} from './entities/payment.entity';
import { ClientPlatformEntity, ClientPlatformSchema } from '../../client/client-platform/entities/client-platform.entity';
import { PaymentRequest, PaymentRequestSchema } from '../payment-request/entities/payment-request.entity';
import { PlatformEntity, PlatformSchema } from '../../platform/platform/entities/platform.entity';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: PaymentEntity.name,
        schema: PaymentSchema,
      },
      {
        name: ClientPlatformEntity.name,
        schema: ClientPlatformSchema,
      },
      {
        name: PaymentRequest.name,
        schema: PaymentRequestSchema,
      },
      {
        name: PlatformEntity.name,
        schema: PlatformSchema,
      }
    ]),
    ConfigModule,
  ],
  controllers: [PaymentController],
  providers: [PaymentService]
})
export class PaymentModule {}
