import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, {Document} from 'mongoose';
import { PaymentRequest } from '../../payment-request/entities/payment-request.entity';

@Schema({ collection: 'payments', timestamps: true })
export class PaymentEntity extends Document {
    @Prop({
        required: true,
        type: mongoose.Schema.Types.ObjectId,
        ref: 'PaymentRequest',
        unique: true
    })
    payment_request: PaymentRequest;

    @Prop({ required: true, default: 0 })
    bank_commission: number;

    @Prop({ required: true, default: 0 })
    subtotal_usd: number;

    @Prop({ required: true, default: 0 })
    subtotal_cop: number;

    @Prop({ required: true, default: 0 })
    xisfo_commission_col: number;

    @Prop({ required: true, default: 0 })
    xisfo_commission_usa: number;

    @Prop({ required: true, default: 0 })
    trm: number;

    @Prop({ required: true, default: 0 })
    with_holding_tax: number;

    //4xmil = impuesto
    @Prop({ required: true, default: 0 })
    tax: number;

    @Prop({ required: true, default: 0 })
    client_total: number;

    @Prop({ required: true, default: 0 })
    client_pay: number;

    @Prop({ required: true })
    client_value_usd: number;

    @Prop({ required: true, default: 0 })
    xisfo_commission_general_usd: number;

    @Prop({ required: true, default: 0 })
    platform_commission: number;

    @Prop({ required: true, default: false })
    is_paid: boolean;

    @Prop({ required: true, default: false })
    is_immediate: boolean;

    @Prop({ required: true, default: true })
    in_management: boolean;

    @Prop({ required: true, default: 0 })
    massive_payment: number;

    @Prop({required: true})
    is_active: boolean;

    @Prop({required: false})
    deleted_at: Date;
}

export const PaymentSchema = SchemaFactory.createForClass(PaymentEntity)
    .plugin(require('mongoose-unique-validator'));
