import {BadRequestException, Injectable, NotFoundException} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Workbook} from 'exceljs';
import {Model} from 'mongoose';
import {ClientPlatformEntity} from '../../client/client-platform/entities/client-platform.entity';
import {PlatformEntity} from '../../platform/platform/entities/platform.entity';
import {PaymentRequest} from '../payment-request/entities/payment-request.entity';
import {CreatePaymentDto} from './dto/create-payment.dto';
import {UpdatePaymentDto} from './dto/update-payment.dto';
import {PaymentEntity} from './entities/payment.entity';
import {ExportPayment} from './interfaces/export-payment.interface';
import {Payment} from './interfaces/payment-interface.interface';
import * as tmp from 'tmp';
import {PaginationDtoFilter} from "../../../commons/paginationDto.filter";
import { PaginationDto } from 'apps/xisfo-payments/src/commons/paginationDto';

@Injectable()
export class PaymentService {

    constructor(
        @InjectModel(PaymentEntity.name) private paymentModel: Model<PaymentEntity>,
        @InjectModel(ClientPlatformEntity.name) private clientPlatformModel: Model<ClientPlatformEntity>,
        @InjectModel(PaymentRequest.name) private paymentRequestModel: Model<PaymentRequest>,
        @InjectModel(PlatformEntity.name) private platformtModel: Model<PlatformEntity>,
    ) {
    }

    async create(createPaymentDto: CreatePaymentDto) {
        const payments: Payment[] = await this.paymentModel.find()
            .populate([
                {
                    path: 'payment_request',
                    populate: {
                        path: 'client_platform', model: 'ClientPlatformEntity',
                        populate: {path: 'client', model: 'ClientEntity'}
                    },
                },
                {
                    path: 'payment_request',
                    populate: {
                        path: 'client_platform', model: 'ClientPlatformEntity',
                        populate: {path: 'platform', model: 'PlatformEntity'}
                    },
                },
                {
                    path: 'payment_request',
                    populate: {
                        path: 'client_platform', model: 'ClientPlatformEntity',
                        populate: {path: 'nicho', model: 'NichoEntity'}
                    },
                },
            ])
            .exec();

        const client = await this.paymentRequestModel.findById(createPaymentDto.payment_request)
            .populate([
                {
                    path: 'client_platform',
                    populate: {path: 'client', model: 'ClientEntity'}
                },
                {
                    path: 'client_platform',
                    populate: {path: 'platform', model: 'PlatformEntity'}
                },
            ])
            .exec();

        if (!client) {
            throw new Error("Payment request not found");
        }
        createPaymentDto.client_value_usd = client.amount

        // Tiene todas las plataformas
        const plataforms = await this.findPlatforms();

        //se valida la plataforma para realizar los pagos segun el id de plataforma
        switch (client.client_platform.platform._id) {
            //seccion para Epay
            case plataforms.epay._id: {
                this.epayPayments(payments, client.client_platform, createPaymentDto);
                break;
            }

            //seccion para Streamate
            case plataforms.streamate._id: {
                this.streamatePayments(payments, client.client_platform, createPaymentDto);
                break;
            }

            //seccion para pagos de plataformas generales
            default: {
                this.generalPlatformsPayments(payments, client.client_platform, createPaymentDto);
                break;
            }
        }

        //funcion para calcular los valores del pago
        this.computeValuesPayments(client.client_platform, createPaymentDto);

    return await this.paymentModel.create({is_active: true, deleted_at: null, ...createPaymentDto});
  }

    async paymentsGroupedByGiro(paginationDto: PaginationDto) {
        try {
            const {limit = 10, offset = 0, orderBy} = paginationDto;
            const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
            const count = await this.paymentModel.count();
            const data = await this.paymentModel.aggregate([
                {
                    $group:
                    {
                        _id: { massive_payment: "$massive_payment" },
                        trm: { $addToSet: "$trm" },
                        amount: { $sum: "$client_pay" },
                        date: { $addToSet: "$createdAt" }
                    }
                }
            ]);
            return {data, count, limit, offset, order};
        }catch (error){
            throw new NotFoundException();
        }
    }

    async findAll(paginationDtoFilter: PaginationDtoFilter) {
        try {
            const {search, limit = 10, offset = 1, orderBy} = paginationDtoFilter;
            const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
            const count = await this.paymentModel.count();
            const data = await this.paymentModel.find()
                .skip(offset)
                .limit(limit)
                .sort(order);
            return {data, count, limit, offset, orderBy};
        }catch (error){
            throw new NotFoundException();
        }
    }

    async findAllInManagementPayments(paginationDto: PaginationDto) {
        try {
            const {limit = 10, offset = 0, orderBy} = paginationDto;
            const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
            const count = await this.paymentModel.count();
            const data = await this.paymentModel.find({
                in_management: true,
                deleted_at: null
            })
            .skip(offset)
            .limit(limit)
            .sort(order);
            return {data, count, limit, offset, orderBy};
        }catch (error){
            throw new NotFoundException();
        }
    }

    async findOne(id: string) {
        try{
            return await this.paymentModel.findById(id);
        }catch (error){
            throw new NotFoundException();
        }
    }

    async findPaymentsByGiro(giro: number, paginationDto: PaginationDto) {
        try{
            const {limit = 10, offset = 0, orderBy} = paginationDto;
            const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
            const count = await this.paymentModel.count();
            const data =  await this.paymentModel.find({
                massive_payment: giro
            })
            .limit(limit)
            .skip(offset)
            .sort(order);
            return {data, count, limit, offset, orderBy};
        }catch (error){
            throw new NotFoundException();
        }
    }

    async update(id: string, updatePaymentDto: UpdatePaymentDto) {
        const PaymentUpdated = await this.paymentModel
            .findByIdAndUpdate(id, updatePaymentDto);
        return PaymentUpdated;
    }

    async remove(id: string) {
        return await this.paymentModel
        .findByIdAndUpdate(id, {
        deleted_at: new Date,
        is_active: false
        })
        .exec();
    }

    /**
     * Esta funcion busca las plataformas que tienen comisiones especiales
     */
    async findPlatforms() {
        const stripchat = await this.platformtModel.findOne({name: 'Stripchat'});
        const epay = await this.platformtModel.findOne({name: 'EpayService'});
        const streamate = await this.platformtModel.findOne({name: 'Streamate'});
        return {stripchat, epay, streamate};
    }

    //calcular pagos para plataformas de epay y de epay
    async epayPayments(payments, client, paymentRequest) {
        //se valida si el cliente es antiguo o no
        if (client.client.old_client == true) {
            //se calcula prorrateo
            this.apportionment(payments, paymentRequest);
            //se valida si el pago es inmediato o no para calcular la comision de la plataforma
            if (paymentRequest.is_immediate == true) {
                paymentRequest.platform_commission = 30 + (paymentRequest.client_value_usd * 0.005);
            } else {
                //se calcula prorrateo si es epay
                this.apportionmentEpay(payments, paymentRequest);
            }
            return paymentRequest;
        } else {
            //se calcula prorrateo
            this.apportionment(payments, paymentRequest);
            //se valida si el pago es inmediato o no para calcular la comision de la plataforma
            if (paymentRequest.is_immediate == true) {
                paymentRequest.bank_commission = 5;
                paymentRequest.platform_commission = 30 + (paymentRequest.client_value_usd * 0.005);
            } else {
                //se valida si ya se cobró la comision o no, y se valida si el cliente es el cliente sin identificar o no
                paymentRequest.bank_commission = this.alreadyExistsBankCommission(client, paymentRequest) && client.client._id != "634f0d07c7f5da9d6cd39a41" ? 0 : 5;
                paymentRequest.platform_commission = this.alreadyExistsPlatformCommission(client) && client.client._id != "634f0d07c7f5da9d6cd39a41" ? 0 : 10;
                paymentRequest.platform_commission = paymentRequest.platform_commission + (paymentRequest.client_value_usd * 0.005);
            }
            return paymentRequest;
        }
    }

    //calcular pagos para streamate
    async streamatePayments(payments, client, paymentRequest) {
        //se valida si el cliente es antiguo o no
        if (client.client.old_client == true) {
            //se prorratea la comision bancaria
            this.apportionment(payments, paymentRequest);
            paymentRequest.platform_commission = 0
            return paymentRequest;
        } else {
            //se valida si ya se cobró la comision o no, y se valida si el cliente es el cliente sin identificar o no
            paymentRequest.bank_commission = this.alreadyExistsBankCommission(client, paymentRequest) && client.client._id != "634f0d07c7f5da9d6cd39a41" ? 0 : 5;
            paymentRequest.platform_commission = this.alreadyExistsPlatformCommission(client) && client.client._id != "634f0d07c7f5da9d6cd39a41" ? 0 : 5;
            return paymentRequest;
        }
    }

    //calcular pagos para otras plataformas
    async generalPlatformsPayments(payments, client, paymentRequest) {
        //se valida si el cliente es antiguo o no
        if (client.client.old_client == true) {
            //se prorratea la comision bancaria
            this.apportionment(payments, paymentRequest);
            paymentRequest.platform_commission = 0;
            return paymentRequest;
        } else {
            //se valida si ya se cobró la comision o no, y se valida si el cliente es el cliente sin identificar o no
            paymentRequest.bank_commission = this.alreadyExistsBankCommission(client, paymentRequest) && client.client._id != "634f0d07c7f5da9d6cd39a41" ? 0 : 5;
            paymentRequest.platform_commission = 0;
            return paymentRequest;
        }
    }

    //calcular prorrateo
    async apportionment(payments, paymentRequest) {
        let accumulatedAmountTotal = paymentRequest.client_value_usd;
        const commissionForeingBank = 40;

        //Se suman todos los pagos que estan en el 'giro'
        payments.map((pay) => {
            if (pay.massive_payment == paymentRequest.massive_payment) {
                accumulatedAmountTotal += pay.client_value_usd;
            }
        });

        //se calcula prorrateo de la comision del banco para el cliente
        paymentRequest.bank_commission = (paymentRequest.client_value_usd * commissionForeingBank) / accumulatedAmountTotal;
        return paymentRequest;
    }

    //calcular prorrateo epay
    async apportionmentEpay(payments, paymentRequest) {
        let accumulatedAmountTotal = 0;
        const commissionEpay = 30;

        //Se suman todos los pagos que estan en el 'giro'
        payments.forEach(function (pay) {
            if (pay['massive_payment'] == paymentRequest.massive_payment) {
                accumulatedAmountTotal += pay['client_value_usd'];
            }
        });

        //se calcula el 0.005 por cada dolarclient_platform
        let dollarCommission = paymentRequest.client_value_usd * 0.005;

        //se calcula el prorrateo
        let apportionment = (paymentRequest.client_value_usd * commissionEpay) / accumulatedAmountTotal;

        //se verifica el minimo del prorrateo, es 5
        apportionment = apportionment < 5 ? 5 : apportionment;

        paymentRequest.platform_commission = dollarCommission + apportionment;
    }

    //comprobar si el cliente ya tiene pagos con comision
    async alreadyExistsBankCommission(client, paymentRequest) {
        const clientPayments = await this.paymentModel.find({
            'client_platform.client.id': client.client.id,
            is_immediate: paymentRequest.is_immediate == false
        });

        return clientPayments.length > 1 ? true : false;
    }

    // comprobar si la plataforma del cliente tiene comision
    async alreadyExistsPlatformCommission(client) {
        const platformsClient = await this.paymentModel.find({
            'client_platform.client.id': client.client.id,
            'client_platform.platform.id': client.platform.id,
            is_immediate: false,
        });

        return platformsClient.length > 1 ? true : false;
    }

    //formula para redondear a 2 decimales = Math.round((expresion) * 100) /100
    async computeValuesPayments(client, paymentRequest, trm?) {

        //Se valida si existe trm, si no, se calculan datos por 0
        paymentRequest.trm = trm ?? 0;
        //Si no se ha puesto trm, se calculan los datos que no requieren de la trm
        paymentRequest.client_total = Math.round((paymentRequest.client_value_usd * (1 - client.client.client_rate)) * 100) / 100;

        paymentRequest.subtotal_usd = paymentRequest.client_value_usd - paymentRequest.bank_commission - paymentRequest.platform_commission;
        paymentRequest.subtotal_cop = Math.round((paymentRequest.subtotal_usd * paymentRequest.trm) * 100) / 100;

        paymentRequest.xisfo_commission_general_usd = Math.round((paymentRequest.client_value_usd * client.client.client_rate) * 100) / 100;
        paymentRequest.xisfo_commission_usa = Math.round((paymentRequest.xisfo_commission_general_usd * 0.4) * 100) / 100;
        paymentRequest.xisfo_commission_col = Math.round((paymentRequest.xisfo_commission_general_usd * 0.6) * 100) / 100;
        paymentRequest.xisfo_commission_col = Math.round((paymentRequest.xisfo_commission_col * paymentRequest.trm) * 100) / 100;

        paymentRequest.with_holding_tax = client.client.with_holding_tax == true ? Math.round(((Math.round((paymentRequest.xisfo_commission_general_usd * 0.07) * 100) / 100) * paymentRequest.trm) * 100) / 100 : 0;
        paymentRequest.client_pay = paymentRequest.subtotal_cop - (Math.round((paymentRequest.xisfo_commission_general_usd * paymentRequest.trm) * 100) / 100) + paymentRequest.with_holding_tax;
        paymentRequest.tax = Math.round(((paymentRequest.client_pay * 4) / 1000) * 100) / 100;

        return paymentRequest;
    }

    //calcular cada pago con la trm ingresada
    async computeValuesTrm(massivePayment, trm) { 
        const payments: Payment[] = await this.paymentModel.find({
            'massive_payment': massivePayment,
            'trm': 0,
        })
            .populate([
                {
                    path: 'payment_request',
                    populate: {
                        path: 'client_platform', model: 'ClientPlatformEntity',
                        populate: {path: 'client', model: 'ClientEntity'}
                    },
                },
                {
                    path: 'payment_request',
                    populate: {
                        path: 'client_platform', model: 'ClientPlatformEntity',
                        populate: {path: 'platform', model: 'PlatformEntity'}
                    },
                },
                {
                    path: 'payment_request',
                    populate: {
                        path: 'client_platform', model: 'ClientPlatformEntity',
                        populate: {path: 'nicho', model: 'NichoEntity'}
                    },
                },
            ])
            .exec();
        
        const payment = await Promise.all(payments.map(async (pay) => {
            const client = await this.clientPlatformModel.findById(pay.payment_request.client_platform)
                .populate('client')
                .populate('platform')
                .exec();

            pay = await this.computeValuesPayments(client, pay, trm);

            await this.paymentModel.findByIdAndUpdate(pay._id, pay);

            return pay;
        }));

        return payment;
    }

    //cerrar un pago y asignarle un giro y estado en gestion = falso
    async closePaymentsManagement() {        
        //traer los pagos pendientes, o sea los pagos que no tienen giro asignado
        const pendingPayments: Payment[] = await this.paymentModel.find({
            massive_payment: 0,
            deleted_at: null
        })
            .populate([
                {
                    path: 'payment_request',
                    populate: {
                        path: 'client_platform', model: 'ClientPlatformEntity',
                        populate: {path: 'client', model: 'ClientEntity'}
                    },
                },
                {
                    path: 'payment_request',
                    populate: {
                        path: 'client_platform', model: 'ClientPlatformEntity',
                        populate: {path: 'platform', model: 'PlatformEntity'}
                    },
                },
                {
                    path: 'payment_request',
                    populate: {
                        path: 'client_platform', model: 'ClientPlatformEntity',
                        populate: {path: 'nicho', model: 'NichoEntity'}
                    },
                }
            ])
            .exec();

        //traer el pago con el giro mas grande
        const lastPayment: Payment = await this.paymentModel.findOne()
            .sort({massive_payment: -1})
            .limit(1);

        //definir el nuevo numero de giro, consecutivo del giro mas grande del pago anterior
        const lastMassivePayment = lastPayment.massive_payment += 1;

        //asignar giro a pagos pendientes y cambiar estado de gestion
        const payment = await Promise.all(pendingPayments.map(async (pay) => {
            if (pay.trm) {
                pay.massive_payment = lastMassivePayment;
                pay.in_management = false;
                pay.is_active = false;

                await this.paymentModel.findByIdAndUpdate(pay._id, pay);

                return pay;
            } else {
                return 'missing to assign the TRM';
            }
        }));

        return payment;
    }

    //exportar pagos a excel
    async downloadExcel(massivePayment) {
        //traer pagos segun el giro
        const payments: Payment[] = await this.paymentModel.find({
            'massive_payment': massivePayment
        })
            .populate([
                {
                    path: 'payment_request',
                    populate: {
                        path: 'client_platform', model: 'ClientPlatformEntity',
                        populate: {path: 'client', model: 'ClientEntity'}
                    },
                },
                {
                    path: 'payment_request',
                    populate: {
                        path: 'client_platform', model: 'ClientPlatformEntity',
                        populate: {path: 'platform', model: 'PlatformEntity'}
                    },
                },
                {
                    path: 'payment_request',
                    populate: {
                        path: 'client_platform', model: 'ClientPlatformEntity',
                        populate: {path: 'nicho', model: 'NichoEntity'}
                    },
                },
            ])
            .exec();

        //asignar pagos a una variable para manejar la data en el documento
        const exportPayments = payments.map((pay) => {
            let dataPayments: ExportPayment = {
                massive_payment: pay.massive_payment,
                client: pay.payment_request.client_platform.client.first_name,
                client_value_usd: pay.client_value_usd,
                subtotal_usd: pay.subtotal_usd,
                trm: pay.trm,
                subtotal_cop: pay.subtotal_cop,
                with_holding_tax: pay.with_holding_tax,
                client_pay: pay.client_pay
            }
            return dataPayments;
        });

        let rows = []

        //se guarda cada dato para ser añadido como fila
        exportPayments.forEach(doc => {
            rows.push(Object.values(doc))
        })

        let book = new Workbook;

        //se crea una hoja de trabajo al documento
        let sheet = book.addWorksheet('payments');

        //se añade un header
        rows.unshift(Object.keys(exportPayments[0]))

        //se añaden multiples filas en la hoja
        sheet.addRows(rows);

        //Se crea un archivo temporal y despues podra descargarse (el archivo se esta guardando en la carpeta temp del sistema)
        let File = await new Promise((resolve, reject) => {
            tmp.file({
                discardDescriptor: true,
                prefix: `Payments`,
                postfix: '.xlsx',
                node: parseInt('0600', 8)
            }, async (err, file) => {
                if (err)
                    throw new BadRequestException(err);

                book.xlsx.writeFile(file).then(_ => {
                    resolve(file)
                }).catch(err => {
                    throw new BadRequestException(err)
                })
            })
        });

        return File;
    }
}
