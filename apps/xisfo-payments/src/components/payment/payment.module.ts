import { Module } from '@nestjs/common';
import { PaymentRequestModule } from './payment-request/payment-request.module';
import { PaymentModule as Payment } from './payment/payment.module';

@Module({
  imports: [PaymentRequestModule, Payment],
  exports: [PaymentRequestModule, Payment]
})
export class PaymentModule {}
