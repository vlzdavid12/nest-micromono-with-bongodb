import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { PaymentRequestService } from './payment-request.service';
import { CreatePaymentRequestDto } from './dto/create-payment-request.dto';
import { CreateUnidentifiedPaymentRequestDto } from './dto/create-unidentified-payment-request';
import { UpdateUnidentifiedPaymentRequestDto } from './dto/update-unidentified-payment-request.dto';
import { UpdatePaymentRequestDto } from './dto/update-payment-request.dto';
import {PaginationDtoFilter} from "../../../commons/paginationDto.filter";

@Controller()
export class PaymentRequestController {
  constructor(private readonly paymentRequestService: PaymentRequestService) {}

  @MessagePattern('createPaymentRequest')
  create(@Payload() createPaymentRequestDto: CreatePaymentRequestDto) {
    return this.paymentRequestService.create(createPaymentRequestDto);
  }

  @MessagePattern('createUnidentifiedPaymentRequest')
  createUnidentifiedPaymentRequest(@Payload() createUnidentifiedPaymentRequestDto: CreateUnidentifiedPaymentRequestDto) {
    return this.paymentRequestService.createUnidentifiedPaymentRequest(createUnidentifiedPaymentRequestDto);
  }

  @MessagePattern('findAllPaymentRequest')
  findAll(@Payload() paginationDtoFilter: PaginationDtoFilter) {
    return this.paymentRequestService.findAll(paginationDtoFilter);
  }

  @MessagePattern('findAllClientsPaymentRequest')
  findAllClientsPaymentRequest(@Payload() payload: any) {
    return this.paymentRequestService.findAllClientsPaymentRequest(payload.client, payload.paginationDto);
  }

  @MessagePattern('findAllUnidentifiedPaymentRequests')
  findAllUnidentifiedPaymentRequests(@Payload() paginationDtoFilter: PaginationDtoFilter) {
    return this.paymentRequestService.findAllUnidentifiedPaymentRequests(paginationDtoFilter);
  }

  @MessagePattern('findOnePaymentRequest')
  findOne(@Payload() id: string) {
    return this.paymentRequestService.findOne(id);
  }

  @MessagePattern('updatePaymentRequest')
  update(@Payload() updatePaymentRequestDto: UpdatePaymentRequestDto) {
    return this.paymentRequestService.update(updatePaymentRequestDto.id, updatePaymentRequestDto);
  }

  @MessagePattern('identifyUnidentifiedPaymentRequest')
  identifyUnidentifiedPaymentRequest(@Payload() identifyUnidentifiedPaymentRequestDto: UpdateUnidentifiedPaymentRequestDto) {
    return this.paymentRequestService.identifyUnidentifiedPaymentRequest(identifyUnidentifiedPaymentRequestDto.id, identifyUnidentifiedPaymentRequestDto);
  }

  @MessagePattern('removePaymentRequest')
  remove(@Payload() id: string) {
    return this.paymentRequestService.remove(id);
  }
}
