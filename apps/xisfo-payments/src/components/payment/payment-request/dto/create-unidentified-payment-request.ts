import { IsBoolean, IsDecimal, IsNotEmpty, IsOptional, IsString } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateUnidentifiedPaymentRequestDto {

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    client_platform: string

    @ApiProperty()
    @IsDecimal()
    @IsNotEmpty()
    amount: number;

    @IsBoolean()
    @IsOptional()
    is_identified: boolean;
}