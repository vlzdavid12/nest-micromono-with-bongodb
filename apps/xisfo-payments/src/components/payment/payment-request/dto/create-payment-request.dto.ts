import { IsBoolean, IsDecimal, IsNotEmpty, IsNotEmptyObject, IsObject, IsOptional, IsString } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreatePaymentRequestDto {

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    client_platform: string

    @ApiProperty()
    @IsDecimal()
    @IsNotEmpty()
    amount: number;

    @IsBoolean()
    @IsOptional()
    is_identified: boolean;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    invoice: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    payment_support: string;
}
