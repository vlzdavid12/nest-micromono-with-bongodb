import { Module } from '@nestjs/common';
import { PaymentRequestService } from './payment-request.service';
import { PaymentRequestController } from './payment-request.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { PaymentRequest, PaymentRequestSchema } from './entities/payment-request.entity';
import { ClientPlatformEntity, ClientPlatformSchema } from '../../client/client-platform/entities/client-platform.entity';
import { ClientEntity, ClientSchema } from '../../client/client-platform/entities/client-entity.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: PaymentRequest.name,
        schema: PaymentRequestSchema,
      },
      {
        name: ClientPlatformEntity.name,
        schema: ClientPlatformSchema,
      },
      {
        name: ClientEntity.name,
        schema: ClientSchema,
      }
    ]),
  ],
  controllers: [PaymentRequestController],
  providers: [PaymentRequestService]
})
export class PaymentRequestModule {}
