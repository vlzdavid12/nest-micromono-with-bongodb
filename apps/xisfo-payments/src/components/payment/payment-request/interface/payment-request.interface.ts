import { ObjectId } from "mongoose";
import { ClientPlatform } from "../../../client/client-platform/interfaces/client-platform-interface.interface";

export interface PaymentRequestInterface {
    results: PaymentRequest
}

export interface PaymentRequest {
    _id?: ObjectId
    client_platform: ClientPlatform;
    amount: number;
}
