import { ObjectId } from "mongoose";
import { ClientPlatformInterface } from "../../../client/client-platform/interfaces/client-platform-interface.interface";

export interface UnidentifiedPaymentRequestInterface {
    _id?: ObjectId;
    client_platform: ClientPlatformInterface;
    amount: number;
    invoice: string;
    payment_support: string;
    is_identified: boolean;
}
