import {Injectable, NotFoundException} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { ClientEntity } from '../../client/client-platform/entities/client-entity.entity';
import { ClientPlatformEntity } from '../../client/client-platform/entities/client-platform.entity';
import { ClientData, ClientInterface } from '../../client/client-platform/interfaces/client-interface.interface';
import { CreatePaymentRequestDto } from './dto/create-payment-request.dto';
import { CreateUnidentifiedPaymentRequestDto } from './dto/create-unidentified-payment-request';
import { UpdatePaymentRequestDto } from './dto/update-payment-request.dto';
import { UpdateUnidentifiedPaymentRequestDto } from './dto/update-unidentified-payment-request.dto';
import { PaymentRequest } from './entities/payment-request.entity';
import { UnidentifiedPaymentRequestInterface } from './interface/unidentified-payment-request.interface';
import {PaginationDtoFilter} from "../../../commons/paginationDto.filter";
import { PaginationDto } from 'apps/xisfo-payments/src/commons/paginationDto';

@Injectable()
export class PaymentRequestService {
  constructor(
    @InjectModel(PaymentRequest.name) private paymentRequestModel: Model<PaymentRequest>,
    @InjectModel(ClientPlatformEntity.name) private clientPlatformModel: Model<ClientPlatformEntity>,
    @InjectModel(ClientEntity.name) private clientModel: Model<ClientEntity>,
  ) { }

  async create(createPaymentRequestDto: CreatePaymentRequestDto) {
    createPaymentRequestDto['is_identified'] = true;
    return await this.paymentRequestModel.create({is_active: true, deleted_at: null, ...createPaymentRequestDto});
  }

  async findAll(paginationDtoFilter: PaginationDtoFilter) {
    try{
      const {search, limit = 12, offset = 1, orderBy} = paginationDtoFilter;
      const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
      const count = await this.paymentRequestModel.count();
      const data = await this.paymentRequestModel.find({
        is_identified: true,
        $or: [{ invoice: search}, {voucher: search}]
      })
      .skip((limit * offset) - limit)
      .limit(limit)
      .sort(order)
      .populate([
        {
          path: 'client_platform',
          populate: {
            path: 'client', model: 'ClientEntity'
          },
        }
      ])
      .populate([
        {
          path: 'client_platform',
          populate: {
            path: 'platform', model: 'PlatformEntity'
          },
        }
      ])
      .populate([
        {
          path: 'client_platform',
          populate: {
            path: 'nicho', model: 'NichoEntity'
          },
        }
      ])
      .exec();

      return {data, count, limit, offset, orderBy};
    }catch (error){
      throw new NotFoundException();
    }

  }
  async findAllClientsPaymentRequest(client: string, paginationDto: PaginationDto) {
    const clientPlatforms = await this.clientPlatformModel.find({
      client: client['client']
    });
    
    let asyncClientPlatforms = async (clientPlatforms) => {
      const promiseArray = clientPlatforms.map(async (element) => {
        const {limit = 12, offset = 1, orderBy} = paginationDto;
        const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
        const count = await this.paymentRequestModel.count();
        const data = await this.paymentRequestModel.find({
          client_platform: element._id.toString()
        })
          .skip((limit * offset) - limit)
          .limit(limit)
          .sort(order)
          .populate([
            {
              path: 'client_platform',
              populate: {
                path: 'client', model: 'ClientEntity'
              },
            }
          ])
          .populate([
            {
              path: 'client_platform',
              populate: {
                path: 'platform', model: 'PlatformEntity'
              },
            }
          ])
          .populate([
            {
              path: 'client_platform',
              populate: {
                path: 'nicho', model: 'NichoEntity'
              },
            }
          ])
          .exec();
            
          return {data, count, limit, offset, orderBy};
      });

      return Promise.all(promiseArray)
      .then(results => {
        let response
        for(let i = 0; i < results.length; i++) {
          response = results[i]
        }
        return response;
      })
    }
    
    return asyncClientPlatforms(clientPlatforms);
    
  }

  async findAllUnidentifiedPaymentRequests(paginationDtoFilter: PaginationDtoFilter) {
    try{
      const {limit = 12, offset = 1, orderBy} = paginationDtoFilter;
      const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
      const count = await this.paymentRequestModel.count();
      const data = await this.paymentRequestModel.find({
        is_identified: false,
        deleted_at: null
      })
      .skip((limit * offset) - limit)
      .limit(limit)
      .sort(order)
      .populate([
        {
          path: 'client_platform',
          populate: {
            path: 'client', model: 'ClientEntity'
          },
        }
      ])
      .populate([
        {
          path: 'client_platform',
          populate: {
            path: 'platform', model: 'PlatformEntity'
          },
        }
      ])
      .populate([
        {
          path: 'client_platform',
          populate: {
            path: 'nicho', model: 'NichoEntity'
          },
        }
      ])
      .exec();

      return {data, count, limit, offset, orderBy};
    }catch (error){
      throw new NotFoundException();
    }

  }

  async findOne(id: string) {
    return await this.paymentRequestModel.findById(id);
  }

  async update(id: string, updatePaymentRequestDto: UpdatePaymentRequestDto) {
    const PaymentRequestUpdated = await this.paymentRequestModel
    .findByIdAndUpdate(id, updatePaymentRequestDto);
    return PaymentRequestUpdated;
  }

  async remove(id: string) {
    return await this.paymentRequestModel
    .findByIdAndUpdate(id, {
      deleted_at: new Date,
      is_active: false
    })
    .exec();
  }

  async createUnidentifiedPaymentRequest(createUnidentifiedPaymentRequestDto: CreateUnidentifiedPaymentRequestDto) {
    let client_platform = await this.clientPlatformModel.findById(createUnidentifiedPaymentRequestDto.client_platform)
    let unidentified_client: ClientData = await this.clientModel.findOne({
      first_name: 'Cliente',
      last_name: 'Sin',
      second_last_name: 'Identificar'
    });
    
    //Se valida que la cuenta sea pertenciente al cliente sin identificar
    if(client_platform.client._id.toString() == unidentified_client._id.toString()) {
      const paymentRequest: UnidentifiedPaymentRequestInterface = {
        client_platform: client_platform._id,
        invoice: 'unidentified',
        payment_support: 'unidentified',
        amount: createUnidentifiedPaymentRequestDto.amount,
        is_identified: false
      }
      console.log(paymentRequest);
      
      return await this.paymentRequestModel.create({is_active: true, deleted_at: null, ...paymentRequest});
    } else {
      return 'Se debe usar una de las cuentas del cliente sin identificar';
    }

  }

  async identifyUnidentifiedPaymentRequest(id: string, identifyUnidentifiedPaymentRequestDto: UpdateUnidentifiedPaymentRequestDto) {
    let paymentRequest: UnidentifiedPaymentRequestInterface = await this.paymentRequestModel.findById(id);

    let client_platform = await this.clientPlatformModel.findById(identifyUnidentifiedPaymentRequestDto.client_platform);

    let unidentified_client: ClientInterface = await this.clientModel.findOne({
      first_name: 'Cliente',
      last_name: 'Sin',
      second_last_name: 'Identificar'
    });

    //se valida si el cliente para asignar es diferente al cliente sin identificar
    if(client_platform.client._id.toString() != unidentified_client.results._id.toString() ) {
      if (paymentRequest.is_identified == false) {
        paymentRequest.is_identified = true;

        const response = await this.paymentRequestModel.findByIdAndUpdate(id, {
          id: id,
          client_platform: client_platform._id.toString(),
          invoice: identifyUnidentifiedPaymentRequestDto.invoice,
          payment_support: identifyUnidentifiedPaymentRequestDto.payment_support,
          amount: paymentRequest.amount,
          is_identified: true,
        });
        return response;
      } else {
        return 'La solicitud de pago ya se ha identificado';
      }
    } else {
      return 'Debe ser un cliente diferente al cliente sin identificar';
    }
  }
}
