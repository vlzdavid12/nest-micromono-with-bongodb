import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, {Document, now} from 'mongoose';
import { ClientPlatformEntity } from '../../../client/client-platform/entities/client-platform.entity';

@Schema({ collection: 'payment_requests',  timestamps: true })
export class PaymentRequest extends Document {

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'ClientPlatformEntity' })
    client_platform: ClientPlatformEntity;

    @Prop({ required: true })
    invoice: string;

    @Prop({ required: true })
    payment_support: string;

    @Prop({ required: true })
    amount: number;

    @Prop({ required: true })
    is_identified: boolean;

    @Prop({required: true})
    is_active: boolean;

    @Prop({required: false})
    deleted_at: Date;
}

export const PaymentRequestSchema = SchemaFactory.createForClass(PaymentRequest);
