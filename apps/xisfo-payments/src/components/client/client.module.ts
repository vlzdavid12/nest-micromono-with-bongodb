import { Module } from '@nestjs/common';
import { ClientPlatformModule } from './client-platform/client-platform.module';

@Module({
  imports: [ClientPlatformModule], 
  exports: [ClientPlatformModule]
})
export class ClientModule {}
