import { ClientData } from "./client-interface.interface";
import { Nicho } from "../../../platform/nicho/interfaces/nicho-interface.interface";
import { Platform } from "../../../platform/platform/interfaces/platform-interface.interface";
import { ObjectId } from "mongoose";

export interface ClientPlatformInterface {
 results: ClientPlatform
}

export interface ClientPlatform {
    _id?: ObjectId;
    account_name: string;
    client: ClientData;
    platform: Platform;
    nicho: Nicho;
}
