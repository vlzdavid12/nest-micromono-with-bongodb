import { ObjectId } from "mongoose";

export interface ClientInterface {
 results: ClientData
}

export interface ClientData {
    _id?: ObjectId;
    first_name: string;
    second_name?: string;
    last_name: string;
    second_last_name?: string;
    email: string;
    email_confirmated_at?: Date;
    phone: string;
    address: string;
    old_client: boolean;
    with_holding_tax?: boolean;
    client_rate?: number;
    client_type: Object;
    registration_status: Object;
    is_active: boolean;
    deleted_at: Date;
}
