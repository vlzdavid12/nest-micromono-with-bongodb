import { ObjectId } from "mongoose";

export interface ClientTypeInterface {
    _id?: ObjectId;
    name: string;
}