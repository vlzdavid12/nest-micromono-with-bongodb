import { ObjectId } from "mongoose";

export interface RegistrationStatusInterface {
    _id?: ObjectId;
    name: string;
}