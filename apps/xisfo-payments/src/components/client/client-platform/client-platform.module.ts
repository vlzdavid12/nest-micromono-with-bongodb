import { Module } from '@nestjs/common';
import { ClientPlatformService } from './client-platform.service';
import { ClientPlatformController } from './client-platform.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ClientType, ClientTypeSchema } from './entities/client-type.entity';
import { RegistrationStatus, RegistrationStatusSchema } from './entities/registration-status.entity';
import { ClientPlatformEntity, ClientPlatformSchema } from './entities/client-platform.entity';
import { ClientEntity, ClientSchema} from './entities/client-entity.entity';
import { PlatformEntity, PlatformSchema } from '../../platform/platform/entities/platform.entity';
import { NichoEntity, NichoSchema } from '../../platform/nicho/entities/nicho.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: ClientPlatformEntity.name,
        schema: ClientPlatformSchema,
      },
      {
        name: ClientEntity.name,
        schema: ClientSchema,
      },
      {
        name: ClientType.name,
        schema: ClientTypeSchema,
      },
      {
        name: RegistrationStatus.name,
        schema: RegistrationStatusSchema,
      },
      {
        name: PlatformEntity.name,
        schema: PlatformSchema,
      },
      {
        name: NichoEntity.name,
        schema: NichoSchema,
      },
    ]),
  ],
  controllers: [ClientPlatformController],
  providers: [ClientPlatformService]
})
export class ClientPlatformModule {}
