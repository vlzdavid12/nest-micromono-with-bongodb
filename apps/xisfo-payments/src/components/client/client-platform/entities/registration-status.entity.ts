import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

@Schema({collection: 'registration_statuses'})
export class RegistrationStatus extends Document {
    @Prop({required: true})
    name: string;
}

export const RegistrationStatusSchema = SchemaFactory.createForClass(RegistrationStatus);
