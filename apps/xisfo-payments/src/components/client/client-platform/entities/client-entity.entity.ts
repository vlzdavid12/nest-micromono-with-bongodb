import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { Document } from "mongoose";

@Schema({collection: 'clients', timestamps: true})
export class ClientEntity extends Document {
    @Prop({ required: false })
    first_name: string;

    @Prop({ required: false })
    second_name: string;

    @Prop({ required: false })
    last_name: string;

    @Prop({ required: false })
    second_last_name: string;

    @Prop({ required: true })
    email: string;

    @Prop({required: true})
    phone: string;

    @Prop({ required: false })
    address: string;

    @Prop({ required: false })
    country: string;

    @Prop({ required: true, default: false })
    old_client: boolean;

    @Prop({ type: 'Decimal', default: '0.25' })
    client_rate: number;

    @Prop({required: false})
    with_holding_tax: boolean;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'ClientTypeEntity' })
    client_type: Object;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'RegistrationStatusEntity' })
    registration_status: Object;

    @Prop({ required: false, type: mongoose.Schema.Types.ObjectId, ref: 'ClientCompanyEntity' })
    client_company: Object;

    @Prop({required: true})
    is_active: boolean;

    @Prop({required: false})
    deleted_at: Date;
}

export const ClientSchema = SchemaFactory.createForClass(ClientEntity);
