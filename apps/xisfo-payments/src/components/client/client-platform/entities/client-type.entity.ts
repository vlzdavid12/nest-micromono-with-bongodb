import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({collection: 'client_types'})
export class ClientType extends Document {
  @Prop({required: true})
  name: string;
}

export const ClientTypeSchema = SchemaFactory.createForClass(ClientType); 