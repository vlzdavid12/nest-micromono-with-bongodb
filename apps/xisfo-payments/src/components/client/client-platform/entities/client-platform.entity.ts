import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { NichoEntity } from '../../../platform/nicho/entities/nicho.entity';
import { PlatformEntity } from '../../../platform/platform/entities/platform.entity';
import { ClientEntity } from './client-entity.entity';

@Schema({ collection: 'client-platforms', timestamps: true })
export class ClientPlatformEntity extends Document {

    @Prop({ required: true })
    account_name: string;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'ClientEntity' })
    client: ClientEntity;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'PlatformEntity' })
    platform: PlatformEntity;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'NichoEntity' })
    nicho: NichoEntity;

    @Prop({required: true})
    is_active: boolean

    @Prop({required: false})
    deleted_at: Date
}

export const ClientPlatformSchema = SchemaFactory.createForClass(ClientPlatformEntity);
