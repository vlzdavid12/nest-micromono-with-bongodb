import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ClientPlatformService } from './client-platform.service';
import { CreateClientPlatformDto } from './dto/create-client-platform.dto';
import { UpdateClientPlatformDto } from './dto/update-client-platform.dto';
import {PaginationDtoFilter} from "../../../commons/paginationDto.filter";

@Controller()
export class ClientPlatformController {
  constructor(private readonly clientPlatformService: ClientPlatformService) {}

  @MessagePattern('createClientPlatform')
  create(@Payload() createClientPlatformDto: CreateClientPlatformDto) {
    return this.clientPlatformService.create(createClientPlatformDto);
  }

  @MessagePattern('findAllUnidentifiedClientPlatforms')
  findAllUnidentifiedClientPlatforms(@Payload() paginationDtoFilter: PaginationDtoFilter) {
    return this.clientPlatformService.findAllUnidentifiedClientPlatforms(paginationDtoFilter);
  }

  @MessagePattern('findAllClientPlatform')
  findAll(@Payload() paginationDtoFilter: PaginationDtoFilter) {
    return this.clientPlatformService.findAll(paginationDtoFilter);
  }

  @MessagePattern('findAllPlatformsClient')
  findAllPlatformsClient(@Payload() payload: any) {
    return this.clientPlatformService.findAllPlatformsClient(payload.paginationDto, payload.client);
  }

  @MessagePattern('findOneClientPlatform')
  findOne(@Payload() id: string) {
    return this.clientPlatformService.findOne(id);
  }

  @MessagePattern('updateClientPlatform')
  update(@Payload() updateClientPlatformDto: UpdateClientPlatformDto) {
    return this.clientPlatformService.update(updateClientPlatformDto.id, updateClientPlatformDto);
  }

  @MessagePattern('removeClientPlatform')
  remove(@Payload() id: string) {
    return this.clientPlatformService.remove(id);
  }

  @MessagePattern('createUnidentifiedClient')
  createUnidentifiedClient() {
    return this.clientPlatformService.createUnidentifiedClient();
  }
}
