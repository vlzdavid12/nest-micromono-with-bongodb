import {Injectable, NotFoundException} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { NichoInterface } from '../../platform/nicho/interfaces/nicho-interface.interface';
import { PlatformInterface } from '../../platform/platform/interfaces/platform-interface.interface';
import { CreateClientPlatformDto } from './dto/create-client-platform.dto';
import { UpdateClientPlatformDto } from './dto/update-client-platform.dto';
import { ClientType } from './entities/client-type.entity';
import { RegistrationStatus } from './entities/registration-status.entity';
import { ClientData } from './interfaces/client-interface.interface';
import { ClientPlatformEntity} from './entities/client-platform.entity';
import { ClientEntity } from './entities/client-entity.entity';
import { PlatformEntity } from '../../platform/platform/entities/platform.entity';
import { NichoEntity } from '../../platform/nicho/entities/nicho.entity';
import { PaginationDto } from 'apps/xisfo-payments/src/commons/paginationDto';
import { PaginationDtoFilter } from 'apps/xisfo-clients/src/commons/paginationDto.filter';

@Injectable()
export class ClientPlatformService {
  constructor(
    @InjectModel(ClientEntity.name) private clientModel: Model<ClientEntity>,
    @InjectModel(ClientType.name) private clientTypeModel: Model<ClientType>,
    @InjectModel(RegistrationStatus.name) private registrationStatusModel: Model<RegistrationStatus>,
    @InjectModel(PlatformEntity.name) private platformModel: Model<PlatformEntity>,
    @InjectModel(NichoEntity.name) private nichoModel: Model<NichoEntity>,
    @InjectModel(ClientPlatformEntity.name) private clientPlatformModel: Model<ClientPlatformEntity>,
  ) { }

  async create(createClientPlatformDto: CreateClientPlatformDto) {
    return await this.clientPlatformModel.create({is_active: true, deleted_at: null, ...createClientPlatformDto});
  }

  async findAll(paginationDto: PaginationDto) {
    const {limit = 12, offset = 1, orderBy} = paginationDto;
    const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
    const count = await this.clientPlatformModel.count();
    const data =  await this.clientPlatformModel.find({
        deleted_at: null
    })
    .skip((limit * offset) - limit)
    .limit(limit)
    .sort(order)
    .populate('client')
    .populate('platform')
    .populate('nicho')
    .exec();

    return {data, count, limit, offset, orderBy};
  }

  async findAllUnidentifiedClientPlatforms(paginationDto: PaginationDto) {
    const {limit = 12, offset = 1, orderBy} = paginationDto;
    const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
    const count = await this.clientPlatformModel.count();
    const unidentifiedClient = await this.clientModel.findOne({
      first_name: 'Cliente',
      last_name: 'Sin',
      second_last_name: 'Identificar'
    })
    const data =  await this.clientPlatformModel.find({
        client: unidentifiedClient._id.toString(),
        deleted_at: null
    })
    .skip((limit * offset) - limit)
    .limit(limit)
    .sort(order)
    .populate('client')
    .populate('platform')
    .populate('nicho')
    .exec();
    
    return {data, count, limit, offset, orderBy};
  }

  async findAllPlatformsClient(paginationFilterDto: PaginationDtoFilter, client: string) {
    try{
      const {limit = 12, offset = 1, orderBy} = paginationFilterDto;
      const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
      const count = await this.clientPlatformModel.count();
      const data = await this.clientPlatformModel.find({
      client: client,
      deleted_at: null
    })
    .skip((limit * offset) - limit)
    .limit(limit)
    .sort(order)
    .populate('client')
    .populate('platform')
    .populate('nicho')
    .exec();
    
    return {data, count, limit, offset, orderBy};
    }catch (error){
      throw new NotFoundException()
    }

  }

  async findOne(id: string) {
    return await this.clientPlatformModel.findById(id);
  }

  async update(id: string, updateClientPlatformDto: UpdateClientPlatformDto) {
    const clientPlatformUpdated = await this.clientPlatformModel
    .findByIdAndUpdate(id, updateClientPlatformDto);
    return clientPlatformUpdated;
  }

  async remove(id: string) {
    return await this.clientPlatformModel
    .findByIdAndUpdate(id, {
      deleted_at: new Date,
      is_active: false
    })
    .exec();
  }

  async createUnidentifiedClient() {
    const clientAlreadyExists = await this.clientModel.find({
      first_name: 'Cliente',
      last_name: 'Sin',
      second_last_name: 'Identificar'
    })
    const clientType = await this.clientTypeModel.findOne({
      name: 'natural'
    })

    const registrationStatus = await this.registrationStatusModel.findOne({
      name: 'moneda_extranjera'
    })

    //Crear cliente sin identificar
    if(clientAlreadyExists.keys.length == 0 ) {
      const unidentifiedClient: ClientData = {
        first_name: 'Cliente',
        last_name: 'Sin',
        second_last_name: 'Identificar',
        email: 'contabilidad@sycgroup.co@',
        phone: "12345678",
        address: 'xisfo_principal',
        old_client: true,
        with_holding_tax: false,
        client_rate: 0.05,
        client_type: clientType._id,
        registration_status: registrationStatus._id,
        is_active: true,
        deleted_at: null
      }

      const client: ClientData = await this.clientModel.create(unidentifiedClient);

      const platforms: PlatformInterface[] = await this.platformModel.find();
      const nicho: NichoInterface = await this.nichoModel.findOne({
        name: 'streaming'
      });
      
      const clientPlatforms = platforms.map(async (platform) => {
        let dataClientPlatform = {
          client: client._id,
          platform: platform.results._id,
          nicho: nicho.results._id,
          account_name: 'cuenta cliente sin identificar',
          is_active: true,
          deleted_at: null
        }
        
        //Crear plataformas para cliente sin identificar
        await this.clientPlatformModel.create(dataClientPlatform);
        return dataClientPlatform
      })
      return clientPlatforms;
    } else {
      return 'The unidentified client already exists';
    }
  }
}
