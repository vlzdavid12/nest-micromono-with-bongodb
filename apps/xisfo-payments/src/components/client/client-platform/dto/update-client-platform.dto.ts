import { PartialType } from '@nestjs/mapped-types';
import { IsMongoId, IsNotEmpty } from 'class-validator';
import { CreateClientPlatformDto } from './create-client-platform.dto';

export class UpdateClientPlatformDto extends PartialType(CreateClientPlatformDto) {

  @IsMongoId()
  @IsNotEmpty()
  id: string;
}
