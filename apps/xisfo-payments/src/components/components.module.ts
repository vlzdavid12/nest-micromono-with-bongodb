import { Module } from '@nestjs/common';
import { PaymentModule } from './payment/payment.module';
import { PlatformModule } from './platform/platform.module';
import { SeedModule } from './seed/seed.module';
import { ClientModule } from './client/client.module';
import { SellTokenModule } from './token-sale/sell-token/sell-token.module';

@Module({
    imports:[PaymentModule, PlatformModule, SeedModule, ClientModule, SellTokenModule],
    exports:[PaymentModule, PlatformModule, SeedModule, ClientModule],
})
export class ComponentsModule {}
