import { Injectable } from "@nestjs/common";
import { Command } from "nestjs-command";
import { SeedNicho } from "./seeders/seed.nicho";
import { SeedPlatform } from "./seeders/seed.platform";


@Injectable()
export class SeedAll {
    constructor(
        private readonly nichoSeeder: SeedNicho,
        private readonly platformSeeder: SeedPlatform
    ) { }

    @Command({ command: 'create:seed', describe: 'seed everything' })
    async create() {
        await this.nichoSeeder.create();
        await this.platformSeeder.create();
    }
}