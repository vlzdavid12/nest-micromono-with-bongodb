import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NichoEntity, NichoSchema } from '../platform/nicho/entities/nicho.entity';
import { PlatformEntity, PlatformSchema } from '../platform/platform/entities/platform.entity';
import { SeedAll } from './seed.seed-all';
import { SeedNicho } from './seeders/seed.nicho';
import { SeedPlatform } from './seeders/seed.platform';

@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: NichoEntity.name,
                schema: NichoSchema,
            },            {
                name: PlatformEntity.name,
                schema: PlatformSchema,
            },
        ]),
    ],
    providers: [
        SeedAll,
        SeedNicho,
        SeedPlatform,
    ]
})
export class SeedModule { }
