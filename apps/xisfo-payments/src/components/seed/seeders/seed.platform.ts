import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { PlatformEntity } from '../../platform/platform/entities/platform.entity';

@Injectable()
export class SeedPlatform {
    constructor(
        @InjectModel(PlatformEntity.name)
        private readonly platformModel: Model<PlatformEntity>
    ) { }
    async create() {
        await this.platformModel.deleteMany({});

        const data = [
            {
                name: 'Stripchat',
                usd_commission: '0.00',
                usd_token_rate: '0.050',
                url: 'www.stripchat.com',
                icon: 'no-icon',
                is_active: true,
                deleted_at: new Date
            },
            {
                name: 'EpayService',
                usd_commission: '30.00',
                usd_token_rate: '0.005',
                url: 'www.epayservice.com',
                icon: 'no-icon',
                is_active: true,
                deleted_at: new Date
            },
            {
                name: 'Streamate',
                usd_commission: '40.00',
                usd_token_rate: '1.000',
                url: 'www.streamate.com',
                icon: 'no-icon',
                is_active: true,
                deleted_at: new Date
            }
        ];

        await this.platformModel.insertMany(data);
        console.log("Platform seed finished successful");
    }
}
