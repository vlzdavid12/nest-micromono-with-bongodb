import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { NichoEntity } from '../../platform/nicho/entities/nicho.entity';

@Injectable()
export class SeedNicho {
    constructor(
        @InjectModel(NichoEntity.name)
        private readonly nichoModel: Model<NichoEntity>
    ) { }
    async create() {
        await this.nichoModel.deleteMany({});

        const data = [
            { name: "streaming", is_active: true, deleted_at: new Date },
            { name: "desarrollador", is_active: true, deleted_at: new Date },
            { name: "casino", is_active: true, deleted_at: new Date },
            { name: "otro", is_active: true, deleted_at: new Date },
        ];

        await this.nichoModel.insertMany(data);
        console.log("Nicho seed finished successful");
    }
}
