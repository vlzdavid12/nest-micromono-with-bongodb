import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {Document} from 'mongoose';

@Schema({ collection: 'platforms', timestamps: true })
export class PlatformEntity extends Document {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true,  type: 'Decimal' })
  usd_commission: number;

  @Prop({ required: true, type: 'Decimal' })
  usd_token_rate: number;

  @Prop({ required: true })
  url: string;

  @Prop({ required: false })
  icon: string;

  @Prop({required: true})
  is_active: boolean;

  @Prop({required: false})
  deleted_at: Date;
}

export const PlatformSchema = SchemaFactory.createForClass(PlatformEntity);
