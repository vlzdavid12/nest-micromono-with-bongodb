import {Injectable, NotFoundException} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginationDto } from 'apps/xisfo-payments/src/commons/paginationDto';
import { Model } from 'mongoose';
import { CreatePlatformDto } from './dto/create-platform.dto';
import { UpdatePlatformDto } from './dto/update-platform.dto';
import { PlatformEntity } from './entities/platform.entity';

@Injectable()
export class PlatformService {
  constructor(
    @InjectModel(PlatformEntity.name) private platformModel: Model<PlatformEntity>,
  ) { }

  async create(createPlatformDto: CreatePlatformDto) {
    return await this.platformModel.create({is_active: true, deleted_at: null, ...createPlatformDto});
  }

  async findAll(paginationDto: PaginationDto) {
    try {
      const {limit = 12, offset = 1, orderBy} = paginationDto;
      const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
      const count = await this.platformModel.count();
      const data =  await this.platformModel.find({
          deleted_at: null
      })
      .skip((limit * offset) - limit)
      .limit(limit)
      .sort(order);
  
      return {data, count, limit, offset, orderBy};
    } catch (error) {
      throw new NotFoundException();
    }
  }

  async findOne(id: string) {
    return await this.platformModel.findById(id);
  }

  async update(id: string, updatePlatformDto: UpdatePlatformDto) {
    const platformUpdated = await this.platformModel
    .findByIdAndUpdate(id, updatePlatformDto);
    return platformUpdated;
  }

  async remove(id: string) {
    return await this.platformModel
    .findByIdAndUpdate(id, {
      deleted_at: new Date,
      is_active: false
    })
    .exec();
  }
}
