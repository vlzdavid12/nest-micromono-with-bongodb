import { Module } from '@nestjs/common';
import { PlatformService } from './platform.service';
import { PlatformController } from './platform.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { PlatformEntity, PlatformSchema } from './entities/platform.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: PlatformEntity.name,
        schema: PlatformSchema,
      }
    ]),
  ],
  controllers: [PlatformController],
  providers: [PlatformService]
})
export class PlatformModule {}
