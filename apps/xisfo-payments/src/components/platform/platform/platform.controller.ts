import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { PlatformService } from './platform.service';
import { CreatePlatformDto } from './dto/create-platform.dto';
import { UpdatePlatformDto } from './dto/update-platform.dto';
import { PaginationDto } from 'apps/xisfo-payments/src/commons/paginationDto';

@Controller()
export class PlatformController {
  constructor(private readonly platformService: PlatformService) {}

  @MessagePattern('createPlatform')
  create(@Payload() createPlatformDto: CreatePlatformDto) {
    return this.platformService.create(createPlatformDto);
  }

  @MessagePattern('findAllPlatform')
  findAll(@Payload() paginationDto: PaginationDto) {
    return this.platformService.findAll(paginationDto);
  }

  @MessagePattern('findOnePlatform')
  findOne(@Payload() id: string) {
    return this.platformService.findOne(id);
  }

  @MessagePattern('updatePlatform')
  update(@Payload() updatePlatformDto: UpdatePlatformDto) {
    return this.platformService.update(updatePlatformDto.id, updatePlatformDto);
  }

  @MessagePattern('removePlatform')
  remove(@Payload() id: string) {
    return this.platformService.remove(id);
  }
}
