import { ObjectId } from "mongoose";

export interface PlatformInterface {
  results: Platform
}


export interface Platform {
    _id?: ObjectId;
    name: string;
    usd_commission: number;
    usd_token_rate: number;
    url: string;
    icon: string;
    is_active: boolean;
    deleted_at: Date;
}
