import { PartialType } from '@nestjs/mapped-types';
import { IsMongoId, IsNotEmpty } from 'class-validator';
import { CreatePlatformDto } from './create-platform.dto';

export class UpdatePlatformDto extends PartialType(CreatePlatformDto) {
  
  @IsMongoId()
  @IsNotEmpty()
  id: string;
}
