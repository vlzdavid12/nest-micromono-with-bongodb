import { Module } from '@nestjs/common';
import { NichoModule } from './nicho/nicho.module';
import { PlatformModule as Platform } from './platform/platform.module';

@Module({
  imports: [NichoModule, Platform],
  exports: [NichoModule, Platform]
})
export class PlatformModule {}
