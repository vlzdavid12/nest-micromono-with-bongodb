import { PartialType } from '@nestjs/mapped-types';
import { IsMongoId, IsNotEmpty } from 'class-validator';
import { CreateNichoDto } from './create-nicho.dto';

export class UpdateNichoDto extends PartialType(CreateNichoDto) {

  @IsMongoId()
  @IsNotEmpty()
  id: string;
}
