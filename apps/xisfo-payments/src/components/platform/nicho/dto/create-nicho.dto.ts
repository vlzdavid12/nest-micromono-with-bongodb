import { IsNotEmpty, IsString } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateNichoDto {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    name: string;
}
