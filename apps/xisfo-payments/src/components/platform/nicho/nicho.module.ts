import { Module } from '@nestjs/common';
import { NichoService } from './nicho.service';
import { NichoController } from './nicho.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { NichoEntity, NichoSchema } from './entities/nicho.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: NichoEntity.name,
        schema: NichoSchema,
      }
    ]),
  ],
  controllers: [NichoController],
  providers: [NichoService]
})
export class NichoModule {}
