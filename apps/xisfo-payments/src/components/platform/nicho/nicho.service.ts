import {Injectable, NotFoundException} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginationDto } from 'apps/xisfo-payments/src/commons/paginationDto';
import { Model } from 'mongoose';
import { CreateNichoDto } from './dto/create-nicho.dto';
import { UpdateNichoDto } from './dto/update-nicho.dto';
import { NichoEntity } from './entities/nicho.entity';

@Injectable()
export class NichoService {
  constructor(
    @InjectModel(NichoEntity.name) private nichoModel: Model<NichoEntity>,
  ) { }

  async create(createNichoDto: CreateNichoDto) {
    return await this.nichoModel.create({is_active: true, deleted_at: null, ...createNichoDto});
  }

  async findAll(paginationDto: PaginationDto) {
    try {
      const {limit = 12, offset = 1, orderBy} = paginationDto;
      const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
      const count = await this.nichoModel.count();
      const data =  await this.nichoModel.find({
          deleted_at: null
      })
      .skip((limit * offset) - limit)
      .limit(limit)
      .sort(order);
  
      return {data, count, limit, offset, orderBy};
    } catch (error) {
      throw new NotFoundException();
    }
  }

  async findOne(id: string) {
    return await this.nichoModel.findById(id);
  }

  async update(id: string, updateNichoDto: UpdateNichoDto) {
    const nichoUpdated = await this.nichoModel
    .findByIdAndUpdate(id, updateNichoDto);
    return nichoUpdated;
  }

  async remove(id: string) {
    return await this.nichoModel
    .findByIdAndUpdate(id, {
      deleted_at: new Date,
      is_active: false
    })
    .exec();
  }
}
