import { ObjectId } from "mongoose";
export interface NichoInterface {
   results: Nicho
}
export interface Nicho {
    _id?: ObjectId;
    name: string;
    is_active: boolean;
    deleted_at: Date;
}
