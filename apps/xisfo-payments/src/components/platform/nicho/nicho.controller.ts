import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { NichoService } from './nicho.service';
import { CreateNichoDto } from './dto/create-nicho.dto';
import { UpdateNichoDto } from './dto/update-nicho.dto';
import { PaginationDto } from 'apps/xisfo-payments/src/commons/paginationDto';

@Controller()
export class NichoController {
  constructor(private readonly nichoService: NichoService) {}

  @MessagePattern('createNicho')
  create(@Payload() createNichoDto: CreateNichoDto) {
    return this.nichoService.create(createNichoDto);
  }

  @MessagePattern('findAllNicho')
  findAll(@Payload() paginationDto: PaginationDto) {
    return this.nichoService.findAll(paginationDto);
  }

  @MessagePattern('findOneNicho')
  findOne(@Payload() id: string) {
    return this.nichoService.findOne(id);
  }

  @MessagePattern('updateNicho')
  update(@Payload() updateNichoDto: UpdateNichoDto) {
    return this.nichoService.update(updateNichoDto.id, updateNichoDto);
  }

  @MessagePattern('removeNicho')
  remove(@Payload() id: string) {
    return this.nichoService.remove(id);
  }
}
