import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ collection: 'nichos', timestamps: true })
export class NichoEntity extends Document {
  @Prop({ required: true })
  name: string;

  @Prop({required: true})
  is_active: boolean;

  @Prop({required: false})
  deleted_at: Date;
}

export const NichoSchema = SchemaFactory.createForClass(NichoEntity);
