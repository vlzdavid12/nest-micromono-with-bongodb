import { Catch, ArgumentsHost, HttpStatus, RpcExceptionFilter } from "@nestjs/common";
import { Response } from 'express';
import { RpcException } from "@nestjs/microservices";
import { Observable, throwError } from "rxjs";

@Catch()
export class ExceptionFilterRpc implements RpcExceptionFilter<RpcException> {
  catch(exception: RpcException, host: ArgumentsHost): Observable<any> {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    
    let error;

    switch (exception.name) {
        case 'DocumentNotFoundError': {
            error = {
              statusCode: HttpStatus.NOT_FOUND,
              message: "Not Found"
            }
            break;
        }
        case 'MongooseError': {
            response.statusCode = HttpStatus.NOT_FOUND;
            error = {
                statusCode: HttpStatus.NOT_FOUND,
                message: 'Not found'
            };
            break;
        }
        case 'CastError': {
            response.statusCode = HttpStatus.NOT_FOUND;
            error = {
                statusCode: HttpStatus.NOT_FOUND,
                message: 'Not found'
            };
            break;
        }
        case 'ValidatorError': {
            console.log(exception.name);
            
            break; 
        }
        case 'ValidationError': {
            console.log(exception.name);
            break;
        }
        default: {
            error = {
              statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
              message: "Internal Error"
            }
            break;
        }
    }
    
    return throwError(() => error);
  }
}
