import { NestFactory } from '@nestjs/core';
import { XisfoPaymentsModule } from './xisfo-payments.module';
import { Transport, MicroserviceOptions } from '@nestjs/microservices';
import { ValidationPipe } from '@nestjs/common';
import { ExceptionFilterRpc } from './filter';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    XisfoPaymentsModule,
    {
      transport: Transport.TCP,
      options: {
        port: 3002,
      },
    },
  );

  app.useGlobalFilters(new ExceptionFilterRpc());

  app.useGlobalPipes(
      new ValidationPipe({
          whitelist: true,
          forbidNonWhitelisted: true
      })
  );

  await app.listen();
}
bootstrap();
