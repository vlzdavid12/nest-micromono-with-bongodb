import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, {Document, now} from "mongoose";
import { ClientEntity } from "../../../client/client/entities/client.entity";
import { EmployeeEntity } from "../../../employee/employee-resource/entities/employee-resource.entity";

@Schema({ collection: 'contracts', timestamps: true })
export class ContractEntity extends Document {

    @Prop({ required: true })
    rut: string

    @Prop({ required: true })
    rut_image: string

    @Prop({ required: true })
    chamber_commerce: string

    @Prop({ required: true })
    bank_certificate: string

    @Prop({ required: false })
    habeas_data: string;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'EmployeeEntity' })
    employee: EmployeeEntity;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'ClientEntity' })
    client: ClientEntity;

    @Prop({required: true})
    is_active: boolean

    @Prop({required: false})
    deleted_at: Date;

}

export const ContractSchema = SchemaFactory.createForClass(ContractEntity);
