import { Module } from '@nestjs/common';
import { ContractService } from './contract.service';
import { ContractController } from './contract.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ContractEntity, ContractSchema } from './entities/contract.entity';
import { ClientService } from '../../client/client/client.service';
import { ClientEntity, ClientSchema } from '../../client/client/entities/client.entity';
import { RegistrationStatusEntity, RegistrationStatusSchema } from '../../client/registration-status/entities/registration-status.entity';

import { WalletXisfoModules } from '../../microservices/wallet';
import {
  ClientIdentificationEntity,
  ClientIdentificationSchema
} from "../../client/client-identification/entities/client-identification.entity";
import { BankAccountEntity, BankAccountEntitySchema } from '../../client/bank_account/entities/bank_account.entity';
import {ClientTypeEntity, ClientTypeSchema} from "../../client/client-type/entities/client-type.entity";

@Module({
  imports: [
    WalletXisfoModules,

    MongooseModule.forFeature([
      {
        name: ContractEntity.name,
        schema: ContractSchema
      },
      {
        name: ClientEntity.name,
        schema: ClientSchema
      },
      {
        name: RegistrationStatusEntity.name,
        schema: RegistrationStatusSchema
      },
      {
        name: ClientIdentificationEntity.name,
        schema: ClientIdentificationSchema,
      },
      {
        name: BankAccountEntity.name,
        schema: BankAccountEntitySchema,
      },
      {
        name: ClientTypeEntity.name,
        schema:ClientTypeSchema,
      },
    
    ])
  ],
  controllers: [ContractController],
  providers: [ContractService, ClientService]
})
export class ContractModule { }
