import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ContractService } from './contract.service';
import { CreateContractDto } from './dto/create-contract.dto';

@Controller()
export class ContractController {
  constructor(private readonly contractService: ContractService) { }
  
  @MessagePattern('createContract')
  create(@Payload() createContractDto: CreateContractDto) {
    return this.contractService.create(createContractDto);
  }

  @MessagePattern('findAllContract')
  findAll() {
    return this.contractService.findAll();
  }

  @MessagePattern('findOneContract')
  async findOne(@Payload() id: string) {
    return await this.contractService.findOne(id);
  }
  
  @MessagePattern('removeContract')
  remove(@Payload() id: string) {
    return this.contractService.remove(id);
  }
}
