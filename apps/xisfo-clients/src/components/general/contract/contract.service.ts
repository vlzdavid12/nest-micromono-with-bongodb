import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ClientService } from '../../client/client/client.service';
import { RegistrationStatusEntity } from '../../client/registration-status/entities/registration-status.entity';
import { CreateContractDto } from './dto/create-contract.dto';
import { ContractEntity } from './entities/contract.entity';

@Injectable()
export class ContractService {

  constructor(
    @InjectModel(ContractEntity.name) private contractModel: Model<ContractEntity>,
    @InjectModel(RegistrationStatusEntity.name) private registrationStatusModel: Model<RegistrationStatusEntity>,
    private clientService: ClientService,
  ) { }

  async create(createContractDto: CreateContractDto) {
    await this.updateClientRegistration(createContractDto.client)
    return await this.contractModel.create({is_active: true, ...createContractDto});
  }

  async updateClientRegistration(clientId: string){
    const registrationStatus = await this.registrationStatusModel.findOne({ name: 'moneda_extranjera' });
    const data: any = {
      id: String(clientId),
      registration_status: registrationStatus
    }
    await this.clientService.update(data.id, data);
  }

  async findAll() {
    return await this.contractModel.find().populate('client').populate('employee').exec();
  }

  async findOne(id: string) {
    const contract = await this.contractModel.findById(id).populate('client').populate('employee').exec();

    if (!contract) {
      throw new NotFoundException();
    }
    return contract;
  }

  async remove(id: string) {
    return await this.contractModel
      .findByIdAndUpdate(id, {
        deleted_at: new Date,
        is_active: false
      })
      .exec();
  }
}
