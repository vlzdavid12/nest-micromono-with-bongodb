import { IsBoolean, IsNotEmpty, IsString } from "class-validator";

export class CreateContractDto {

    @IsString()
    @IsNotEmpty()
    rut: string

    @IsString()
    @IsNotEmpty()
    rut_image: string

    @IsString()
    @IsNotEmpty()
    chamber_commerce: string;

    @IsString()
    @IsNotEmpty()
    bank_certificate: string;

    @IsString()
    @IsNotEmpty()
    habeas_data: string;

    @IsString()
    @IsNotEmpty()
    employee: string;

    @IsString()
    @IsNotEmpty()
    client: string;

}
