import { Controller } from '@nestjs/common';
import {MessagePattern, Payload} from '@nestjs/microservices';
import { CityService } from './city.service';


@Controller()
export class CityController {
  constructor(private readonly cityService: CityService) { }

  @MessagePattern('findAllCity')
  async findAll() {
    return await this.cityService.findAll();
  }

  @MessagePattern('findOneCity')
  findOne(@Payload() id: string) {
    return this.cityService.findOne(id);
  }
}
