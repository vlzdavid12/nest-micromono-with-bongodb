import { Module } from '@nestjs/common';
import { CityService } from './city.service';
import { CityController } from './city.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { CityEntity, CitySchema } from './entities/city.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: CityEntity.name,
        schema: CitySchema
      }
    ]),
  ],
  controllers: [CityController],
  providers: [CityService],
})
export class CityModule {}
