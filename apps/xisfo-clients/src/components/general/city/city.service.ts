import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CityEntity } from './entities/city.entity';

@Injectable()
export class CityService {
  constructor(
    @InjectModel(CityEntity.name) private cityModel: Model<CityEntity>,
  ) { }

  async findAll() {
    return await this.cityModel.find().populate('department').exec();
  }

  async findOne(id: string) {
    return await this.cityModel.findById(id).populate('department').exec();
  }

}
