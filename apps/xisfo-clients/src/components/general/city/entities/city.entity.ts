import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { DepartmentEntity } from '../../department/entities/department.entity';

@Schema({ collection: 'cities' })
export class CityEntity extends Document {
  @Prop({ required: true })
  name: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'DepartmentEntity' })
  department: DepartmentEntity
}

export const CitySchema = SchemaFactory.createForClass(CityEntity);
