import { Module } from '@nestjs/common';
import { DepartmentService } from './department.service';
import { DepartmentController } from './department.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { DepartmentEntity, DepartmentSchema } from './entities/department.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: DepartmentEntity.name,
        schema: DepartmentSchema
      }
    ])
  ],
  controllers: [DepartmentController],
  providers: [DepartmentService]
})
export class DepartmentModule {}
