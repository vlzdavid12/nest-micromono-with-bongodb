import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { DepartmentService } from './department.service';

@Controller()
export class DepartmentController {
  constructor(private readonly departmentService: DepartmentService) {}

  @MessagePattern('findAllDepartment')
  async findAll() {
    return await this.departmentService.findAll();
  }
}
