import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema({collection: 'departments'})
export class DepartmentEntity {
    @Prop()
    name: string
}

export const DepartmentSchema = SchemaFactory.createForClass(DepartmentEntity);
