import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { DepartmentEntity } from './entities/department.entity';

@Injectable()
export class DepartmentService {
  
  constructor(
    @InjectModel(DepartmentEntity.name) private departmentModel: Model<DepartmentEntity>
  ){}

  async findAll() {
    return await this.departmentModel.find();
  }

}
