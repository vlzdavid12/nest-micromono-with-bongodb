import { Module } from '@nestjs/common';
import { CityModule } from './city/city.module';
import { DocumentTypeModule } from './document-type/document-type.module';
import { DepartmentModule } from './department/department.module';
import { TypeBankAccountModule } from './type_bank_account/type_bank_account.module';
import { BankModule } from './bank/bank.module';
import { EmployeeModule } from '../employee/employee.module';
import { ContractModule } from './contract/contract.module';
import { CountryModule } from './country/country.module';

@Module({
  imports: [CityModule, EmployeeModule, DocumentTypeModule, DepartmentModule, TypeBankAccountModule, BankModule, ContractModule, CountryModule],
  exports: [CityModule, EmployeeModule, DocumentTypeModule, DepartmentModule, TypeBankAccountModule, BankModule, ContractModule, CountryModule],
})
export class GeneralModule {}
