import { Controller } from '@nestjs/common';
import { CountryService } from './country.service';
import {MessagePattern} from "@nestjs/microservices";

@Controller()
export class CountryController {
  constructor(private readonly countryService: CountryService) {}

  @MessagePattern('findAllCountry')
  findAll() {
    return this.countryService.findAll();
  }
}
