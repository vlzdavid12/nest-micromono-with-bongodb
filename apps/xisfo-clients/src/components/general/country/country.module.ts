import { Module } from '@nestjs/common';
import { CountryService } from './country.service';
import { CountryController } from './country.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {CountryEntity, CountrySchema} from "../../client/client/entities/country.entity";

@Module({
  imports:[
    MongooseModule.forFeature([
      {
        name: CountryEntity.name,
        schema: CountrySchema
      }
    ]),
  ],
  controllers: [CountryController],
  providers: [CountryService]
})
export class CountryModule {}
