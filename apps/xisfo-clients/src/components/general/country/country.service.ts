import { Injectable } from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {CountryEntity} from "../../client/client/entities/country.entity";

@Injectable()
export class CountryService {
    constructor(@InjectModel(CountryEntity.name) private countryModel: Model<CountryEntity>) {}


    findAll() {
        return this.countryModel.find();
    }
}
