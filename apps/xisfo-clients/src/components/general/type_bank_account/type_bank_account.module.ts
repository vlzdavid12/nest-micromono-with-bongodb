import { Module } from '@nestjs/common';
import { TypeBankAccountService } from './type_bank_account.service';
import { TypeBankAccountController } from './type_bank_account.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { TypeBankAccountEntity, TypeBankAccountSchema } from './entities/type_bank_account.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: TypeBankAccountEntity.name,
        schema: TypeBankAccountSchema,
      }
    ])
  ],
  controllers: [TypeBankAccountController],
  providers: [TypeBankAccountService]
})
export class TypeBankAccountModule {}
