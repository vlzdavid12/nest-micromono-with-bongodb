import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({collection: 'type_bank_accounts'})
export class TypeBankAccountEntity extends Document {
  @Prop({required: true})
  name: string;
}

export const TypeBankAccountSchema = SchemaFactory.createForClass(TypeBankAccountEntity);
