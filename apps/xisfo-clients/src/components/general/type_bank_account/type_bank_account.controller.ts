import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { TypeBankAccountService } from './type_bank_account.service';

@Controller()
export class TypeBankAccountController {
  constructor(private readonly typeBankAccountService: TypeBankAccountService) {}

  @MessagePattern('findAllTypeBankAccount')
  findAll() {
    return this.typeBankAccountService.findAll();
  }

  @MessagePattern('findOneTypeBankAccount')
  findOne(@Payload() id: string) {
    return this.typeBankAccountService.findOne(id);
  }
}
