import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { TypeBankAccountEntity } from './entities/type_bank_account.entity';

@Injectable()
export class TypeBankAccountService {
  constructor(
    @InjectModel(TypeBankAccountEntity.name) private typeBankAccountModel: Model<TypeBankAccountEntity>,
  ) { }

  async findAll() {
    return await this.typeBankAccountModel.find();
  }

  async findOne(id: string) {
    return await this.typeBankAccountModel.findById(id);
  }
}
