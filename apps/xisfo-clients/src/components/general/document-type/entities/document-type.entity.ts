import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

@Schema({ collection: 'document_types' })
export class DocumentTypeEntity extends Document {
    @Prop()
    name: string
}

export const DocumentTypeSchema = SchemaFactory.createForClass(DocumentTypeEntity)
