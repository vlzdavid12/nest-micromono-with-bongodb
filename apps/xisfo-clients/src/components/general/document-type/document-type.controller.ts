import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { DocumentTypeService } from './document-type.service';

@Controller()
export class DocumentTypeController {
  constructor(private readonly documentTypeService: DocumentTypeService) {}

  @MessagePattern('findAllDocumentType')
  async findAll() {
    return await this.documentTypeService.findAll();
  }
}
