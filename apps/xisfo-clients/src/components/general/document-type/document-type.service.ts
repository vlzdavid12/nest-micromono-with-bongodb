import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { DocumentTypeEntity } from './entities/document-type.entity';

@Injectable()
export class DocumentTypeService {

  constructor(
    @InjectModel(DocumentTypeEntity.name) private documentTypeModel: Model<DocumentTypeEntity>
  ){}

  async findAll() {
    return await this.documentTypeModel.find();
  }
}
