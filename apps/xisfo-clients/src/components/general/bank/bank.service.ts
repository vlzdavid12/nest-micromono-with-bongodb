import {Injectable, NotFoundException} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateBankDto } from './dto/create-bank.dto';
import { UpdateBankDto } from './dto/update-bank.dto';
import { BankEntity } from './entities/bank.entity';
import {PaginationDto} from "../../../commons/paginationDto";

@Injectable()
export class BankService {
  constructor(
    @InjectModel(BankEntity.name) private bankModel: Model<BankEntity>,
  ) { }

  async create(createBankDto: CreateBankDto) {
    return await this.bankModel.create({is_active: true, deleted_at: null, ...createBankDto});
  }

  async findAll(paginationDto: PaginationDto){
    const {limit = 12, offset = 1, orderBy} = paginationDto;
    const order: any = (orderBy === 'ASC') ? {updatedAt: 1} : {updatedAt: -1};
    try{
      const count = await this.bankModel.count();
      const data = await this.bankModel.find()
          .skip((limit * offset) - limit)
          .limit(limit)
          .sort(order);
      return {data, count, limit, offset, orderBy}
    }catch (error){
      throw new NotFoundException();
    }

  }

  async findOne(id: string) {
    return await this.bankModel.findById(id);
  }

  async update(id: string, updateBankDto: UpdateBankDto) {
    const bankUpdated = await this.bankModel
    .findByIdAndUpdate(id, updateBankDto);
    return bankUpdated;
  }

  async remove(id: string) {
    return await this.bankModel
      .findByIdAndUpdate(id, {
        deleted_at: new Date,
        is_active: false
      })
      .exec();
  }
}
