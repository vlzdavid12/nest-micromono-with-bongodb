import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { BankService } from './bank.service';
import { CreateBankDto } from './dto/create-bank.dto';
import { UpdateBankDto } from './dto/update-bank.dto';
import {PaginationDto} from "../../../commons/paginationDto";

@Controller()
export class BankController {
  constructor(private readonly bankService: BankService) {}

  @MessagePattern('createBank')
  create(@Payload() createBankDto: CreateBankDto) {
    return this.bankService.create(createBankDto);
  }

  @MessagePattern('findAllBank')
  findAll(@Payload() paginationDto: PaginationDto) {
    return this.bankService.findAll(paginationDto);
  }

  @MessagePattern('findOneBank')
  findOne(@Payload() id: string) {
    return this.bankService.findOne(id);
  }

  @MessagePattern('updateBank')
  update(@Payload() updateBankDto: UpdateBankDto) {
    return this.bankService.update(updateBankDto.id, updateBankDto);
  }

  @MessagePattern('removeBank')
  remove(@Payload() id: string) {
    return this.bankService.remove(id);
  }
}
