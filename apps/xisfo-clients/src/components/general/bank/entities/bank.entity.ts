import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import {Document, now} from "mongoose";

@Schema({collection: 'banks', timestamps: true})
export class BankEntity extends Document {
    @Prop({required: true, unique: true})
    name: string;

    @Prop({required: true})
    is_active: boolean;

    @Prop({required: false})
    deleted_at: Date;
}

export const BankSchema = SchemaFactory.createForClass(BankEntity);
