import { Module } from '@nestjs/common';
import { BankService } from './bank.service';
import { BankController } from './bank.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { BankEntity, BankSchema } from './entities/bank.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: BankEntity.name,
        schema: BankSchema,
      }
    ])
  ],
  controllers: [BankController],
  providers: [BankService]
})
export class BankModule {}
