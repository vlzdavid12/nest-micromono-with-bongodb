import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {ClientTypeEntity, ClientTypeSchema} from '../client/client-type/entities/client-type.entity';
import {
    RegistrationStatusEntity,
    RegistrationStatusSchema
} from '../client/registration-status/entities/registration-status.entity';
import {EducationLevelEntity, EducationLevelSchema} from '../employee/education-level/entities/education-level.entity';
import {GenderEntity, GenderSchema} from '../employee/gender/entities/gender.entity';
import {HealthProviderEntity, HealthProviderSchema} from '../employee/health-provider/entities/health-provider.entity';
import {HousingTypeEntity, HousingTypeSchema} from '../employee/housing-type/entities/housing-type.entity';
import {
    OccupationalRiskManagerEntity,
    OccupationalRiskManagerSchema
} from '../employee/occupational-risk-manager/entities/occupational-risk-manager.entity';
import {PensionFundEntity, PensionFundSchema} from '../employee/pension-fund/entities/pension-fund.entity';
import {PositionEntity, PositionSchema} from '../employee/position/entities/position.entity';
import {ProfesionEntity, ProfesionSchema} from '../employee/profesion/entities/profesion.entity';
import {StrataEntity, StrataSchema} from '../employee/strata/entities/strata.entity';
import {DepartmentEntity, DepartmentSchema} from '../general/department/entities/department.entity';
import {DocumentTypeSchema, DocumentTypeEntity} from '../general/document-type/entities/document-type.entity';
import {
    TypeBankAccountEntity,
    TypeBankAccountSchema
} from '../general/type_bank_account/entities/type_bank_account.entity';
import {SeedCivilStatus} from './seeders/seed.civil-status';
import {ContractTypeEntity, ContractTypeSchema} from '../employee/contract-type/entities/contract-type.entity';
import {KinshipEntity, KinshipSchema} from '../employee/kinship/entities/kinship.entity';
import {CivilStatusEntity, CivilStatusSchema} from '../employee/civil-status/entities/civil-status.entity';
import {BankEntity, BankSchema} from '../general/bank/entities/bank.entity';
import {SeedBank} from './seeders/seed.bank';
import {SeedAll} from './seed.seed-all';
import {SeedEmployee} from './seeders/seed.employee';
import {SeedKinship} from './seeders/seed.kinship';
import {SeedContractType} from './seeders/seed.contract-type';
import {SeedGender} from './seeders/seed.gender';
import {SeedHealthProvider} from './seeders/seed.heatlhProvider';
import {SeedOccupationalRiskManager} from './seeders/seed.occupational-risk-manager';
import {SeedPensionFund} from './seeders/seed.pension-fund';
import {SeedEducationLevel} from './seeders/seed.education-level';
import {SeedProfesion} from './seeders/seed.profesion';
import {SeedPosition} from './seeders/seed.position';
import {SeedHousingType} from './seeders/seed.housing-type';
import {SeedStrata} from './seeders/seed.strata';
import {SeedDocumentType} from './seeders/seed.document-type';
import {SeedDepartment} from './seeders/seed.department';
import {SeedClientType} from './seeders/seed.client-type';
import {SeedRegistrationStatus} from './seeders/seed.registration-status';
import {SeedTypeBankAccount} from './seeders/seed.type-bank-account';
import {SeedCity} from './seeders/seed.city';
import {SeedHeadQuarter} from "./seeders/seed.headquarter";
import {SeedSocioDemoGraphic} from "./seeders/seed.sociodemographic";
import {EmployeeEntity, EmployeeSchema} from '../employee/employee-resource/entities/employee-resource.entity';
import {CityEntity, CitySchema} from '../general/city/entities/city.entity';
import {UserEntity, UserSchema} from 'apps/xisfo-gateway/src/components/auth/entities/user.entity';
import {CountryEntity, CountrySchema} from '../client/client/entities/country.entity';
import {SeedCountry} from './seeders/seed.country';
import {HeadquarterEntity, HeadquarterSchema} from "../employee/headquarter/entities/headquarter.entity";
import {
    SociodemographicProfileEntity,
    SociodemographicProfileSchema
} from "../employee/sociodemographic-profile/entities/sociodemographic-profile.entity";
import {SeedClient} from "./seeders/seed.client";
import {ClientEntity, ClientSchema} from "../client/client/entities/client.entity";
import {
    EmployeeIdentificationEntity, EmployeeIdentificationSchema
} from "../employee/employee-identification/entities/employee-identification.entity";
import {
    ClientIdentificationEntity,
    ClientIdentificationSchema
} from "../client/client-identification/entities/client-identification.entity";


@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: UserEntity.name,
                schema: UserSchema
            },
            {
                name: ClientEntity.name,
                schema: ClientSchema,
            },
            {
                name: EmployeeEntity.name,
                schema: EmployeeSchema
            },
            {
                name: CivilStatusEntity.name,
                schema: CivilStatusSchema
            },
            {
                name: KinshipEntity.name,
                schema: KinshipSchema
            },
            {
                name: ContractTypeEntity.name,
                schema: ContractTypeSchema
            },
            {
                name: GenderEntity.name,
                schema: GenderSchema
            },
            {
                name: HealthProviderEntity.name,
                schema: HealthProviderSchema
            },
            {
                name: OccupationalRiskManagerEntity.name,
                schema: OccupationalRiskManagerSchema
            },
            {
                name: PensionFundEntity.name,
                schema: PensionFundSchema
            },
            {
                name: EducationLevelEntity.name,
                schema: EducationLevelSchema
            },
            {
                name: ProfesionEntity.name,
                schema: ProfesionSchema
            },
            {
                name: PositionEntity.name,
                schema: PositionSchema
            },
            {
                name: HousingTypeEntity.name,
                schema: HousingTypeSchema
            },
            {
                name: StrataEntity.name,
                schema: StrataSchema
            },
            {
                name: DocumentTypeEntity.name,
                schema: DocumentTypeSchema
            },
            {
                name: DepartmentEntity.name,
                schema: DepartmentSchema
            },
            {
                name: CityEntity.name,
                schema: CitySchema
            },
            {
                name: ClientTypeEntity.name,
                schema: ClientTypeSchema,
            },
            {
                name: RegistrationStatusEntity.name,
                schema: RegistrationStatusSchema,
            },
            {
                name: TypeBankAccountEntity.name,
                schema: TypeBankAccountSchema,
            },
            {
                name: BankEntity.name,
                schema: BankSchema,
            },
            {
                name: CountryEntity.name,
                schema: CountrySchema,
            },
            {
                name: HeadquarterEntity.name,
                schema: HeadquarterSchema,
            },
            {
                name: SociodemographicProfileEntity.name,
                schema: SociodemographicProfileSchema,
            },
            {
                name: EmployeeIdentificationEntity.name,
                schema: EmployeeIdentificationSchema,
            },{
            name: ClientIdentificationEntity.name,
                schema: ClientIdentificationSchema
            }

        ]),
    ],
    providers: [
        SeedAll,
        SeedEmployee,
        SeedClient,
        SeedCivilStatus,
        SeedKinship,
        SeedContractType,
        SeedGender,
        SeedHealthProvider,
        SeedOccupationalRiskManager,
        SeedPensionFund,
        SeedEducationLevel,
        SeedProfesion,
        SeedPosition,
        SeedHousingType,
        SeedStrata,
        SeedDocumentType,
        SeedDepartment,
        SeedCity,
        SeedClientType,
        SeedRegistrationStatus,
        SeedTypeBankAccount,
        SeedBank,
        SeedCountry,
        SeedHeadQuarter,
        SeedSocioDemoGraphic,

    ]
})
export class SeedModule {
}
