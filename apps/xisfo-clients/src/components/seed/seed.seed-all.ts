import {Injectable} from '@nestjs/common';
import {Command} from "nestjs-command";
import {SeedCivilStatus} from './seeders/seed.civil-status';
import {SeedBank} from './seeders/seed.bank';
import {SeedEmployee} from './seeders/seed.employee';
import {SeedKinship} from './seeders/seed.kinship';
import {SeedContractType} from './seeders/seed.contract-type';
import {SeedGender} from './seeders/seed.gender';
import {SeedHealthProvider} from './seeders/seed.heatlhProvider';
import {SeedOccupationalRiskManager} from './seeders/seed.occupational-risk-manager';
import {SeedPensionFund} from './seeders/seed.pension-fund';
import {SeedEducationLevel} from './seeders/seed.education-level';
import {SeedProfesion} from './seeders/seed.profesion';
import {SeedPosition} from './seeders/seed.position';
import {SeedHousingType} from './seeders/seed.housing-type';
import {SeedStrata} from './seeders/seed.strata';
import {SeedDocumentType} from './seeders/seed.document-type';
import {SeedDepartment} from './seeders/seed.department';
import {SeedClientType} from './seeders/seed.client-type';
import {SeedRegistrationStatus} from './seeders/seed.registration-status';
import {SeedTypeBankAccount} from './seeders/seed.type-bank-account';
import {SeedCity} from './seeders/seed.city';
import {SeedCountry} from './seeders/seed.country';
import {SeedHeadQuarter} from "./seeders/seed.headquarter";
import {SeedSocioDemoGraphic} from "./seeders/seed.sociodemographic";
import {SeedClient} from "./seeders/seed.client";

@Injectable()
export class SeedAll {
    constructor(
        private readonly civilStatusSeeder: SeedCivilStatus,
        private readonly employeeSeeder: SeedEmployee,
        private readonly kinshipSeed: SeedKinship,
        private readonly contractTypeSeed: SeedContractType,
        private readonly genderSeed: SeedGender,
        private readonly healthProviderSeed: SeedHealthProvider,
        private readonly occupationalRiskManagerSeed: SeedOccupationalRiskManager,
        private readonly pensionFundSeed: SeedPensionFund,
        private readonly educationLevelSeed: SeedEducationLevel,
        private readonly profesionSeed: SeedProfesion,
        private readonly positionSeed: SeedPosition,
        private readonly housingTypeSeed: SeedHousingType,
        private readonly strataSeed: SeedStrata,
        private readonly documentTypeSeed: SeedDocumentType,
        private readonly departmentSeed: SeedDepartment,
        private readonly citySeed: SeedCity,
        private readonly clientTypeSeeder: SeedClientType,
        private readonly registrationStatusSeeder: SeedRegistrationStatus,
        private readonly typeBankAccountSeeder: SeedTypeBankAccount,
        private readonly bankSeeder: SeedBank,
        private readonly countrySeeder: SeedCountry,
        private readonly headquarter: SeedHeadQuarter,
        private readonly socioDemography: SeedSocioDemoGraphic,
        private readonly client: SeedClient
    ) {
    }

    @Command({command: 'create:seed', describe: 'Seed Tables  Xisfo'})
    async create() {
        await this.civilStatusSeeder.create();
        await this.clientTypeSeeder.create();
        await this.kinshipSeed.create();
        await this.contractTypeSeed.create();
        await this.genderSeed.create();
        await this.healthProviderSeed.create();
        await this.occupationalRiskManagerSeed.create();
        await this.pensionFundSeed.create();
        await this.educationLevelSeed.create();
        await this.profesionSeed.create();
        await this.positionSeed.create();
        await this.housingTypeSeed.create();
        await this.strataSeed.create();
        await this.documentTypeSeed.create();
        await this.departmentSeed.create();
        await this.citySeed.create();
        await this.registrationStatusSeeder.create();
        await this.typeBankAccountSeeder.create();
        await this.bankSeeder.create();
        await this.countrySeeder.create();
    }

    @Command({command: 'create:employee', describe: 'Seed Employee'})
    async create_employee() {
        await this.headquarter.create();
        await this.socioDemography.create();
        await this.employeeSeeder.create();
    }


    @Command({command: 'create:client', describe: 'Seed Client'})
    async create_client() {
        await this.client.create();
    }
}
