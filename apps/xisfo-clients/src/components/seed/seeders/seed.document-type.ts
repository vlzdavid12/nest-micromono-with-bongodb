import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { DocumentTypeEntity } from '../../general/document-type/entities/document-type.entity';

@Injectable()
export class SeedDocumentType {
    constructor(
        @InjectModel(DocumentTypeEntity.name)
        private readonly documentTypeModel: Model<DocumentTypeEntity>
    ) { }
    async create() {
        await this.documentTypeModel.deleteMany({});

        const data = [
            { name: "Cedula de ciudadania" },
            { name: "Pasaporte" },
            { name: "Cedula de extranjeria" },
            { name: "Otro" }
        ];

        await this.documentTypeModel.insertMany(data);
        console.log("Document type seed finished successful");
    }
}
