import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { ClientTypeEntity } from '../../client/client-type/entities/client-type.entity';

@Injectable()
export class SeedClientType {
    constructor(
        @InjectModel(ClientTypeEntity.name)
        private readonly clientTypeModel: Model<ClientTypeEntity>
    ) { }
    async create() {
        try{
            await this.clientTypeModel.deleteMany({});

            const data = [
                { name: "natural" },
                { name: "juridico" },
            ];

            await this.clientTypeModel.insertMany(data);
            console.log("Client type seed finished successful");
        }catch (error){
            console.log("Error Client type seed");
        }

    }
}
