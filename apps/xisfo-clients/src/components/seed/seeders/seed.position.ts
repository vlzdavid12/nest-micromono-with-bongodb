import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { PositionEntity } from '../../employee/position/entities/position.entity';

@Injectable()
export class SeedPosition {
    constructor(
        @InjectModel(PositionEntity.name)
        private readonly positionModel: Model<PositionEntity>
    ) { }
    async create() {
        await this.positionModel.deleteMany({});

        const data = [
            { name: "Asesor" },
            { name: "CEO" },
            { name: "Creador Contenido" },
            { name: "Director/a" },
            { name: "Desarrollador/a" },
            { name: "Diseñador/a" },
            { name: "Operador" },
            { name: "Supervisor/a" },

        ];

        await this.positionModel.insertMany(data);
        console.log("Position seed finished successful");
    }
}
