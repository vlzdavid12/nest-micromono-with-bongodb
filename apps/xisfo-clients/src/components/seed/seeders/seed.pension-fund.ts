import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { PensionFundEntity } from '../../employee/pension-fund/entities/pension-fund.entity';

@Injectable()
export class SeedPensionFund {
    constructor(
        @InjectModel(PensionFundEntity.name)
        private readonly pensionFundModel: Model<PensionFundEntity>
    ) { }
    async create() {
        await this.pensionFundModel.deleteMany({});

        const data = [
            { name: "Porvenir" },
            { name: "Colpensiones" },
            { name: "Colfondos" },
            { name: "Protección" },
            { name: "Zurich" },
            { name: "Otro" }
        ];

        await this.pensionFundModel.insertMany(data);
        console.log("Pension fund seed finished successful");
    }
}
