import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CityEntity } from '../../general/city/entities/city.entity';
import { DepartmentEntity } from '../../general/department/entities/department.entity';

@Injectable()
export class SeedCity {
    constructor(
        @InjectModel(CityEntity.name)
        private readonly cityModel: Model<CityEntity>,
        @InjectModel(DepartmentEntity.name)
        private readonly deparmentModel: Model<DepartmentEntity>
    ) { }
    async create() {
        await this.cityModel.deleteMany({});
        const data = [
            { name: "Leticia", department: await this.deparmentModel.findOne({ name: 'Amazonas' }) },
            { name: "Medellin", department: await this.deparmentModel.findOne({ name: 'Antioquia' }) },
            { name: "Arauca", department: await this.deparmentModel.findOne({ name: 'Arauca' }) },
            { name: "Barranquilla", department: await this.deparmentModel.findOne({ name: 'Atlántico' }) },
            { name: "Bogotá", department: await this.deparmentModel.findOne({ name: 'Bogotá' }) },
            { name: "Cartagena de Indias", department: await this.deparmentModel.findOne({ name: 'Bolívar' }) },
            { name: "Tunja", department: await this.deparmentModel.findOne({ name: 'Boyacá' }) },
            { name: "Manizales", department: await this.deparmentModel.findOne({ name: 'Caldas' }) },
            { name: "Florencia", department: await this.deparmentModel.findOne({ name: 'Caquetá' }) },
            { name: "Yopal", department: await this.deparmentModel.findOne({ name: 'Casanare' }) },
            { name: "Popayán", department: await this.deparmentModel.findOne({ name: 'Cauca' }) },
            { name: "Florencia", department: await this.deparmentModel.findOne({ name: 'Caquetá' }) },
            { name: "Yopal", department: await this.deparmentModel.findOne({ name: 'Casanare' }) },
            { name: "Popayán", department: await this.deparmentModel.findOne({ name: 'Cauca' }) },
            { name: "Valledupar", department: await this.deparmentModel.findOne({ name: 'Cesar' }) },
            { name: "Quibdó", department: await this.deparmentModel.findOne({ name: 'Chocó' }) },
            { name: "Montería", department: await this.deparmentModel.findOne({ name: 'Córdoba' }) },
            { name: "Bogotá", department: await this.deparmentModel.findOne({ name: 'Cundinamarca' }) },
            { name: "Inírida", department: await this.deparmentModel.findOne({ name: 'Guainía' }) },
            { name: "San jose Guaviare", department: await this.deparmentModel.findOne({ name: 'Guaviare' }) },
            { name: "Neiva", department: await this.deparmentModel.findOne({ name: 'Huila' }) },
            { name: "Riohacha", department: await this.deparmentModel.findOne({ name: 'La Guajira' }) },
            { name: "Santa Marta", department: await this.deparmentModel.findOne({ name: 'Magdalena' }) },
            { name: "Villavicencio", department: await this.deparmentModel.findOne({ name: 'Meta' }) },
            { name: "Pasto", department: await this.deparmentModel.findOne({ name: 'Nariño' }) },
            { name: "San José de Cúcuta", department: await this.deparmentModel.findOne({ name: 'Norte de Santander' }) },
            { name: "Mocoa", department: await this.deparmentModel.findOne({ name: 'Putumayo' }) },
            { name: "Armenia", department: await this.deparmentModel.findOne({ name: 'Quindío' }) },
            { name: "Pereira", department: await this.deparmentModel.findOne({ name: 'Risaralda' }) },
            { name: "Dosquebradas", department: await this.deparmentModel.findOne({ name: 'Risaralda' }) },
            { name: "San Andrés", department: await this.deparmentModel.findOne({ name: 'San Andrés y Providencia' }) },
            { name: "Bucaramanga", department: await this.deparmentModel.findOne({ name: 'Santander' }) },
            { name: "Sincelejo", department: await this.deparmentModel.findOne({ name: 'Sucre' }) },
            { name: "Ibagué", department: await this.deparmentModel.findOne({ name: 'Tolima' }) },
            { name: "Cali", department: await this.deparmentModel.findOne({ name: 'Valle del Cauca' }) },
            { name: "Mitú", department: await this.deparmentModel.findOne({ name: 'Vaupés' }) },
            { name: "Puerto Carreño", department: await this.deparmentModel.findOne({ name: 'Vichada' }) },
            { name: "Otra", department: await this.deparmentModel.findOne({ name: 'Otro' }) },
        ];

        await this.cityModel.insertMany(data);
        console.log("City seed finished successful");
    }
}
