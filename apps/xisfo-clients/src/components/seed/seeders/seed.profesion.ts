import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { ProfesionEntity } from '../../employee/profesion/entities/profesion.entity';

@Injectable()
export class SeedProfesion {
    constructor(
        @InjectModel(ProfesionEntity.name)
        private readonly profesionModel: Model<ProfesionEntity>
    ) { }
    async create() {
        await this.profesionModel.deleteMany({});

        const data = [
            { name: "Ingeniero" },
            { name: "Electricista" },
            { name: "Creador Contenidos" },
            { name: "Contador" },
            { name: "Mecánico" },
            { name: "Traductor" },
            { name: "Lingüista" },
            { name: "Músico" },
            { name: "Mátematico" },
            { name: "Profesor" },
            { name: "Vendedor" },
            { name: "Otro" }
        ];

        await this.profesionModel.insertMany(data);
        console.log("Profesion seed finished successful");
    }
}
