import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { TypeBankAccountEntity } from '../../general/type_bank_account/entities/type_bank_account.entity';

@Injectable()
export class SeedTypeBankAccount {
    constructor(
        @InjectModel(TypeBankAccountEntity.name)
        private readonly typeBankAccount: Model<TypeBankAccountEntity>
    ) { }
    async create() {
        await this.typeBankAccount.deleteMany({});

        const data = [
            { name: "Ahorros" },
            { name: "Corriente" },
        ];

        await this.typeBankAccount.insertMany(data);
        console.log("Type bank account seed finished successful");
    }
}
