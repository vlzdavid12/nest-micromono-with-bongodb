import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { OccupationalRiskManagerEntity } from '../../employee/occupational-risk-manager/entities/occupational-risk-manager.entity';

@Injectable()
export class SeedOccupationalRiskManager {
    constructor(
        @InjectModel(OccupationalRiskManagerEntity.name)
        private readonly occupationalRiskManagerModel: Model<OccupationalRiskManagerEntity>
    ) { }
    async create() {
        await this.occupationalRiskManagerModel.deleteMany({});

        const data = [
            { name: "AXA COLPATRIA S.A." },
            { name: "COLMENA SEGUROS" },
            { name: "COMPAÑÍA DE SEGUROS DE VIDA AURORA S.A." },
            { name: "LA EQUIDAD SEGUROS DE VIDA" },
            { name: "LIBERTY SEGUROS DE VIDA S.A." },
            { name: "MAPFRE SEGUROS" },
            { name: "POSITIVA" },
            { name: "SEGUROS BOLÍVAR S.A." },
            { name: "SEGUROS SURA" },
            { name: "OTRO" },
        ];

        await this.occupationalRiskManagerModel.insertMany(data);
        console.log("Occupational risk manager seed finished successful");
    }
}
