import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { EducationLevelEntity } from '../../employee/education-level/entities/education-level.entity';

@Injectable()
export class SeedEducationLevel {
    constructor(
        @InjectModel(EducationLevelEntity.name)
        private readonly educationLevelModel: Model<EducationLevelEntity>
    ) { }
    async create() {
        await this.educationLevelModel.deleteMany({});

        const data = [
            { name: "Bachiller" },
            { name: "Técnico" },
            { name: "Tecnologíco" },
            { name: "Profesional" },
        ];

        await this.educationLevelModel.insertMany(data);
        console.log("Education level seed finished successful");
    }
}
