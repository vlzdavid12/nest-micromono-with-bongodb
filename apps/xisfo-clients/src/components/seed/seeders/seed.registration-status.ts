import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { RegistrationStatusEntity } from '../../client/registration-status/entities/registration-status.entity';

@Injectable()
export class SeedRegistrationStatus {
    constructor(
        @InjectModel(RegistrationStatusEntity.name)
        private readonly registrationStatusModel: Model<RegistrationStatusEntity>
    ) { }
    async create() {
        await this.registrationStatusModel.deleteMany({});

        const data = [
            { name: "moneda_extranjera" },
            { name: "moneda_local" },
        ];

        await this.registrationStatusModel.insertMany(data);
        console.log("Registration status seed finished successful");
    }
}
