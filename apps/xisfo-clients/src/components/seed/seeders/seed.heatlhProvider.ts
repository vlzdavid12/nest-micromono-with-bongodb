import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { HealthProviderEntity } from '../../employee/health-provider/entities/health-provider.entity';

@Injectable()
export class SeedHealthProvider {
    constructor(
        @InjectModel(HealthProviderEntity.name)
        private readonly healthProviderModel: Model<HealthProviderEntity>
    ) { }
    async create() {
        await this.healthProviderModel.deleteMany({});

        const data = [
            { name: "Salud Total" },
            { name: "Sura" },
            { name: "Nueva Eps" },
            { name: "Eps Sanitas" },
            { name: "Otro" }
        ];

        await this.healthProviderModel.insertMany(data);
        console.log("Health provider seed finished successful");
    }
}
