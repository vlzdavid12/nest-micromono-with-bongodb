import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { GenderEntity } from '../../employee/gender/entities/gender.entity';

@Injectable()
export class SeedGender {
    constructor(
        @InjectModel(GenderEntity.name)
        private readonly genderModel: Model<GenderEntity>
    ) { }
    async create() {
        await this.genderModel.deleteMany({});

        const data = [
            { name: "masculino" },
            { name: "femenino" },
            { name: "otro" }
        ];

        await this.genderModel.insertMany(data);
        console.log("Gender seed finished successful");
    }
}
