import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { DepartmentEntity } from '../../general/department/entities/department.entity';

@Injectable()
export class SeedDepartment {
    constructor(
        @InjectModel(DepartmentEntity.name)
        private readonly departmentModel: Model<DepartmentEntity>
    ) { }
    async create() {
        await this.departmentModel.deleteMany({});

        const data = [
            { name: "Amazonas" },
            { name: "Antioquia" },
            { name: "Arauca" },
            { name: "Atlántico" },
            { name: "Bogotá" },
            { name: "Bolívar" },
            { name: "Boyacá" },
            { name: "Caldas" },
            { name: "Caquetá" },
            { name: "Casanare" },
            { name: "Cauca" },
            { name: "Cesar" },
            { name: "Cauca" },
            { name: "Chocó" },
            { name: "Córdoba" },
            { name: "Cundinamarca" },
            { name: "Guainía" },
            { name: "Guaviare" },
            { name: "Huila" },
            { name: "La Guajira" },
            { name: "Magdalena" },
            { name: "Meta" },
            { name: "Nariño" },
            { name: "Norte de Santander" },
            { name: "Putumayo" },
            { name: "Quindío" },
            { name: "Risaralda" },
            { name: "San Andrés y Providencia" },
            { name: "Satander" },
            { name: "Sucre" },
            { name: "Tolima" },
            { name: "Valle del Cauca" },
            { name: "Vaupés" },
            { name: "Vichada" },
            { name: "Otro" },
        ];

        await this.departmentModel.insertMany(data);
        console.log("Department seed finished successful");
    }
}
