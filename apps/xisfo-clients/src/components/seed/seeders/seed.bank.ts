import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { BankEntity } from '../../general/bank/entities/bank.entity';

@Injectable()
export class SeedBank {
    constructor(
        @InjectModel(BankEntity.name)
        private readonly bankModel: Model<BankEntity>
    ) { }
    async create() {
        await this.bankModel.deleteMany({});

        const data = [
            { name: "caja social", is_active: true, deleted_at: null },
            { name: "bancolombia", is_active: true, deleted_at: null },
            { name: "banco agrario", is_active: true, deleted_at: null },
            { name: "davivienda", is_active: true, deleted_at: null },
            { name: "av villas", is_active: true, deleted_at: null },
            { name: "BBVA", is_active: true, deleted_at: null },
        ];

        await this.bankModel.insertMany(data);
        console.log("Bank seed finished successful");
    }
}
