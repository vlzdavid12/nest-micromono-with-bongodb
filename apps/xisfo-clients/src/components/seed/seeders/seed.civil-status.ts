import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CivilStatusEntity } from '../../employee/civil-status/entities/civil-status.entity';

@Injectable()
export class SeedCivilStatus {
    constructor(
        @InjectModel(CivilStatusEntity.name)
        private readonly civilStatusModel: Model<CivilStatusEntity>
    ) { }
    async create() {
        await this.civilStatusModel.deleteMany({});

        const data = [
            { name: "soltero" },
            { name: "casado" },
            { name: "divorsiado" },
            { name: "union libre" },
        ];

        try{
            await this.civilStatusModel.insertMany(data);
            console.log("Civil status seed finished successful");
        }catch (error){
            console.log("Error status seed Civil");
        }


    }
}
