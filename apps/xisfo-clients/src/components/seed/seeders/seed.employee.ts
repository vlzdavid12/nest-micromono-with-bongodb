import {Injectable} from '@nestjs/common';
import {InjectModel, Prop} from "@nestjs/mongoose";
import {Model} from "mongoose";
import * as bcrypt from "bcrypt";
import {EmployeeEntity} from "../../employee/employee-resource/entities/employee-resource.entity";
import {UserEntity} from "../../client/entity/user.entity";
import {ValidPermission, ValidRoles} from "../../../../../xisfo-gateway/src/components/auth/interfaces";
import {UserInputInterface} from "../../../../../xisfo-gateway/src/components/auth/interfaces/user-input.Interface";
import {
    InputEmployeeRequest
} from "../../../../../xisfo-gateway/src/components/auth/interfaces/employee-input.interface";
import {CivilStatusEntity} from "../../employee/civil-status/entities/civil-status.entity";
import {ContractTypeEntity} from "../../employee/contract-type/entities/contract-type.entity";
import {GenderEntity} from "../../employee/gender/entities/gender.entity";
import {PositionEntity} from "../../employee/position/entities/position.entity";
import {HeadquarterEntity} from "../../employee/headquarter/entities/headquarter.entity";
import {HealthProviderEntity} from "../../employee/health-provider/entities/health-provider.entity";
import {
    OccupationalRiskManagerEntity
} from "../../employee/occupational-risk-manager/entities/occupational-risk-manager.entity";
import {
    SociodemographicProfileEntity
} from "../../employee/sociodemographic-profile/entities/sociodemographic-profile.entity";
import {PensionFundEntity} from "../../employee/pension-fund/entities/pension-fund.entity";
import {EducationLevelEntity} from "../../employee/education-level/entities/education-level.entity";
import {ProfesionEntity} from "../../employee/profesion/entities/profesion.entity";
import {CityEntity} from "../../general/city/entities/city.entity";
import {DocumentTypeEntity} from "../../general/document-type/entities/document-type.entity";
import mongoose from "mongoose";
import {
    EmployeeIdentificationEntity
} from "../../employee/employee-identification/entities/employee-identification.entity";
import {
    CreateEmployeeIdentificationDto
} from "../../employee/employee-identification/dto/create-employee-identification.dto";
import { CreateEmployeeResourceDto } from '../../employee/employee-resource/dto/create-employee-resource.dto';

@Injectable()
export class SeedEmployee {

    constructor(
        @InjectModel(UserEntity.name)
        private readonly userModel: Model<UserEntity>,
        @InjectModel(EmployeeEntity.name)
        private readonly employeeModel: Model<EmployeeEntity>,
        @InjectModel(CivilStatusEntity.name)
        private readonly civilModel: Model<CivilStatusEntity>,
        @InjectModel(ContractTypeEntity.name)
        private readonly contractTypeModel: Model<ContractTypeEntity>,
        @InjectModel(GenderEntity.name)
        private readonly genderModel: Model<GenderEntity>,
        @InjectModel(PositionEntity.name)
        private readonly positionModel: Model<PositionEntity>,
        @InjectModel(HeadquarterEntity.name)
        private readonly headquarterModel: Model<HeadquarterEntity>,
        @InjectModel(HealthProviderEntity.name)
        private readonly healthProviderModel: Model<HealthProviderEntity>,
        @InjectModel(OccupationalRiskManagerEntity.name)
        private readonly occupationalRiskManagerModel: Model<OccupationalRiskManagerEntity>,
        @InjectModel(SociodemographicProfileEntity.name)
        private readonly socioDemoGraphProfileModel: Model<SociodemographicProfileEntity>,
        @InjectModel(PensionFundEntity.name)
        private readonly pensionModel: Model<PensionFundEntity>,
        @InjectModel(EducationLevelEntity.name)
        private readonly educationLevelModel: Model<EducationLevelEntity>,
        @InjectModel(ProfesionEntity.name)
        private readonly profesionModel: Model<ProfesionEntity>,
        @InjectModel(CityEntity.name)
        private readonly cityModel: Model<CityEntity>,
        @InjectModel(DocumentTypeEntity.name)
        private readonly documentTypeModel: Model<DocumentTypeEntity>,
        @InjectModel(EmployeeIdentificationEntity.name)
        private readonly employeeIdentificationModel: Model<EmployeeIdentificationEntity>,
    ) {
    }

    async create() {
        await this.userModel.deleteMany({});
        await this.employeeModel.deleteMany({});
        await this.employeeIdentificationModel.deleteMany({});

        //Generate New ID
        let newID =  new mongoose.Types.ObjectId();

        let employee_Request: CreateEmployeeResourceDto = {
            first_name: "edgar",
            second_name: "esteban",
            last_name: "duarte",
            second_last_name: "cordova",
            phone: "3213487458",
            expedition_date: new Date(),
            identification_number: '10190899567',
            birthday: new Date(),
            childrens: "2",
            email: "micorreo_employee@gmail.com",
            profesions: [
                await this.profesionModel.findOne({name: 'otro'})
            ],
            expedition_city: await this.cityModel.findOne({name: 'Medellin'}),
            document_type_id: await this.documentTypeModel.findOne({name: 'Cedula de ciudadania'}),
            civil_status_id: await this.civilModel.findOne({name: 'soltero'}),
            contract_type_id: await this.contractTypeModel.findOne({name: 'Indefinido'}),
            gender_id: await this.genderModel.findOne({name: 'masculino'}),
            position_id: await this.positionModel.findOne({name: 'Desarrollador/a'}),
            headquarter_id: await this.headquarterModel.findOne({name: 'Xisfo Fintech'}),
            health_provider_id: await this.healthProviderModel.findOne({name: 'Salud Total'}),
            occupational_risk_manager_id: await this.occupationalRiskManagerModel.findOne({name: 'POSITIVA'}),
            sociodemographic_profile_id: await this.socioDemoGraphProfileModel.findOne({address: 'CRA 45 NO. 90 - 80'}),
            pension_fund_id: await this.pensionModel.findOne({name: 'Porvenir'}),
            education_level_id: await this.educationLevelModel.findOne({name: 'Técnico'})
        }


        let authDB: UserInputInterface = {
            avatar: "https://via.placeholder.com/120",
            full_name: "edgar esteban duarte cordova",
            password: bcrypt.hashSync("valenzuela21", 10),
            email: "micorreo@gmail.com",
            phone: "3213487458",
            roles: [ValidRoles.user],
            kyc: false,
            permissions: [ValidPermission.read, ValidPermission.write, ValidPermission.delete],
            isActive: true,
            isClient: false,
            data_extra: [],
            profile_id: newID
        }

        let employeeIdentification: CreateEmployeeIdentificationDto = {
            identification_number: employee_Request.identification_number,
            expedition_date: new Date(employee_Request.expedition_date),
            expedition_city: Object(employee_Request.expedition_city),
            document_type: Object(employee_Request.document_type_id),
            employee: Object(newID)
        }

        try {
            await this.userModel.create(authDB);
            await this.employeeModel.create(employee_Request);
            await this.employeeIdentificationModel.create(employeeIdentification);
            console.log("Insert Seeds of Employees");
        }catch (error){
            console.log(error);
            console.log("Error Insert Seeds of Employees");
        }

    }
}
