import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";

import {ClientEntity} from "../../client/client/entities/client.entity";
import * as bcrypt from "bcrypt";

import {RegistrationStatusEntity} from "../../client/registration-status/entities/registration-status.entity";
import {ClientTypeEntity} from "../../client/client-type/entities/client-type.entity";
import {UserEntity} from "../../client/entity/user.entity";
import {DocumentTypeEntity} from "../../general/document-type/entities/document-type.entity";
import {DocumentInputInterface} from "../../interfaces/document-input.interface";
import {ClientInputInterface} from "../../interfaces/client-input.Interface";
import {UserInputInterface} from "../../interfaces/user-input.Interface";
import {ValidPermission} from "../../interfaces/valid-permission";
import {ValidRoles} from "../../interfaces/valid-roles";
import * as mongoose from "mongoose";
import {GenderEntity} from "../../employee/gender/entities/gender.entity";
import {
    EmployeeIdentificationEntity
} from "../../employee/employee-identification/entities/employee-identification.entity";
import {ClientIdentificationEntity} from "../../client/client-identification/entities/client-identification.entity";

@Injectable()
export class SeedClient {
    constructor(
        @InjectModel(UserEntity.name)
        private readonly userModel: Model<UserEntity>,
        @InjectModel(ClientEntity.name)
        private readonly clientModel: Model<ClientEntity>,
        @InjectModel(RegistrationStatusEntity.name)
        private readonly registrationStatus: Model<RegistrationStatusEntity>,
        @InjectModel(ClientTypeEntity.name)
        private readonly clientStatus: Model<ClientTypeEntity>,
        @InjectModel(DocumentTypeEntity.name)
        private readonly documentType: Model<DocumentTypeEntity>,
        @InjectModel(GenderEntity.name)
        private readonly genderModel: Model<GenderEntity>,
        @InjectModel(ClientIdentificationEntity.name)
        private readonly clientIdentificationModel: Model<ClientIdentificationEntity>,
    ) {
    }

    async create() {

        //Generate New ID
        let newID =  new mongoose.Types.ObjectId();
        let phoneRandom =  Math.floor(Math.random() * 99999999999);
        await this.clientModel.deleteMany({});
        await this.clientIdentificationModel.deleteMany({});
        let authDB: UserInputInterface = {
            avatar: "https://via.placeholder.com/120",
            full_name: "david fernando valenzuela pardo",
            password: bcrypt.hashSync("valenzuela21", 10),
            email: "micorreo@gmail.com",
            phone: phoneRandom.toString(),
            roles: [ValidRoles.user],
            kyc: false,
            permissions: [ValidPermission.read, ValidPermission.write, ValidPermission.delete],
            isActive: true,
            isClient: true,
            data_extra: [],
            profile_id: newID
        }


        let clientDB: ClientInputInterface = {
            _id: newID,
            first_name: "David",
            second_name: "Fernando",
            last_name: "Valenzuela",
            second_last_name: "Pardo",
            email: "micorreo@gmail.com",
            country: "Colombia",
            phone: phoneRandom.toString(),
            address: "cra 45 no 67-89",
            client_rate: '2.5',
            with_holding_tax: true,
            old_client: false,
            client_type:  await this.clientStatus.findOne({name: 'natural'}),
            registration_status:  await this.registrationStatus.findOne({name: 'moneda_local'}),
            is_active: true,
            deleted_at: null
        }

        const {_id} = await this.genderModel.findOne({name: 'masculino'});

        let documentDB: DocumentInputInterface = {
            country_document: "Bogotá Colombia",
            expedition_date: new Date("2/1/2022"),
            date_birth: new Date("8/1/1092"),
            identification_number: "23458697009",
            gender: _id,
            document_type: await  this.documentType.findOne({name: 'Cedula de ciudadania'}),
        }

        let request = new Promise(async (myResolve, myReject) => {
            try {
                await this.userModel.create(authDB);
                myResolve(true);
            } catch (error) {
                console.log(error);
                myReject(false);
            }
        })

        if (request) {
            await this.clientModel.create(clientDB);
            documentDB.client = newID;
            await this.clientIdentificationModel.create(documentDB);
            console.log("Insert Seeds Client with Document!!");
        } else {
            console.log("Error Insert Seeds of Client");
        }



    }
}
