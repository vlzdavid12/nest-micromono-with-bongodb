import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {HeadquarterEntity} from "../../employee/headquarter/entities/headquarter.entity";
import {CityEntity} from "../../general/city/entities/city.entity";

@Injectable()
export class SeedHeadQuarter {
    constructor(
        @InjectModel(HeadquarterEntity.name)
        private readonly headquarterModel: Model<HeadquarterEntity>,
        @InjectModel(CityEntity.name)
        private readonly cityModel: Model<CityEntity>
    ) {
    }

    async create() {

        try{
            await this.headquarterModel.deleteMany({});

            const data = [
                {
                    "name": "Lefemme Dosquebradas",
                    "city": await this.cityModel.findOne({name: 'Dosquebradas'}),
                "is_active": true,
                "deleted_at": null
                },
                {
                    "name": "Xisfo Fintech",
                    "city": await this.cityModel.findOne({name: 'Pereira'}),
                "is_active": true,
                "deleted_at": null
                },
                {
                    "name": "Lefemme Principal",
                    "city": await this.cityModel.findOne({name: 'Pereira'}),
                "is_active": true,
                "deleted_at": null
                }
            ];

            await this.headquarterModel.insertMany(data);
            console.log("Headquarter seed finished successful");

        }catch (error){
            console.log(error)
            console.log("Error headquarter seed");
        }

    }
}
