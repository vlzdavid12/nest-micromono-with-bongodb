import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {
    SociodemographicProfileEntity
} from "../../employee/sociodemographic-profile/entities/sociodemographic-profile.entity";
import {StrataEntity} from "../../employee/strata/entities/strata.entity";
import {CityEntity} from "../../general/city/entities/city.entity";
import {HousingTypeEntity} from "../../employee/housing-type/entities/housing-type.entity";

@Injectable()
export class SeedSocioDemoGraphic {
    constructor(
        @InjectModel(SociodemographicProfileEntity.name)
        private readonly sociodemographicProfileModel: Model<SociodemographicProfileEntity>,
        @InjectModel(StrataEntity.name)
        private readonly strataModel: Model<StrataEntity>,
        @InjectModel(CityEntity.name)
        private readonly cityModel: Model<CityEntity>,
        @InjectModel(HousingTypeEntity.name)
        private readonly housingTypeModel: Model<HousingTypeEntity>
    ) {
    }

    async create() {
        await this.sociodemographicProfileModel.deleteMany({});

        const data = [
            {
                "antiquity": 2,
                "address": "CRA 45 NO. 90 - 80",
                "housing_type": await this.housingTypeModel.findOne({name: 'Propia'}),
                "strata": await this.strataModel.findOne({name: 'Estrato I'}),
                "city": await this.cityModel.findOne({name: 'Medellin'})
            },
            {
                "antiquity": 8,
                "address": "CALLE 156 NO.89-90",
                "housing_type": await this.housingTypeModel.findOne({name: 'Propia'}),
                "strata": await this.strataModel.findOne({name: 'Estrato I'}),
                "city": await this.cityModel.findOne({name: 'Medellin'})
            }
        ];

        await this.sociodemographicProfileModel.insertMany(data);
        console.log("Type SeedSocioDemoGraphic seed finished successful");
    }
}
