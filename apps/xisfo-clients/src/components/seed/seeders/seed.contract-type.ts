import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { ContractTypeEntity } from '../../employee/contract-type/entities/contract-type.entity';

@Injectable()
export class SeedContractType {
    constructor(
        @InjectModel(ContractTypeEntity.name)
        private readonly contractTypeModel: Model<ContractTypeEntity>
    ) { }
    async create() {
        await this.contractTypeModel.deleteMany({});

        const data = [
            { name: "Indefinido" },
            { name: "Definido" },
            { name: "Obra o labor" },
            { name: "Prestacion Servicios" },
        ];

        await this.contractTypeModel.insertMany(data);
        console.log("Contract type seed finished successful");
    }
}
