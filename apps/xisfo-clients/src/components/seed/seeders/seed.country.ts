import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CountryEntity } from '../../client/client/entities/country.entity';

@Injectable()
export class SeedCountry {
    constructor(
        @InjectModel(CountryEntity.name)
        private readonly countryModel: Model<CountryEntity>
    ) { }
    async create() {
        await this.countryModel.deleteMany({});
        const data = [
            { name: "Alemania" },
            { name: "Andorra" },
            { name: "Angola" },
            { name: "Argentina" },
            { name: "Australia" },
            { name: "Bahamas" },
            { name: "Bélgica" },
            { name: "Bolivia" },
            { name: "Brasil" },
            { name: "Canadá" },
            { name: "Cuba" },
            { name: "Colombia" },
            { name: "Dinamarca" },
            { name: "Republic Dominicana" },
            { name: "El Salvador" },
            { name: "España" },
            { name: "Estonia" },
            { name: "Filipinas" },
            { name: "Filandia" },
            { name: "Francia" },
            { name: "Georgia" },
            { name: "Grecia" },
            { name: "Guatemala" },
            { name: "Honduras" },
            { name: "India" },
            { name: "Iran" },
            { name: "Islandia" },
            { name: "Israel" },
            { name: "Italia" },
            { name: "Jamaica" },
            { name: "Japon" },
            { name: "Letonia" },
            { name: "Liberia" },
            { name: "Libia" },
            { name: "Madagascar" },
            { name: "México" },
            { name: "Malta" },
            { name: "Marruecos"},
            { name: "Monáco" },
            { name: "Mongolia" },
            { name: "Nepal" },
            { name: "Noruega" },
            { name: "Nueva Zelanda" },
            { name: "Pises Bajos" },
            { name: "Palestina" },
            { name: "Paraguay" },
            { name: "Perú" },
            { name: "Portugal" },
            { name: "Reino Unido" },
            { name: "Rumania" },
            { name: "Rusia" },
            { name: "Serbia" },
            { name: "Singapur" },
            { name: "Siria" },
            { name: "Somalia" },
            { name: "Suecia" },
            { name: "Suiza" },
            { name: "Tailandia" },
            { name: "Turquia" },
            { name: "Ucrania" },
            { name: "Ciudad Vaticano" },
            { name: "Vietnam" },
            { name: "Venezuela" },
            { name: "United States" },
        ];

        await this.countryModel.insertMany(data);
        console.log("Country seed finished successful");
    }
}
