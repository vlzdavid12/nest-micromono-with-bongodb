import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { KinshipEntity } from '../../employee/kinship/entities/kinship.entity';

@Injectable()
export class SeedKinship {
    constructor(
        @InjectModel(KinshipEntity.name)
        private readonly kinshipModel: Model<KinshipEntity>
    ) { }
    async create() {
        await this.kinshipModel.deleteMany({});

        const data = [
            { name: "madre" },
            { name: "padre" },
            { name: "hermano/a" },
            { name: "tio/a" },
            { name: "abuelo/a" },
        ];

        await this.kinshipModel.insertMany(data);
        console.log("Kinship seed finished successful");
    }
}
