import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { HousingTypeEntity } from '../../employee/housing-type/entities/housing-type.entity';

@Injectable()
export class SeedHousingType {
    constructor(
        @InjectModel(HousingTypeEntity.name)
        private readonly housingType: Model<HousingTypeEntity>
    ) { }
    async create() {
        await this.housingType.deleteMany({});

        const data = [
            { name: "Arrendada" },
            { name: "Propia" }
        ];

        await this.housingType.insertMany(data);
        console.log("Housing type seed finished successful");
    }
}
