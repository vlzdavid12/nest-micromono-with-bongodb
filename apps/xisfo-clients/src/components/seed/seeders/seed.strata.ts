import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { StrataEntity } from '../../employee/strata/entities/strata.entity';

@Injectable()
export class SeedStrata {
    constructor(
        @InjectModel(StrataEntity.name)
        private readonly strataModel: Model<StrataEntity>
    ) { }
    async create() {
        await this.strataModel.deleteMany({});

        const data = [
            { name: "Estrato I" },
            { name: "Estrato II" },
            { name: "Estrato III" },
            { name: "Estrato IV" },
            { name: "Estrato V" },
            { name: "Estrato VI" },
        ];

        await this.strataModel.insertMany(data);
        console.log("Strata seed finished successful");
    }
}
