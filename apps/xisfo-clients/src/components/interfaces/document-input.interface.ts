export class DocumentInputInterface{
    country_document: string;
    document_type: string;
    expedition_date: Date;
    date_birth: Date;
    identification_number: string;
    gender: string;
    client?: object | string;
}
