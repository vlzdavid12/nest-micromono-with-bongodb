export  interface ClientInputInterface{
    _id?: object,
    first_name: string,
    second_name: string,
    last_name: string,
    second_last_name: string,
    email: string,
    country: string,
    phone: string,
    address: string,
    client_rate: string,
    with_holding_tax: boolean,
    old_client: false,
    client_type: string,
    registration_status: string,
    is_active: boolean,
    deleted_at: Date
}
