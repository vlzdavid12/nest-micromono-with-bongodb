export enum ValidPermission {
    read = 'read',
    write = 'write',
    delete = 'delete',
    auth_phone = 'auth.phone',

    //Client
    client_all = 'client.read.all',
    client_read = 'client.read',
    client_create = 'client.create',
    client_update = 'client.update',
    client_delete = 'client.delete',

    //Client Identification
    client_identification_all = 'client_identification.read.all',
    client_identification_read = 'client_identification.read',
    client_identification_create = 'client_identification.create',
    client_identification_update = 'client_identification.update',
    client_identification_delete = 'client_identification.delete',

    //Client Platform
    client_platform_all = 'client_platform.read.all',
    client_platform_read = 'client_platform.read',
    client_platform_create = 'client_platform.create',
    client_platform_update = 'client_platform.update',
    client_platform_delete = 'client_platform.delete',

    //Client Type
    client_type_all = 'client_type.read.all',
    client_type_read = 'client_type.read',

    //Registration Status
    registration_status_all = 'registration_status.read.all',
    registration_status_read = 'registration_status.read',

    //Email Address employee
    email_address_all = 'email_address.read.all',
    email_address_read = 'email_address.read',
    email_address_create = 'email_address.create',
    email_address_update = 'email_address.update',
    email_address_delete = 'email_address.delete',

    //Emergency Contacts Employee
    emergency_contact_all = 'emergency_contact.read.all',
    emergency_contact_read = 'emergency_contact.read',
    emergency_contact_create = 'emergency_contact.create',
    emergency_contact_update = 'emergency_contact.update',
    emergency_contact_delete = 'emergency_contact.delete',

    //Identification Employee
    employee_identification_all = 'employee_identification.read.all',
    employee_identification_read = 'employee_identification.read',
    employee_identification_create = 'employee_identification.create',
    employee_identification_update = 'employee_identification.update',
    employee_identification_delete = 'employee_identification.delete',

    //Employee
    employee_all = 'employee.read.all',
    employee_read = 'employee.read',
    employee_create = 'employee.create',
    employee_update = 'employee.update',
    employee_delete = 'employee.delete',

    //Headquarter
    headquarter_all = 'headquarter.read.all',
    headquarter_read = 'headquarter.read',
    headquarter_create = 'headquarter.create',
    headquarter_update = 'headquarter.update',
    headquarter_delete = 'headquarter.delete',

    //Phone Number Employee
    phone_number_all = 'phone_number.read.all',
    phone_number_read = 'phone_number.read',
    phone_number_create = 'phone_number.create',
    phone_number_update = 'phone_number.update',
    phone_number_delete = 'phone_number.delete',

    //SocioDemographic Profile
    sociodemographic_profile_all = 'sociodemographic_profile.read.all',
    sociodemographic_profile_read = 'sociodemographic_profile.read',
    sociodemographic_profile_create = 'sociodemographic_profile.create',
    sociodemographic_profile_update = 'sociodemographic_profile.update',
    sociodemographic_profile_delete = 'sociodemographic_profile.delete',

    //Bank
    bank_all = 'bank.read.all',
    bank_read = 'bank.read',
    bank_create = 'bank.create',
    bank_update = 'bank.update',
    bank_delete = 'bank.delete',

    //Contract
    contract_all = 'contract.read.all',
    contract_read = 'contract.read',
    contract_create = 'contract.create',

    //Payment
    payment_all = 'payment.read.all',
    payment_read = 'payment.read',
    payment_create = 'payment.create',
    payment_update = 'payment.update',
    payment_delete = 'payment.delete',

    //Payment Request
    payment_request_all = 'payment_request.read.all',
    payment_request_read = 'payment_request.read',
    payment_request_create = 'payment_request.create',
    payment_request_update = 'payment_request.update',
    payment_request_delete = 'payment_request.delete',

    //Platform
    platform_all = 'platform.read.all',
    platform_read = 'platform.read',
    platform_create = 'platform.create',
    platform_update = 'platform.update',
    platform_delete = 'platform.delete',

    //Nicho
    nicho_all = 'nicho.read.all',
    nicho_read = 'nicho.read',
    nicho_create = 'nicho.create',
    nicho_update = 'nicho.update',
    nicho_delete = 'nicho.delete',

    //Sell Token
    sell_token_all = 'sell_token.read.all',
    sell_token_read = 'sell_token.read',
    sell_token_create = 'sell_token.create',
    sell_token_update = 'sell_token.update',
    sell_token_delete = 'sell_token.delete',

    //Transfer
    transfer_all = 'transfer.read.all',
    transfer_read = 'transfer.read',
    transfer_create = 'transfer.create',
    transfer_update = 'transfer.update',
    transfer_delete = 'transfer.delete',

    //wallet
    wallet_all = 'wallet.read.all',
    wallet_read = 'wallet.read',
    wallet_create = 'wallet.create',
    wallet_update = 'wallet.update',
    wallet_delete = 'wallet.delete',
}
