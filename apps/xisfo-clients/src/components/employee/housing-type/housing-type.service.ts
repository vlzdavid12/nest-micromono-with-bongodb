import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { HousingTypeEntity } from './entities/housing-type.entity';

@Injectable()
export class HousingTypeService {

  constructor(
    @InjectModel(HousingTypeEntity.name) private housingTypeModel: Model<HousingTypeEntity>
  ){

  }

  async findAll() {
    return await this.housingTypeModel.find();
  }
}
