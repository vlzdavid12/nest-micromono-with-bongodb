import { Module } from '@nestjs/common';
import { HousingTypeService } from './housing-type.service';
import { HousingTypeController } from './housing-type.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { HousingTypeEntity, HousingTypeSchema } from './entities/housing-type.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: HousingTypeEntity.name,
        schema: HousingTypeSchema
      }
    ])
  ],
  controllers: [HousingTypeController],
  providers: [HousingTypeService]
})
export class HousingTypeModule {}
