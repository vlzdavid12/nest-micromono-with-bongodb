import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema({ collection: 'housing_types' })
export class HousingTypeEntity {
    @Prop()
    name: string
}

export const HousingTypeSchema = SchemaFactory.createForClass(HousingTypeEntity);
