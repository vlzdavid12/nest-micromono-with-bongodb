import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { HousingTypeService } from './housing-type.service';

@Controller()
export class HousingTypeController {
  constructor(private readonly housingTypeService: HousingTypeService) {}

  @MessagePattern('findAllHousingType')
  async findAll() {
    return await this.housingTypeService.findAll();
  }
}
