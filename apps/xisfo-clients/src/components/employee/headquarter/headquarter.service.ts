import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateHeadquarterDto } from './dto/create-headquarter.dto';
import { UpdateHeadquarterDto } from './dto/update-headquarter.dto';
import { HeadquarterEntity } from './entities/headquarter.entity';
import {PaginationDto} from "../../../commons/paginationDto";

@Injectable()
export class HeadquarterService {

  constructor(
    @InjectModel(HeadquarterEntity.name) private headquarterModel: Model<HeadquarterEntity>
  ) { }

  async create(createHeadquarterDto: CreateHeadquarterDto) {
    return await this.headquarterModel.create({is_active: true, deleted_at: null, ...createHeadquarterDto});
  }

  async findAll(paginationDto: PaginationDto) {
    const {limit = 12, offset = 1, orderBy} = paginationDto;
    const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
    try {
      const count = await this.headquarterModel.count();
      const data = await this.headquarterModel.find()
          .skip((limit * offset) - limit)
          .limit(limit)
          .sort(order)
          .populate({
        path: 'city',
        populate: {
          path: 'department',
          model: 'DepartmentEntity'
        }
      }).exec();
      return {data, count, limit, offset, orderBy};
    }catch (error){
      console.log(error)
    }
  }

  async findOne(id: string) {
    const headquarter = await this.headquarterModel.findById(id).populate({
      path: 'city',
      populate: {
        path: 'department',
        model: 'DepartmentEntity'
      }
    }).exec();

    if (!headquarter) {
      throw new NotFoundException();
    }

    return headquarter;
  }

  async update(id: string, updateHeadquarterDto: UpdateHeadquarterDto) {

    const updateHeadquarter = await this.headquarterModel
      .findByIdAndUpdate(id, updateHeadquarterDto)
      .setOptions({ new: true })

    if (!updateHeadquarter) {
      throw new NotFoundException();
    }

    return updateHeadquarter;
  }

  async remove(id: string) {
    return await this.headquarterModel.findByIdAndUpdate(id, {
      deleted_at: new Date,
      is_active: false
    })
    .exec();
  }
}
