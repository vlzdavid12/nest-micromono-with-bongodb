import { Module } from '@nestjs/common';
import { HeadquarterService } from './headquarter.service';
import { HeadquarterController } from './headquarter.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { HeadquarterEntity, HeadquarterSchema } from './entities/headquarter.entity';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: HeadquarterEntity.name, schema: HeadquarterSchema }])
  ],
  controllers: [HeadquarterController],
  providers: [HeadquarterService]
})
export class HeadquarterModule { }
