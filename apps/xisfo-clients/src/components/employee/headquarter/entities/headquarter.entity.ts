import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { Document } from "mongoose";
import { CityEntity } from "../../../general/city/entities/city.entity";

@Schema({ collection: 'headquarters', timestamps: true })
export class HeadquarterEntity extends Document{

    @Prop({ unique: true, required: true })
    name: string;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'CityEntity', required: true })
    city: CityEntity

    @Prop({required: true})
    is_active: boolean

    @Prop({required: false})
    deleted_at: Date
}

export const HeadquarterSchema = SchemaFactory.createForClass(HeadquarterEntity);
