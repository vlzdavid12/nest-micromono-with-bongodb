import { IsNotEmpty, Length } from "class-validator";

export class CreateHeadquarterDto {

    @IsNotEmpty()
    @Length(3, 100)
    name: string;

    @IsNotEmpty()
    city: string
}
