import { PartialType } from '@nestjs/mapped-types';
import { IsMongoId } from 'class-validator';
import { CreateHeadquarterDto } from './create-headquarter.dto';

export class UpdateHeadquarterDto extends PartialType(CreateHeadquarterDto) {
  @IsMongoId()
  id: string;
}
