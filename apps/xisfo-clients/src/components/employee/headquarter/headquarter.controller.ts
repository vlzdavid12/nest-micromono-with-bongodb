import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { HeadquarterService } from './headquarter.service';
import { CreateHeadquarterDto } from './dto/create-headquarter.dto';
import { UpdateHeadquarterDto } from './dto/update-headquarter.dto';
import {PaginationDto} from "../../../commons/paginationDto";

@Controller()
export class HeadquarterController {
  constructor(private readonly headquarterService: HeadquarterService) {}

  @MessagePattern('createHeadquarter')
  async create(@Payload() createHeadquarterDto: CreateHeadquarterDto) {
    return await this.headquarterService.create(createHeadquarterDto);
  }

  @MessagePattern('findAllHeadquarter')
  async findAll(@Payload() paginationDto: PaginationDto) {
    return await this.headquarterService.findAll(paginationDto);
  }

  @MessagePattern('findOneHeadquarter')
  async findOne(@Payload() id: string) {
    return await this.headquarterService.findOne(id);
  }

  @MessagePattern('updateHeadquarter')
  async update(@Payload() updateHeadquarterDto: UpdateHeadquarterDto) {
    return await this.headquarterService.update(updateHeadquarterDto.id, updateHeadquarterDto);
  }

  @MessagePattern('removeHeadquarter')
  remove(@Payload() id: string) {
    return this.headquarterService.remove(id);
  }
}
