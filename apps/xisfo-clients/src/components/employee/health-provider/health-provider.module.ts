import { Module } from '@nestjs/common';
import { HealthProviderService } from './health-provider.service';
import { HealthProviderController } from './health-provider.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { HealthProviderEntity, HealthProviderSchema } from './entities/health-provider.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: HealthProviderEntity.name,
        schema: HealthProviderSchema
      }
    ])
  ],
  controllers: [HealthProviderController],
  providers: [HealthProviderService]
})
export class HealthProviderModule {}
