import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { HealthProviderService } from './health-provider.service';

@Controller()
export class HealthProviderController {
  constructor(private readonly healthProviderService: HealthProviderService) {}

  @MessagePattern('findAllHealthProvider')
  async findAll() {
    return await this.healthProviderService.findAll();
  }
}
