import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

@Schema({collection: 'health_providers'})
export class HealthProviderEntity extends Document{
    @Prop()
    name: string
}

export const HealthProviderSchema = SchemaFactory.createForClass(HealthProviderEntity);
