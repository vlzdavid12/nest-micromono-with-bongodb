import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { HealthProviderEntity } from './entities/health-provider.entity';

@Injectable()
export class HealthProviderService {

  constructor(
    @InjectModel(HealthProviderEntity.name) private healthProviderModel: Model<HealthProviderEntity>
  ){}

  async findAll() {
    return await this.healthProviderModel.find();
  }
}
