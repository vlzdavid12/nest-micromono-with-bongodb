import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { PhoneNumberService } from './phone-number.service';
import { CreatePhoneNumberDto } from './dto/create-phone-number.dto';
import { UpdatePhoneNumberDto } from './dto/update-phone-number.dto';

@Controller()
export class PhoneNumberController {
  constructor(private readonly phoneNumberService: PhoneNumberService) {}

  @MessagePattern('createPhoneNumber')
  create(@Payload() createPhoneNumberDto: CreatePhoneNumberDto) {
    return this.phoneNumberService.create(createPhoneNumberDto);
  }

  @MessagePattern('findAllPhoneNumber')
  findAll() {
    return this.phoneNumberService.findAll();
  }

  @MessagePattern('findOnePhoneNumber')
  findOne(@Payload() id: string) {
    return this.phoneNumberService.findOne(id);
  }

  @MessagePattern('updatePhoneNumber')
  update(@Payload() updatePhoneNumberDto: UpdatePhoneNumberDto) {
    return this.phoneNumberService.update(updatePhoneNumberDto.id, updatePhoneNumberDto);
  }

  @MessagePattern('removePhoneNumber')
  remove(@Payload() id: string) {
    return this.phoneNumberService.remove(id);
  }
}
