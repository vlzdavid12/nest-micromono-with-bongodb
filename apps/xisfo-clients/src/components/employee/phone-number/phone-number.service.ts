import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreatePhoneNumberDto } from './dto/create-phone-number.dto';
import { UpdatePhoneNumberDto } from './dto/update-phone-number.dto';
import { PhoneNumber } from './entities/phone-number.entity';

@Injectable()
export class PhoneNumberService {

  constructor(
    @InjectModel(PhoneNumber.name) private phoneNumberModel: Model<PhoneNumber>
  ) { }

  async create(createPhoneNumberDto: CreatePhoneNumberDto) {
    try {
      return await this.phoneNumberModel.create({is_active: true, deleted_at: null, ...createPhoneNumberDto});
    } catch (error) {
      throw Error(error);
    }
  }

  async findAll() {
    return await this.phoneNumberModel.find().populate('employee').exec();
  }

  async findOne(id: string) {
    const phoneNumber = await this.phoneNumberModel.findById(id).populate('employee').exec();

    if (!phoneNumber) {
      throw new NotFoundException()
    }
    return phoneNumber;
  }

  async update(id: string, updatePhoneNumberDto: UpdatePhoneNumberDto) {
    const phoneNumberUpdated = await this.phoneNumberModel
      .findByIdAndUpdate(id, updatePhoneNumberDto)
      .setOptions({ new: true });

    if (!phoneNumberUpdated) {
      throw new NotFoundException()
    }
    return phoneNumberUpdated;
  }

  async remove(id: string) {
    const phoneNumberDeleted = await this.phoneNumberModel.findByIdAndUpdate(id, {
      deleted_at: new Date,
      is_active: false
    })
    .exec();

    if (!phoneNumberDeleted) {
      throw new NotFoundException()
    }
    return phoneNumberDeleted;
  }
}
