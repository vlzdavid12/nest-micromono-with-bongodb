import { Module } from '@nestjs/common';
import { PhoneNumberService } from './phone-number.service';
import { PhoneNumberController } from './phone-number.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { PhoneNumber, PhoneNumberSchema } from './entities/phone-number.entity';

@Module({
  imports:[
    MongooseModule.forFeature([
      {
        name: PhoneNumber.name,
        schema: PhoneNumberSchema
      }
    ])
  ],
  controllers: [PhoneNumberController],
  providers: [PhoneNumberService]
})
export class PhoneNumberModule {}
