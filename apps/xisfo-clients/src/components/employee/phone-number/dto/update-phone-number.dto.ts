import { PartialType } from '@nestjs/mapped-types';
import { IsMongoId } from 'class-validator';
import { CreatePhoneNumberDto } from './create-phone-number.dto';

export class UpdatePhoneNumberDto extends PartialType(CreatePhoneNumberDto) {
  
  @IsMongoId()
  id: string;
}
