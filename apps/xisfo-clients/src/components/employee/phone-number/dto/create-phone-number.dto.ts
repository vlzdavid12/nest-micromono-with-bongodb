import { IsNotEmpty, IsString } from "class-validator";

export class CreatePhoneNumberDto {

    @IsString()
    phone_number: string

    @IsNotEmpty()
    employee: string

}
