import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { Document } from "mongoose";
import { EmployeeEntity } from "../../employee-resource/entities/employee-resource.entity";

@Schema({ collection: 'phone_numbers', timestamps: true })
export class PhoneNumber extends Document {

    @Prop({ required: true })
    phone_number: string

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'EmployeeEntity' })
    employee: EmployeeEntity

    @Prop({required: true})
    is_active: boolean

    @Prop({required: false})
    deleted_at: Date
}

export const PhoneNumberSchema = SchemaFactory.createForClass(PhoneNumber);
