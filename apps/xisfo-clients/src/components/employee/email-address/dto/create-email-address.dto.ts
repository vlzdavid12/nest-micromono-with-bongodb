import { IsEmail, IsNotEmpty } from "class-validator";

export class CreateEmailAddressDto {

    @IsNotEmpty()
    @IsEmail()
    email_address: string

    @IsNotEmpty()
    employee: string
}
