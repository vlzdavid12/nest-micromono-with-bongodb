import { PartialType } from '@nestjs/mapped-types';
import { IsMongoId } from 'class-validator';
import { CreateEmailAddressDto } from './create-email-address.dto';

export class UpdateEmailAddressDto extends PartialType(CreateEmailAddressDto) {
  @IsMongoId()
  id: string;
}
