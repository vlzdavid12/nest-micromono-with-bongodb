import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose from "mongoose";
import { EmployeeEntity } from "../../employee-resource/entities/employee-resource.entity";

@Schema({ collection: 'email_addresses', timestamps: true })
export class EmailAddress {

    @Prop({ required: true, unique: true })
    email_address: string

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'EmployeeEntity' })
    employee: EmployeeEntity

    @Prop({required: true})
    is_active: boolean

    @Prop({required: false})
    deleted_at: Date
}

export const EmailAddressSchema = SchemaFactory.createForClass(EmailAddress);
