import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateEmailAddressDto } from './dto/create-email-address.dto';
import { UpdateEmailAddressDto } from './dto/update-email-address.dto';
import { EmailAddress } from './entities/email-address.entity';

@Injectable()
export class EmailAddressService {

  constructor(
    @InjectModel(EmailAddress.name) private emailAddressModel: Model<EmailAddress>
  ) { }

  async create(createEmailAddressDto: CreateEmailAddressDto) {
    try {
      return await this.emailAddressModel.create({is_active: true, deleted_at: null, ...createEmailAddressDto});
    } catch (error) {
      throw new Error(error);
    }
  }

  async findAll() {
    return await this.emailAddressModel.find().populate('employee').exec();
  }

  async findOne(id: string) {

    const emailAddress = await this.emailAddressModel.findById(id).populate('employee').exec();
    if (!emailAddress) {
      throw new NotFoundException()
    }
    return emailAddress;
  }

  async update(id: string, updateEmailAddressDto: UpdateEmailAddressDto) {
    const emailUpdated = await this.emailAddressModel
      .findByIdAndUpdate(id, updateEmailAddressDto)
      .setOptions({ new: true });
    if (!emailUpdated) {
      throw new NotFoundException()
    }
    return emailUpdated;
  }

  async remove(id: string) {
    const emailAddressDeleted = await this.emailAddressModel.findByIdAndUpdate(id, {
      deleted_at: new Date,
      is_active: false
    })
    .exec();
    if (!emailAddressDeleted) {
      throw new NotFoundException()
    }
    return emailAddressDeleted;
  }
}
