import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { EmailAddressService } from './email-address.service';
import { CreateEmailAddressDto } from './dto/create-email-address.dto';
import { UpdateEmailAddressDto } from './dto/update-email-address.dto';

@Controller()
export class EmailAddressController {
  constructor(private readonly emailAddressService: EmailAddressService) {}

  @MessagePattern('createEmailAddress')
  create(@Payload() createEmailAddressDto: CreateEmailAddressDto) {
    return this.emailAddressService.create(createEmailAddressDto);
  }

  @MessagePattern('findAllEmailAddress')
  findAll() {
    return this.emailAddressService.findAll();
  }

  @MessagePattern('findOneEmailAddress')
  findOne(@Payload() id: string) {
    return this.emailAddressService.findOne(id);
  }

  @MessagePattern('updateEmailAddress')
  update(@Payload() updateEmailAddressDto: UpdateEmailAddressDto) {
    return this.emailAddressService.update(updateEmailAddressDto.id, updateEmailAddressDto);
  }

  @MessagePattern('removeEmailAddress')
  remove(@Payload() id: string) {
    return this.emailAddressService.remove(id);
  }
}
