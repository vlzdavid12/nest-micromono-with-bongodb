import { Module } from '@nestjs/common';
import { EmailAddressService } from './email-address.service';
import { EmailAddressController } from './email-address.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { EmailAddress, EmailAddressSchema } from './entities/email-address.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: EmailAddress.name,
        schema: EmailAddressSchema
      }
    ])
  ],
  controllers: [EmailAddressController],
  providers: [EmailAddressService]
})
export class EmailAddressModule {}
