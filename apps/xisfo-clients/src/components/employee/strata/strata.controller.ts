import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { StrataService } from './strata.service';

@Controller()
export class StrataController {
  constructor(private readonly strataService: StrataService) {}

  @MessagePattern('findAllStrata')
  async findAll() {
    return await this.strataService.findAll();
  }
}
