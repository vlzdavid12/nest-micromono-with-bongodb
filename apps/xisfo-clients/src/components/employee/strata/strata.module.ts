import { Module } from '@nestjs/common';
import { StrataService } from './strata.service';
import { StrataController } from './strata.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { StrataEntity, StrataSchema } from './entities/strata.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: StrataEntity.name,
        schema: StrataSchema
      }
    ])
  ],
  controllers: [StrataController],
  providers: [StrataService]
})
export class StrataModule {}
