import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { StrataEntity } from './entities/strata.entity';

@Injectable()
export class StrataService {

  constructor(
    @InjectModel(StrataEntity.name) private strataModel: Model<StrataEntity>
  ){}

  async findAll() {
    return await this.strataModel.find();
  }
}
