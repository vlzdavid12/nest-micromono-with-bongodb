import { Test, TestingModule } from '@nestjs/testing';
import { StrataController } from './strata.controller';
import { StrataService } from './strata.service';

describe('StrataController', () => {
  let controller: StrataController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StrataController],
      providers: [StrataService],
    }).compile();

    controller = module.get<StrataController>(StrataController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
