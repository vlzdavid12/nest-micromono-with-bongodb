import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema({collection: 'stratas'})
export class StrataEntity {
    @Prop()
    name: string
}

export const StrataSchema = SchemaFactory.createForClass(StrataEntity)
