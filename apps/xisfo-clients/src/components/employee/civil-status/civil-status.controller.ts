import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { CivilStatusService } from './civil-status.service';

@Controller()
export class CivilStatusController {
  constructor(private readonly civilStatusService: CivilStatusService) {}

  @MessagePattern('findAllCivilStatus')
  async findAll() {
    return await this.civilStatusService.findAll();
  }
}
