import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';

@Schema({ collection: 'civil_statuses'})
export class CivilStatusEntity extends Document{
    @Prop({required: true})
    name: string
}

export const CivilStatusSchema = SchemaFactory.createForClass(CivilStatusEntity);
