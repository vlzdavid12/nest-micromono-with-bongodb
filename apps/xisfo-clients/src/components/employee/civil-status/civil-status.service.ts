import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CivilStatusEntity } from './entities/civil-status.entity';

@Injectable()
export class CivilStatusService {

  constructor(
    @InjectModel(CivilStatusEntity.name) private civilStatusModel: Model<CivilStatusEntity>
  ) { }

  findAll() {
    return this.civilStatusModel.find();
  }
}
