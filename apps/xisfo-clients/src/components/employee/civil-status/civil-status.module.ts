import { Module } from '@nestjs/common';
import { CivilStatusService } from './civil-status.service';
import { CivilStatusController } from './civil-status.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { CivilStatusEntity, CivilStatusSchema } from './entities/civil-status.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: CivilStatusEntity.name,
        schema: CivilStatusSchema
      }
    ])
  ],
  controllers: [CivilStatusController],
  providers: [CivilStatusService]
})
export class CivilStatusModule { }
