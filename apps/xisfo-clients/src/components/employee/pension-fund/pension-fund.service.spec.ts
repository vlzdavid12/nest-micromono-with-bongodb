import { Test, TestingModule } from '@nestjs/testing';
import { PensionFundService } from './pension-fund.service';

describe('PensionFundService', () => {
  let service: PensionFundService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PensionFundService],
    }).compile();

    service = module.get<PensionFundService>(PensionFundService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
