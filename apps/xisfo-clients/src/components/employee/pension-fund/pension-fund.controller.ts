import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { PensionFundService } from './pension-fund.service';

@Controller()
export class PensionFundController {
  constructor(private readonly pensionFundService: PensionFundService) {}

  @MessagePattern('findAllPensionFund')
  async findAll() {
    return await this.pensionFundService.findAll();
  }
}
