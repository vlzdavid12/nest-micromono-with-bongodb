import { Module } from '@nestjs/common';
import { PensionFundService } from './pension-fund.service';
import { PensionFundController } from './pension-fund.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { PensionFundEntity, PensionFundSchema } from './entities/pension-fund.entity';

@Module({
  imports:[
    MongooseModule.forFeature([
      {
        name: PensionFundEntity.name,
        schema: PensionFundSchema
      }
    ])
  ],
  controllers: [PensionFundController],
  providers: [PensionFundService]
})
export class PensionFundModule {}
