import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

@Schema({ collection: 'pension_funds' })
export class PensionFundEntity extends Document {
    @Prop()
    name: string
}

export const PensionFundSchema = SchemaFactory.createForClass(PensionFundEntity);
