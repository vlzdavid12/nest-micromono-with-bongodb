import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PensionFundEntity } from './entities/pension-fund.entity';

@Injectable()
export class PensionFundService {

  constructor(
    @InjectModel(PensionFundEntity.name) private pensionFundModel: Model<PensionFundEntity>
  ){}

  async findAll() {
    return await this.pensionFundModel.find();
  }
}
