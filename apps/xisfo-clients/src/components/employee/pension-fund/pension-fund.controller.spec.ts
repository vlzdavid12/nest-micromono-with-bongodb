import { Test, TestingModule } from '@nestjs/testing';
import { PensionFundController } from './pension-fund.controller';
import { PensionFundService } from './pension-fund.service';

describe('PensionFundController', () => {
  let controller: PensionFundController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PensionFundController],
      providers: [PensionFundService],
    }).compile();

    controller = module.get<PensionFundController>(PensionFundController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
