import { Module } from '@nestjs/common';
import { SociodemographicProfileService } from './sociodemographic-profile.service';
import { SociodemographicProfileController } from './sociodemographic-profile.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { SociodemographicProfileEntity, SociodemographicProfileSchema } from './entities/sociodemographic-profile.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: SociodemographicProfileEntity.name,
        schema: SociodemographicProfileSchema
      }
    ])
  ],
  controllers: [SociodemographicProfileController],
  providers: [SociodemographicProfileService]
})
export class SociodemographicProfileModule {}
