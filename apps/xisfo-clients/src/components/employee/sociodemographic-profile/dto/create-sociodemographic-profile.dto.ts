import { IsNotEmpty, IsNumber, IsString, Length } from "class-validator";

export class CreateSociodemographicProfileDto {
    @IsNotEmpty()
    @IsNumber()
    antiquity: number;

    @IsNotEmpty()
    @Length(5, 100)
    address: string;

    @IsString()
    @IsNotEmpty()
    housing_type: string;
    
    @IsString()
    @IsNotEmpty()
    strata: string;

    @IsString()
    @IsNotEmpty()
    city: string;
}
