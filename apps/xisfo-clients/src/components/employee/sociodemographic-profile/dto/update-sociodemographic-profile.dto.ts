import { PartialType } from '@nestjs/mapped-types';
import { IsMongoId } from 'class-validator';
import { CreateSociodemographicProfileDto } from './create-sociodemographic-profile.dto';

export class UpdateSociodemographicProfileDto extends PartialType(CreateSociodemographicProfileDto) {
  
  @IsMongoId()
  id: string;
}
