import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose from "mongoose";
import { CityEntity } from "../../../general/city/entities/city.entity";
import { HousingTypeEntity } from "../../housing-type/entities/housing-type.entity";
import { StrataEntity } from "../../strata/entities/strata.entity";

@Schema({ collection: 'sociodemographic_profiles', timestamps: true })
export class SociodemographicProfileEntity {

    @Prop({ required: true })
    antiquity: number

    @Prop({ required: true })
    address: string

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'StrataEntity' })
    strata: StrataEntity

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'CityEntity' })
    city: CityEntity

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'HousingTypeEntity' })
    housing_type: HousingTypeEntity

    @Prop({required: true})
    is_active: boolean

    @Prop({required: false})
    deleted_at: Date
}

export const SociodemographicProfileSchema = SchemaFactory.createForClass(SociodemographicProfileEntity)
