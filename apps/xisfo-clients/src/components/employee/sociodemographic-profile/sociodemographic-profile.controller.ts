import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { SociodemographicProfileService } from './sociodemographic-profile.service';
import { CreateSociodemographicProfileDto } from './dto/create-sociodemographic-profile.dto';
import { UpdateSociodemographicProfileDto } from './dto/update-sociodemographic-profile.dto';

@Controller()
export class SociodemographicProfileController {
  constructor(private readonly sociodemographicProfileService: SociodemographicProfileService) {}

  @MessagePattern('createSociodemographicProfile')
  create(@Payload() createSociodemographicProfileDto: CreateSociodemographicProfileDto) {
    return this.sociodemographicProfileService.create(createSociodemographicProfileDto);
  }

  @MessagePattern('findAllSociodemographicProfile')
  findAll() {
    return this.sociodemographicProfileService.findAll();
  }

  @MessagePattern('findOneSociodemographicProfile')
  findOne(@Payload() id: string) {
    return this.sociodemographicProfileService.findOne(id);
  }

  @MessagePattern('updateSociodemographicProfile')
  update(@Payload() updateSociodemographicProfileDto: UpdateSociodemographicProfileDto) {
    return this.sociodemographicProfileService.update(updateSociodemographicProfileDto.id, updateSociodemographicProfileDto);
  }

  @MessagePattern('removeSociodemographicProfile')
  remove(@Payload() id: string) {
    return this.sociodemographicProfileService.remove(id);
  }
}
