import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateSociodemographicProfileDto } from './dto/create-sociodemographic-profile.dto';
import { UpdateSociodemographicProfileDto } from './dto/update-sociodemographic-profile.dto';
import { SociodemographicProfileEntity } from './entities/sociodemographic-profile.entity';

@Injectable()
export class SociodemographicProfileService {

  constructor(
    @InjectModel(SociodemographicProfileEntity.name)
    private sociodemographicProfileModel: Model<SociodemographicProfileEntity>
  ) { }

  async create(createSociodemographicProfileDto: CreateSociodemographicProfileDto) {
    return await this.sociodemographicProfileModel.create({is_active: true, deleted_at: null, ...createSociodemographicProfileDto});
  }

  async findAll() {
    return await this.sociodemographicProfileModel
      .find()
      .populate('city')
      .populate('strata')
      .populate('housing_type')
      .exec();

  }

  async findOne(id: string) {
    try{
     return await this.sociodemographicProfileModel
          .findById(id)
          .populate('city')
          .populate('strata')
          .populate('housing_type')
          .exec();

    }catch (error){
      console.log(error)
      throw new NotFoundException();
    }

  }

  async update(id: string, updateSociodemographicProfileDto: UpdateSociodemographicProfileDto) {
    const sociodemographic = await this.sociodemographicProfileModel
      .findByIdAndUpdate(id, updateSociodemographicProfileDto)
      .setOptions({ new: true });

    if (!sociodemographic) {
      throw new NotFoundException();
    }

    return sociodemographic;
  }

  async remove(id: string) {
    const sociodemographic = await this.sociodemographicProfileModel.findByIdAndUpdate(id, {
      deleted_at: new Date,
      is_active: false
    })
    .exec();

    if (!sociodemographic) {
      throw new NotFoundException();
    }

    return sociodemographic;
  }
}
