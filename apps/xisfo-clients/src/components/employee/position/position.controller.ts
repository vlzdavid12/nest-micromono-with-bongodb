import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { PositionService } from './position.service';

@Controller()
export class PositionController {
  constructor(private readonly positionService: PositionService) {}

  @MessagePattern('findAllPosition')
  async findAll() {
    return await this.positionService.findAll();
  }
}
