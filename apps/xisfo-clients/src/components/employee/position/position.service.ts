import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PositionEntity } from './entities/position.entity';

@Injectable()
export class PositionService {

  constructor(
    @InjectModel(PositionEntity.name) private positionModel: Model<PositionEntity>
  ) { }

  async findAll() {
    return await this.positionModel.find();
  }
}
