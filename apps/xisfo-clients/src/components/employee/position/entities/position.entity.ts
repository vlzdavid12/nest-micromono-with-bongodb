import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

@Schema({ collection: 'positions' })
export class PositionEntity extends Document {
    @Prop()
    name: string
}

export const PositionSchema = SchemaFactory.createForClass(PositionEntity);
