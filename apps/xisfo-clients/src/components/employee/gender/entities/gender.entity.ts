import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

@Schema({collection: 'genders'})
export class GenderEntity extends Document{
    @Prop({required: true})
    name: string
}

export const GenderSchema = SchemaFactory.createForClass(GenderEntity);
