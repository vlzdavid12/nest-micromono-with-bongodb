import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { GenderEntity} from './entities/gender.entity';

@Injectable()
export class GenderService {

  constructor(@InjectModel(GenderEntity.name) private genderModel: Model<GenderEntity>){}

  async findAll() {
    return await this.genderModel.find();
  }
}
