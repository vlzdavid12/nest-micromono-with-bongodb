import { Module } from '@nestjs/common';
import { GenderService } from './gender.service';
import { GenderController } from './gender.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { GenderEntity, GenderSchema } from './entities/gender.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: GenderEntity.name,
        schema: GenderSchema
      }
    ])
  ],
  controllers: [GenderController],
  providers: [GenderService]
})
export class GenderModule {}
