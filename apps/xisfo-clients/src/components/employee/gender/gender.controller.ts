import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { GenderService } from './gender.service';

@Controller()
export class GenderController {
  constructor(private readonly genderService: GenderService) {}

  @MessagePattern('findAllGender')
  async findAll() {
    return await this.genderService.findAll();
  }
}
