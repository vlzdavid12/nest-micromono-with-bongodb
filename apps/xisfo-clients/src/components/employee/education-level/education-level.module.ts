import { Module } from '@nestjs/common';
import { EducationLevelService } from './education-level.service';
import { EducationLevelController } from './education-level.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { EducationLevelEntity, EducationLevelSchema} from './entities/education-level.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: EducationLevelEntity.name,
        schema: EducationLevelSchema
      }
    ])
  ],
  controllers: [EducationLevelController],
  providers: [EducationLevelService]
})
export class EducationLevelModule {}
