import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { EducationLevelService } from './education-level.service';

@Controller()
export class EducationLevelController {
  constructor(private readonly educationLevelService: EducationLevelService) {}

  @MessagePattern('findAllEducationLevel')
  async findAll() {
    return await this.educationLevelService.findAll();
  }
}
