import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

@Schema({collection: 'education_levels'})
export class EducationLevelEntity extends Document{
    @Prop()
    name: string
}

export const EducationLevelSchema = SchemaFactory.createForClass(EducationLevelEntity)
