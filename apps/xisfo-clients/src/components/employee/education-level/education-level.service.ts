import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {EducationLevelEntity} from './entities/education-level.entity';

@Injectable()
export class EducationLevelService {

  constructor(
    @InjectModel(EducationLevelEntity.name) private educationLevelModel: Model<EducationLevelEntity>
  ){}

  async findAll() {
    return await this.educationLevelModel.find();
  }
}
