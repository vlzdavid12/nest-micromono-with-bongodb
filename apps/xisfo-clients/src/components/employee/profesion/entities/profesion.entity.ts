import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

@Schema({ collection: 'profesion' })
export class ProfesionEntity extends Document {
    @Prop()
    name: string
}

export const ProfesionSchema = SchemaFactory.createForClass(ProfesionEntity);
