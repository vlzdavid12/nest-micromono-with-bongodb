import { Module } from '@nestjs/common';
import { ProfesionService } from './profesion.service';
import { ProfesionController } from './profesion.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ProfesionEntity, ProfesionSchema } from './entities/profesion.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: ProfesionEntity.name,
        schema: ProfesionSchema
      }
    ])
  ],
  controllers: [ProfesionController],
  providers: [ProfesionService]
})
export class ProfesionModule {}
