import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ProfesionEntity } from './entities/profesion.entity';

@Injectable()
export class ProfesionService {

  constructor(
    @InjectModel(ProfesionEntity.name) private profesionModel: Model<ProfesionEntity>
  ){}

  findAll() {
    return this.profesionModel.find();
  }
}
