import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ProfesionService } from './profesion.service';

@Controller()
export class ProfesionController {
  constructor(private readonly profesionService: ProfesionService) {}

  @MessagePattern('findAllProfesion')
  findAll() {
    return this.profesionService.findAll();
  }
}
