import { Module } from '@nestjs/common';
import { EmergencyContactService } from './emergency-contact.service';
import { EmergencyContactController } from './emergency-contact.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { EmergencyContact, EmergencyContactSchema } from './entities/emergency-contact.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: EmergencyContact.name,
        schema: EmergencyContactSchema
      }
    ])
  ],
  controllers: [EmergencyContactController],
  providers: [EmergencyContactService]
})
export class EmergencyContactModule { }
