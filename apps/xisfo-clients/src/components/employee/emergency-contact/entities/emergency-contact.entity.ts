import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { Document } from "mongoose";
import { EmployeeEntity } from "../../employee-resource/entities/employee-resource.entity";
import { KinshipEntity } from "../../kinship/entities/kinship.entity";

@Schema({ collection: 'emergency_contacts' })
export class EmergencyContact extends Document{

    @Prop({ required: true })
    name: string

    @Prop({ required: true })
    phone_number: number

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'KinshipEntity' })
    kinship: KinshipEntity

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'EmployeeEntity' })
    employee: EmployeeEntity
}

export const EmergencyContactSchema = SchemaFactory.createForClass(EmergencyContact);
