import { IsNotEmpty, IsNotEmptyObject } from "class-validator";
import { EmployeeEntity } from "../../employee-resource/entities/employee-resource.entity";
import { KinshipEntity } from "../../kinship/entities/kinship.entity";

export class CreateEmergencyContactDto {

    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    phone_number: number;

    @IsNotEmptyObject()
    kinship: KinshipEntity;
    
    @IsNotEmptyObject()
    employee: EmployeeEntity
}
