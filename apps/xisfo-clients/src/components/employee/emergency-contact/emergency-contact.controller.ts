import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { EmergencyContactService } from './emergency-contact.service';
import { CreateEmergencyContactDto } from './dto/create-emergency-contact.dto';
import { UpdateEmergencyContactDto } from './dto/update-emergency-contact.dto';

@Controller()
export class EmergencyContactController {
  constructor(private readonly emergencyContactService: EmergencyContactService) { }

  @MessagePattern('createEmergencyContact')
  create(@Payload() createEmergencyContactDto: CreateEmergencyContactDto) {
    return this.emergencyContactService.create(createEmergencyContactDto);
  }

  @MessagePattern('findAllEmergencyContact')
  findAll() {
    return this.emergencyContactService.findAll();
  }

  @MessagePattern('findOneEmergencyContact')
  findOne(@Payload() id: string) {
    return this.emergencyContactService.findOne(id);
  }

  @MessagePattern('updateEmergencyContact')
  update(@Payload() updateEmergencyContactDto: UpdateEmergencyContactDto) {
    return this.emergencyContactService.update(updateEmergencyContactDto.id, updateEmergencyContactDto);
  }

  @MessagePattern('removeEmergencyContact')
  remove(@Payload() id: string) {
    return this.emergencyContactService.remove(id);
  }
}
