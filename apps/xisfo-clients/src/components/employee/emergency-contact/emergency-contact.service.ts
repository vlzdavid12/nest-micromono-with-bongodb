import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateEmergencyContactDto } from './dto/create-emergency-contact.dto';
import { UpdateEmergencyContactDto } from './dto/update-emergency-contact.dto';
import { EmergencyContact } from './entities/emergency-contact.entity';

@Injectable()
export class EmergencyContactService {

  constructor(
    @InjectModel(EmergencyContact.name) private emergencyContactModel: Model<EmergencyContact>
  ) { }

  async create(createEmergencyContactDto: CreateEmergencyContactDto) {
    return await this.emergencyContactModel.create(createEmergencyContactDto);
  }

  async findAll() {
    return this.emergencyContactModel.find()
      .populate('kinship')
      .populate('employee')
      .exec();
  }

  async findOne(id: string) {
    const emergencyContact = await this.emergencyContactModel.findById(id)
      .populate('kinship')
      .populate('employee')
      .exec();

    if (!emergencyContact) {
      throw new NotFoundException();
    }

    return emergencyContact;
  }

  async update(id: string, updateEmergencyContactDto: UpdateEmergencyContactDto) {
    try {
      return await this.emergencyContactModel
        .findByIdAndUpdate(id, updateEmergencyContactDto)
        .setOptions({ new: true });
    } catch (error) {
      throw new BadRequestException();
    }
  }

  async remove(id: string) {
    try {
      return await this.emergencyContactModel.findByIdAndRemove(id)
    } catch (error) {
      throw new BadRequestException();
    }
  }
}
