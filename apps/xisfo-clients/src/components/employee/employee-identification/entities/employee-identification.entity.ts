import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { Document } from "mongoose";
import { CityEntity } from "../../../general/city/entities/city.entity";
import { DocumentTypeEntity } from "../../../general/document-type/entities/document-type.entity";
import { EmployeeEntity } from "../../employee-resource/entities/employee-resource.entity";

@Schema({ collection: 'employee-identifications', timestamps: true })
export class EmployeeIdentificationEntity extends Document {

    @Prop({ required: true })
    identification_number: string

    @Prop({ type: Date, required: true })
    expedition_date: Date

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'CityEntity' })
    expedition_city: CityEntity

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'DocumentTypeEntity' })
    document_type: DocumentTypeEntity

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'EmployeeEntity' })
    employee: EmployeeEntity

    @Prop({required: true})
    is_active: boolean

    @Prop({required: false})
    deleted_at: Date
}

export const EmployeeIdentificationSchema = SchemaFactory.createForClass(EmployeeIdentificationEntity);
