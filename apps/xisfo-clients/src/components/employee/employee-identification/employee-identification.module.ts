import { Module } from '@nestjs/common';
import { EmployeeIdentificationService } from './employee-identification.service';
import { EmployeeIdentificationController } from './employee-identification.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { EmployeeIdentificationEntity, EmployeeIdentificationSchema } from './entities/employee-identification.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: EmployeeIdentificationEntity.name,
        schema: EmployeeIdentificationSchema
      }
    ])
  ],
  controllers: [EmployeeIdentificationController],
  providers: [EmployeeIdentificationService]
})
export class EmployeeIdentificationModule { }
