import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateEmployeeIdentificationDto } from './dto/create-employee-identification.dto';
import { UpdateEmployeeIdentificationDto } from './dto/update-employee-identification.dto';
import { EmployeeIdentificationEntity } from './entities/employee-identification.entity';

@Injectable()
export class EmployeeIdentificationService {

  constructor(
    @InjectModel(EmployeeIdentificationEntity.name)
    private employeeIdentificationModel: Model<EmployeeIdentificationEntity>
  ) { }

  async create(createEmployeeIdentificationDto: CreateEmployeeIdentificationDto) {
    return await this.employeeIdentificationModel.create({is_active: true, deleted_at: null, ...createEmployeeIdentificationDto});
  }

  async findAll() {
    return await this.employeeIdentificationModel.find()
      .populate('employee')
      .populate('expedition_city')
      .populate('document_type')
      .exec();
  }

  async findOne(id: string) {
    try{
      return await this.employeeIdentificationModel.findOne({
        $or:[{_id: id}, {employee: id}]
      })
          .populate('employee')
          .populate({ path: 'expedition_city', populate: { path: 'department', model: 'DepartmentEntity' } })
          .populate('document_type')
          .exec();
    }catch (error){
      throw new NotFoundException()
    }

  }

  async findDocument(no: string) {
    try{
      const employeeIdentification = await this.employeeIdentificationModel.findOne({ identification_number: no})
          .populate('employee')
          .populate({ path: 'expedition_city', populate: { path: 'department', model: 'DepartmentEntity' } })
          .populate('document_type')
          .exec();

      if (!employeeIdentification) {
        throw new NotFoundException()
      }

      return employeeIdentification;
    }catch (error){
      console.log(error)
    }
  }


  update(id: string, updateEmployeeIdentificationDto: UpdateEmployeeIdentificationDto) {

    const employeeIdentification = this.employeeIdentificationModel
      .findByIdAndUpdate(id, updateEmployeeIdentificationDto)
      .setOptions({ new: true })

    if (!employeeIdentification) {
      throw new NotFoundException()
    }

    return employeeIdentification;
  }

  remove(id: string) {
    const employeeIdentification = this.employeeIdentificationModel
      .findByIdAndUpdate(id, {
        deleted_at: new Date,
        is_active: false
      })
      .exec();
    if (!employeeIdentification) {
      throw new NotFoundException()
    }

    return employeeIdentification;
  }
}
