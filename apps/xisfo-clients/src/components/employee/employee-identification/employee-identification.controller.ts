import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { EmployeeIdentificationService } from './employee-identification.service';
import { CreateEmployeeIdentificationDto } from './dto/create-employee-identification.dto';
import { UpdateEmployeeIdentificationDto } from './dto/update-employee-identification.dto';

@Controller()
export class EmployeeIdentificationController {
  constructor(private readonly employeeIdentificationService: EmployeeIdentificationService) { }

  @MessagePattern('createEmployeeIdentification')
  create(@Payload() createEmployeeIdentificationDto: CreateEmployeeIdentificationDto) {
    return this.employeeIdentificationService.create(createEmployeeIdentificationDto);
  }

  @MessagePattern('findAllEmployeeIdentification')
  findAll() {
    return this.employeeIdentificationService.findAll();
  }

  @MessagePattern('findOneEmployeeIdentification')
  findOne(@Payload() id: string) {
    return this.employeeIdentificationService.findOne(id);
  }


  @MessagePattern('updateEmployeeIdentification')
  update(@Payload() updateEmployeeIdentificationDto: UpdateEmployeeIdentificationDto) {
    return this.employeeIdentificationService.update(updateEmployeeIdentificationDto.id, updateEmployeeIdentificationDto);
  }

  @MessagePattern('removeEmployeeIdentification')
  remove(@Payload() id: string) {
    return this.employeeIdentificationService.remove(id);
  }

  @MessagePattern('documentValidate')
  findOneDocument(@Payload() no: string){
    return this.employeeIdentificationService.findDocument(no);
  }
}
