import { IsNotEmpty } from "class-validator"

export class CreateEmployeeIdentificationDto {

    @IsNotEmpty()
    identification_number: string

    @IsNotEmpty()
    expedition_date: Date

    @IsNotEmpty()
    expedition_city: string

    @IsNotEmpty()
    document_type: string

    @IsNotEmpty()
    employee: string
}
