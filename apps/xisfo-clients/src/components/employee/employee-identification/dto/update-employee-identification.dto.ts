import { PartialType } from '@nestjs/mapped-types';
import { IsMongoId } from 'class-validator';
import { CreateEmployeeIdentificationDto } from './create-employee-identification.dto';

export class UpdateEmployeeIdentificationDto extends PartialType(CreateEmployeeIdentificationDto) {

  @IsMongoId()
  id: string;
}
