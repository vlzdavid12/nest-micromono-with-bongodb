import { Module } from '@nestjs/common';
import { OccupationalRiskManagerService } from './occupational-risk-manager.service';
import { OccupationalRiskManagerController } from './occupational-risk-manager.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { OccupationalRiskManagerEntity, OccupationalRiskManagerSchema } from './entities/occupational-risk-manager.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: OccupationalRiskManagerEntity.name,
        schema: OccupationalRiskManagerSchema
      }
    ])
  ],
  controllers: [OccupationalRiskManagerController],
  providers: [OccupationalRiskManagerService]
})
export class OccupationalRiskManagerModule {}
