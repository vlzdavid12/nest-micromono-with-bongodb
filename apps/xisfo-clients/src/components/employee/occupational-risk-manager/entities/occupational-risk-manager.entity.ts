import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

@Schema({ collection: 'occupational_risk-managers' })
export class OccupationalRiskManagerEntity extends Document {
    @Prop()
    name: string
}

export const OccupationalRiskManagerSchema = SchemaFactory.createForClass(OccupationalRiskManagerEntity);
