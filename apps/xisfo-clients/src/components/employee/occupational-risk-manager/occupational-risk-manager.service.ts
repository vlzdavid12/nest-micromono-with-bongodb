import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { OccupationalRiskManagerEntity } from './entities/occupational-risk-manager.entity';

@Injectable()
export class OccupationalRiskManagerService {

  constructor(
    @InjectModel(OccupationalRiskManagerEntity.name) private occupationalRiskManagerModel: Model<OccupationalRiskManagerEntity>
  ){}

  async findAll() {
    return await this.occupationalRiskManagerModel.find();
  }
}
