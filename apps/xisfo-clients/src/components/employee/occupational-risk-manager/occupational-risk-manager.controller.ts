import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { OccupationalRiskManagerService } from './occupational-risk-manager.service';
@Controller()
export class OccupationalRiskManagerController {
  constructor(private readonly occupationalRiskManagerService: OccupationalRiskManagerService) {}

  @MessagePattern('findAllOccupationalRiskManager')
  async findAll() {
    return await this.occupationalRiskManagerService.findAll();
  }
}
