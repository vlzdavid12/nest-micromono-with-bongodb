import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { KinshipService } from './kinship.service';

@Controller()
export class KinshipController {
  constructor(private readonly kinshipService: KinshipService) {}


  @MessagePattern('findAllKinship')
  findAll() {
    return this.kinshipService.findAll();
  }
  
}
