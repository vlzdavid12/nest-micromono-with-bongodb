import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';
@Schema({collection: 'kinships'})
export class KinshipEntity extends Document{
    @Prop({required: true})
    name: string
}

export const KinshipSchema = SchemaFactory.createForClass(KinshipEntity);
