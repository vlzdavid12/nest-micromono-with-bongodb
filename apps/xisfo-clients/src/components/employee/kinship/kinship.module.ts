import { Module } from '@nestjs/common';
import { KinshipService } from './kinship.service';
import { KinshipController } from './kinship.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { KinshipEntity, KinshipSchema } from './entities/kinship.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: KinshipEntity.name,
        schema: KinshipSchema
      }
    ])
  ],
  controllers: [KinshipController],
  providers: [KinshipService]
})
export class KinshipModule { }
