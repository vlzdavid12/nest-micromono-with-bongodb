import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { KinshipEntity } from './entities/kinship.entity';
@Injectable()
export class KinshipService {

  constructor(
    @InjectModel(KinshipEntity.name) private kinshipModel: Model<KinshipEntity>
  ){}

  findAll() {
    return this.kinshipModel.find();
  }
}
