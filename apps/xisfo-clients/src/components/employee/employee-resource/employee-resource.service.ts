import {Injectable, NotFoundException} from '@nestjs/common';
import {CreateEmployeeResourceDto} from './dto/create-employee-resource.dto';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {EmployeeEntity} from "./entities/employee-resource.entity";
import {UpdateEmployeeResourceDto} from './dto/update-employee-resource.dto';
import {PaginationDto} from "../../../commons/paginationDto";
import {EmployeeIdentificationEntity} from "../employee-identification/entities/employee-identification.entity";
import {CreateEmployeeIdentificationDto} from "../employee-identification/dto/create-employee-identification.dto";
import {PaginationDtoFilter} from "../../../commons/paginationDto.filter";
import {PaginationDtoAdvancedEmployeesFilter} from "../../../commons/filter/paginationDtoAdvancedEmployees.filter";
import {CivilStatusEntity} from "../civil-status/entities/civil-status.entity";
import {ContractTypeEntity} from "../contract-type/entities/contract-type.entity";
import {GenderEntity} from "../gender/entities/gender.entity";
import {PositionEntity} from "../position/entities/position.entity";
import {OccupationalRiskManagerEntity} from "../occupational-risk-manager/entities/occupational-risk-manager.entity";
import {HealthProviderEntity} from "../health-provider/entities/health-provider.entity";
import {HousingTypeEntity} from "../housing-type/entities/housing-type.entity";
import {PensionFundEntity} from "../pension-fund/entities/pension-fund.entity";
import {EducationLevelEntity} from "../education-level/entities/education-level.entity";
import mongoose from "mongoose";

@Injectable()
export class EmployeeResourceService {
    constructor(@InjectModel(EmployeeEntity.name)
                private employeeModule: Model<EmployeeEntity>,
                @InjectModel(EmployeeIdentificationEntity.name)
                private employeeIdentificationModule: Model<EmployeeIdentificationEntity>,
                @InjectModel(CivilStatusEntity.name)
                private civilStatusModule: Model<CivilStatusEntity>,
                @InjectModel(ContractTypeEntity.name)
                private contractTypeModule: Model<ContractTypeEntity>,
                @InjectModel(GenderEntity.name)
                private genderTypeModule: Model<GenderEntity>,
                @InjectModel(PositionEntity.name)
                private positionModule: Model<PositionEntity>,
                @InjectModel(OccupationalRiskManagerEntity.name)
                private occupationalRiskManagerModule: Model<OccupationalRiskManagerEntity>,
                @InjectModel(HealthProviderEntity.name)
                private healthProviderModule: Model<HealthProviderEntity>,
                @InjectModel(HousingTypeEntity.name)
                private hosingTypeModule: Model<HousingTypeEntity>,
                @InjectModel(PensionFundEntity.name)
                private pensionFundModule: Model<PensionFundEntity>,
                @InjectModel(EducationLevelEntity.name)
                private educationLevelModule: Model<EducationLevelEntity>,
    ) {
    }

    async create(createEmployeeDto: CreateEmployeeResourceDto) {
        let phoneExist = await this.employeeModule.findOne({phone: createEmployeeDto.phone});

        if (phoneExist) {
            return {msg: "El telefóno se encuentra ocupado, comunicate con el administrador"}
        }
        
        return this.employeeModule.create({is_active: true, deleted_at: null, ...createEmployeeDto}).then(async (resp) => {
            let employeeDocument: CreateEmployeeIdentificationDto = {
                identification_number: createEmployeeDto.identification_number,
                expedition_date: new Date(createEmployeeDto.expedition_date),
                expedition_city: createEmployeeDto.expedition_city,
                document_type: createEmployeeDto.document_type_id,
                employee: resp.id
            }
            let result = await this.employeeIdentificationModule.create({is_active: true, deleted_at: null, ...employeeDocument});
            if (result) {
                return resp;
            }
        }).catch(error => console.log(error));
    }


    async findAll(paginationDto: PaginationDto) {
        const {limit = 12, offset = 1, orderBy} = paginationDto;
        const order: any = (orderBy === 'ASC') ? {updatedAt: 1} : {updatedAt: -1};
        try {
            const count = await this.employeeModule.count();
            const data = await this.employeeModule.find()
                .skip((limit * offset) - limit)
                .limit(limit)
                .sort(order)
                .populate('civil_status_id')
                .populate('contract_type_id')
                .populate('gender_id')
                .populate('position_id')
                .populate('headquarter_id')
                .populate('health_provider_id')
                .populate('occupational_risk_manager_id')
                .populate([
                    {
                        path: 'sociodemographic_profile_id',
                        populate: {path: 'strata', model: 'StrataEntity'}
                    },
                    {
                        path: 'sociodemographic_profile_id',
                        populate: {
                            path: 'city',
                            model: 'CityEntity',
                            populate: {path: 'department', model: 'DepartmentEntity'}
                        }
                    },
                    {
                        path: 'sociodemographic_profile_id',
                        populate: {path: 'housing_type', model: 'HousingTypeEntity'}
                    },
                ])
                .populate('pension_fund_id')
                .populate('education_level_id')
                .exec()
            return {data, count, limit, offset, orderBy};
        } catch (error) {
            console.log(error)
        }

    }

    async findOne(id: string) {

        const employee = await this.employeeModule.findById(id)
            .populate('civil_status_id')
            .populate('contract_type_id')
            .populate('gender_id')
            .populate('position_id')
            .populate('headquarter_id')
            .populate('health_provider_id')
            .populate('occupational_risk_manager_id')
            .populate([
                {
                    path: 'sociodemographic_profile_id',
                    populate: {path: 'strata', model: 'StrataEntity'}
                },
                {
                    path: 'sociodemographic_profile_id',
                    populate: {path: 'city', model: 'CityEntity'}
                },
                {
                    path: 'sociodemographic_profile_id',
                    populate: {path: 'housing_type', model: 'HousingTypeEntity'}
                },
            ])
            .populate('pension_fund_id')
            .populate('education_level_id')
            .populate('profesions')
            .exec();
        if (!employee) {
            throw new NotFoundException();
        }
        return employee;
    }

    async update(id: string, updateEmployeeResourceDto: UpdateEmployeeResourceDto) {
        const employeeUpdated = await this.employeeModule
            .findByIdAndUpdate(id, updateEmployeeResourceDto)
            .setOptions({new: true})

        if (!employeeUpdated) {
            throw new NotFoundException();
        }

        return employeeUpdated;

    }

    async remove(id: string) {
        const employee = await this.employeeModule.findByIdAndUpdate(id, {
            deleted_at: new Date,
            is_active: false
          })
          .exec();

        if (!employee) {
            throw new NotFoundException();
        }

        return employee;
    }

    async filterEmployee(employeeFilter: PaginationDtoFilter) {
        let {search, limit = 10, offset = 1, orderBy} = employeeFilter;
        const order: any = (orderBy === 'ASC') ? {updatedAt: 1} : {updatedAt: -1};
        try {
            const data = await this.employeeModule.find({
                $or: [{
                    first_name: search?.split(' ')[0],
                    second_name: search?.split(' ')[1],
                    last_name: search?.split(' ')[2],
                    second_last_name: search?.split(' ')[3],
                }, {
                    first_name: search?.split(' ')[0],
                    second_name: search?.split(' ')[1],
                }, {
                    last_name: search?.split(' ')[0],
                    second_last_name: search?.split(' ')[1],
                },
                    {
                        first_name: search,

                    }, {
                        second_name: search,
                    }, {
                        last_name: search || search?.split(' ')[2],
                    }, {
                        second_last_name: search
                    }, {
                        email: search
                    }, {
                        phone: search
                    }]
            })
                .skip((limit * offset) - limit)
                .limit(limit)
                .sort(order)
                .populate('civil_status_id')
                .populate('contract_type_id')
                .populate('gender_id')
                .populate('position_id')
                .populate('headquarter_id')
                .populate('health_provider_id')
                .populate('occupational_risk_manager_id')
                .populate([
                    {
                        path: 'sociodemographic_profile_id',
                        populate: {path: 'strata', model: 'StrataEntity'}
                    },
                    {
                        path: 'sociodemographic_profile_id',
                        populate: {path: 'city', model: 'CityEntity'}
                    },
                    {
                        path: 'sociodemographic_profile_id',
                        populate: {path: 'housing_type', model: 'HousingTypeEntity'}
                    },
                ])
                .populate('pension_fund_id')
                .populate('education_level_id')
                .populate('profesions')
                .exec();

            return {data, limit, offset, orderBy};
        } catch (error) {
            throw new NotFoundException();
        }
    }

    async filterEmployeeAdvanced(paginationDtoAdvancedFilter: PaginationDtoAdvancedEmployeesFilter) {

        const {limit, offset, orderBy} = paginationDtoAdvancedFilter;

        let id_civil_status, id_contract_type, id_gender_type, id_position_type, id_risk_manager, id_health_provider,
            id_housingType, id_pensionFunc, id_education_level;

        /*Civil Status*/
        let civilStatusID = mongoose.Types.ObjectId.isValid(paginationDtoAdvancedFilter.civil_status_id);
        let objectCivilStatus = civilStatusID? { _id: paginationDtoAdvancedFilter.civil_status_id} :  { name: paginationDtoAdvancedFilter.civil_status_id}
        const civil_status =  this.civilStatusModule.find(objectCivilStatus).select('_id').exec();


        /*Contract Type*/
        let contractTypeID = mongoose.Types.ObjectId.isValid(paginationDtoAdvancedFilter.contract_type_id);
        let objectContractType = contractTypeID? { _id: paginationDtoAdvancedFilter.contract_type_id} :  { name: paginationDtoAdvancedFilter.contract_type_id}
        const contract_type = this.contractTypeModule.find(objectContractType).select('_id').exec();

        /*Gender Type*/
        let genderTypeID = mongoose.Types.ObjectId.isValid(paginationDtoAdvancedFilter.gender_id);
        let objectGenderType = genderTypeID? { _id: paginationDtoAdvancedFilter.gender_id} :  { name: paginationDtoAdvancedFilter.gender_id}
        const gender_type = this.genderTypeModule.find(objectGenderType).select('_id').exec();

        /*Position*/
        let positionID = mongoose.Types.ObjectId.isValid(paginationDtoAdvancedFilter.position_id);
        let objectPositionType = positionID? { _id: paginationDtoAdvancedFilter.position_id} :  { name: paginationDtoAdvancedFilter.position_id}
        const position = this.positionModule.find(objectPositionType).select('_id').exec();


        /*Health*/
        let healthID = mongoose.Types.ObjectId.isValid(paginationDtoAdvancedFilter.health_provider_id);
        let objectHealthType = healthID? { _id: paginationDtoAdvancedFilter.health_provider_id} :  { name: paginationDtoAdvancedFilter.health_provider_id}
        const healthProvider = this.healthProviderModule.find(objectHealthType).select('_id').exec();

        /*Occupation*/
        let occupationalID = mongoose.Types.ObjectId.isValid(paginationDtoAdvancedFilter.occupational_risk_manager_id);
        let objectOccupational = occupationalID? { _id: paginationDtoAdvancedFilter.occupational_risk_manager_id} :  { name: paginationDtoAdvancedFilter.occupational_risk_manager_id}
        const occupationRisk = this.occupationalRiskManagerModule.find(objectOccupational).select('_id').exec();

        /*Housing Type*/
        let housingTypeID = mongoose.Types.ObjectId.isValid(paginationDtoAdvancedFilter.housing_type);
        let objectHousingType = housingTypeID? { _id: paginationDtoAdvancedFilter.housing_type} :  { name: paginationDtoAdvancedFilter.housing_type}
        const housingType = this.hosingTypeModule.find(objectHousingType).select('_id').exec();

        /*PensionFunc*/
        let pensionFuncID = mongoose.Types.ObjectId.isValid(paginationDtoAdvancedFilter.pension_fund_id);
        let objectPensionFunc = pensionFuncID? { _id: paginationDtoAdvancedFilter.pension_fund_id} :  { name: paginationDtoAdvancedFilter.pension_fund_id}
        const pensionFunc = this.pensionFundModule.find(objectPensionFunc).select('_id').exec();

        /*Education Level*/
        let educationLevelID = mongoose.Types.ObjectId.isValid(paginationDtoAdvancedFilter.education_level_id);
        let objectEducationLevel = educationLevelID? { _id: paginationDtoAdvancedFilter.education_level_id} :  { name: paginationDtoAdvancedFilter.education_level_id}
        const educationLevel = this.educationLevelModule.find(objectEducationLevel).select('_id').exec();


        const [civil_status_resp, contract_type_resp, gender_type_resp, position_resp, occupationRisk_resp, healthProvider_resp, housingType_resp, pensionFunc_resp, educationLevel_resp] = await Promise.all([civil_status, contract_type, gender_type, position, occupationRisk, healthProvider, housingType, pensionFunc, educationLevel]);

        civil_status_resp.forEach(item => id_civil_status = item._id);
        contract_type_resp.forEach(item => id_contract_type = item._id);
        gender_type_resp.forEach(item => id_gender_type = item._id);
        position_resp.forEach(item => id_position_type = item._id);
        occupationRisk_resp.forEach(item => id_risk_manager = item._id);
        healthProvider_resp.forEach(item => id_health_provider = item._id);
        housingType_resp.forEach(item => id_housingType = item._id);
        pensionFunc_resp.forEach(item => id_pensionFunc = item._id);
        educationLevel_resp.forEach(item => id_education_level = item.id);


        const data = await this.employeeModule.find({
            $or: [{
                civil_status_id: id_civil_status
            }, {
                contract_type_id: id_contract_type
            }, {
                gender_id: id_gender_type,
            }, {
                position_id: id_position_type
            }, {
                health_provider_id: id_health_provider
            }, {
                occupational_risk_manager_id: id_risk_manager
            }, {
                pension_fund_id: id_pensionFunc
            }, {
                education_level_id: id_education_level
            }]
        })

        const count = data.length;
        return {
            data,
            count,
            limit,
            offset,
            orderBy
        }
    }


}
