import {Controller} from '@nestjs/common';
import {EventPattern, MessagePattern, Payload} from '@nestjs/microservices';
import { EmployeeResourceService } from './employee-resource.service';
import { CreateEmployeeResourceDto } from './dto/create-employee-resource.dto';
import { UpdateEmployeeResourceDto } from './dto/update-employee-resource.dto';
import {PaginationDto} from "../../../commons/paginationDto";
import {PaginationDtoFilter} from "../../../commons/paginationDto.filter";
import {PaginationDtoAdvancedEmployeesFilter} from "../../../commons/filter/paginationDtoAdvancedEmployees.filter";


@Controller()
export class EmployeeResourceController {
  constructor(private readonly employeeService: EmployeeResourceService) {}

  @EventPattern('createEmployee')
  create(@Payload() createEmployeeDto: CreateEmployeeResourceDto) {
    return this.employeeService.create(createEmployeeDto);
  }

  @MessagePattern('findAllEmployee')
  findAll(@Payload() paginationDto: PaginationDto) {
    return this.employeeService.findAll(paginationDto);
  }

  @MessagePattern('findOneEmployee')
  findOne(@Payload() id: string) {
    return this.employeeService.findOne(id);
  }

  @MessagePattern('updateEmployee')
  update(@Payload() updateEmployeeResourceDto: UpdateEmployeeResourceDto) {
    return this.employeeService.update(updateEmployeeResourceDto.id, updateEmployeeResourceDto);
  }

  @MessagePattern('removeEmployee')
  remove(@Payload() id: string) {
    return this.employeeService.remove(id);
  }

  //Filter Search
  @MessagePattern('filterEmployee')
  filterEmployee(@Payload() employeeFilter: PaginationDtoFilter){
    return this.employeeService.filterEmployee(employeeFilter);
  }

  //Filter Search Advanced
  @MessagePattern('filterAdvancedEmployee')
  filterAdvancedClient(@Payload() paginationDtoAdvancedFilter: PaginationDtoAdvancedEmployeesFilter){
    return this.employeeService.filterEmployeeAdvanced(paginationDtoAdvancedFilter);
  }
}
