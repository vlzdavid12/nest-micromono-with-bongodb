import {IsArray, IsEmail, IsNotEmpty, IsObject, IsOptional, IsString} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateEmployeeResourceDto {

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    first_name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    second_name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    last_name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    second_last_name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    birthday: Date;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    phone: string

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    childrens: string

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    @IsEmail()
    email: string

    @ApiProperty()
    @IsArray()
    @IsOptional()
    profesions: string[];

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    civil_status_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    contract_type_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    gender_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    position_id: string;

    @ApiProperty()
    @IsString()
    headquarter_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    health_provider_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    occupational_risk_manager_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    sociodemographic_profile_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    pension_fund_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    education_level_id:string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    identification_number: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    expedition_date: Date;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    expedition_city: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    document_type_id: string;
}

