import { PartialType } from '@nestjs/mapped-types';
import { IsMongoId } from 'class-validator';
import { CreateEmployeeResourceDto } from './create-employee-resource.dto';

export class UpdateEmployeeResourceDto extends PartialType(CreateEmployeeResourceDto) {
    @IsMongoId()
    id: string
}
