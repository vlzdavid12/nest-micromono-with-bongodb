import {Module} from '@nestjs/common';
import {EmployeeResourceService} from './employee-resource.service';
import {EmployeeResourceController} from './employee-resource.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {EmployeeEntity, EmployeeSchema} from "./entities/employee-resource.entity";
import {ProfesionEntity, ProfesionSchema} from '../profesion/entities/profesion.entity';
import {
    EmployeeIdentificationEntity,
    EmployeeIdentificationSchema
} from "../employee-identification/entities/employee-identification.entity";
import {CivilStatusEntity, CivilStatusSchema} from "../civil-status/entities/civil-status.entity";
import {ContractTypeEntity, ContractTypeSchema} from "../contract-type/entities/contract-type.entity";
import {GenderEntity, GenderSchema} from "../gender/entities/gender.entity";
import {PositionEntity, PositionSchema} from "../position/entities/position.entity";
import {
    OccupationalRiskManagerEntity,
    OccupationalRiskManagerSchema
} from "../occupational-risk-manager/entities/occupational-risk-manager.entity";
import {HealthProviderEntity, HealthProviderSchema} from "../health-provider/entities/health-provider.entity";
import {HousingTypeEntity, HousingTypeSchema} from "../housing-type/entities/housing-type.entity";
import {PensionFundEntity, PensionFundSchema} from "../pension-fund/entities/pension-fund.entity";
import {EducationLevelEntity, EducationLevelSchema} from "../education-level/entities/education-level.entity";

@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: EmployeeEntity.name,
                schema: EmployeeSchema
            },
            {
                name: ProfesionEntity.name,
                schema: ProfesionSchema
            },
            {
                name: EmployeeIdentificationEntity.name,
                schema: EmployeeIdentificationSchema
            },
            {
                name: CivilStatusEntity.name,
                schema: CivilStatusSchema
            },
            {
                name: ContractTypeEntity.name,
                schema: ContractTypeSchema
            },
            {
                name: GenderEntity.name,
                schema: GenderSchema
            },
            {
                name: PositionEntity.name,
                schema: PositionSchema
            },
            {
                name: OccupationalRiskManagerEntity.name,
                schema: OccupationalRiskManagerSchema
            },
            {
                name: HealthProviderEntity.name,
                schema: HealthProviderSchema
            },
            {
                name: HousingTypeEntity.name,
                schema: HousingTypeSchema
            },
            {
                name: PensionFundEntity.name,
                schema: PensionFundSchema
            },
            {
                name: EducationLevelEntity.name,
                schema: EducationLevelSchema
            },
        ])
    ],
    controllers: [EmployeeResourceController],
    providers: [EmployeeResourceService]
})
export class EmployeeResourceModule {
}
