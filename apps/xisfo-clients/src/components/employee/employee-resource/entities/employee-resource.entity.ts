import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, {Document} from "mongoose";
import { CivilStatusEntity } from "../../civil-status/entities/civil-status.entity";
import { ContractTypeEntity } from "../../contract-type/entities/contract-type.entity";
import { EducationLevelEntity} from "../../education-level/entities/education-level.entity";
import { GenderEntity } from "../../gender/entities/gender.entity";
import { HeadquarterEntity } from "../../headquarter/entities/headquarter.entity";
import { HealthProviderEntity} from "../../health-provider/entities/health-provider.entity";
import { OccupationalRiskManagerEntity } from "../../occupational-risk-manager/entities/occupational-risk-manager.entity";
import { PensionFundEntity } from "../../pension-fund/entities/pension-fund.entity";
import { PositionEntity } from "../../position/entities/position.entity";
import { ProfesionEntity } from "../../profesion/entities/profesion.entity";
import { SociodemographicProfileEntity } from "../../sociodemographic-profile/entities/sociodemographic-profile.entity";

@Schema({ collection: 'employees', timestamps: true })
export class EmployeeEntity extends Document {

    @Prop({ required: true })
    first_name: string;

    @Prop({ required: true })
    second_name: string;

    @Prop({ required: true })
    last_name: string;

    @Prop({ required: true })
    second_last_name: string;

    @Prop({ required: true })
    email: string;

    @Prop({ required: true })
    phone: string;

    @Prop({ required: true, type: Date })
    birthday: Date;

    @Prop({ required: true })
    childrens: string

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'CivilStatusEntity', required: true })
    civil_status_id: CivilStatusEntity;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'ContractTypeEntity', required: true })
    contract_type_id: ContractTypeEntity;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'GenderEntity', required: true })
    gender_id: GenderEntity;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'PositionEntity', required: true })
    position_id: PositionEntity;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'HeadquarterEntity', required: true })
    headquarter_id: HeadquarterEntity;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'HealthProviderEntity', required: true })
    health_provider_id: HealthProviderEntity;


    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'OccupationalRiskManagerEntity', required: true })
    occupational_risk_manager_id: OccupationalRiskManagerEntity;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'SociodemographicProfileEntity', required: true })
    sociodemographic_profile_id: SociodemographicProfileEntity;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'PensionFundEntity', required: true })
    pension_fund_id: PensionFundEntity;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'EducationLevelEntity', required: true })
    education_level_id: EducationLevelEntity;

    @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ProfesionEntity' }] })
    profesions: ProfesionEntity[];

    @Prop({required: true})
    is_active: boolean;
    
    @Prop({required: false})
    deleted_at: Date;
}


export const EmployeeSchema = SchemaFactory.createForClass(EmployeeEntity);