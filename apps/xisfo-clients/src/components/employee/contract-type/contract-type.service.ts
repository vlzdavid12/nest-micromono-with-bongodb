import {Injectable, NotFoundException} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {ContractTypeEntity} from './entities/contract-type.entity';
import {PaginationDto} from "../../../commons/paginationDto";
@Injectable()
export class ContractTypeService {

  constructor(
    @InjectModel(ContractTypeEntity.name) private contractTypeModule: Model<ContractTypeEntity>
  ){}

  async findAll(paginationDto: PaginationDto) {
    const {limit = 12, offset = 1, orderBy} = paginationDto;
    const order: any = (orderBy === 'ASC') ? {updatedAt: 1} : {updatedAt: -1};
    try {
      const count = await this.contractTypeModule.count();
      const data = await this.contractTypeModule.find()
          .skip((limit * offset) - limit)
          .limit(limit)
          .sort(order);
      return {data, limit, offset, count, orderBy}
    } catch (error){
      throw new NotFoundException();
    }
  }
}
