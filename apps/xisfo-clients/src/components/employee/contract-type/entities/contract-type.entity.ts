import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ collection: 'contract_types'})
export class ContractTypeEntity extends Document{

  @Prop({required: true})
  name: string;
}

export const ContractTypeSchema = SchemaFactory.createForClass(ContractTypeEntity);
