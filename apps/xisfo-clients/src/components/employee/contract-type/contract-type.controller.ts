import { Controller } from '@nestjs/common';
import {MessagePattern, Payload} from '@nestjs/microservices';
import { ContractTypeService } from './contract-type.service';
import {PaginationDto} from "../../../commons/paginationDto";

@Controller()
export class ContractTypeController {
  constructor(private readonly contractTypeService: ContractTypeService) {}

  @MessagePattern('findAllContractType')
  findAll(@Payload() paginationDto: PaginationDto) {
    return this.contractTypeService.findAll(paginationDto);
  }
}
