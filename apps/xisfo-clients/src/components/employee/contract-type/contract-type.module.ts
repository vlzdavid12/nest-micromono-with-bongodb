import { Module } from '@nestjs/common';
import { ContractTypeService } from './contract-type.service';
import { ContractTypeController } from './contract-type.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ContractTypeEntity, ContractTypeSchema } from './entities/contract-type.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: ContractTypeEntity.name,
        schema: ContractTypeSchema
      }
    ])
  ],
  controllers: [ContractTypeController],
  providers: [ContractTypeService]
})
export class ContractTypeModule { }
