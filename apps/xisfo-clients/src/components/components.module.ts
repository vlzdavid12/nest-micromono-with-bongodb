import { Module } from '@nestjs/common';
import { GeneralModule } from './general/general.module';
import { SeedModule } from './seed/seed.module';
import { ClientModule } from './client/client.module';
import {EmployeeModule} from "./employee/employee.module";

@Module({
    imports: [
        GeneralModule,
        SeedModule,
        ClientModule,
        EmployeeModule,
    ],
    exports: [
        GeneralModule,
        SeedModule,
        ClientModule,
        EmployeeModule,
    ],
    providers: []
})
export class ComponentsModule { }
