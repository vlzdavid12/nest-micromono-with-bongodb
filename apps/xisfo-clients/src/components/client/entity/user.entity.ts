import {Prop, Schema, SchemaFactory} from "@nestjs/mongoose";
import {Document} from "mongoose";

@Schema({collection: 'users'})
export class UserEntity extends Document {

    @Prop({required: true})
    first_name: string;

    @Prop({required: true})
    second_name: string;

    @Prop({required: true})
    last_name: string;

    @Prop({required: true})
    second_last_name: string

    @Prop({required: true})
    birthday: Date;

    @Prop({required: true})
    password: string

    @Prop({
        required: true,
        unique: true
    })
    email: string

    @Prop({
        required: true,
        unique: true
    })
    phone: string

    @Prop({required: true})
    childrens: number


    @Prop({required: true, default: ['user']})
    roles: string[]

    @Prop()
    isActive: boolean;


}

export const UserSchema = SchemaFactory.createForClass(UserEntity);
