import { Module } from '@nestjs/common';
import { ClientTypeModule } from './client-type/client-type.module';
import { ClientModule as Client } from './client/client.module';
import { RegistrationStatusModule } from './registration-status/registration-status.module';
import { ClientIdentificationModule } from './client-identification/client-identification.module';
import { BankAccountModule } from './bank_account/bank_account.module';
import { ClientCompanyModule } from './client-company/client-company.module';

@Module({
    imports: [ClientTypeModule, Client, RegistrationStatusModule, ClientIdentificationModule, BankAccountModule, ClientCompanyModule],
    exports: [ClientTypeModule, Client, RegistrationStatusModule, ClientIdentificationModule, BankAccountModule],
})
export class ClientModule {}
