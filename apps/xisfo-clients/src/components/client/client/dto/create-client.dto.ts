import {
    IsBoolean,
    IsDecimal,
    IsEmail,
    IsNotEmpty,
    IsNumberString,
    IsObject,
    IsOptional,
    IsString,
    MinLength
} from "class-validator";

export class CreateClientDto {
   
    @IsString()
    @MinLength(3)
    @IsNotEmpty()
    first_name: string;


    @IsString()
    @MinLength(3)
    @IsOptional()
    second_name: string;


    @IsString()
    @MinLength(3)
    @IsNotEmpty()
    last_name: string;


    @IsString()
    @MinLength(3)
    @IsOptional()
    second_last_name: string;

    @IsEmail()
    @IsNotEmpty()
    email: string;

    @IsNumberString()
    @IsNotEmpty()
    phone: string;


    @IsString()
    @IsNotEmpty()
    address: string;

    @IsString()
    @IsNotEmpty()
    country: string;

    @IsNotEmpty()
    client_type: string;

    @IsNotEmpty()
    registration_status: string;

    @IsBoolean()
    @IsNotEmpty()
    with_holding_tax: boolean;

    @IsDecimal()
    client_rate: string;

    @IsBoolean()
    @IsNotEmpty()
    old_client: boolean;

    @IsString()
    @IsOptional()
    client_company_id: string;

    @IsBoolean()
    @IsNotEmpty()
    data_policy: boolean;
}
