import {Module} from '@nestjs/common';
import {ClientService} from './client.service';
import {ClientController} from './client.controller';
import {MongooseModule} from '@nestjs/mongoose';
import {ClientEntity, ClientSchema} from './entities/client.entity';
import {WalletXisfoModules} from '../../microservices/wallet';
import {
    RegistrationStatusEntity,
    RegistrationStatusSchema
} from '../registration-status/entities/registration-status.entity';
import {
    ClientIdentificationEntity,
    ClientIdentificationSchema
} from "../client-identification/entities/client-identification.entity";
import { BankAccountEntity, BankAccountEntitySchema } from '../bank_account/entities/bank_account.entity';
import { ClientCompanyEntity, ClientCompanySchema } from '../client-company/entities/client-company.entity';
import { ClientTypeEntity, ClientTypeSchema } from '../client-type/entities/client-type.entity';

@Module({
  imports: [
    WalletXisfoModules,
    MongooseModule.forFeature([
      {
        name: ClientEntity.name,
        schema: ClientSchema,
        
      },
      {
        name: RegistrationStatusEntity.name,
        schema: RegistrationStatusSchema
      },
      {
        name: ClientIdentificationEntity.name,
        schema: ClientIdentificationSchema,
      },
      {
        name: BankAccountEntity.name,
        schema: BankAccountEntitySchema,
      },
      {
        name: ClientCompanyEntity.name,
        schema: ClientCompanySchema,
      },
      {
        name: ClientTypeEntity.name,
        schema: ClientTypeSchema,
      }
    ]),
  ],
  controllers: [ClientController],
  providers: [ClientService]
})
export class ClientModule {
}
