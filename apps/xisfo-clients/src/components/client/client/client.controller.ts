import {Controller, UseInterceptors} from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ClientService } from './client.service';
import { CreateClientDto } from './dto/create-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';
import {CreateClientIdentificationDto} from "../client-identification/dto/create-client-identification.dto";
import {PaginationDto} from "../../../commons/paginationDto";
import {PaginationDtoFilter} from "../../../commons/paginationDto.filter";
import {PaginationDtoAdvancedClientsFilter} from "../../../commons/filter/paginationDtoAdvancedClients.filter";


@Controller()
export class ClientController {
  constructor(private readonly clientService: ClientService) {}

  @MessagePattern('createClient')
  create(@Payload() createClientDto: {clientDB: CreateClientDto, documentDB: CreateClientIdentificationDto}) {
    return this.clientService.create(createClientDto.clientDB, createClientDto.documentDB);
  }

  @MessagePattern('findAllClient')
  findAll(@Payload() paginationDto: PaginationDto) {
    return this.clientService.findAll(paginationDto);
  }

  @MessagePattern('findAllForeingCurrencyClients')
  findAllForeingCurrencyClients(@Payload() paginationDto: PaginationDto) {
    return this.clientService.findAllForeingCurrencyClients(paginationDto);
  }

  @MessagePattern('findOneClient')
  findOne(@Payload() id: string) {
    return this.clientService.findOne(id);
  }

  @MessagePattern('findClientBankAccount')
  findClientBankAccount(@Payload() id: string) {
    return this.clientService.findClientBankAccount(id);
  }

  @MessagePattern('updateClient')
  update(@Payload() updateClientDto: UpdateClientDto) {
    return this.clientService.update(updateClientDto.id, updateClientDto);
  }

  @MessagePattern('removeClient')
  remove(@Payload() id: string) {
    return this.clientService.remove(id);
  }

  //Filter Search
  @MessagePattern('filterClient')
  filterClientSearch(@Payload() searchQuery: PaginationDtoFilter){
    return this.clientService.filterClient(searchQuery);
  }

  //Filter Search Advanced
  @MessagePattern('filterAdvancedClient')
  filterAdvancedClient(@Payload() paginationDtoAdvancedFilter: PaginationDtoAdvancedClientsFilter){
    return this.clientService.filterClientAdvanced(paginationDtoAdvancedFilter);
  }
}
