import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

@Schema({ collection: 'countries' })
export class CountryEntity extends Document {

    @Prop({ required: true })
    name: string;
}

export const CountrySchema = SchemaFactory.createForClass(CountryEntity);
