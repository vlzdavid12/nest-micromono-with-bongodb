import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, {Document} from "mongoose";
import { ClientCompanyEntity } from "../../client-company/entities/client-company.entity";
import { ClientTypeEntity } from "../../client-type/entities/client-type.entity";
import { RegistrationStatusEntity } from "../../registration-status/entities/registration-status.entity";
@Schema({ collection: 'clients', timestamps: true })
export class ClientEntity extends Document {

    @Prop({ required: false })
    first_name: string;

    @Prop({ required: false })
    second_name: string;

    @Prop({ required: false })
    last_name: string;

    @Prop({ required: false })
    second_last_name: string;

    @Prop({ required: true })
    email: string;

    @Prop({required: true})
    phone: string;

    @Prop({ required: false })
    address: string;

    @Prop({ required: false })
    country: string;

    @Prop({ required: true, default: false })
    old_client: boolean;

    @Prop({ type: 'Decimal', default: '0.25' })
    client_rate: number;

    @Prop({required: false})
    with_holding_tax: boolean;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'ClientTypeEntity' })
    client_type: ClientTypeEntity;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'RegistrationStatusEntity' })
    registration_status: RegistrationStatusEntity;

    @Prop({ required: false, type: mongoose.Schema.Types.ObjectId, ref: 'ClientCompanyEntity' })
    client_company: ClientCompanyEntity;

    @Prop({required: true})
    is_active: boolean;

    @Prop({required: false})
    deleted_at: Date;
}

export const ClientSchema = SchemaFactory.createForClass(ClientEntity)
    .plugin(require('mongoose-unique-validator'));
