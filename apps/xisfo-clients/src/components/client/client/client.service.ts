import { Injectable, Inject, NotFoundException } from '@nestjs/common';
import {ClientProxy} from '@nestjs/microservices';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import {RegistrationStatusEntity} from '../registration-status/entities/registration-status.entity';
import {CreateClientDto} from './dto/create-client.dto';
import {UpdateClientDto} from './dto/update-client.dto';
import {ClientEntity} from './entities/client.entity';
import {ClientIdentificationEntity} from "../client-identification/entities/client-identification.entity";
import {CreateClientIdentificationDto} from "../client-identification/dto/create-client-identification.dto";
import {PaginationDto} from "../../../commons/paginationDto";
import {PaginationDtoFilter} from "../../../commons/paginationDto.filter";
import { BankAccountEntity } from '../bank_account/entities/bank_account.entity';
import {ClientTypeEntity} from "../client-type/entities/client-type.entity";
import {PaginationDtoAdvancedClientsFilter} from "../../../commons/filter/paginationDtoAdvancedClients.filter";
import { Types } from 'mongoose';

@Injectable()
export class ClientService {
    constructor(
        @Inject('XISFO_WALLET') private xisfoClient: ClientProxy,
        @InjectModel(ClientEntity.name) private clientModel: Model<ClientEntity>,
        @InjectModel(RegistrationStatusEntity.name) private registrationStatusModel: Model<RegistrationStatusEntity>,
        @InjectModel(ClientIdentificationEntity.name) private clientIdentificationModel: Model<ClientIdentificationEntity>,
        @InjectModel(BankAccountEntity.name) private bankAccountModel: Model<BankAccountEntity>,
        @InjectModel(ClientTypeEntity.name) private clientTypeModule: Model<ClientTypeEntity>,
    ) {
    }

    async create(createClientDto: CreateClientDto, document: CreateClientIdentificationDto) {
        let phoneExist = await this.clientModel.findOne({phone: createClientDto.phone});
        const registration_status = await this.registrationStatusModel.findOne({name: 'moneda_local'});
        if (phoneExist) {
            return {msg: "No se puede registrar con este número telefónico"};
        }

        try{
            // Create Client
            const client = await this.clientModel.create({is_active: true, deleted_at: null, registration_status, ...createClientDto });

            // Create Document
            await this.createClientDocument({...document, client});

            // Create Wallet
            this.createWallet(createClientDto.phone, client.id, createClientDto.data_policy);
            
            return client;

        }catch (error){
            console.log(error)
        }


    }

    async createClientDocument(createIdentificationDto: CreateClientIdentificationDto) {
        // Create Document
        const document = await this.clientIdentificationModel.create({is_active: true, deleted_at: null, ...createIdentificationDto});
        return document;
    }

    createWallet(number_wallet: string, clientId: string, data_policy: boolean) {
        const walletData = {
            number_wallet: number_wallet,
            client: clientId,
            data_policy: data_policy
        }
        
        this.xisfoClient.emit('createWallet', walletData);
    }

    async findAll(paginationDto: PaginationDto) {
        const {limit = 12, offset = 1, orderBy} = paginationDto;
        const order: any = (orderBy === 'ASC') ? {updatedAt: 1} : {updatedAt: -1};
        try{
            const count = await this.clientModel.count();
            const data = await this.clientModel.find({
            deleted_at: null
        })
                .skip((limit * offset) - limit)
                .limit(limit)
                .sort(order)
                .populate('registration_status')
                .populate('client_type')
                .populate('client_company')
            .exec();
            return {data, count, limit, offset, orderBy};
        }catch (error){
            throw new NotFoundException()
        }

    }

    async findAllForeingCurrencyClients(paginationDto: PaginationDto) {
        const registration_status = await this.registrationStatusModel.findOne({name: 'moneda_extranjera'});
        const {limit = 12, offset = 1, orderBy} = paginationDto;
        const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
        const count = await this.clientModel.count();
        const data =  await this.clientModel.find({
            'registration_status': registration_status.id,
            deleted_at: null
        })
        .skip((limit * offset) - limit)
        .limit(limit)
        .sort(order)
        .populate('registration_status')
        .populate('client_type')
        .populate('client_company')
        .exec();
        return {data, count, limit, offset, orderBy};
    }

    async findOne(id: string) {
        return await this.clientModel.findById(id)
        .populate('client_type')
        .populate('registration_status')
        .populate('client_company')
        .exec();
    }

    async findClientBankAccount(id: string) {
        return await this.bankAccountModel.find({
            client: id
        })
        .populate('bank')
        .populate('type_bank_account')
        .exec();
    }

    async update(id: string, updateClientDto: UpdateClientDto) {
        const clientUpdated = await this.clientModel
            .findByIdAndUpdate(id, updateClientDto);
        return clientUpdated;
    }

    async remove(id: string) {
        return await this.clientModel
        .findByIdAndUpdate(id, {
            deleted_at: new Date,
            is_active: false
        })
        .exec();
    }

    async filterClient(paginationQuery: PaginationDtoFilter) {
        const {limit = 12, offset = 1, orderBy, search} = paginationQuery;
        const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};

        if (!search) {
            return null;
        }

        try {
            const data = await this.clientModel.find({
                $or: [
                    {
                        first_name: search.split(' ')[0],
                        second_name: search.split(' ')[1],
                        last_name: search.split(' ')[2],
                        second_last_name: search.split(' ')[3],
                    }, {
                        first_name: search.split(' ')[0],
                        second_name: search.split(' ')[1],
                    }, {
                        last_name: search.split(' ')[0],
                        second_last_name: search.split(' ')[1],
                    }, {
                        first_name: {$regex: `${search}`}
                    }, {
                        second_name: {$regex: `${search}`}
                    }, {
                        last_name: {$regex: `${search}`}
                    }, {
                        second_last_name: {$regex: `${search}`}
                    }, {
                        email: search
                    }, {
                        phone: search
                    }]
            }).skip((limit * offset) - limit)
                .limit(limit)
                .sort(order)
                .populate('registration_status')
                .populate('client_type')
                .exec()

            return {data, limit, offset, orderBy};
        } catch (error) {
            throw new NotFoundException();
        }

    }


    async filterClientAdvanced(paginationDtoAdvancedFilter: PaginationDtoAdvancedClientsFilter) {
        const {client_type, registration_statuses, limit, offset, orderBy} = paginationDtoAdvancedFilter;
        let id_registration_status;
        let id_client_types;

        let registrationID = Types.ObjectId.isValid(registration_statuses?.replace('_', ''));
        let objectRegistration = registrationID? { _id: registration_statuses} :  { name: registration_statuses}
        const id_registration = await this.registrationStatusModel.find(objectRegistration).select('_id').exec();

        let clientTypeID =  Types.ObjectId.isValid(client_type);
        let objectClientType = clientTypeID? { _id: client_type} :  { name: client_type}
        const id_statuses = await this.clientTypeModule.find(objectClientType).select('_id').exec();

        id_registration.forEach((item: RegistrationStatusEntity) => {
            id_registration_status = item.id
        });

        id_statuses.forEach((item: ClientTypeEntity) => {
            id_client_types = item._id
        });

        const data = await this.clientModel.find({
            $or:[{
                client_type: id_client_types,
                registration_status: id_registration_status

            },{
                client_type: id_client_types,
            },{
                registration_status: id_registration_status
            }]
        });

        let count = data.length;

        return {data, count, limit, offset, orderBy};

    }


}
