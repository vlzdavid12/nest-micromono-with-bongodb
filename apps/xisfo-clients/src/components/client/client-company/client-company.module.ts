import { Module } from '@nestjs/common';
import { ClientCompanyService } from './client-company.service';
import { ClientCompanyController } from './client-company.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ClientEntity, ClientSchema } from '../client/entities/client.entity';
import { ClientCompanyEntity, ClientCompanySchema } from './entities/client-company.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: ClientCompanyEntity.name,
        schema: ClientCompanySchema,
      },
      {
        name: ClientEntity.name,
        schema: ClientSchema,
      }
    ])
  ],
  controllers: [ClientCompanyController],
  providers: [ClientCompanyService]
})
export class ClientCompanyModule {}
