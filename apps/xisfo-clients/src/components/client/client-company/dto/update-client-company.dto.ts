import { PartialType } from '@nestjs/mapped-types';
import { IsMongoId } from 'class-validator';
import { CreateClientCompanyDto } from './create-client-company.dto';

export class UpdateClientCompanyDto extends PartialType(CreateClientCompanyDto) {

  @IsMongoId()
  id: string;
}
