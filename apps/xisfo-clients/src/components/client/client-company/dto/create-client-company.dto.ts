import { IsNotEmpty, IsString } from "class-validator"

export class CreateClientCompanyDto {

    @IsString()
    @IsNotEmpty()
    legal_name: string;

    @IsString()
    @IsNotEmpty()
    commercial_name: string;

    @IsString()
    @IsNotEmpty()
    nit: string;

    @IsString()
    @IsNotEmpty()
    company_phone: string;

    @IsString()
    @IsNotEmpty()
    legal_representative: string;
}
