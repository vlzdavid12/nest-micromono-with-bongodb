import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginationDto } from 'apps/xisfo-clients/src/commons/paginationDto';
import { Model } from 'mongoose';
import { ClientEntity } from '../client/entities/client.entity';
import { CreateClientCompanyDto } from './dto/create-client-company.dto';
import { UpdateClientCompanyDto } from './dto/update-client-company.dto';
import { ClientCompanyEntity } from './entities/client-company.entity';

@Injectable()
export class ClientCompanyService {
  constructor(
    @InjectModel(ClientCompanyEntity.name) private clientCompanyModel: Model<ClientCompanyEntity>,
    @InjectModel(ClientEntity.name) private clientModel: Model<ClientEntity>
  ) { }

  async create(createClientCompanyDto: CreateClientCompanyDto) {
    return await this.clientCompanyModel.create({is_active: true, deleted_at: null, ...createClientCompanyDto});
  }

  async findAll(paginationDto: PaginationDto) {
    const {limit = 12, offset = 1, orderBy} = paginationDto;
    const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
    const count = await this.clientCompanyModel.count();
    const data =  await this.clientCompanyModel.find({
        deleted_at: null
    })
    .skip((limit * offset) - limit)
    .limit(limit)
    .sort(order)
    .populate('legal_representative')
    .exec();
    return {data, count, limit, offset, orderBy};
  }

  async findOne(id: string) {
    return await this.clientCompanyModel.findById(id)
    .populate('legal_representative')
    .exec();
  }

  async findAllCompanyClients(id: string, paginationDto: PaginationDto) {
    const company = await this.clientCompanyModel.findById(id.toString());

    const {limit = 12, offset = 1, orderBy} = paginationDto;
    const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
    const count = await this.clientModel.count();
    const data = await this.clientModel.find({
      client_company: company._id
    })
    .skip((limit * offset) - limit)
    .limit(limit)
    .sort(order);

    return {data, count, limit, offset, orderBy};
  }

  async update(id: string, updateClientCompanyDto: UpdateClientCompanyDto) {
    const clientCompanyUpdated = await this.clientCompanyModel
    .findByIdAndUpdate(id, updateClientCompanyDto);
    return clientCompanyUpdated;
  }

  async remove(id: string) {
    return await this.clientCompanyModel
    .findByIdAndUpdate(id, {
      deleted_at: new Date,
      is_active: false
    })
    .exec();
  }
}
