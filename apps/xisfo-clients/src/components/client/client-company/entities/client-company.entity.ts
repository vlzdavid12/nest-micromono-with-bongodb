import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose"
import mongoose, { Document } from "mongoose"
import { ClientEntity } from "../../client/entities/client.entity"

@Schema({collection: 'client_companies', timestamps: true})
export class ClientCompanyEntity extends Document {
    @Prop({ required: true })
    legal_name: string

    @Prop({required: true})
    commercial_name: string

    @Prop({required: true, unique: true})
    nit: string

    @Prop({required: true, unique: true})
    company_phone: string

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'ClientEntity' })
    legal_representative: ClientEntity;

    @Prop({required: true})
    is_active: boolean

    @Prop({required: false})
    deleted_at: Date
}

export const ClientCompanySchema = SchemaFactory.createForClass(ClientCompanyEntity);
