import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { PaginationDto } from 'apps/xisfo-clients/src/commons/paginationDto';
import { ClientCompanyService } from './client-company.service';
import { CreateClientCompanyDto } from './dto/create-client-company.dto';
import { UpdateClientCompanyDto } from './dto/update-client-company.dto';

@Controller()
export class ClientCompanyController {
  constructor(private readonly clientCompanyService: ClientCompanyService) {}

  @MessagePattern('createClientCompany')
  create(@Payload() createClientCompanyDto: CreateClientCompanyDto) {
    return this.clientCompanyService.create(createClientCompanyDto);
  }

  @MessagePattern('findAllClientCompany')
  findAll(@Payload() paginationDto: PaginationDto) {
    return this.clientCompanyService.findAll(paginationDto);
  }

  @MessagePattern('findAllCompanyClients')
  findAllCompanyClients(@Payload() payload: any) {
    return this.clientCompanyService.findAllCompanyClients(payload.id, payload.paginationDto);
  }

  @MessagePattern('findOneClientCompany')
  findOne(@Payload() id: string) {
    return this.clientCompanyService.findOne(id);
  }

  @MessagePattern('updateClientCompany')
  update(@Payload() updateClientCompanyDto: UpdateClientCompanyDto) {
    return this.clientCompanyService.update(updateClientCompanyDto.id, updateClientCompanyDto);
  }

  @MessagePattern('removeClientCompany')
  remove(@Payload() id: string) {
    return this.clientCompanyService.remove(id);
  }
}
