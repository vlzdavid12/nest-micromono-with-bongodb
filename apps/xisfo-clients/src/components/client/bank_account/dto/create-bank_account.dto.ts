import { IsNotEmpty, IsString } from "class-validator";

export class CreateBankAccountDto {
    @IsString()
    @IsNotEmpty()
    account_number: string;

    @IsString()
    @IsNotEmpty()
    bank: string;

    @IsString()
    @IsNotEmpty()
    client: string;

    @IsString()
    @IsNotEmpty()
    type_bank_account: string;
}
