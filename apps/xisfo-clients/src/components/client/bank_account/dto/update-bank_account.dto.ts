import { PartialType } from '@nestjs/mapped-types';
import { IsMongoId, IsNotEmpty } from 'class-validator';
import { CreateBankAccountDto } from './create-bank_account.dto';

export class UpdateBankAccountDto extends PartialType(CreateBankAccountDto) {

  @IsMongoId()
  @IsNotEmpty()
  id: string;
}
