import { Module } from '@nestjs/common';
import { BankAccountService } from './bank_account.service';
import { BankAccountController } from './bank_account.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { BankAccountEntity, BankAccountEntitySchema } from './entities/bank_account.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: BankAccountEntity.name,
        schema: BankAccountEntitySchema,
      }
    ])
  ],
  controllers: [BankAccountController],
  providers: [BankAccountService]
})
export class BankAccountModule {}
