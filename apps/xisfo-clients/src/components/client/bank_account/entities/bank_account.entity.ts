import { Client } from "@nestjs/microservices/external/nats-client.interface";
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose from "mongoose";
import { BankEntity } from "../../../general/bank/entities/bank.entity";
import { TypeBankAccountEntity } from "../../../general/type_bank_account/entities/type_bank_account.entity";

@Schema({collection: 'bank_accounts', timestamps: true})
export class BankAccountEntity {
    @Prop({required: true, unique: true})
    account_number: number;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'BankEntity' })
    bank: BankEntity;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'ClientEntity' })
    client: Client;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'TypeBankAccountEntity' })
    type_bank_account: TypeBankAccountEntity;

    @Prop({required: true})
    is_active: boolean

    @Prop({required: false})
    deleted_at: Date
}

export const BankAccountEntitySchema = SchemaFactory.createForClass(BankAccountEntity);
