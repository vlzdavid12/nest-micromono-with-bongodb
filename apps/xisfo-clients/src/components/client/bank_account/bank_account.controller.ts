import {Controller, UseInterceptors} from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { BankAccountService } from './bank_account.service';
import { CreateBankAccountDto } from './dto/create-bank_account.dto';
import { UpdateBankAccountDto } from './dto/update-bank_account.dto';
import {TransformInterceptor} from "../../../../../xisfo-gateway/src/filter/transform.interceptor";

@Controller()
export class BankAccountController {
  constructor(private readonly bankAccountService: BankAccountService) {}

  @MessagePattern('createBankAccount')
  @UseInterceptors(TransformInterceptor)
  create(@Payload() createBankAccountDto: CreateBankAccountDto) {
    return this.bankAccountService.create(createBankAccountDto);
  }

  @MessagePattern('findAllBankAccount')
  @UseInterceptors(TransformInterceptor)
  findAll() {
    return this.bankAccountService.findAll();
  }

  @MessagePattern('findOneBankAccount')
  @UseInterceptors(TransformInterceptor)
  findOne(@Payload() id: string) {
    return this.bankAccountService.findOne(id);
  }

  @MessagePattern('updateBankAccount')
  @UseInterceptors(TransformInterceptor)
  update(@Payload() updateBankAccountDto: UpdateBankAccountDto) {
    return this.bankAccountService.update(updateBankAccountDto.id, updateBankAccountDto);
  }

  @MessagePattern('removeBankAccount')
  @UseInterceptors(TransformInterceptor)
  remove(@Payload() id: string) {
    return this.bankAccountService.remove(id);
  }
}
