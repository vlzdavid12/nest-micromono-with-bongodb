import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateBankAccountDto } from './dto/create-bank_account.dto';
import { UpdateBankAccountDto } from './dto/update-bank_account.dto';
import { BankAccountEntity } from './entities/bank_account.entity';

@Injectable()
export class BankAccountService {
  constructor(
    @InjectModel(BankAccountEntity.name) private bankAccountModel: Model<BankAccountEntity>,
  ) { }

  async create(createBankAccountDto: CreateBankAccountDto) {
    return await this.bankAccountModel.create({is_active: true, deleted_at: null, ...createBankAccountDto});
  }

  async findAll() {
    return await this.bankAccountModel.find();
  }

  async findOne(id: string) {
    return await this.bankAccountModel.findById(id);
  }

  async update(id: string, updateBankAccountDto: UpdateBankAccountDto) {
    const bankAccountUpdated = await this.bankAccountModel
    .findByIdAndUpdate(id, updateBankAccountDto);
    return bankAccountUpdated;
  }

  async remove(id: string) {
    const bankAccountDeleted = await this.bankAccountModel
    .findByIdAndUpdate(id, {
      deleted_at: new Date,
      is_active: false
    })
    .exec();
    return bankAccountDeleted;
  }
}
