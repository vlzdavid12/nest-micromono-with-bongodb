import { Module } from '@nestjs/common';
import { ClientTypeService } from './client-type.service';
import { ClientTypeController } from './client-type.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ClientTypeEntity, ClientTypeSchema } from './entities/client-type.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: ClientTypeEntity.name,
        schema: ClientTypeSchema
      }
    ])
  ],
  controllers: [ClientTypeController],
  providers: [ClientTypeService]
})
export class ClientTypeModule {}
