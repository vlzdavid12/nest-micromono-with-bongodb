import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ClientTypeEntity } from './entities/client-type.entity';

@Injectable()
export class ClientTypeService {
  constructor(
    @InjectModel(ClientTypeEntity.name) private clientTypeModule: Model<ClientTypeEntity>,
  ) { }

  async findAll() {
    return await this.clientTypeModule.find();
  }

  async findOne(id: string) {
    return await this.clientTypeModule.findById(id);
  }
}
