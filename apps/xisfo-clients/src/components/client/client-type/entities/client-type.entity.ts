import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({collection: 'client_types'})
export class ClientTypeEntity extends Document {
  @Prop({required: true})
  name: string;
}

export const ClientTypeSchema = SchemaFactory.createForClass(ClientTypeEntity);
