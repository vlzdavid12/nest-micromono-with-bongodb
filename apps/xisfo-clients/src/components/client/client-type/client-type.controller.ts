import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ClientTypeService } from './client-type.service';

@Controller()
export class ClientTypeController {
  constructor(private readonly clientTypeService: ClientTypeService) {}

  @MessagePattern('findAllClientType')
  findAll() {
    return this.clientTypeService.findAll();
  }

  @MessagePattern('findOneClientType')
  findOne(@Payload() id: string) {
    return this.clientTypeService.findOne(id);
  }
}
