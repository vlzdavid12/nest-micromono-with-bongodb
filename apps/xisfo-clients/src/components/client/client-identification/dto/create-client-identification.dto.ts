import {IsNotEmpty, IsObject, IsString} from "class-validator";

export class CreateClientIdentificationDto {

    @IsNotEmpty()
    @IsString()
    identification_number: string;

    @IsNotEmpty()
    @IsString()
    expedition_date: string;

    @IsNotEmpty()
    @IsString()
    date_birth: string;

    @IsNotEmpty()
    @IsString()
    gender: string;

    @IsObject()
    client?: object;

    @IsObject()
    document_type?: object;


    @IsString()
    country_document: string

}
