import { PartialType } from '@nestjs/mapped-types';
import { IsMongoId, IsNotEmpty } from 'class-validator';
import { CreateClientIdentificationDto } from './create-client-identification.dto';

export class UpdateClientIdentificationDto extends PartialType(CreateClientIdentificationDto) {

  @IsMongoId()
  @IsNotEmpty()
  id: string;
}
