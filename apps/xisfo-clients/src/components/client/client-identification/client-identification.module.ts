import { Module } from '@nestjs/common';
import { ClientIdentificationService } from './client-identification.service';
import { ClientIdentificationController } from './client-identification.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ClientIdentificationEntity, ClientIdentificationSchema } from './entities/client-identification.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: ClientIdentificationEntity.name,
        schema: ClientIdentificationSchema,
      }
    ])
  ],
  controllers: [ClientIdentificationController],
  providers: [ClientIdentificationService]
})
export class ClientIdentificationModule {}
