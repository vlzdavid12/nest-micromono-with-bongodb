import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ClientIdentificationService } from './client-identification.service';
import { CreateClientIdentificationDto } from './dto/create-client-identification.dto';
import { UpdateClientIdentificationDto } from './dto/update-client-identification.dto';

@Controller()
export class ClientIdentificationController {
  constructor(private readonly clientIdentificationService: ClientIdentificationService) {}

  @MessagePattern('createClientIdentification')
  create(@Payload() createClientIdentificationDto: CreateClientIdentificationDto) {
    return this.clientIdentificationService.create(createClientIdentificationDto);
  }

  @MessagePattern('findAllClientIdentification')
  findAll() {
    return this.clientIdentificationService.findAll();
  }

  @MessagePattern('findOneClientIdentification')
  findOne(@Payload() id: string) {
    return this.clientIdentificationService.findOne(id);
  }

  @MessagePattern('updateClientIdentification')
  update(@Payload() updateClientIdentificationDto: UpdateClientIdentificationDto) {
    return this.clientIdentificationService.update(updateClientIdentificationDto.id, updateClientIdentificationDto);
  }

  @MessagePattern('removeClientIdentification')
  remove(@Payload() id: string) {
    return this.clientIdentificationService.remove(id);
  }
}
