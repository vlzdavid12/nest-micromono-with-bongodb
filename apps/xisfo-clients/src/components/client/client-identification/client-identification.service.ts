import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateClientIdentificationDto } from './dto/create-client-identification.dto';
import { UpdateClientIdentificationDto } from './dto/update-client-identification.dto';
import { ClientIdentificationEntity } from './entities/client-identification.entity';

@Injectable()
export class ClientIdentificationService {
  constructor(
    @InjectModel(ClientIdentificationEntity.name) private clientIdentificationModel: Model<ClientIdentificationEntity>,
  ) { }

  async create(createClientIdentificationDto: CreateClientIdentificationDto) {
    return await this.clientIdentificationModel.create({is_active: true, deleted_at: null, ...createClientIdentificationDto});
  }

  async findAll() {
    return await this.clientIdentificationModel.find();
  }

  async findOne(id: string) {
    return await this.clientIdentificationModel.findById(id);
  }

  async update(id: string, updateClientIdentificationDto: UpdateClientIdentificationDto) {
    const clientIdentificationUpdated = await this.clientIdentificationModel
    .findByIdAndUpdate(id, updateClientIdentificationDto);
    return clientIdentificationUpdated;
  }

  async remove(id: string) {
    return await this.clientIdentificationModel
    .findByIdAndUpdate(id, {
      deleted_at: new Date,
      is_active: false
    })
    .exec();
  }
}
