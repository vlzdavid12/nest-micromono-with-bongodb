import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { Document } from "mongoose";
import {DocumentTypeEntity} from "../../../general/document-type/entities/document-type.entity";
import {ClientEntity} from "../../client/entities/client.entity";
import {GenderEntity} from "../../../employee/gender/entities/gender.entity";

@Schema({collection: 'client_identifications', timestamps: true})
export class ClientIdentificationEntity extends Document {

    @Prop({required: true})
    country_document: string

    @Prop({required: true})
    identification_number: number

    @Prop({required: true, type: String})
    expedition_date: string

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'ClientEntity' })
    client: ClientEntity

    @Prop({ required: false, type: mongoose.Schema.Types.ObjectId, ref: 'DocumentTypeEntity' })
    document_type: DocumentTypeEntity

    @Prop({required: true})
    is_active: boolean

    @Prop({required: false})
    deleted_at: Date
}

export const ClientIdentificationSchema = SchemaFactory.createForClass(ClientIdentificationEntity);
