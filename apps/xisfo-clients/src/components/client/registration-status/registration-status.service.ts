import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { RegistrationStatusEntity } from './entities/registration-status.entity';

@Injectable()
export class RegistrationStatusService {
  constructor(
    @InjectModel(RegistrationStatusEntity.name) private registrationStatusModel: Model<RegistrationStatusEntity>,
  ) { }

  async findAll() {
    return await this.registrationStatusModel.find();
  }

  async findOne(id: string) {
    return await this.registrationStatusModel.findById(id);
  }
}
