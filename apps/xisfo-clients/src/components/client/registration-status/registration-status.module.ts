import { Module } from '@nestjs/common';
import { RegistrationStatusService } from './registration-status.service';
import { RegistrationStatusController } from './registration-status.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { RegistrationStatusEntity, RegistrationStatusSchema } from './entities/registration-status.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: RegistrationStatusEntity.name,
        schema: RegistrationStatusSchema,
      }
    ])
  ],
  controllers: [RegistrationStatusController],
  providers: [RegistrationStatusService]
})
export class RegistrationStatusModule {}
