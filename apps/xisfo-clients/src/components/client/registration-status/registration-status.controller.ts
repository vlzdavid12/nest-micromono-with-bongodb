import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { RegistrationStatusService } from './registration-status.service';

@Controller()
export class RegistrationStatusController {
  constructor(private readonly registrationStatusService: RegistrationStatusService) {}

  @MessagePattern('findAllRegistrationStatus')
  findAll() {
    return this.registrationStatusService.findAll();
  }

  @MessagePattern('findOneRegistrationStatus')
  findOne(@Payload() id: string) {
    return this.registrationStatusService.findOne(id);
  }
}
