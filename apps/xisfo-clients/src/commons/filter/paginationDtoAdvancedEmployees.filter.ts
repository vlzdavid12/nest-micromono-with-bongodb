import {IsNumber, IsOptional, IsPositive, IsString, Min} from "class-validator";

export class PaginationDtoAdvancedEmployeesFilter {

    @IsOptional()
    civil_status_id?: string

    @IsOptional()
    contract_type_id?: string

    @IsOptional()
    gender_id?: string

    @IsOptional()
    position_id?: string

    @IsOptional()
    health_provider_id?: string

    @IsOptional()
    occupational_risk_manager_id?: string

    @IsOptional()
    housing_type?: string

    @IsOptional()
    pension_fund_id?: string

    @IsOptional()
    education_level_id?: string

    //Pagination
    @IsOptional()
    @IsNumber()
    @Min(1)
    @IsPositive()
    limit?: number;

    @IsOptional()
    @IsNumber()
    @Min(0)
    @IsPositive()
    offset?: number;

    @IsOptional()
    @IsString()
    orderBy?: string


}
