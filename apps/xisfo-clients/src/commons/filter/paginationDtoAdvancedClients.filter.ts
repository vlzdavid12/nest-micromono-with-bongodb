import {IsNumber, IsOptional, IsPositive, IsString, Min} from "class-validator";

export class PaginationDtoAdvancedClientsFilter {
    @IsOptional()
    client_type?: string

    @IsOptional()
    registration_statuses?: string

    //Pagination
    @IsOptional()
    @IsNumber()
    @Min(1)
    @IsPositive()
    limit?: number;

    @IsOptional()
    @IsNumber()
    @Min(0)
    @IsPositive()
    offset?: number;

    @IsOptional()
    @IsString()
    orderBy?: string
}
