import {ValidationPipe} from '@nestjs/common';
import {NestFactory} from '@nestjs/core';
import {Transport, MicroserviceOptions} from '@nestjs/microservices';
import { ExceptionFilterRpc } from './commons/filter/exceptionFilterRpc';
import {XisfoClientsModule} from './xisfo-clients.module';

const bootstrap = async () => {
    const app = await NestFactory.createMicroservice<MicroserviceOptions>(
        XisfoClientsModule,
        {
            transport: Transport.TCP,
            options: {
                port: 3001,
            },
        },
    );


    app.useGlobalPipes(
        new ValidationPipe({
            whitelist: true,
            forbidNonWhitelisted: true
        })
    )

    // app.useGlobalFilters(new ExceptionFilterRpc());

    await app.listen();
}

bootstrap();
