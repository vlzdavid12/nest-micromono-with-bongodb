import { Module } from '@nestjs/common';
import { ComponentsModule } from './components/components.module';
import { MongooseModule } from '@nestjs/mongoose';
import {CommandModule} from "nestjs-command";

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/xisfo'),
    ComponentsModule,
    CommandModule,
  ],
})
export class XisfoClientsModule {}
