import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SeedAll } from './seed.seed-all';
import { WithdrawalCostEntity, WithdrawalCostSchema } from '../wallet/withdrawal-cost/entities/withdrawal-cost.entity';
import { SeedWithdrawalCost } from './seeders/seed.withdrawal-cost';

@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: WithdrawalCostEntity.name,
                schema: WithdrawalCostSchema
            },
        ])
    ],
    providers: [
        SeedAll,
        SeedWithdrawalCost,
    ]
})
export class SeedModule {}
