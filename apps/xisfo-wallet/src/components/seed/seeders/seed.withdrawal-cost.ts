import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { WithdrawalCostEntity } from '../../wallet/withdrawal-cost/entities/withdrawal-cost.entity';

@Injectable()
export class SeedWithdrawalCost {
    constructor(
        @InjectModel(WithdrawalCostEntity.name)
        private readonly withdrawalCostModel: Model<WithdrawalCostEntity>
    ) { }
    async create() {
        await this.withdrawalCostModel.deleteMany({});

        const data = [
            { 
                price: 3000,
                iva: 0.19,
                is_active: true,
                deleted_at: null
            },
        ];

        await this.withdrawalCostModel.insertMany(data);
        console.log("Withdrawal Cost seed finished successful");
    }
}
