import {Injectable} from '@nestjs/common';
import {Command} from "nestjs-command";
import { SeedWithdrawalCost } from './seeders/seed.withdrawal-cost';

@Injectable()
export class SeedAll {
    constructor(
        private readonly withdrawalCostSeeder: SeedWithdrawalCost,
    ) {
    }

    @Command({command: 'create:seed', describe: 'Seed Tables  Xisfo'})
    async create() {
        await this.withdrawalCostSeeder.create();
    }
}
