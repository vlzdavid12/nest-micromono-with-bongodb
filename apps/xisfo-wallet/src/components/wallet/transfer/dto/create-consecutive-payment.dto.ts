import { IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator"

export class CreateConsecutivePaymentDto {

    @IsString()
    @IsNotEmpty()
    sender_wallet: string

    @IsString()
    @IsNotEmpty()
    excel_file: string
}
