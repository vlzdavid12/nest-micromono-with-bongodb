import { IsArray, IsNotEmpty, IsString } from "class-validator"

export class CreateTransferFromPaymentDto {

    @IsArray()
    @IsNotEmpty()
    payments: string[]
}
