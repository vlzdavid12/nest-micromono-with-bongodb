import { IsBoolean, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator"

export class CreateTransferWithdrawalsDto {

    @IsString()
    @IsNotEmpty()
    sender_wallet: string

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    receptor_wallet: string

    @IsBoolean()
    @IsOptional()
    is_paid: boolean

    @IsNumber()
    @IsNotEmpty()
    amount: number
}
