import { IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator"

export class CreateTransferFromWalletDto {

    @IsString()
    @IsNotEmpty()
    sender_wallet: string

    @IsString()
    @IsNotEmpty()
    receptor_wallet: string

    @IsNumber()
    @IsNotEmpty()
    amount: number
}
