import {Injectable, BadRequestException, NotFoundException} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Workbook } from 'exceljs';
import { Model } from 'mongoose';
import { PaymentEntity } from '../entities/payment.entity';
import { ExportWithdrawalInterface } from '../interfaces/export-withdrawal.interface';
import { HistoryWalletInterface } from '../interfaces/history-wallet-interface';
import { TransferWallet } from '../interfaces/transfer-wallet.interface';
import { Wallet } from '../interfaces/wallet.interface';
import { WalletEntity } from '../wallet/entities/wallet.entity';
import { CreateExportWithdrawalsDto } from './dto/create-export-withdrawals.dto';
import { CreateTransferFromPaymentDto } from './dto/create-transfer-from-payment.dto';
import { CreateTransferFromWalletDto } from './dto/create-transfer-from-wallet.dto';
import { CreateTransferWithdrawalsDto } from './dto/create-transfer-withdrawals.dto';
import { BankAccountEntity } from './entities/bank-account.entity';
import {TransferEntity} from './entities/transfer.entity';
import { WithdrawalsEntity } from './entities/withdrawal.entity';
import { PaymentInterface } from './interfaces/payment-interface.interface';
import * as tmp from 'tmp';
import { CreateConsecutivePaymentDto } from './dto/create-consecutive-payment.dto';
import * as path from 'path';
import { ConfigService } from '@nestjs/config';
import { WithdrawalCostEntity } from '../withdrawal-cost/entities/withdrawal-cost.entity';
import {PaginationDtoFilter} from "../../../commons/paginationDto.filter";
import { PaginationDto } from 'apps/xisfo-wallet/src/commons/paginationDto';

@Injectable()
export class TransferService {
  private storage_images:
  string = this.configService.get('FILES_GATEWAY')

  constructor(
    @InjectModel(TransferEntity.name) private transferModel: Model<TransferEntity>,
    @InjectModel(WalletEntity.name) private walletModel: Model<WalletEntity>,
    @InjectModel(WithdrawalCostEntity.name) private withdrawalCostModel: Model<WithdrawalCostEntity>,
    @InjectModel(PaymentEntity.name) private paymentModel: Model<PaymentEntity>,
    @InjectModel(WithdrawalsEntity.name) private withdrawalsModel: Model<WithdrawalsEntity>,
    @InjectModel(BankAccountEntity.name) private BankAccountModel: Model<BankAccountEntity>,
    private readonly configService: ConfigService,
  ) { }

  /**
   * Esta funcion crea una transferencia unicamente hacia otra wallet
   */
  async createTransferWallet(createTransferDto: CreateTransferFromWalletDto) {
    // Verifica si las cuentas existen y trae todos los datos de cada wallet
    const sender_wallet: Wallet = await this.walletModel.findById(createTransferDto.sender_wallet);
    const receptor_wallet: Wallet = await this.walletModel.findById(createTransferDto.receptor_wallet);

    // Verifica el saldo de la wallet que envia mediante el historial de transferencias
    const total_balance = await this.verifyBalance(String(sender_wallet._id));

    /**
     *  Si el historial de transferencias es diferente al saldo de la wallet retorna un error,
     *  esto con el fin de evitar modificaciones en el saldo de la wallet   t(>.<t)
     */
    if (sender_wallet._id == receptor_wallet._id) {
      return { error: 'The receptor wallet must be different' }
    }

    // Si la wallet tiene menos dinero del que va a transferir retorna error
    if (sender_wallet.amount_total < createTransferDto.amount) {
      return { error: 'The wallet have not enough money' }
    }
    
    // Si se intenta transferir a la misma wallet retorna error
    if (total_balance != sender_wallet.amount_total) {
      return { error: 'The balance is different than the total amount in wallet' }
    }

    // Se le resta el saldo transferido a la wallet que envia 
    await this.walletModel.findByIdAndUpdate(sender_wallet._id, {
      amount_total: sender_wallet.amount_total - createTransferDto.amount
    })

    // Se le suma el saldo transferido a la wallet que recibe 
    await this.walletModel.findByIdAndUpdate(receptor_wallet._id, {
      amount_total: receptor_wallet.amount_total + createTransferDto.amount
    })

    const is_xisfo_payment = false;
    const withdrawals = false;
    const transfer = await this.transferModel.create({ is_active: true, deleted_at: null, withdrawals, is_xisfo_payment, ...createTransferDto })

    return transfer;
  }

  /**
   * funcion para crear transferencias con retiros
   * @param createTransferWithdrawalsDto 
   * @returns transfers
   */
  async createTransferWithdrawals(createTransferWithdrawalsDto: CreateTransferWithdrawalsDto) {
    // Verifica si las cuentas existen y trae todos los datos de cada wallet
    const sender_wallet: Wallet = await this.walletModel.findById(createTransferWithdrawalsDto.sender_wallet);
    //trae el costo de retiro que se encuentre activo en el momento
    const withdrawal_cost = await this.withdrawalCostModel.findOne({
      is_active: true
    });
    
    // Verifica el saldo de la wallet que envia mediante el historial de transferencias
    const total_balance = await this.verifyBalance(String(sender_wallet._id));

    // Si la wallet tiene menos dinero del que va a transferir retorna error
    if (sender_wallet.amount_total < createTransferWithdrawalsDto.amount) {
      return { error: 'The wallet have not enough money' }
    }

    
    // Se le resta el saldo transferido a la wallet que envia 
    await this.walletModel.findByIdAndUpdate(sender_wallet._id, {
      amount_total: sender_wallet.amount_total - createTransferWithdrawalsDto.amount
    })

    //Se calcula el iva según el precio de retiro
    withdrawal_cost.iva = withdrawal_cost.price * withdrawal_cost.iva;
    //se resta el costo del retiro al monto
    createTransferWithdrawalsDto.amount = createTransferWithdrawalsDto.amount - withdrawal_cost.price - withdrawal_cost.iva;

    createTransferWithdrawalsDto.receptor_wallet = createTransferWithdrawalsDto.sender_wallet

    const bank_account = await this.BankAccountModel.findOne({
      'client': sender_wallet.client._id
    })
    const is_xisfo_payment = false;
    const withdrawals = true;
    const transfer = await this.transferModel.create({ is_xisfo_payment, withdrawals, ...createTransferWithdrawalsDto });

    const withdrawalsData = {
      bank_account: bank_account._id,
      transfer: transfer._id,
      is_paid: false,
      withdrawal_cost: withdrawal_cost._id,
      is_active: true,
      deleted_at: null
    }
    await this.withdrawalsModel.create({...withdrawalsData})

    return transfer;
  }

  //crear pagos consecutivos a partir de un archivo excel
  async createConsecutivePayments(createConsecutivePaymentDto: CreateConsecutivePaymentDto){
    //se trae el archivo de la ruta public en la gateway
    let filePath = await path.resolve(this.storage_images, createConsecutivePaymentDto.excel_file);

    //se inicia instancia para el archivo
    const workbook = new Workbook;
    //se lee el documento excel y se guarda el contenido 
    const content = await workbook.xlsx.readFile(filePath);

    //Se trae la hoja de trabajo principal
    const worksheet = content.getWorksheet(1);
    
    //variable para asignar la fila por donde empezará a leer las filas
    const rowStartIndex = 2;
    //se cuentan la cantidad de filas que hay en el documento
    const numberOfRows = worksheet.rowCount - 1;

    //se trae las dos variables de arriba y se revisa si no está vacío
    const rows = worksheet.getRows(rowStartIndex, numberOfRows) ?? [];

    //se crea variable objeto para guardar la información de cada fila
    const dataConsecutivePayments = rows.map((row) => {
      return {
        name: this.getCellValue(row,1),
        last_name: this.getCellValue(row,2),
        wallet: this.getCellValue(row,3),
        amount: this.getCellValue(row,4)
      }
    })
    //se crea una lista que tendrá una función asinctrona que recorre el objeto que contiene la información
    let asyncPaymentConsecutive = async (dataConsecutivePayments) => {
      //se crea una variable que contendrá las promesas que devolverá el map
      const promiseArray = dataConsecutivePayments.map(async (pay) => {
        //Se trae las wallets correspondientes y se carga el monto
        const amount = pay.amount;
        const wallet: Wallet = await this.walletModel.findOne({
          number_wallet: pay.wallet
        })
        .populate('client')
        .exec()

        return {wallet, amount}
      })

      //retorna todas las promesas resueltas, y manjea sus resultados
      return Promise.all(promiseArray)
      .then(results => {
        //variable para contener las promesas de results
        let transferConsecutivePayment: CreateTransferFromWalletDto[] = []
        let transfer
        for(let i = 0; i < results.length; i++) {
          //se crea el objeto que con la información necesaria para poder crear una transferencia
          let transferConse = {
            sender_wallet: createConsecutivePaymentDto.sender_wallet,
            receptor_wallet: results[i].wallet._id.toString(),
            amount: parseInt(results[i].amount)
          }
          //se sube la información a una variable
          transferConsecutivePayment.push(transferConse)
        }
        //se emite la variable con todas las transferencias a realizar a otra funcion
        transfer = this.createConsecutiveTransferWallet(transferConsecutivePayment)
        .then(response => {
          return response
        })
        return transfer
      })
    }

    return asyncPaymentConsecutive(dataConsecutivePayments);
  }

  //funcion para recorrer las celdas del archivo y sacar cada uno de los datos
  getCellValue(row, cellIndex: number){
    const cell = row.getCell(cellIndex);
    return cell.value ? cell.value.toString() : '';
  };

  /**
   * Esta funcion crea muchas transferencias unicamente hacia otras wallet
   */
   async createConsecutiveTransferWallet(createTransferDto: CreateTransferFromWalletDto[]) {
    //se crea un listado asincrono de las transferencias que recibe el dto
    let asyncTransfers = async (createTransferDto) => {
      //se crea un array para contener las promesas que devuelve el map
      const promiseArray = createTransferDto.map(async (request) => {
        //se buscan las wallets correspondientes
        const sender_wallet: Wallet = await this.walletModel.findById(request.sender_wallet);
        const receptor_wallet: Wallet = await this.walletModel.findById(request.receptor_wallet);
        //Se trae el balance actual de su wallet
        const total_balance = await this.verifyBalance(String(sender_wallet._id));

        /**
         *  Si el historial de transferencias es diferente al saldo de la wallet retorna un error,
         *  esto con el fin de evitar modificaciones en el saldo de la wallet   t(>.<t)
         */
        if (sender_wallet._id == receptor_wallet._id) {
          return { error: 'The receptor wallet must be different' }
        }
  
        // Si la wallet tiene menos dinero del que va a transferir retorna error
        if (sender_wallet.amount_total < request.amount) {
          return { error: 'The wallet have not enough money' }
        }
        
        // Si se intenta transferir a la misma wallet retorna error
        if (total_balance != sender_wallet.amount_total) {
          return { error: 'The balance is different than the total amount in wallet' }
        }

        return {request, sender_wallet, receptor_wallet};
      });

      //retornamos todas las promesas resueltas, y manejamos la data
      return Promise.all(promiseArray)
      //se trae el resultado de la data y se hacen los calculos para las wallets
      .then(async (result) => {
        let amount_total2 = 0;
        let amount_total1 = 0;
        for(let i = 0; i < result.length; i++) {
          if(!result[i].error) {
            //el primer calculo se hace con la variable inicializada en 0, después se asignara el resultado a la varaiable amount
            if(amount_total1 != 0) {
              result[i].sender_wallet.amount_total = amount_total1;
            }
            amount_total1 = result[i].sender_wallet.amount_total - result[i].request.amount;
            //si la resta da menos que 0, se deja de hacer los calculos
            if (amount_total1 <= 0) {
              return 'not enough money, last transfer to: ' + result[i].receptor_wallet.number_wallet;
            }
            await this.walletModel.findByIdAndUpdate(result[i].sender_wallet.id, {
            amount_total: amount_total1
            })

            //el primer calculo se hace con la variable inicializada en 0, después se asignara el resultado a la varaiable amount
            if(amount_total2 != 0) {
              result[i].receptor_wallet.amount_total = amount_total2;
            }
            amount_total2 = result[i].receptor_wallet.amount_total + result[i].request.amount;
            // Se le suma el saldo transferido a la wallet que recibe 
            await this.walletModel.findByIdAndUpdate(result[i].receptor_wallet.id, {
            amount_total: amount_total2
            })
          } else {
            return result[i];
          }
        } 
        return result;
      })
      .then((result) => {
        if(!result.error) {
          const promiseArray = result.map(async (element) => {
            if(!element.error) {
              const is_xisfo_payment = false;
              const withdrawals = false;
              //se hace las transferencias despues de los calculos
              const transfer = await this.transferModel.create({is_active: true, deleted_at: null, withdrawals, is_xisfo_payment, ...element.request })
              return transfer;
            } else {
              return element.error;
            }
          })
          return Promise.all(promiseArray);
        } else {
          return result.error;
        }
      })
    }

    return asyncTransfers(createTransferDto);
  }

  /**
   * Esta funcion verifica el historial de transferencias para saber realmente el saldo de la wallet
   * @param walletId 
   */
  async verifyBalance(walletId: string) {
    // Se traen todas las transferencias donde la wallet haya recibido dinero
    const historyWalletReceptor = await this.transferModel.find({ receptor_wallet: walletId });

    //Se traen todas las transferencias donde la wallet haya enviado dinero
    const historyWalletSender = await this.transferModel.find({ sender_wallet: walletId });

    let totalIncomming = 0
    let totalSend = 0

    // Se suman los ingresos
    historyWalletReceptor.forEach(element => {
      totalIncomming = totalIncomming + element.amount
    });

    // Se suman los egresos
    historyWalletSender.forEach(element => {
      totalSend = totalSend + element.amount
    });

    // Al hacer la resta de ingresos sobre egresos se obtiene el saldo actual de la wallet
    return totalIncomming - totalSend;
  }

  /**
   * Esta funcion retorna el historial de transacciones de una wallet
   * @param walletId 
   */
  async getHistoryWallet(walletId: string) {

    let historyWalletReceptor: TransferWallet[] = await this.transferModel.find({ receptor_wallet: walletId })
      .populate('sender_wallet')
      .sort({ createdAt: -1 });

    let historyWalletSender: TransferWallet[] = await this.transferModel.find({ sender_wallet: walletId })
      .populate('receptor_wallet')
      .sort({ createdAt: -1 });

    let historyWalletIncomming = this.restructuredHistory(historyWalletReceptor, false)
    let historyWalletSending = this.restructuredHistory(historyWalletSender, true)

    const history = historyWalletIncomming.concat(historyWalletSending).sort()
    return this.organizeHistory(history)
  }

  /**
   * Esta funcion re estructura la transaccion individual de una wallet
   * en esta misma se le coloca si la transaccion es un ingreso o un egreso de la wallet 
   * @param historyWallet 
   * @param isSending 
   */
  restructuredHistory(historyWallet, isSending: boolean) {
    let historyWalletRestructured = [];

    for (let i = 0; i < historyWallet.length; i++) {
      let data = {
        sender_wallet: historyWallet[i]?.sender_wallet?.number_wallet ?? null,
        amount: historyWallet[i].amount,
        is_xisfo_payment: historyWallet[i].is_xisfo_payment,
        is_sending: isSending,
        payment: historyWallet[i].payment ?? null,
        date: historyWallet[i].createdAt
      }
      historyWalletRestructured.push(data);
    }

    return historyWalletRestructured
  }

  /**
   * Esta funcion recibe el array de todas las transacciones de manera desordenada
   * lo ordena mediante metodo de burbuja, se ordena desde la primera fecha a la ultima
   * @param history 
   */
  organizeHistory(history: HistoryWalletInterface[]) {
    let tempArray: HistoryWalletInterface[] = history;
    let volverAOrdenar: boolean = false
    tempArray.forEach(function (valor, key) {
      if (tempArray[key]?.results.date < tempArray[key + 1]?.results.date && tempArray.length - 1 != key) {
        let firstTransfer = tempArray[key]
        let secondTransfer = tempArray[key + 1]
        tempArray[key] = secondTransfer
        tempArray[key + 1] = firstTransfer
        volverAOrdenar = true
      }
    })
    if (volverAOrdenar) {
      this.organizeHistory(tempArray)
    }
    return tempArray
  }

  /**
   * Esta funcion realiza una transferencia desde un pago de xisfo hacia la wallet del cliente
   * @param createTransferDto solo recibe el ID del payment
   * la transferencia se realiza en pesos colombianos
   */
  async createTransferPayment(createTransferDto: CreateTransferFromPaymentDto) {
    // Busca cada payment por id y trae la data
    let payments: PaymentInterface[] = await this.getPayments(createTransferDto.payments);    
    
    // Recorre todos los payments
    await Promise.all(payments.map(async (payment) => {
      
      // Si payment ya fue pagada bota error
      if (payment.is_paid) {
        throw new Error("The payment have been paid");
      }
      
      // Si payment sigue en proceso bota error
      if (payment.in_management) {
        throw new Error(`The payment #${payment._id} is in process`);
      }

      // Busca la wallet filtrando por numero de wallet
      const wallet: Wallet = await this.walletModel
      .findOne(
        { number_wallet: payment.payment_request.client_platform.client.results.phone }
        ).exec()
      // Estructura la data para crear la transferencia 
      const transfer = {
        receptor_wallet: wallet._id,
        payment: payment._id,
        amount: payment.client_pay,
        is_xisfo_payment: true,
        withdrawals: false,
        is_active: true,
        deleted_at: null
      }

      // Crea transferencia
      await this.transferModel.create(transfer);

      // Busca la wallet por id y actualiza el monto
      await this.walletModel.findByIdAndUpdate(
        wallet._id, { amount_total: wallet.amount_total + payment.client_pay })

      // Busca el payment por id y actualiza el estado a pagado
      await this.paymentModel.findByIdAndUpdate(payment._id, { is_paid: true });
    }));

    return payments;
  }

  /**
   * Esta funcion se encarga de buscar por id en base 
   * de datos cada payment
   * @param paymentsId 
   * @returns retorna los payments con toda su data
   */
  async getPayments(paymentsId: string[]) {
    let payments: PaymentInterface[] = []
    await Promise.all(paymentsId.map(async (paymentId) => {
      const payment: PaymentInterface = await this.paymentModel.findById(paymentId)
      .populate([
        {
          path: 'payment_request',
          populate: {
            path: 'client_platform', model: 'ClientPlatformEntity',
            populate: { path: 'client', model: 'ClientEntity' }
          },
        },
        {
          path: 'payment_request',
          populate: {
            path: 'client_platform', model: 'ClientPlatformEntity',
            populate: { path: 'platform', model: 'PlatformEntity' }
          },
        },
        {
          path: 'payment_request',
          populate: {
            path: 'client_platform', model: 'ClientPlatform',
            populate: { path: 'nicho', model: 'NichoEntity' }
          },
        }
      ])
      payments.push(payment)
    }));
    return payments;
  }

  //Traer información de los retiros según las transferencias, y asignarlos en una variable para crear un excel
  async exportTransferWithdrawals(withdrawals: CreateExportWithdrawalsDto) {  
    //se crea una funcion asincrona que recibe las transferencias y las maneja
    let asyncDrawal = async (transfers) => {
      //variable para cargar la información a exportar
      let drawalExportData: ExportWithdrawalInterface[] = [];
      //se recorre las transferencias y se guardan las promesas que devuelve en una variable
      const promiseArray = transfers.map(async (transfer) => {
        let number_wallet = transfer.sender_wallet.number_wallet;
        let amount = transfer.amount;
        //se trae los retiros que tienen las transferencias
        const allWithdrawals = await this.withdrawalsModel.find({
          transfer: transfer._id,
          is_paid: false
        })
        .populate([
          {
            path: 'bank_account',
            populate: {
              path: 'client', model: 'ClientEntity',
            },
          },
          {
            path: 'withdrawal_cost',
          },
        ])
        .exec()

        return {allWithdrawals, number_wallet, amount};
      });

      //se recorren las promesas para poder manipularlas
      return Promise.all(promiseArray)
      .then(results => {
        //se recorre la información de las promesas
        results.forEach(element => {
          let exportData;
          //se recorre la información del objeto que traía la promesa
          element.allWithdrawals.map(drawal => {
            //se asigna la data a la variable a exportar
            exportData = ({
              first_name: drawal.bank_account.client.first_name,
              second_name: drawal.bank_account.client.second_name,
              last_name: drawal.bank_account.client.last_name,
              second_last_name: drawal.bank_account.client.second_last_name,
              bank_account: drawal.bank_account.account_number,
              is_paid: drawal.is_paid,
              wallet: element.number_wallet,
              amount: element.amount,
              withdrawal_cost: drawal.withdrawal_cost.price + (drawal.withdrawal_cost.price * drawal.withdrawal_cost.iva),
            });
            
          })

          //se sube la data a la variable anteriormente definida
          drawalExportData.push(exportData)
        })
        //Se ejecuta la función para descargar el archivo excel
        this.downloadExcel(drawalExportData);
      });
    }

    //se trae todas las transferencias con retiros
    const transfers: TransferWallet[] = await this.transferModel.find({
      sender_wallet: withdrawals.sender_wallet,
      withdrawals: true,
    })
    .populate([
      {
        path: 'sender_wallet',
        populate: { path: 'client', model: 'ClientEntity' }
      },
    ])
    .exec()
    //se ejecuta la función que creará la variable(o la función que se definió anteriormente arriba) y se le envía transfers
    asyncDrawal(transfers)

    return 'file downloading';
  }

  //funcion para descargar un archivo excel para retiros
  async downloadExcel(drawl) {

    let rows = []

    //se guarda cada dato para ser añadido como fila
    drawl.forEach(doc => {
      rows.push(Object.values(doc))
    })

    let book = new Workbook;

    //se crea una hoja de trabajo al documento
    let sheet = book.addWorksheet('retiros');

    //se añade un header
    rows.unshift(Object.keys(drawl[0]))

    //se añaden multiples filas en la hoja
    sheet.addRows(rows);

    //Se crea un archivo temporal y despues podra descargarse
    let File = await new Promise((resolve, reject) => {
      tmp.file({ discardDescriptor: true, prefix: `Retiros`, postfix: '.xlsx', node: parseInt('0600', 8) }, async (err, file) => {
        if(err)
          throw new BadRequestException(err);
        
        book.xlsx.writeFile(file).then(_ => {
          resolve(file)
        }).catch(err => {
          throw new BadRequestException(err)
        })
      })
    });

    return File;
  }

  //funcion para crear una plantilla excel para escribir pagos consecutivos
  async downloadExcelTemplateConsecutivePayments() {
    //Se crea objeto que contiene los campos que tendrá el archivo excel
    const template = {
      name: 'nombre_completo_destinatario',
      last_name: 'apellido_completo_destinatario',
      receptor_wallet: 'numero_wallet_destino',
      amount: 'monto'
    }

    let rows = []

    //se guarda cada dato para ser añadido como fila
    rows.push(Object.values(template))
    let book = new Workbook;
    
    //se crea una hoja de trabajo al documento
    let sheet = book.addWorksheet('pagos_consecutivos');
    
    //se añaden multiples filas en la hoja
    sheet.addRows(rows);
    
    //Se crea un archivo temporal y despues podra descargarse
    let File = await new Promise((resolve, reject) => {
      tmp.file({ discardDescriptor: true, prefix: `PagosConsecutivos`, postfix: '.xlsx', node: parseInt('0600', 8) }, async (err, file) => {
        if(err)
        throw new BadRequestException(err);
        
        book.xlsx.writeFile(file).then(_ => {
          resolve(file)
        }).catch(err => {
          throw new BadRequestException(err)
        })
      })
    });

    return File;
  }

  async findAll(paginationDtoFilter: PaginationDtoFilter) {
    const {search, limit = 12, offset = 1, orderBy} = paginationDtoFilter;
    const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
    try {
      const data = await this.transferModel.find()
          .skip((limit * offset) - limit)
          .limit(limit)
          .sort(order);
      return {data, limit, offset, orderBy}
    }catch (error){
      throw new NotFoundException();
    }
  }

  async findAllPendingsWithdrawals(paginationDto: PaginationDto) {
    const {limit = 12, offset = 1, orderBy} = paginationDto;
    const order: any = (orderBy === 'ASC') ? { updatedAt: 1 } : {updatedAt: -1};
    try {
      const data = await this.withdrawalsModel.find({
        is_paid: false,
        deleted_at: null
      })
        .skip((limit * offset) - limit)
        .limit(limit)
        .sort(order);
      return {data, limit, offset, orderBy}
    }catch (error){
      throw new NotFoundException();
    }
  }

  findOne(id: string) {
    return this.transferModel.findById(id);
  }
}
