export interface WithdrawalExcelInterface {
    first_name: string;
    second_name: string;
    last_name: string;
    second_last_name: string;
    bank_account: number;
    is_paid: boolean;
    wallet: number;
    amount: number;
}
