import { ObjectId } from "mongoose";
import { PaymentRequestInterface } from "./payment-request.interface";

export interface PaymentInterface {
    _id?: ObjectId;
    client_total: number;
    client_pay: number;
    client_value_usd: number;
    xisfo_commission_general_usd: number;
    trm: number;
    tax: number;
    subtotal_usd:number;
    subtotal_cop:number;
    bank_commission: number;
    platform_commission: number;
    with_holding_tax: number;
    is_immediate: boolean;
    is_paid: boolean;
    in_management: boolean;
    massive_payment: number;
    payment_request: PaymentRequestInterface;
}