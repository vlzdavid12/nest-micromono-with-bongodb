import { ObjectId } from "mongoose";
import { ClientInterface } from "../../interfaces/client.interface";

export interface ClientPlatformInterface {
    _id?: ObjectId;
    client: ClientInterface;
}
