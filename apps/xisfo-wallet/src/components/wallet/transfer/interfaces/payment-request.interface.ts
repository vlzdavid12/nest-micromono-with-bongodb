import { ObjectId } from "mongoose";
import { ClientPlatformInterface } from "./client-platform-interface.interface";

export interface PaymentRequestInterface {
    _id?: ObjectId
    client_platform: ClientPlatformInterface;
    amount: number;
}
