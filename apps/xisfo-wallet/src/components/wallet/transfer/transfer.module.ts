import { Module } from '@nestjs/common';
import { TransferService } from './transfer.service';
import { TransferController } from './transfer.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { TransferEntity, TransferSchema } from './entities/transfer.entity';
import { WalletEntity, WalletSchema } from '../wallet/entities/wallet.entity';
import { PaymentEntity, PaymentSchema} from '../entities/payment.entity';
import { ClientEntity, ClientSchema } from '../entities/client.entity';
import {ClientPlatformEntity, ClientPlatformSchema} from '../entities/client-platform.entity';
import { PaymentRequest, PaymentRequestSchema } from './entities/payment-request.entity';
import {PlatformEntity, PlatformSchema} from '../entities/platform.entity';
import { NichoEntity, NichoSchema} from '../entities/nicho.entity';
import { BankAccountEntity, BankAccountSchema } from './entities/bank-account.entity';
import { WithdrawalsEntity, WithdrawalsSchema } from './entities/withdrawal.entity';
import { ConfigService } from '@nestjs/config';
import { WithdrawalCostEntity, WithdrawalCostSchema } from '../withdrawal-cost/entities/withdrawal-cost.entity';

@Module({
  imports:[
    MongooseModule.forFeature([
      {
        name: TransferEntity.name,
        schema: TransferSchema
      },
      {
        name: WalletEntity.name,
        schema: WalletSchema
      },
      {
        name: PaymentEntity.name,
        schema: PaymentSchema
      },
      {
        name: PaymentRequest.name,
        schema: PaymentRequestSchema
      },
      {
        name: PlatformEntity.name,
        schema: PlatformSchema
      },
      {
        name: NichoEntity.name,
        schema: NichoSchema
      },
      {
        name: ClientEntity.name,
        schema: ClientSchema
      },
      {
        name: ClientPlatformEntity.name,
        schema: ClientPlatformSchema,
      },
      {
        name: BankAccountEntity.name,
        schema: BankAccountSchema,
      },
      {
        name: WithdrawalsEntity.name,
        schema: WithdrawalsSchema,
      },
      {
        name: WithdrawalCostEntity.name,
        schema: WithdrawalCostSchema,
      }
    ])
  ],
  controllers: [TransferController],
  providers: [TransferService, ConfigService]
})
export class TransferModule {}
