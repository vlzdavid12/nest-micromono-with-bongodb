import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { CreateTransferFromWalletDto } from './dto/create-transfer-from-wallet.dto';
import { CreateTransferFromPaymentDto } from './dto/create-transfer-from-payment.dto';
import { TransferService } from './transfer.service';
import { CreateTransferWithdrawalsDto } from './dto/create-transfer-withdrawals.dto';
import { CreateExportWithdrawalsDto } from './dto/create-export-withdrawals.dto';
import {PaginationDtoFilter} from "../../../commons/paginationDto.filter";
import { PaginationDto } from 'apps/xisfo-wallet/src/commons/paginationDto';


@Controller()
export class TransferController {
  constructor(private readonly transferService: TransferService) { }

  @MessagePattern('createTransferWallet')
  createTransferWallet(@Payload() createTransferDto: CreateTransferFromWalletDto) {
    return this.transferService.createTransferWallet(createTransferDto);
  }

  @MessagePattern('createTransferPayment')
  createTransferPayment(@Payload() createTransferDto: CreateTransferFromPaymentDto) {
    return this.transferService.createTransferPayment(createTransferDto);
  }

  @MessagePattern('createTransferWithdrawals')
  createTransferWithdrawals(@Payload() createTransferWithdrawalsDto: CreateTransferWithdrawalsDto) {
    return this.transferService.createTransferWithdrawals(createTransferWithdrawalsDto);
  }

  @MessagePattern('createConsecutivePayments')
  createConsecutivePayments(@Payload() createConsecutivePaymentDto: any) {
    return this.transferService.createConsecutivePayments(createConsecutivePaymentDto);
  }

  @MessagePattern('exportTransferWithdrawals')
  exportTransferWithdrawals(@Payload() withdrawals: CreateExportWithdrawalsDto) {
    return this.transferService.exportTransferWithdrawals(withdrawals);
  }

  @MessagePattern('getHistoryWallet')
  getHistoryWallet(@Payload() walletId: string) {
    return this.transferService.getHistoryWallet(walletId);
  }

  @MessagePattern('downloadExcelTemplateConsecutivePayments')
  downloadExcelTemplateConsecutivePayments() {
    return this.transferService.downloadExcelTemplateConsecutivePayments();
  }

  @MessagePattern('findAllTransfer')
  findAll(@Payload() paginationDtoFilter: PaginationDtoFilter  ) {
    return this.transferService.findAll(paginationDtoFilter);
  }

  @MessagePattern('findAllPendingsWithdrawals')
  findAllPendingsWithdrawals(@Payload() paginationDto: PaginationDto  ) {
    return this.transferService.findAllPendingsWithdrawals(paginationDto);
  }

  @MessagePattern('findOneTransfer')
  findOne(@Payload() id: string) {
    return this.transferService.findOne(id);
  }
}
