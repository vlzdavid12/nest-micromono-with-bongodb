import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose from "mongoose";
import { ClientEntity } from "../../entities/client.entity";

@Schema({collection: 'bank_accounts'})
export class BankAccountEntity {
    @Prop({required: true, unique: true})
    account_number: number;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'BankEntity' })
    bank: Object;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'ClientEntity' })
    client: ClientEntity;

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'TypeBankAccountEntity' })
    type_bank_account: Object;
}

export const BankAccountSchema = SchemaFactory.createForClass(BankAccountEntity);
