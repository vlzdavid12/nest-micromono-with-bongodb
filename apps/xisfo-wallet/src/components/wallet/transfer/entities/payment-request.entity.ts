import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { ClientPlatformEntity } from '../../entities/client-platform.entity';

@Schema({ collection: 'payment-requests' })
export class PaymentRequest extends Document {

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'ClientPlatformEntity' })
    client_platform: ClientPlatformEntity;

    @Prop({ required: true })
    amount: number;
}

export const PaymentRequestSchema = SchemaFactory.createForClass(PaymentRequest);
