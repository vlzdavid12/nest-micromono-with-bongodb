import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { Document } from "mongoose";
import { WalletInterface } from "../../interfaces/wallet.interface";

@Schema({ collection: 'transfers', timestamps: true })
export class TransferEntity extends Document {

    /**
     * Si la transferencia se hace desde una wallet, 
     * este dato debe estar lleno, pero es nulo si la
     * transferencia se hace desde un pago de xisfo
     */
    @Prop({ type: mongoose.Types.ObjectId, ref: 'WalletEntity' })
    sender_wallet: WalletInterface

    /**
     * Si la transferencia se hace dsde un pago de xisfo, 
     * este dato debe estar lleno, pero es nulo si 
     * la transferencia se hace desde una wallet
     */
    @Prop({ type: mongoose.Types.ObjectId, ref: 'PaymentEntity' })
    payment: string

    /**
     * Siempre debe estar la wallet que recibe el dinero
     */
    @Prop({ type: mongoose.Types.ObjectId, ref: 'WalletEntity', required: true })
    receptor_wallet: WalletInterface

    @Prop({ required: true })
    withdrawals: boolean

    /**
     * Este dato debe ser verdadero si la transferencia 
     * se hizo desde un pago de xisfo, y debe ser falso 
     * si la transferencia se hizo desde una wallet
     */
    @Prop({ required: true })
    is_xisfo_payment: boolean

    /**
     * El monto de dinero que se esta transfiriendo
     */
    @Prop({ required: true})
    amount: number

    @Prop({required: true})
    is_active: boolean;

    @Prop({required: false})
    deleted_at: Date;
}

export const TransferSchema = SchemaFactory.createForClass(TransferEntity);
