import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema({collection: 'bank_accounts'})
export class BankAccountEntity {
    @Prop({required: true})
    route: number;

    @Prop()
    createdAt: Date
}

export const BankAccountSchema = SchemaFactory.createForClass(BankAccountEntity);
