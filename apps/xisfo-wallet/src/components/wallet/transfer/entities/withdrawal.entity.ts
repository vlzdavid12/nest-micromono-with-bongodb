import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { Document } from "mongoose";
import { WithdrawalCostEntity } from "../../withdrawal-cost/entities/withdrawal-cost.entity";
import { BankAccountEntity } from "./bank-account.entity";
import { TransferEntity } from "./transfer.entity";

@Schema({ collection: 'withdrawals', timestamps: true })
export class WithdrawalsEntity extends Document {

    @Prop({ type: mongoose.Types.ObjectId, ref: 'BankAccountEntity', required: true })
    bank_account: BankAccountEntity

    @Prop({ type: mongoose.Types.ObjectId, ref: 'TransferEntityEntity', required: true })
    transfer: TransferEntity

    @Prop({ type: mongoose.Types.ObjectId, ref: 'WithdrawalCostEntity', required: true })
    withdrawal_cost: WithdrawalCostEntity

    @Prop({ required: true })
    is_paid: boolean

    @Prop({required: true})
    is_active: boolean;

    @Prop({required: false})
    deleted_at: Date;
}

export const WithdrawalsSchema = SchemaFactory.createForClass(WithdrawalsEntity);
