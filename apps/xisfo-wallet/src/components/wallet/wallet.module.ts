import { Module } from '@nestjs/common';
import { WalletModule as ModuleWallet } from './wallet/wallet.module';
import { TransferModule } from './transfer/transfer.module';
import { WithdrawalCostModule } from './withdrawal-cost/withdrawal-cost.module';

@Module({
  imports: [ModuleWallet, TransferModule, WithdrawalCostModule],
  exports: [ModuleWallet, TransferModule, WithdrawalCostModule]
})
export class WalletModule {}
