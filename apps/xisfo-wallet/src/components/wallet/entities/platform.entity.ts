import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ collection: 'platforms' })
export class PlatformEntity extends Document {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true,  type: 'Decimal' })
  usd_commission: number;

  @Prop({ required: true, type: 'Decimal' })
  usd_token_rate: number;

  @Prop({ required: true })
  url: string;

  @Prop({ required: false })
  icon: string;
}

export const PlatformSchema = SchemaFactory.createForClass(PlatformEntity);
