import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ collection: 'nichos' })
export class NichoEntity extends Document {
  @Prop({ required: true })
  name: string;
}

export const NichoSchema = SchemaFactory.createForClass(NichoEntity);
