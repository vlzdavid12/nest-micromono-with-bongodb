import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { ClientPlatformEntity } from './client-platform.entity';

@Schema({ collection: 'payment-requests' })
export class PaymentRequestEntity extends Document {

    @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'ClientPlatformEntity' })
    client_platform: ClientPlatformEntity;

    @Prop({ required: true })
    amount: number;
}

export const PaymentRequestSchema = SchemaFactory.createForClass(PaymentRequestEntity);
