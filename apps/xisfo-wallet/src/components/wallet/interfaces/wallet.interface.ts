import { ObjectId } from "mongoose"
import { Client } from "./client.interface"

export interface WalletInterface {
   results: Wallet
}


export interface Wallet {
    _id?: ObjectId
    number_wallet: string
    amount_total: number
    client: Client
}
