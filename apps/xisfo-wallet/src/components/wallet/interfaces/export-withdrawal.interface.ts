export interface ExportWithdrawalInterface {
 results: ExportWithdrawal
}

export interface ExportWithdrawal {
    first_name: string
    second_name: string
    last_name: string
    second_last_name: string
    bank_account: number
    is_paid: boolean
    wallet: string
    amount: number
}
