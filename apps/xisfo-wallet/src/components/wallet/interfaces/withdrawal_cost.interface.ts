import { ObjectId } from "mongoose"

export interface WithdrawalCostInterface {
  results: WithdrawalCostData
}


export interface WithdrawalCostData {
    id?: ObjectId
    price: number
    iva: number
    is_active: boolean
}
