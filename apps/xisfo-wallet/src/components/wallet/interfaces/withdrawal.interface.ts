import { ObjectId } from "mongoose"
import { BankAccountInterface } from "./bank-account.interface"
import { TransferWalletInterface } from "./transfer-wallet.interface"
import { WithdrawalCostInterface } from "./withdrawal_cost.interface"

export interface WithdrawalInterface{
 results: Withdrawal
}

export interface Withdrawal{
    id?: ObjectId
    bank_account: BankAccountInterface
    transfer: TransferWalletInterface
    is_paid: boolean
}
