export interface HistoryWalletInterface {
    results: HistoryWallet
}

export interface HistoryWallet {
    sender_wallet: string;
    amount: number;
    is_xisfo_payment: boolean;
    is_sending: boolean;
    payment: null;
    date: Date;
}
