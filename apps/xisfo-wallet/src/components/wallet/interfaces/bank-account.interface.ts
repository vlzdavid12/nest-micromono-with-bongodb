import { ObjectId } from "mongoose"
import { ClientInterface } from "./client.interface"

export interface BankAccountInterface {
    results: BankAccount
}


export interface BankAccount {
    id?: ObjectId
    account_number: number
    bank: object
    client: ClientInterface
    type_bank_account: object
}
