import { ObjectId } from "mongoose";

export interface ClientInterface {
    results: Client
}

export interface Client {
    _id?: ObjectId;
    first_name: string;
    second_name: string;
    last_name: string;
    second_last_name: string;
    email: string;
    phone: string;
    address: string;
    country: string;
    old_client: boolean;
    client_rate: number;
    client_type: object;
    registration_status: object;
}