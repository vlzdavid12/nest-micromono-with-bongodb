import { ObjectId } from "mongoose"
import { WalletInterface } from "./wallet.interface";
export interface TransferWalletInterface {
 results: TransferWallet
}

export interface TransferWallet {
    _id?: ObjectId
    sender_wallet: WalletInterface
    payment?: string
    receptor_wallet: WalletInterface
    is_xisfo_payment: boolean
    amount: number
    is_sending?: boolean
    is_active: boolean
    deleted_at: Date
}
