import {Injectable, NotFoundException} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginationDtoFilter } from 'apps/xisfo-wallet/src/commons/paginationDto.filter';
import mongoose, { Model } from 'mongoose';
import { CreateWalletDto } from './dto/create-wallet.dto';
import { WalletEntity } from './entities/wallet.entity';

@Injectable()
export class WalletService {

  constructor(
    @InjectModel(WalletEntity.name) private walletModel: Model<WalletEntity>
  ) { }

  async findAll(paginationQuery: PaginationDtoFilter) {
    const {limit = 12, offset = 1, orderBy, search} = paginationQuery;
    const order: any = (orderBy === 'ASC') ? {updatedAt: 1} : {updatedAt: -1};

    try{
      const count = await this.walletModel.count();
      const data = await this.walletModel.find({
        deleted_at: null
      })
      .skip((limit * offset) - limit)
      .limit(limit)
      .sort(order)
      .populate('client')
      .exec();

      return {data, limit, offset, count, orderBy}
    }catch (error){
      throw new NotFoundException();
    }


  }

  async findOne(id: string) {
    return await this.walletModel.findById(id).populate('client').exec();
  }

  async create(createWalletDto: CreateWalletDto) {
    const client = new mongoose.Types.ObjectId(createWalletDto.client);

    const walletData = {
      number_wallet: createWalletDto.number_wallet,
      amount_total: 0,
      client: client,
      data_policy: createWalletDto.data_policy,
      is_active: true,
      deleted_at: null
    }

    return await this.walletModel.create(walletData)
  }
}
