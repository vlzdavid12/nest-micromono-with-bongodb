import { Module } from '@nestjs/common';
import { WalletService } from './wallet.service';
import { WalletController } from './wallet.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { WalletEntity, WalletSchema } from './entities/wallet.entity';
import { ClientEntity, ClientSchema} from '../entities/client.entity';

@Module({
  imports:[
    MongooseModule.forFeature([
      {
        name: WalletEntity.name,
        schema: WalletSchema
      },
      {
        name: ClientEntity.name,
        schema: ClientSchema
      }
    ])
  ],
  controllers: [WalletController],
  providers: [WalletService]
})
export class WalletModule {}
