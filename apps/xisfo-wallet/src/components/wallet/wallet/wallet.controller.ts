import { Controller } from '@nestjs/common';
import { EventPattern, MessagePattern, Payload } from '@nestjs/microservices';
import { PaginationDtoFilter } from 'apps/xisfo-wallet/src/commons/paginationDto.filter';
import { CreateWalletDto } from './dto/create-wallet.dto';
import { WalletService } from './wallet.service';

@Controller()
export class WalletController {
  constructor(private readonly walletService: WalletService) { }

  @MessagePattern('findAllWallet')
  findAll(@Payload() paginationQuery: PaginationDtoFilter) {
    return this.walletService.findAll(paginationQuery);
  }

  @MessagePattern('findOneWallet')
  findOne(@Payload() id: string) {
    return this.walletService.findOne(id);
  }

  @EventPattern('createWallet')
  create(@Payload() createWalletDto: CreateWalletDto){
    return this.walletService.create(createWalletDto);
  }
}
