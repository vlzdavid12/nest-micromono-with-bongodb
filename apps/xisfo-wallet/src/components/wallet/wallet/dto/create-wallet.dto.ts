import { IsBoolean, IsNotEmpty, IsString,  } from "class-validator"

export class CreateWalletDto {

    @IsNotEmpty()
    @IsString()
    number_wallet: string

    @IsNotEmpty()
    @IsString()
    client: string

    @IsNotEmpty()
    @IsBoolean()
    data_policy: boolean
}
