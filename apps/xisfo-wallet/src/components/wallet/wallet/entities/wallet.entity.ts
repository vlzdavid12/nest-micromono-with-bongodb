import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose"
import mongoose, {Document} from "mongoose"
import { ClientEntity } from "../../entities/client.entity"


@Schema({ collection: 'wallets', timestamps: true })
export class WalletEntity extends Document {

    @Prop({ unique: true })
    number_wallet: string;

    @Prop()
    amount_total: number;

    @Prop({ type: mongoose.Types.ObjectId, ref: 'ClientEntity' })
    client: ClientEntity;

    @Prop({required: true})
    data_policy: boolean;

    @Prop({required: true})
    is_active: boolean;

    @Prop({required: false})
    deleted_at: Date;
}

export const WalletSchema = SchemaFactory.createForClass(WalletEntity)
    .plugin(require('mongoose-unique-validator'))
