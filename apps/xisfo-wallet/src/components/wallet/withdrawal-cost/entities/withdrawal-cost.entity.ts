import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

@Schema({ collection: 'withdrawal-costs', timestamps: true })
export class WithdrawalCostEntity extends Document {

    @Prop({ required: true })
    price: number

    @Prop({ required: true })
    iva: number

    @Prop({ required: true })
    deleted_at: Date

    @Prop({ required: true })
    is_active: boolean
}

export const WithdrawalCostSchema = SchemaFactory.createForClass(WithdrawalCostEntity);
