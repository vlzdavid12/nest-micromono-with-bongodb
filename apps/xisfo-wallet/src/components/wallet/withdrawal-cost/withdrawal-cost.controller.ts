import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { WithdrawalCostService } from './withdrawal-cost.service';

@Controller()
export class WithdrawalCostController {
  constructor(private readonly withdrawalCostService: WithdrawalCostService) {}

  @MessagePattern('findAllWithdrawalCost')
  findAll() {
    return this.withdrawalCostService.findAll();
  }

  @MessagePattern('findOneWithdrawalCost')
  findOne(@Payload() id: string) {
    return this.withdrawalCostService.findOne(id);
  }
}
