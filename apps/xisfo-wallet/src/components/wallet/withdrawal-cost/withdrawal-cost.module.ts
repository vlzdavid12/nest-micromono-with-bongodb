import { Module } from '@nestjs/common';
import { WithdrawalCostService } from './withdrawal-cost.service';
import { WithdrawalCostController } from './withdrawal-cost.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { WithdrawalCostEntity, WithdrawalCostSchema } from './entities/withdrawal-cost.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: WithdrawalCostEntity.name,
        schema: WithdrawalCostSchema,
      }
    ])
  ],
  controllers: [WithdrawalCostController],
  providers: [WithdrawalCostService]
})
export class WithdrawalCostModule {}