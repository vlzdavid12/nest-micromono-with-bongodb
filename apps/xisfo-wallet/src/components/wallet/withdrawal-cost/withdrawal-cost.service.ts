import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { WithdrawalCostEntity } from './entities/withdrawal-cost.entity';

@Injectable()
export class WithdrawalCostService {

  constructor(
    @InjectModel(WithdrawalCostEntity.name) private withdrawalCostModel: Model<WithdrawalCostEntity>
  ) { }

  async findAll() {
    return await this.withdrawalCostModel.find();
  }

  async findOne(id: string) {
    return await this.withdrawalCostModel.findById(id);
  }
}
