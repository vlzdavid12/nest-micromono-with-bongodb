import { Module } from '@nestjs/common';
import { WalletModule } from './wallet/wallet.module';
import { SeedModule } from './seed/seed.module';

@Module({
    imports: [WalletModule, SeedModule],
    exports: [WalletModule, SeedModule]
})
export class ComponentsModule {}
