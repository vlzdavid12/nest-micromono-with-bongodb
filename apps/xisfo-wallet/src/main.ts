import {NestFactory} from '@nestjs/core';
import {XisfoWalletModule} from './xisfo-wallet.module';
import {ValidationPipe} from "@nestjs/common";
import {MicroserviceOptions, Transport} from "@nestjs/microservices";

async function bootstrap() {
    const app = await NestFactory.createMicroservice<MicroserviceOptions>(
        XisfoWalletModule,
        {
            transport: Transport.TCP,
            options: {
                port: 3003,
            },
        },
    );

    app.useGlobalPipes(
        new ValidationPipe({
            whitelist: true,
            forbidNonWhitelisted: true
        })
    )

    await app.listen();
}

bootstrap();
