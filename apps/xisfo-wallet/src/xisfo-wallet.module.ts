import {Module} from '@nestjs/common';
import {ComponentsModule} from './components/components.module';
import {MongooseModule} from "@nestjs/mongoose";
import { WalletModule } from './components/wallet/wallet.module';
import { CommandModule } from 'nestjs-command';

@Module({
    imports: [
        ComponentsModule,
        MongooseModule.forRoot('mongodb://localhost:27017/xisfo'),
        WalletModule,
        CommandModule,
    ],
})
export class XisfoWalletModule {
}
