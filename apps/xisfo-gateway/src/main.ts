import {NestFactory} from '@nestjs/core';
import {Logger, UseInterceptors, ValidationPipe} from "@nestjs/common";
import {NestExpressApplication} from '@nestjs/platform-express';
import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";
import {join} from 'path';
import {AppModule} from './app.module';
import {TransformInterceptor} from "./filter/transform.interceptor";
import { HttpExceptionFilter } from './filter/exception.filter';

async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule);
    const logger = new Logger('Bootstrap');

    //This is for serve static images
    app.useStaticAssets(join(process.cwd(), './apps/xisfo-gateway/public'), {
        prefix: '/public/'
    });

    // app.useGlobalFilters(new HttpExceptionFilter());

    app.setGlobalPrefix('api');
    app.enableCors();
    app.useGlobalPipes(
        new ValidationPipe({
            whitelist: true,
            forbidNonWhitelisted: true
        })
    );
    app.useGlobalInterceptors(new TransformInterceptor());

    const config = new DocumentBuilder()
        .setTitle('Xisfo Fintech')
        .setDescription('API microservice of xisfo')
        .setVersion('1.0')
        .addSecurity('bearer ', {
            type: 'http',
            scheme: 'bearer'
        })
        .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);

    await app.listen(process.env.PORT);
    logger.log(`App running on port ${process.env.PORT}`)
}

bootstrap();
