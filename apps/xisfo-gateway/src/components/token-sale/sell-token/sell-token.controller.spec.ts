import { Test, TestingModule } from '@nestjs/testing';
import { SellTokenController } from './sell-token.controller';
import { SellTokenService } from './sell-token.service';

describe('SellTokenController', () => {
  let controller: SellTokenController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SellTokenController],
      providers: [SellTokenService],
    }).compile();

    controller = module.get<SellTokenController>(SellTokenController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
