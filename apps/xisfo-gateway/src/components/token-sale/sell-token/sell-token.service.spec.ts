import { Test, TestingModule } from '@nestjs/testing';
import { SellTokenService } from './sell-token.service';

describe('SellTokenService', () => {
  let service: SellTokenService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SellTokenService],
    }).compile();

    service = module.get<SellTokenService>(SellTokenService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
