import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateSellTokenDto } from './dto/create-sell-token.dto';
import { UpdateSellTokenDto } from './dto/update-sell-token.dto';
import {PaginationDto} from "../../../commons/paginationDto";
import { ConfigService } from '@nestjs/config';

@Injectable()
export class SellTokenService {
  constructor(
    @Inject('XISFO_PAYMENTS') private clientXisfo: ClientProxy,
    private readonly configService: ConfigService
    ) { }
  
  private storage_images:string = this.configService.get('URI_API') + this.configService.get('IMAGES_GATEWAY')

  create(createSellTokenDto: CreateSellTokenDto, files: {
    invoice?:         Express.Multer.File[],
    payment_support?:  Express.Multer.File[],
   }) {
    // This take the name of each images and put on it the app's url
    const invoice = this.storage_images + files.invoice[0].fieldname;
    const payment_support = this.storage_images +  files.payment_support[0].fieldname;

    // This will concatenate all the images to the dto
    const dataSellTokens = { invoice, payment_support, ...createSellTokenDto }

    return this.clientXisfo.send('createSellToken', dataSellTokens);
  }
  
  findAll(paginationDto: PaginationDto) {
    return this.clientXisfo.send('findAllSellToken', paginationDto);
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOneSellToken', id);
  }

  update(id: string, updateSellTokenDto: UpdateSellTokenDto) {
    return this.clientXisfo.send('updateSellToken', {id, ...updateSellTokenDto});
  }

  remove(id: string) {
    return this.clientXisfo.send('removeSellToken', id);
  }
}
