import { PartialType } from '@nestjs/mapped-types';
import { CreateSellTokenDto } from './create-sell-token.dto';

export class UpdateSellTokenDto extends PartialType(CreateSellTokenDto) {}
