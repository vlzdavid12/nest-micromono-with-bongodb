import { Module } from '@nestjs/common';
import { SellTokenService } from './sell-token.service';
import { SellTokenController } from './sell-token.controller';
import { ConfigModule } from '@nestjs/config';
import { ClientsXisfoModules } from '../../microservices/payments';
import { MulterModule } from '@nestjs/platform-express';
import { fileFilter } from 'apps/xisfo-gateway/src/helpers/file-filter.helper';
import { diskStorage } from 'multer';
import { fileNamer } from 'apps/xisfo-gateway/src/helpers/file-namer.helper';

@Module({
  imports: [
    ConfigModule,
    MulterModule.register(
      {
        fileFilter: fileFilter,
        storage: diskStorage({
          destination: function (req, file, callback) {
            callback(null, './apps/xisfo-gateway/public')
          },
          filename: fileNamer
        })
    }),
    ClientsXisfoModules,
  ],
  controllers: [SellTokenController],
  providers: [SellTokenService]
})
export class SellTokenModule {}
