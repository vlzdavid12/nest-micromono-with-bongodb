import {Controller, Get, Post, Body, Patch, Param, Delete, Query, UseInterceptors, UploadedFiles} from '@nestjs/common';
import { SellTokenService } from './sell-token.service';
import { CreateSellTokenDto } from './dto/create-sell-token.dto';
import { UpdateSellTokenDto } from './dto/update-sell-token.dto';
import {ApiTags} from "@nestjs/swagger";
import {PaginationDto} from "../../../commons/paginationDto";
import { FileFieldsInterceptor } from '@nestjs/platform-express';

@ApiTags('Sell Token')
@Controller('sell-token')
export class SellTokenController {
  constructor(private readonly sellTokenService: SellTokenService) {}

  @Post()
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'invoice', maxCount: 1 },
    { name: 'payment_support', maxCount: 1 }
  ]))
  create(@Body() createSellTokenDto: CreateSellTokenDto,
  @UploadedFiles() files: {
    invoice?: Express.Multer.File[],
    payment_support?: Express.Multer.File[]
  }) {
    return this.sellTokenService.create(createSellTokenDto, files);
  }

  @Get()
  findAll(@Query() paginationDto: PaginationDto) {
    return this.sellTokenService.findAll(paginationDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.sellTokenService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSellTokenDto: UpdateSellTokenDto) {
    return this.sellTokenService.update(id, updateSellTokenDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.sellTokenService.remove(id);
  }
}
