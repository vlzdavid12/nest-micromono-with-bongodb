import {Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFiles, Query} from '@nestjs/common';
import { PaymentRequestService } from './payment-request.service';
import { CreatePaymentRequestDto } from './dto/create-payment-request.dto';
import { UpdatePaymentRequestDto } from './dto/update-payment-request.dto';
import {ApiTags} from "@nestjs/swagger";
import { CreateUnidentifiedPaymentRequestDto } from './dto/create-unidentified-payment-request';
import { UpdateUnidentifiedPaymentRequestDto } from './dto/update-unidentified-payment-request.dto';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import {PaginationDtoSearch} from "../../../commons/paginationDto.filter";
import { PaginationDto } from 'apps/xisfo-gateway/src/commons/paginationDto';


@ApiTags('Payment Request')
@Controller('payment-request')
export class PaymentRequestController {
  constructor(private readonly paymentRequestService: PaymentRequestService) {}

  @Post()
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'invoice', maxCount: 1 },
    { name: 'payment_support', maxCount: 1 }
  ]))
  create(
    @Body() createPaymentRequestDto: CreatePaymentRequestDto,
    @UploadedFiles() files: {
      invoice?: Express.Multer.File[],
      payment_support?: Express.Multer.File[]
    }
    ) {
    return this.paymentRequestService.create(createPaymentRequestDto, files);
  }

  @Post('create-unidentified-payment-request')
  createUnidentifiedPaymentRequest(@Body() createUnidentifiedPaymentRequestDto: CreateUnidentifiedPaymentRequestDto) {
    return this.paymentRequestService.createUnidentifiedPaymentRequest(createUnidentifiedPaymentRequestDto);
  }

  @Get()
  findAll(@Query() paginationDtoSearch: PaginationDtoSearch) {
    return this.paymentRequestService.findAll(paginationDtoSearch);
  }

  @Get('find-all-clients-payment-request')
  findAllClientsPaymentRequest(@Query() paginationDto: PaginationDto, @Param() client: string) {
    return this.paymentRequestService.findAllClientsPaymentRequest(client, paginationDto);
  }

  @Get('find-all-unidentified-payment-requests')
  findAllUnidentifiedPaymentRequests(@Query() paginationDtoFilter: PaginationDtoSearch) {
    return this.paymentRequestService.findAllUnidentifiedPaymentRequests(paginationDtoFilter);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.paymentRequestService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePaymentRequestDto: UpdatePaymentRequestDto) {
    return this.paymentRequestService.update(id, updatePaymentRequestDto);
  }

  @Patch('identify-unidentified-payment-request/:id')
  identifyUnidentifiedPaymentRequest(@Param('id') id: string, @Body() identifyUnidentifiedPaymentRequestDto: UpdateUnidentifiedPaymentRequestDto) {
    return this.paymentRequestService.identifyUnidentifiedPaymentRequest(id, identifyUnidentifiedPaymentRequestDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.paymentRequestService.remove(id);
  }
}
