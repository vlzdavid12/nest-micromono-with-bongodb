import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsNotEmpty, IsOptional, IsString } from "class-validator";


export class UpdateUnidentifiedPaymentRequestDto {

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    client_platform: string;

    @IsBoolean()
    @IsOptional()
    is_identified: boolean;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    invoice: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    payment_support: string;
}
