import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { CreatePaymentRequestDto } from './create-payment-request.dto';

export class UpdatePaymentRequestDto extends PartialType(CreatePaymentRequestDto) {}
