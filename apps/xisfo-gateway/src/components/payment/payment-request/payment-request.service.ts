import {Injectable, Inject} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import { PaginationDto } from 'apps/xisfo-gateway/src/commons/paginationDto';
import { CreatePaymentRequestDto } from './dto/create-payment-request.dto';
import { CreateUnidentifiedPaymentRequestDto } from './dto/create-unidentified-payment-request';
import { UpdatePaymentRequestDto } from './dto/update-payment-request.dto';
import { UpdateUnidentifiedPaymentRequestDto } from './dto/update-unidentified-payment-request.dto';
import {PaginationDtoSearch} from "../../../commons/paginationDto.filter";


@Injectable()
export class PaymentRequestService {
  constructor(
    @Inject('XISFO_PAYMENTS') private clientXisfo: ClientProxy,
    private readonly configService: ConfigService
  ) { }

  private storage_images:string = this.configService.get('URI_API') + this.configService.get('IMAGES_GATEWAY')

  create(createPaymentRequestDto: CreatePaymentRequestDto,  files: {
    invoice?:         Express.Multer.File[],
    payment_support?:  Express.Multer.File[],
  }) {
    // This take the name of each images and put on it the app's url
    const invoice = this.storage_images + files.invoice[0].filename;
    const payment_support = this.storage_images +  files.payment_support[0].filename;
  
    // This will concatenate all the images to the dto
    const datapaymentRequest = { invoice, payment_support, ...createPaymentRequestDto }
    return this.clientXisfo.send('createPaymentRequest', datapaymentRequest);
  }

  findAllClientsPaymentRequest(client: string, paginationDto: PaginationDto) {
    return this.clientXisfo.send('findAllClientsPaymentRequest', {client, paginationDto});
  }

  findAll(paginationDtoFilter: PaginationDtoSearch) {
    return this.clientXisfo.send('findAllPaymentRequest', paginationDtoFilter);
  }

  findAllUnidentifiedPaymentRequests(paginationDtoFilter: PaginationDtoSearch) {
    return this.clientXisfo.send('findAllUnidentifiedPaymentRequests', paginationDtoFilter);
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOnePaymentRequest', id);
  }

  update(id: string, updatePaymentRequestDto: UpdatePaymentRequestDto) {
    return this.clientXisfo.send('updatePaymentRequest', {id, ...updatePaymentRequestDto});
  }

  remove(id: string) {
    return this.clientXisfo.send('removePaymentRequest', id);
  }

  createUnidentifiedPaymentRequest(createUnidentifiedPaymentRequestDto: CreateUnidentifiedPaymentRequestDto) {    
    return this.clientXisfo.send('createUnidentifiedPaymentRequest', createUnidentifiedPaymentRequestDto);
  }

  identifyUnidentifiedPaymentRequest(id: string, identifyUnidentifiedPaymentRequestDto: UpdateUnidentifiedPaymentRequestDto) {
    return this.clientXisfo.send('identifyUnidentifiedPaymentRequest', {id, ...identifyUnidentifiedPaymentRequestDto});
  }
}
