import { Controller, Get, Post, Body, Patch, Param, Delete, Query} from '@nestjs/common';
import { PaymentService } from './payment.service';
import { CreatePaymentDto } from './dto/create-payment.dto';
import { UpdatePaymentDto } from './dto/update-payment.dto';
import {ApiTags} from "@nestjs/swagger";
import {PaginationDtoSearch} from "../../../commons/paginationDto.filter";
import { PaginationDto } from 'apps/xisfo-gateway/src/commons/paginationDto';

@ApiTags('Payment')
@Controller('payment')
export class PaymentController {
  constructor(private readonly paymentService: PaymentService) {}

  @Post()
  create(@Body() createPaymentDto: CreatePaymentDto) {
    return this.paymentService.create(createPaymentDto);
  }

  @Get()
  findAll(@Query() paginationDtoSearch: PaginationDtoSearch) {
    return this.paymentService.findAll(paginationDtoSearch);
  }

  @Get('find-all-in-management-payments')
  findAllInManagementPayments(@Query() paginationDto: PaginationDto) {
    return this.paymentService.findAllInManagementPayments(paginationDto);
  }

  @Get('payments-grouped-by-giro')
  paymentsGroupedByGiro(@Query() paginationDto: PaginationDto) {
    return this.paymentService.paymentsGroupedByGiro(paginationDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.paymentService.findOne(id);
  }

  @Get('find-payments-by-giro/:giro')
  findPaymentsByGiro(@Query() paginationDto: PaginationDto, @Param('giro') giro: number) {
    return this.paymentService.findPaymentsByGiro(giro, paginationDto);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePaymentDto: UpdatePaymentDto) {
    return this.paymentService.update(id, updatePaymentDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.paymentService.remove(id);
  }

  @Patch('compute-values-trm/:massivePayment')
  computeValuesTrm(@Param('massivePayment') massive_payment: number, @Body() trm: any) { 
    return this.paymentService.computeValuesTrm(massive_payment, trm);
  }

  @Post('close-payments-management')
  closePaymentsManagement() {
    return this.paymentService.closePaymentsManagement();
  }

  @Post('export-payment/:massivePayment')
  exportPaymentExcel(@Param('massivePayment') massive_payment: number) {
    return this.paymentService.downloadExcel(massive_payment);
  }
}
