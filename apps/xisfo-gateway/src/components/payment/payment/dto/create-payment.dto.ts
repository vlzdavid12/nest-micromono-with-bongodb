import { IsBoolean, IsDecimal, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreatePaymentDto {
    @ApiProperty()
    @IsDecimal()
    @IsOptional()
    @IsNotEmpty()
    client_total: number;

    @ApiProperty()
    @IsDecimal()
    @IsNotEmpty()
    @IsOptional()
    client_value_usd: number;

    @ApiProperty()
    @IsOptional()
    @IsDecimal()
    xisfo_commission_general_usd: number;

    @ApiProperty()
    @IsOptional()
    @IsDecimal()
    trm: number;

    @ApiProperty()
    @IsOptional()
    @IsDecimal()
    subtotal_usd:number;

    @ApiProperty()
    @IsOptional()
    @IsDecimal()
    subtotal_cop:number;

    @ApiProperty()
    @IsOptional()
    @IsDecimal()
    bank_commission: number;

    @ApiProperty()
    @IsOptional()
    @IsDecimal()
    platform_commission: number;

    @ApiProperty()
    @IsOptional()
    @IsBoolean()
    is_immediate: boolean;

    @ApiProperty()
    @IsOptional()
    @IsBoolean()
    is_paid: boolean;

    @ApiProperty()
    @IsOptional()
    @IsBoolean()
    in_management: boolean;

    @ApiProperty()
    @IsOptional()
    @IsNumber()
    massive_payment: number;

    @ApiProperty()
    @IsOptional()
    @IsDecimal()
    xisfo_commission_usa: number;

    @ApiProperty()
    @IsOptional()
    @IsDecimal()
    with_holding_tax: number;

    @ApiProperty()
    @IsOptional()
    @IsDecimal()
    client_pay: number;

    @ApiProperty()
    @IsOptional()
    @IsDecimal()
    tax: number;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    payment_request: string;

}
