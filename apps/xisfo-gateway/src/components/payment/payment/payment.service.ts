import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreatePaymentDto } from './dto/create-payment.dto';
import { UpdatePaymentDto } from './dto/update-payment.dto';
import {PaginationDtoSearch} from "../../../commons/paginationDto.filter";
import { PaginationDto } from 'apps/xisfo-gateway/src/commons/paginationDto';

@Injectable()
export class PaymentService {
  constructor(@Inject('XISFO_PAYMENTS') private clientXisfo: ClientProxy) { }

  create(createPaymentDto: CreatePaymentDto) {
    return this.clientXisfo.send('createPayment', createPaymentDto);
  }

  findAll(paginationDtoSearch: PaginationDtoSearch ) {
    return this.clientXisfo.send('findAllPayment', paginationDtoSearch);
  }

  findAllInManagementPayments(paginationDto: PaginationDto ) {
    return this.clientXisfo.send('findAllInManagementPayments', paginationDto);
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOnePayment', id);
  }

  findPaymentsByGiro(id: number, paginationDto: PaginationDto) {
    return this.clientXisfo.send('findPaymentsByGiro', {id, paginationDto});
  }

  paymentsGroupedByGiro(paginationDto: PaginationDto) {
    return this.clientXisfo.send('paymentsGroupedByGiro', paginationDto);
  }

  update(id: string, updatePaymentDto: UpdatePaymentDto) {
    return this.clientXisfo.send('updatePayment', {id, ...updatePaymentDto});
  }

  remove(id: string) {
    return this.clientXisfo.send('removePayment', id);
  }

  computeValuesTrm(massive_payment: number, trm: any) {
    return this.clientXisfo.send('computeValuesTrm', {massive_payment, ...trm});
  }

  closePaymentsManagement() {
    return this.clientXisfo.send('closePaymentsManagement', '');
  }

  downloadExcel(massive_payment) {
    return this.clientXisfo.send('exportPayment', massive_payment);
  }


}
