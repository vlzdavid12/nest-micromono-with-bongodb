import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { ClientPlatformService } from './client-platform.service';
import { CreateClientPlatformDto } from './dto/create-client-platform.dto';
import { UpdateClientPlatformDto } from './dto/update-client-platform.dto';
import {ApiTags} from "@nestjs/swagger";
import { PaginationDtoSearch } from 'apps/xisfo-gateway/src/commons/paginationDto.filter';
import { PaginationDto } from 'apps/xisfo-gateway/src/commons/paginationDto';

@ApiTags('Client Platform')
@Controller('client-platform')
export class ClientPlatformController {
  constructor(private readonly clientPlatformService: ClientPlatformService) {}

  @Post()
  create(@Body() createClientPlatformDto: CreateClientPlatformDto) {
    return this.clientPlatformService.create(createClientPlatformDto);
  }

  @Get('find-all-unidentified-client-platforms')
  findAllUnidentifiedClientPlatforms(@Query() paginationDtoSearch : PaginationDtoSearch) {
    return this.clientPlatformService.findAllUnidentifiedClientPlatforms(paginationDtoSearch);
  }

  @Get()
  findAll(@Query() paginationDtoSearch : PaginationDtoSearch) {
    return this.clientPlatformService.findAll(paginationDtoSearch);
  }

  @Get('find-all-platforms-client/:client')
  findAllPlatformsClient(@Query() paginationDto: PaginationDto, @Param('client') client: string) {
    return this.clientPlatformService.findAllPlatformsClient(client, paginationDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.clientPlatformService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateClientPlatformDto: UpdateClientPlatformDto) {
    return this.clientPlatformService.update(id, updateClientPlatformDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.clientPlatformService.remove(id);
  }

  @Post('create-unidentified-client')
  createUnidentifiedClient() {
    return this.clientPlatformService.createUnidentifiedClient();
  }
}
