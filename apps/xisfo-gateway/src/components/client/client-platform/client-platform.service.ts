import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateClientPlatformDto } from './dto/create-client-platform.dto';
import { UpdateClientPlatformDto } from './dto/update-client-platform.dto';
import {PaginationDtoSearch} from "../../../commons/paginationDto.filter";
import { PaginationDto } from 'apps/xisfo-gateway/src/commons/paginationDto';

@Injectable()
export class ClientPlatformService {
  constructor(@Inject('XISFO_PAYMENTS') private clientXisfo: ClientProxy) { }

  create(createClientPlatformDto: CreateClientPlatformDto) {
    return this.clientXisfo.send('createClientPlatform', createClientPlatformDto);
  }

  findAll(paginationSearch: PaginationDtoSearch) {
    return this.clientXisfo.send('findAllClientPlatform', paginationSearch);
  }

  findAllUnidentifiedClientPlatforms(paginationSearch: PaginationDtoSearch) {
    return this.clientXisfo.send('findAllUnidentifiedClientPlatforms', paginationSearch);
  }

  findAllPlatformsClient(client: string, paginationDto: PaginationDtoSearch) {
    return this.clientXisfo.send('findAllPlatformsClient', {client, paginationDto});
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOneClientPlatform', id);
  }

  update(id: string, updateClientPlatformDto: UpdateClientPlatformDto) {
    return this.clientXisfo.send('updateClientPlatform', {id, ...updateClientPlatformDto});
  }

  remove(id: string) {
    return this.clientXisfo.send('removeClientPlatform', id);
  }

  createUnidentifiedClient() {
    return this.clientXisfo.send('createUnidentifiedClient', '');
  }
}
