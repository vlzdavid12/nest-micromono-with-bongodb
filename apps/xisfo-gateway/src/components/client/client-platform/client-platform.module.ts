import { Module } from '@nestjs/common';
import { ClientPlatformService } from './client-platform.service';
import { ClientPlatformController } from './client-platform.controller';
import { ConfigModule } from '@nestjs/config';
import { ClientsXisfoModules } from '../../microservices/payments';

@Module({
  imports: [
    ConfigModule,
    ClientsXisfoModules,
  ],
  controllers: [ClientPlatformController],
  providers: [ClientPlatformService]
})
export class ClientPlatformModule {}
