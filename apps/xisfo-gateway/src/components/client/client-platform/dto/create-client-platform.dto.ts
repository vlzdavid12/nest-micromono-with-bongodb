import { IsNotEmpty, IsString } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateClientPlatformDto {

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    account_name: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    client: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    platform: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    nicho: string;
}
