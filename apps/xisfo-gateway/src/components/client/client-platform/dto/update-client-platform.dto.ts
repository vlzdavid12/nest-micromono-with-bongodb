import { PartialType } from '@nestjs/mapped-types';
import { CreateClientPlatformDto } from './create-client-platform.dto';

export class UpdateClientPlatformDto extends PartialType(CreateClientPlatformDto) {}
