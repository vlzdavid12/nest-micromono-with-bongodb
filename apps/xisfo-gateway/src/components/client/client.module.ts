import { Module } from '@nestjs/common';
import { ClientModule as Client } from './client/client.module';
import { ClientTypeModule } from './client-type/client-type.module';
import { RegistrationStatusModule } from './registration-status/registration-status.module';
import { ClientIdentificationModule } from './client-identification/client-identification.module';
import { ClientPlatformModule } from './client-platform/client-platform.module';
import { BankAccountModule } from './bank_account/bank_account.module';
import { ClientCompanyModule } from './client-company/client-company.module';

@Module({
  imports: [Client, ClientTypeModule, RegistrationStatusModule, ClientIdentificationModule, ClientPlatformModule, BankAccountModule, ClientCompanyModule],
  exports: [Client, ClientTypeModule, RegistrationStatusModule, ClientIdentificationModule, ClientPlatformModule],
})
export class ClientModule {}
