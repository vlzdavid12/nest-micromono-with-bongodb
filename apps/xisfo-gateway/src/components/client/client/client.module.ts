import { Module } from '@nestjs/common';
import { ClientService } from './client.service';
import { ClientController } from './client.controller';
import { ConfigModule } from '@nestjs/config';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [
    ConfigModule,
    ClientsXisfoModules,
  ],
  controllers: [ClientController],
  providers: [ClientService]
})
export class ClientModule {}
