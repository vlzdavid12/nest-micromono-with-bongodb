import {Controller, Get, Post, Body, Patch, Param, Delete, Query} from '@nestjs/common';
import { ClientService } from './client.service';
import { CreateClientDto } from './dto/create-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';
import {ApiTags} from "@nestjs/swagger";
import {PaginationDto} from "../../../commons/paginationDto";
import {PaginationDtoSearch} from "../../../commons/paginationDto.filter";
import {PaginationDtoAdvancedClientsFilter} from "../../../commons/filters/paginationDtoAdvancedClients.filter";

@ApiTags('Client')
@Controller('client')
export class ClientController {
  constructor(private readonly clientService: ClientService) {}

  @Post()
  create(@Body() createClientDto: CreateClientDto) {
    return this.clientService.create(createClientDto);
  }

  // @Auth( [ValidRoles.admin, ValidRoles.user], [ValidPermission.read])
  @Get()
  findAll(@Query() paginationDto: PaginationDto) {
    return this.clientService.findAll(paginationDto);
  }

  @Get('find-all-foreing-currency-clients')
  findAllForeingCurrencyClients(@Query() paginationDto: PaginationDto) {
    return this.clientService.findAllForeingCurrencyClients(paginationDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.clientService.findOne(id);
  }

  @Get('/find-client-bank-account/:id')
  findClientBankAccount(@Param('id') id: string) {
    return this.clientService.findClientBankAccount(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateClientDto: UpdateClientDto) {
    return this.clientService.update(id, updateClientDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.clientService.remove(id);
  }

  @Get('filter/search')
  filterClient(@Query() paginationDtoSearch: PaginationDtoSearch) {
    return this.clientService.filterClient(paginationDtoSearch);
  }

  @Get('filter/advanced/search')
  filterAdvanced(@Query() paginationFilterAdvanced: PaginationDtoAdvancedClientsFilter) {
    return this.clientService.filterAdvanced(paginationFilterAdvanced);
  }

}
