import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateClientDto } from './dto/create-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';
import {PaginationDto} from "../../../commons/paginationDto";
import {PaginationDtoAdvancedClientsFilter} from "../../../commons/filters/paginationDtoAdvancedClients.filter";
import {PaginationDtoSearch} from "../../../commons/paginationDto.filter";

@Injectable()
export class ClientService {
  constructor(@Inject('XISFO_CLIENTS') private clientXisfo: ClientProxy) { }

  create(createClientDto: CreateClientDto) {
    return this.clientXisfo.send('createClient', createClientDto);
  }

  findAll(paginationDto: PaginationDto) {
    return this.clientXisfo.send('findAllClient', paginationDto);
  }
  
  findAllForeingCurrencyClients(paginationDto: PaginationDto) {
    return this.clientXisfo.send('findAllForeingCurrencyClients', paginationDto);
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOneClient', id);
  }

  findClientBankAccount(id: string) {
    return this.clientXisfo.send('findClientBankAccount', id);
  }

  update(id: string, updateClientDto: UpdateClientDto) {
    return this.clientXisfo.send('updateClient', {id, ...updateClientDto});
  }

  remove(id: string) {
    return this.clientXisfo.send('removeClient', id);
  }


  filterClient(search: PaginationDtoSearch){
    console.log("gog")
    return this.clientXisfo.send('filterClient', search);
  }


  filterAdvanced(searchAdvanced: PaginationDtoAdvancedClientsFilter){
    return this.clientXisfo.send('filterAdvancedClient', searchAdvanced)
  }
}
