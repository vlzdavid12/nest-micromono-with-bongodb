import {
    IsBoolean,
    IsDecimal,
    IsEmail,
    IsNotEmpty,
    IsNumberString, IsObject,
    IsOptional,
    IsString,
    MinLength
} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateClientDto {

    @ApiProperty()
    @IsString()
    @MinLength(3)
    @IsNotEmpty()
    first_name: string;

    @ApiProperty()
    @IsString()
    @MinLength(3)
    @IsOptional()
    second_name: string;

    @ApiProperty()
    @IsString()
    @MinLength(3)
    @IsNotEmpty()
    last_name: string;

    @ApiProperty()
    @IsString()
    @MinLength(3)
    @IsOptional()
    second_last_name: string;

    @ApiProperty()
    @IsEmail()
    @IsNotEmpty()
    email: string;

    @ApiProperty()
    @IsNumberString()
    @IsNotEmpty()
    phone: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    address: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    country: string;

    @ApiProperty()
    @IsNotEmpty()
    client_type: string;

    @ApiProperty()
    @IsNotEmpty()
    registration_status: string;

    @ApiProperty()
    @IsBoolean()
    @IsNotEmpty()
    with_holding_tax: boolean;

    @ApiProperty()
    @IsDecimal()
    client_rate: string;

    @ApiProperty()
    @IsBoolean()
    @IsNotEmpty()
    old_client: boolean;

    @IsString()
    @IsOptional()
    client_company_id: string;

    @IsBoolean()
    @IsNotEmpty()
    data_policy: boolean;
}
