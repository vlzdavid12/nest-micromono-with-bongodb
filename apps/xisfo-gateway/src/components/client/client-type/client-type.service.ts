import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class ClientTypeService {
  constructor(@Inject('XISFO_CLIENTS') private clientXisfo: ClientProxy) { }

  findAll() {
    return this.clientXisfo.send('findAllClientType', '');
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOneClientType', id);
  }
}