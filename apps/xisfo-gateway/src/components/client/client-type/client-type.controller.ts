import { Controller, Get, Param, } from '@nestjs/common';
import { ClientTypeService } from './client-type.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Client Type')
@Controller('client-type')
export class ClientTypeController {
  constructor(private readonly clientTypeService: ClientTypeService) {}

  @Get()
  findAll() {
    return this.clientTypeService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.clientTypeService.findOne(id);
  }
}
