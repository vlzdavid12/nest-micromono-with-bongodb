import { Module } from '@nestjs/common';
import { ClientTypeService } from './client-type.service';
import { ClientTypeController } from './client-type.controller';
import { ConfigModule } from '@nestjs/config';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [
    ConfigModule,
    ClientsXisfoModules,
  ],
  controllers: [ClientTypeController],
  providers: [ClientTypeService]
})
export class ClientTypeModule {}