import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class RegistrationStatusService {
  constructor(@Inject('XISFO_CLIENTS') private clientXisfo: ClientProxy) { }

  findAll() {
    return this.clientXisfo.send('findAllRegistrationStatus', '');
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOneRegistrationStatus', id);
  }
}
