import { Module } from '@nestjs/common';
import { RegistrationStatusService } from './registration-status.service';
import { RegistrationStatusController } from './registration-status.controller';
import { ConfigModule } from '@nestjs/config';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [
    ConfigModule,
    ClientsXisfoModules,
  ],
  controllers: [RegistrationStatusController],
  providers: [RegistrationStatusService]
})
export class RegistrationStatusModule {}
