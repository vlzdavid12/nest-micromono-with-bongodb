import { Controller, Get, Param } from '@nestjs/common';
import { RegistrationStatusService } from './registration-status.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Registration Status')
@Controller('registration-status')
export class RegistrationStatusController {
  constructor(private readonly registrationStatusService: RegistrationStatusService) {}

  @Get()
  findAll() {
    return this.registrationStatusService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.registrationStatusService.findOne(id);
  }
}
