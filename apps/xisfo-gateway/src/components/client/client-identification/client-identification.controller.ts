import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ClientIdentificationService } from './client-identification.service';
import { CreateClientIdentificationDto } from './dto/create-client-identification.dto';
import { UpdateClientIdentificationDto } from './dto/update-client-identification.dto';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Client Identification')
@Controller('client-identification')
export class ClientIdentificationController {
  constructor(private readonly clientIdentificationService: ClientIdentificationService) {}

  @Post()
  create(@Body() createClientIdentificationDto: CreateClientIdentificationDto) {
    return this.clientIdentificationService.create(createClientIdentificationDto);
  }

  @Get()
  findAll() {
    return this.clientIdentificationService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.clientIdentificationService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateClientIdentificationDto: UpdateClientIdentificationDto) {
    return this.clientIdentificationService.update(id, updateClientIdentificationDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.clientIdentificationService.remove(id);
  }
}
