import { IsNotEmpty, IsNotEmptyObject, IsNumber, IsObject } from "class-validator";
import { Date } from "mongoose";
import {ApiProperty} from "@nestjs/swagger";

export class CreateClientIdentificationDto {

    @ApiProperty()
    @IsNotEmpty()
    identification_number: number;

    @ApiProperty()
    @IsNotEmpty()
    expedition_date: Date;

    @ApiProperty()
    @IsNotEmptyObject()
    @IsObject()
    client: object;

    @ApiProperty()
    @IsNotEmptyObject()
    @IsObject()
    document_type: object;
}
