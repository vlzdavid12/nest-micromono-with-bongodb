import { PartialType } from '@nestjs/mapped-types';
import { CreateClientIdentificationDto } from './create-client-identification.dto';

export class UpdateClientIdentificationDto extends PartialType(CreateClientIdentificationDto) {}
