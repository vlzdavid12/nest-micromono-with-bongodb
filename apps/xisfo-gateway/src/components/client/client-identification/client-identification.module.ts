import { Module } from '@nestjs/common';
import { ClientIdentificationService } from './client-identification.service';
import { ClientIdentificationController } from './client-identification.controller';
import { ConfigModule } from '@nestjs/config';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [
    ConfigModule,
    ClientsXisfoModules,
  ],
  controllers: [ClientIdentificationController],
  providers: [ClientIdentificationService]
})
export class ClientIdentificationModule {}
