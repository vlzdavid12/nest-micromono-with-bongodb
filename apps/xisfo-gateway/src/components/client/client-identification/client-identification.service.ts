import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateClientIdentificationDto } from './dto/create-client-identification.dto';
import { UpdateClientIdentificationDto } from './dto/update-client-identification.dto';

@Injectable()
export class ClientIdentificationService {
  constructor(@Inject('XISFO_CLIENTS') private clientXisfo: ClientProxy) { }

  create(createClientIdentificationDto: CreateClientIdentificationDto) {
    return this.clientXisfo.send('createClientIdentification', createClientIdentificationDto);
  }

  findAll() {
    return this.clientXisfo.send('findAllClientIdentification', '');
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOneClientIdentification', id);
  }

  update(id: string, updateClientIdentificationDto: UpdateClientIdentificationDto) {
    return this.clientXisfo.send('updateClientIdentification', {id, ...updateClientIdentificationDto});
  }

  remove(id: string) {
    return this.clientXisfo.send('removeClientIdentification', id);
  }
}
