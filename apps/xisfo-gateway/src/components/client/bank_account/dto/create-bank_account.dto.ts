import { IsNotEmpty, IsNumber, IsString } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateBankAccountDto {
    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    account_number: number;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    bank: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    client: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    type_bank_account: string;
}
