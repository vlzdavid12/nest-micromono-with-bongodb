import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateBankAccountDto } from './dto/create-bank_account.dto';
import { UpdateBankAccountDto } from './dto/update-bank_account.dto';

@Injectable()
export class BankAccountService {
  constructor(@Inject('XISFO_CLIENTS') private clientXisfo: ClientProxy) { }

  create(createBankAccountDto: CreateBankAccountDto) {
    return this.clientXisfo.send('createBankAccount', createBankAccountDto);
  }

  findAll() {
     return this.clientXisfo.send('findAllBankAccount', '');
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOneBankAccount', id);
  }

  update(id: string, updateBankAccountDto: UpdateBankAccountDto) {
    return this.clientXisfo.send('updateBankAccount', {id, ...updateBankAccountDto});
  }

  remove(id: string) {
    return this.clientXisfo.send('removeBankAccount', id);
  }
}
