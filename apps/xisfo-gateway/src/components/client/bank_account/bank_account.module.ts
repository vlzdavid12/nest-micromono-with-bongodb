import { Module } from '@nestjs/common';
import { BankAccountService } from './bank_account.service';
import { BankAccountController } from './bank_account.controller';
import { ConfigModule } from '@nestjs/config';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [
    ConfigModule,
    ClientsXisfoModules,
  ],
  controllers: [BankAccountController],
  providers: [BankAccountService]
})
export class BankAccountModule {}
