import { Module } from '@nestjs/common';
import { ClientCompanyService } from './client-company.service';
import { ClientCompanyController } from './client-company.controller';
import { ConfigModule } from '@nestjs/config';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [
    ConfigModule,
    ClientsXisfoModules,
  ],
  controllers: [ClientCompanyController],
  providers: [ClientCompanyService]
})
export class ClientCompanyModule {}
