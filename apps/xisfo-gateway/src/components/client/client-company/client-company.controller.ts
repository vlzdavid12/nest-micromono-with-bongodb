import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { PaginationDto } from 'apps/xisfo-gateway/src/commons/paginationDto';
import { ClientCompanyService } from './client-company.service';
import { CreateClientCompanyDto } from './dto/create-client-company.dto';
import { UpdateClientCompanyDto } from './dto/update-client-company.dto';

@Controller('client-company')
export class ClientCompanyController {
  constructor(private readonly clientCompanyService: ClientCompanyService) {}

  @Post()
  create(@Body() createClientCompanyDto: CreateClientCompanyDto) {
    return this.clientCompanyService.create(createClientCompanyDto);
  }

  @Get()
  findAll(@Query() paginationDto: PaginationDto) {
    return this.clientCompanyService.findAll(paginationDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.clientCompanyService.findOne(id);
  }

  @Get('find-all-company-clients/:id')
  findAllCompanyClients(@Query() paginationDto: PaginationDto, @Param('id') id: string) {
    return this.clientCompanyService.findAllCompanyClients(id, paginationDto);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateClientCompanyDto: UpdateClientCompanyDto) {
    return this.clientCompanyService.update(id, updateClientCompanyDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.clientCompanyService.remove(id);
  }
}
