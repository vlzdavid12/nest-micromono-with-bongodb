import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { PaginationDto } from 'apps/xisfo-gateway/src/commons/paginationDto';
import { CreateClientCompanyDto } from './dto/create-client-company.dto';
import { UpdateClientCompanyDto } from './dto/update-client-company.dto';

@Injectable()
export class ClientCompanyService {
  constructor(@Inject('XISFO_CLIENTS') private clientXisfo: ClientProxy) { }

  create(createClientCompanyDto: CreateClientCompanyDto) {
    return this.clientXisfo.send('createClientCompany', createClientCompanyDto);
  }

  findAll(paginationDto: PaginationDto) {
    return this.clientXisfo.send('findAllClientCompany', paginationDto);
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOneClientCompany', id);
  }

  findAllCompanyClients(id: string, paginationDto: PaginationDto) {
    return this.clientXisfo.send('findAllCompanyClients', {id, paginationDto});
  }

  update(id: string, updateClientCompanyDto: UpdateClientCompanyDto) {
    return this.clientXisfo.send('updateClientCompany', {id, ...updateClientCompanyDto});
  }

  remove(id: string) {
    return this.clientXisfo.send('removeClientCompany', id);
  }
}
