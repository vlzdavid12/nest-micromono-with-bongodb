import { Controller, Get, Param } from '@nestjs/common';
import { TypeBankAccountService } from './type_bank_account.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Type Bank Account')
@Controller('type-bank-account')
export class TypeBankAccountController {
  constructor(private readonly typeBankAccountService: TypeBankAccountService) {}

  @Get()
  findAll() {
    return this.typeBankAccountService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.typeBankAccountService.findOne(id);
  }
}
