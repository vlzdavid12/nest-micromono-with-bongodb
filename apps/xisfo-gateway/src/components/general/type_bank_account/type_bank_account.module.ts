import { Module } from '@nestjs/common';
import { TypeBankAccountService } from './type_bank_account.service';
import { TypeBankAccountController } from './type_bank_account.controller';
import { ConfigModule } from '@nestjs/config';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [
    ConfigModule,
    ClientsXisfoModules,
  ],
  controllers: [TypeBankAccountController],
  providers: [TypeBankAccountService]
})
export class TypeBankAccountModule {}
