import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class TypeBankAccountService {
  constructor(@Inject('XISFO_CLIENTS') private clientXisfo: ClientProxy) { }


  findAll() {
    return this.clientXisfo.send('findAllTypeBankAccount', '');
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOneTypeBankAccount', id);
  }
}
