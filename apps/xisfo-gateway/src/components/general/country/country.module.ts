import { Module } from '@nestjs/common';
import { CountryService } from './country.service';
import { CountryController } from './country.controller';
import {ClientsXisfoModules} from "../../microservices/clients";
import {AuthModule} from "../../auth/auth.module";
import {ConfigModule} from "@nestjs/config";

@Module({
  imports:[
    AuthModule,
    ConfigModule,
    ClientsXisfoModules
  ],
  controllers: [CountryController],
  providers: [CountryService]
})
export class CountryModule {}
