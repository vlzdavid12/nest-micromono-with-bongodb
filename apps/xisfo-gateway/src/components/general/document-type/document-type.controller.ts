import { Controller, Get } from '@nestjs/common';
import { DocumentTypeService } from './document-type.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Document Type')
@Controller('document-type')
export class DocumentTypeController {
  constructor(private readonly documentTypeService: DocumentTypeService) {}

  @Get()
  findAll() {
    return this.documentTypeService.findAll();
  }
}
