import { Module } from '@nestjs/common';
import { BankService } from './bank.service';
import { BankController } from './bank.controller';
import { ClientsXisfoModules } from '../../microservices/clients';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ClientsXisfoModules,
    ConfigModule,
  ],
  controllers: [BankController],
  providers: [BankService]
})
export class BankModule {}
