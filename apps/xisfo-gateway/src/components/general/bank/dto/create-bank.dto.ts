import { IsNotEmpty, IsString, MinLength } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateBankDto {
    @ApiProperty()
    @IsString()
    @MinLength(3)
    @IsNotEmpty()
    name: string;
}
