import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateBankDto } from './dto/create-bank.dto';
import { UpdateBankDto } from './dto/update-bank.dto';
import {PaginationDto} from "../../../commons/paginationDto";

@Injectable()
export class BankService {
  constructor(@Inject('XISFO_CLIENTS') private clientXisfo: ClientProxy) { }

  create(createBankDto: CreateBankDto) {
    return this.clientXisfo.send('createBank', createBankDto);
  }

  findAll(paginationDto: PaginationDto) {
    return this.clientXisfo.send('findAllBank', paginationDto);
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOneBank', id);
  }

  update(id: string, updateBankDto: UpdateBankDto) {
    return this.clientXisfo.send('updateBank', {id, ...updateBankDto});
  }

  remove(id: string) {
    return this.clientXisfo.send('removeBank', id);
  }
}
