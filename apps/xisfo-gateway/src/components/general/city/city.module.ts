import { Module } from '@nestjs/common';
import { CityController } from './city.controller';
import { CityService } from './city.service';
import {ConfigModule} from "@nestjs/config";
import {AuthModule} from "../../auth/auth.module";
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [
    ClientsXisfoModules,
      AuthModule,
    ConfigModule

  ],
  controllers: [CityController],
  providers: [CityService]

})
export class CityModule {}