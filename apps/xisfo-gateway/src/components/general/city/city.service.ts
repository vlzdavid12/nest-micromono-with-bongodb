import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class CityService {
  constructor(@Inject('XISFO_CLIENTS') private clientXisfo: ClientProxy) { }

  getAllCities() {
    return this.clientXisfo.send('findAllCity', '');
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOneCity', id);
  }
}
