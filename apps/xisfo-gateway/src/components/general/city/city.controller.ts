import {Controller, Get, Param} from '@nestjs/common';
import { CityService } from './city.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('City')
@Controller('city')
export class CityController {
  constructor(private readonly cityService: CityService) { }

  // @Auth()
  @Get()
  findAll() {
    return  this.cityService.getAllCities();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.cityService.findOne(id);
  }


}
