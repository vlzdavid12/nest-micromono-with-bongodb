import { Injectable , Inject} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class DepartmentService {

  constructor(
    @Inject('XISFO_CLIENTS') private xisfoClient: ClientProxy
  ){}

  findAll() {
    return this.xisfoClient.send('findAllDepartment', '');
  }
}
