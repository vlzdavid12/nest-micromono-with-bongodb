import { Controller, Get } from '@nestjs/common';
import { DepartmentService } from './department.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Departament')
@Controller('department')
export class DepartmentController {
  constructor(private readonly departmentService: DepartmentService) {}

  @Get()
  findAll() {
    return this.departmentService.findAll();
  }
}
