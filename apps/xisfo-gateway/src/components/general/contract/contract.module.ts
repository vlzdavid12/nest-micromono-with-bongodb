import { Module } from '@nestjs/common';
import { ContractService } from './contract.service';
import { ContractController } from './contract.controller';
import { ClientsXisfoModules } from '../../microservices/clients';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { fileFilter, fileNamer } from '../../../helpers/index.helper';
import { ConfigService } from '@nestjs/config';
@Module({
  imports: [ClientsXisfoModules,
    MulterModule.register(
      {
        limits: {
          fileSize: 3 * 1024 * 1024
        },
        fileFilter: fileFilter,
        storage: diskStorage({
          destination: function (req, file, callback) {
            callback(null, './apps/xisfo-gateway/public')
          },
          filename: fileNamer
        })
      }
    )],
  controllers: [ContractController],
  providers: [ContractService, ConfigService]
})
export class ContractModule { }
