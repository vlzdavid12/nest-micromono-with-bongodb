import { Controller, Get, Body, Patch, Param, Delete, Post, UseInterceptors, UploadedFiles } from '@nestjs/common';
import { ContractService } from './contract.service';
import { CreateContractDto } from './dto/create-contract.dto';
import { FileFieldsInterceptor, FilesInterceptor } from '@nestjs/platform-express/multer';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Contract')
@Controller('contract')
export class ContractController {
  constructor(
    private readonly contractService: ContractService,
  ) { }

  @Post()
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'rut_image', maxCount: 1 },
    { name: 'chamber_commerce', maxCount: 1 },
    { name: 'bank_certificate', maxCount: 1 }
  ]))
  async create(
    @Body() createContractDto: CreateContractDto,
    @UploadedFiles() files: {
      rut_image?: Express.Multer.File[],
      chamber_commerce?: Express.Multer.File[],
      bank_certificate?: Express.Multer.File[],
    }
  ) {
    return this.contractService.create(createContractDto, files);
  }

  @Get()
  findAll() {
    return this.contractService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.contractService.findOne(id);
  }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateContractDto: UpdateContractDto) {
  //   return this.contractService.update(id, updateContractDto);
  // }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.contractService.remove(id);
  }
}
