import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import { CreateContractDto } from './dto/create-contract.dto';
import { UpdateContractDto } from './dto/update-contract.dto';

@Injectable()
export class ContractService {

  constructor(
    @Inject('XISFO_CLIENTS') private xisfoClient: ClientProxy,
    private readonly configService: ConfigService
  ) { }

  private storage_images:
    string = this.configService.get('URI_API') + this.configService.get('IMAGES_GATEWAY')

  create(createContractDto: CreateContractDto,
    files: {
      rut_image?:         Express.Multer.File[],
      chamber_commerce?:  Express.Multer.File[],
      bank_certificate?:  Express.Multer.File[],
    }
  ) {
    // This take the name of each images and put on it the app's url
    const rut_image =         this.storage_images + files.rut_image[0].filename;
    const chamber_commerce =  this.storage_images + files.chamber_commerce[0].filename;
    const bank_certificate =  this.storage_images + files.bank_certificate[0].filename;
    
    // This will concatenate all the images to the dto
    const dataContract = { rut_image, chamber_commerce, bank_certificate, ...createContractDto }

    return this.xisfoClient.send('createContract', dataContract);
  }

  findAll() {
    return this.xisfoClient.send('findAllContract', '');
  }

  findOne(id: string) {
    return this.xisfoClient.send('findOneContract', id);
  }

  // update(id: string, updateContractDto: UpdateContractDto) {
  //   return this.xisfoClient.send('updateContract', { id, ...updateContractDto });
  // }

  remove(id: string) {
    return this.xisfoClient.send('removeContract', id);
  }
}
