import { IsNotEmpty, IsString } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateContractDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    rut: string
    
    //debe ser boolean cuando tenga front
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    habeas_data: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    employee: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    client: string;
}
