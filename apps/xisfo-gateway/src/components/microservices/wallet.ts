import { ClientsModule, Transport } from "@nestjs/microservices";

export const WalletXisfoModules = ClientsModule.register([
    {
        name: 'XISFO_WALLET',
        transport: Transport.TCP,
        options: {
            port: 3003
        }
    },
])

