import {ClientsModule, Transport} from "@nestjs/microservices";

export const ClientsXisfoModules = ClientsModule.register([
    {
        name: 'XISFO_PAYMENTS',
        transport: Transport.TCP,
        options: {
            port: 3002
        }
    },
])

