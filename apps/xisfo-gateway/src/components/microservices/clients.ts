import {ClientsModule, Transport} from "@nestjs/microservices";

export const ClientsXisfoModules = ClientsModule.register([
    {
        name: 'XISFO_CLIENTS',
        transport: Transport.TCP,
        options: {
            port: 3001
        }
    },
])

