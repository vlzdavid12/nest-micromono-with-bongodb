import { Module } from '@nestjs/common';
import { WalletService } from './wallet.service';
import { WalletController } from './wallet.controller';
import { WalletXisfoModules } from '../../microservices/wallet';

@Module({
  imports:[WalletXisfoModules],
  controllers: [WalletController],
  providers: [WalletService]
})
export class WalletModule {}
