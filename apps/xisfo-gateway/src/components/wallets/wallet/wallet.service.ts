import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { PaginationDtoSearch } from 'apps/xisfo-gateway/src/commons/paginationDto.filter';

@Injectable()
export class WalletService {

  constructor(
    @Inject('XISFO_WALLET') private xisfoClient: ClientProxy
  ){}

  findAll(paginationQuery: PaginationDtoSearch) {
    return this.xisfoClient.send('findAllWallet', paginationQuery);
  }

  findOne(id: string) {
    return this.xisfoClient.send('findOneWallet', id);
  }
}

