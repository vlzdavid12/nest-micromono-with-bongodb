import { Controller, Get, Param, Query } from '@nestjs/common';
import { WalletService } from './wallet.service';
import {ApiTags} from "@nestjs/swagger";
import { PaginationDtoSearch } from 'apps/xisfo-gateway/src/commons/paginationDto.filter';

@ApiTags('Wallet')
@Controller('wallet')
export class WalletController {
  constructor(private readonly walletService: WalletService) { }

  @Get()
  findAll(@Query() paginationQuery: PaginationDtoSearch) {
    return this.walletService.findAll(paginationQuery);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.walletService.findOne(id);
  }
}
