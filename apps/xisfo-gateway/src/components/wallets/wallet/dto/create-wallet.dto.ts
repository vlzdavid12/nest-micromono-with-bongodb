import { IsBoolean, IsNotEmpty, IsString } from "class-validator"
import {ApiProperty} from "@nestjs/swagger";

export class CreateWalletDto {

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    number_wallet: string

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    client: string

    @IsNotEmpty()
    @IsBoolean()
    data_policy: boolean

}
