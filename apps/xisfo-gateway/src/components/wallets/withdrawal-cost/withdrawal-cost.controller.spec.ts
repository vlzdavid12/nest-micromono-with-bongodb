import { Test, TestingModule } from '@nestjs/testing';
import { WithdrawalCostController } from './withdrawal-cost.controller';
import { WithdrawalCostService } from './withdrawal-cost.service';

describe('WithdrawalCostController', () => {
  let controller: WithdrawalCostController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WithdrawalCostController],
      providers: [WithdrawalCostService],
    }).compile();

    controller = module.get<WithdrawalCostController>(WithdrawalCostController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
