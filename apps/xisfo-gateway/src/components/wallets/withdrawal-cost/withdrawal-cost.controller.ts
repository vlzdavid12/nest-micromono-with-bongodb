import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { WithdrawalCostService } from './withdrawal-cost.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('withdrawal')
@Controller('withdrawal-cost')
export class WithdrawalCostController {
  constructor(private readonly withdrawalCostService: WithdrawalCostService) {}

  @Get()
  findAll() {
    return this.withdrawalCostService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.withdrawalCostService.findOne(id);
  }
}
