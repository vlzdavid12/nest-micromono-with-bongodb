import { Module } from '@nestjs/common';
import { WithdrawalCostService } from './withdrawal-cost.service';
import { WithdrawalCostController } from './withdrawal-cost.controller';
import { ConfigModule } from '@nestjs/config';
import { WalletXisfoModules } from '../../microservices/wallet';

@Module({
  imports: [
    ConfigModule,
    WalletXisfoModules,
  ],
  controllers: [WithdrawalCostController],
  providers: [WithdrawalCostService]
})
export class WithdrawalCostModule {}
