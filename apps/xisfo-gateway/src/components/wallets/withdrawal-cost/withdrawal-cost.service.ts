import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class WithdrawalCostService {
  constructor(
    @Inject('XISFO_WALLET') private xisfoClient: ClientProxy
  ){}

  findAll() {
    return this.xisfoClient.send('findAllWithdrawalCost', '');
  }

  findOne(id: string) {
    return this.xisfoClient.send('findOneWithdrawalCost', id);
  }

}
