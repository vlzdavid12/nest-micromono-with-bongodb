import { Test, TestingModule } from '@nestjs/testing';
import { WithdrawalCostService } from './withdrawal-cost.service';

describe('WithdrawalCostService', () => {
  let service: WithdrawalCostService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WithdrawalCostService],
    }).compile();

    service = module.get<WithdrawalCostService>(WithdrawalCostService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
