import { Module } from '@nestjs/common';
import { TransferService } from './transfer.service';
import { TransferController } from './transfer.controller';
import { WalletXisfoModules } from '../../microservices/wallet';
import { MulterModule } from '@nestjs/platform-express';
import { fileFilter } from 'apps/xisfo-gateway/src/helpers/file-excel-filter.helper';
import { diskStorage } from 'multer';
import { fileNamer } from 'apps/xisfo-gateway/src/helpers/file-namer.helper';
import { ConfigService } from '@nestjs/config';

@Module({
  imports:[
    WalletXisfoModules,
    MulterModule.register(
      {
        fileFilter: fileFilter,
        storage: diskStorage({
          destination: function (req, file, callback) {
            callback(null, './apps/xisfo-gateway/public')
          },
          filename: fileNamer
        })
      }
    )],
  controllers: [TransferController],
  providers: [TransferService, ConfigService]
})
export class TransferModule {}
