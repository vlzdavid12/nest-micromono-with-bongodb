import {Controller, Get, Post, Body, Param, UseInterceptors, UploadedFiles, Query} from '@nestjs/common';
import { CreateTransferFromPaymentDto } from './dto/create-transfer-from-payment.dto';
import { CreateTransferFromWalletDto } from './dto/create-transfer-from-wallet.dto';
import { TransferService } from './transfer.service';
import {ApiTags} from "@nestjs/swagger";
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { CreateConsecutivePaymentDto } from './dto/create-consecutive-payment.dto';
import {PaginationDtoSearch} from "../../../commons/paginationDto.filter";
import { CreateTransferWithdrawalsDto } from './dto/create-transfer-withdrawals.dto';
import { CreateExportWithdrawalsDto } from './dto/create-export-withdrawals.dto';
import { PaginationDto } from 'apps/xisfo-gateway/src/commons/paginationDto';

@ApiTags('Transfer')
@Controller('transfer')
export class TransferController {
  constructor(private readonly transferService: TransferService) { }

  @Post('wallet')
  createTransferWallet(@Body() createTransferDto: CreateTransferFromWalletDto) {
    return this.transferService.createTransferWallet(createTransferDto);
  }

  @Post('payment')
  createTransferPayment(@Body() createTransferDto: CreateTransferFromPaymentDto) {
    return this.transferService.createTransferPayment(createTransferDto);
  }

  @Post('withdrawal')
  createTransferWithdrawals(@Body() createTransferWithdrawalsDto: CreateTransferWithdrawalsDto) {
    return this.transferService.createTransferWithdrawals(createTransferWithdrawalsDto);
  }

  @Post('exportWithdrawal')
  getTransferWithdrawals(@Body() withdrawals: CreateExportWithdrawalsDto) {
    return this.transferService.exportTransferWithdrawals(withdrawals);
  }

  @Post('consecutivePayment')
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'excel_file', maxCount: 1 }
  ]))
  createConsecutivePayments(
    @Body() payments: CreateConsecutivePaymentDto,
    @UploadedFiles() file: {
      excel_file: Express.Multer.File
    }
    ) {
    return this.transferService.createConsecutivePayments(payments, file);
  }

  @Get('downloadExcelTemplateConsecutivePayments')
  downloadExcelTemplateConsecutivePayments() {
    return this.transferService.downloadExcelTemplateConsecutivePayments();
  }

  @Get()
  findAll(@Query() paginationDtoSearch: PaginationDtoSearch) {
    return this.transferService.findAll(paginationDtoSearch);
  }

  @Get('find-all-pendings-withdrawals')
  findAllPendingsWithdrawals(@Query() paginationDto: PaginationDto) {
    return this.transferService.findAllPendingsWithdrawals(paginationDto);
  }

  @Get('wallet-history/:id')
  getHistoryWallet(@Param('id') id: string) {
    return this.transferService.getHistoryWallet(id);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.transferService.findOne(id);
  }
}
