import { IsBoolean, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator"
import {ApiProperty} from "@nestjs/swagger";

export class CreateTransferWithdrawalsDto {

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    sender_wallet: string

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    receptor_wallet: string

    @ApiProperty()
    @IsBoolean()
    @IsOptional()
    is_paid: boolean

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    amount: number
}
