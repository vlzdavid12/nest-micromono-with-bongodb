import { IsArray, IsNotEmpty } from "class-validator"

export class CreateExportWithdrawalsDto {
    @IsArray()
    @IsNotEmpty()
    sender_wallet: string[]
}
