import { IsArray, IsNotEmpty, IsNumber, IsString } from "class-validator"
import {ApiProperty} from "@nestjs/swagger";

export class CreateTransferFromPaymentDto {

    @ApiProperty()
    @IsArray()
    @IsNotEmpty()
    payments: string[]
}
