import { IsNotEmpty, IsOptional, IsString } from "class-validator"

export class CreateConsecutivePaymentDto {

    @IsString()
    @IsNotEmpty()
    sender_wallet: string

}
