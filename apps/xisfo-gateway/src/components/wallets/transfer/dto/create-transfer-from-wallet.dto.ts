import { IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator"
import {ApiProperty} from "@nestjs/swagger";

export class CreateTransferFromWalletDto {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    sender_wallet: string

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    receptor_wallet: string

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    withdrawals: string

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    amount: number
}
