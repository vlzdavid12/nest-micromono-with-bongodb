import { Injectable, Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import { CreateConsecutivePaymentDto } from './dto/create-consecutive-payment.dto';
import { CreateExportWithdrawalsDto } from './dto/create-export-withdrawals.dto';
import { CreateTransferFromPaymentDto } from './dto/create-transfer-from-payment.dto';
import { CreateTransferFromWalletDto } from './dto/create-transfer-from-wallet.dto';
import { CreateTransferWithdrawalsDto } from './dto/create-transfer-withdrawals.dto';
import {PaginationDtoSearch} from "../../../commons/paginationDto.filter";
import { PaginationDto } from 'apps/xisfo-gateway/src/commons/paginationDto';

@Injectable()
export class TransferService {

  string = this.configService.get('IMAGES_GATEWAY')

  constructor(
    @Inject('XISFO_WALLET') private xisfoWallet: ClientProxy,
    private readonly configService: ConfigService,
  ){}

  createTransferWallet(createTransferDto: CreateTransferFromWalletDto) {
    return this.xisfoWallet.send('createTransferWallet', createTransferDto);
  }

  createTransferPayment(createTransferDto: CreateTransferFromPaymentDto) {
    return this.xisfoWallet.send('createTransferPayment', createTransferDto);
  }

  createTransferWithdrawals(createTransferWithdrawalsDto: CreateTransferWithdrawalsDto) {
    return this.xisfoWallet.send('createTransferWithdrawals', createTransferWithdrawalsDto);
  }
  
  getHistoryWallet(walletId: string) {
    return this.xisfoWallet.send('getHistoryWallet', walletId);
  }

  createConsecutivePayments(payments: CreateConsecutivePaymentDto, file: {
    excel_file: Express.Multer.File,
  }) {
    const excel_file = file.excel_file[0].originalname;

    const dataConsecutivePayment = {excel_file, ...payments}
    return this.xisfoWallet.send('createConsecutivePayments', dataConsecutivePayment);
  }
  
  exportTransferWithdrawals(withdrawals: CreateExportWithdrawalsDto) {
    return this.xisfoWallet.send('exportTransferWithdrawals', withdrawals);
  }

  downloadExcelTemplateConsecutivePayments() {
    return this.xisfoWallet.send('downloadExcelTemplateConsecutivePayments', '');
  }

  findAll(paginationDtoSearch: PaginationDtoSearch) {
    return this.xisfoWallet.send('findAllTransfer', paginationDtoSearch);
  }

  findAllPendingsWithdrawals(paginationDto: PaginationDto) {
    return this.xisfoWallet.send('findAllPendingsWithdrawals', paginationDto);
  }

  findOne(id: string) {
    return this.xisfoWallet.send('findOneTransfer', id);
  }
}
