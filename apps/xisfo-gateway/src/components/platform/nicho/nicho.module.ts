import { Module } from '@nestjs/common';
import { NichoService } from './nicho.service';
import { NichoController } from './nicho.controller';
import { ConfigModule } from '@nestjs/config';
import { ClientsXisfoModules } from '../../microservices/payments';

@Module({
  imports: [
    ConfigModule,
    ClientsXisfoModules,
  ],
  controllers: [NichoController],
  providers: [NichoService]
})
export class NichoModule {}
