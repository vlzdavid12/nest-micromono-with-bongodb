import { PartialType } from '@nestjs/mapped-types';
import { CreateNichoDto } from './create-nicho.dto';

export class UpdateNichoDto extends PartialType(CreateNichoDto) {}
