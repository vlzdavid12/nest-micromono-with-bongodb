import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { NichoService } from './nicho.service';
import { CreateNichoDto } from './dto/create-nicho.dto';
import { UpdateNichoDto } from './dto/update-nicho.dto';
import {ApiTags} from "@nestjs/swagger";
import { PaginationDto } from 'apps/xisfo-gateway/src/commons/paginationDto';

@ApiTags('Nicho')
@Controller('nicho')
export class NichoController {
  constructor(private readonly nichoService: NichoService) {}

  @Post()
  create(@Body() createNichoDto: CreateNichoDto) {
    return this.nichoService.create(createNichoDto);
  }

  @Get()
  findAll(@Query() paginationDto: PaginationDto) {
    return this.nichoService.findAll(paginationDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.nichoService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateNichoDto: UpdateNichoDto) {
    return this.nichoService.update(id, updateNichoDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.nichoService.remove(id);
  }
}
