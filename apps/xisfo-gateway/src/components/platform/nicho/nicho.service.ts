import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { PaginationDto } from 'apps/xisfo-gateway/src/commons/paginationDto';
import { CreateNichoDto } from './dto/create-nicho.dto';
import { UpdateNichoDto } from './dto/update-nicho.dto';

@Injectable()
export class NichoService {
  constructor(@Inject('XISFO_PAYMENTS') private clientXisfo: ClientProxy) { }

  create(createNichoDto: CreateNichoDto) {
    return this.clientXisfo.send('createNicho', createNichoDto);
  }

  findAll(paginationDto: PaginationDto) {
    return this.clientXisfo.send('findAllNicho', paginationDto);
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOneNicho', id);
  }

  update(id: string, updateNichoDto: UpdateNichoDto) {
    return this.clientXisfo.send('updateNicho', {id, ...updateNichoDto});
  }

  remove(id: string) {
    return this.clientXisfo.send('removeNicho', id);
  }
}
