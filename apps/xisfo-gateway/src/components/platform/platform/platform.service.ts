import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { PaginationDto } from 'apps/xisfo-gateway/src/commons/paginationDto';
import { CreatePlatformDto } from './dto/create-platform.dto';
import { UpdatePlatformDto } from './dto/update-platform.dto';

@Injectable()
export class PlatformService {
  constructor(@Inject('XISFO_PAYMENTS') private clientXisfo: ClientProxy) { }

  create(createPlatformDto: CreatePlatformDto) {
    return this.clientXisfo.send('createPlatform', createPlatformDto);
  }

  findAll(paginationDto: PaginationDto) {
    return this.clientXisfo.send('findAllPlatform', paginationDto);
  }

  findOne(id: string) {
    return this.clientXisfo.send('findOnePlatform', id);
  }

  update(id: string, updatePlatformDto: UpdatePlatformDto) {
    return this.clientXisfo.send('updatePlatform', {id, ...updatePlatformDto});
  }

  remove(id: string) {
    return this.clientXisfo.send('removePlatform', id);
  }
}
