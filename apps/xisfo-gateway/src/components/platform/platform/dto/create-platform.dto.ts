import { IsDecimal, IsNotEmpty, IsString, IsUrl } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreatePlatformDto {

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsDecimal()
    @IsNotEmpty()
    usd_commission: number;

    @ApiProperty()
    @IsDecimal()
    @IsNotEmpty()
    usd_token_rate: number;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    @IsUrl()
    url: string;

    @ApiProperty()
    @IsString()
    icon: string;
}
