import { Reflector } from '@nestjs/core';
import {
  CanActivate,
  ExecutionContext,
  Injectable,
  BadRequestException,
  ForbiddenException,
  Logger
} from '@nestjs/common';
import { Observable } from 'rxjs';
import {META_PERMISSION, META_ROLES} from '../decorators/role-protected.decorator';
import {UserEntity} from "../entities/user.entity";
import {ValidRoles} from "../interfaces";

@Injectable()
export class UserRolePermissionGuard implements CanActivate {
  logger = new Logger();
  constructor(
    private readonly reflector: Reflector
  ) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {

    const validRoles: string[] = this.reflector.get( META_ROLES , context.getHandler() )
    const validPermission: string[] = this.reflector.get( META_PERMISSION , context.getHandler() )

    if ( !validRoles ) return true;
    if ( validRoles.length === 0 ) return true;
    
    const req = context.switchToHttp().getRequest();

    const ctx = context.switchToHttp();

    const request = ctx.getRequest<Request>();

    const user = req.user as UserEntity;

    if ( !user ) 
      throw new BadRequestException('User not found');

    if ( user.roles.includes(ValidRoles.superUser)) {
      return true;
    }

    for (const role of user.roles ) {
      if ( validRoles.includes( role )) {
        let permission = user.permissions.find(per => validPermission.includes(per))
        if(permission){
       /*   console.log(request.url);
          console.log(request.method.toLowerCase());*/
          return true;
        }
      }
    }
    
    throw new ForbiddenException(
      `User ${ user.full_name } need a valid role: [${ validRoles }, ${validPermission}]`
    );
  }
}
