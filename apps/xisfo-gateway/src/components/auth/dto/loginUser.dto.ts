import { IsNotEmpty, IsString} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class  LoginUserDto{
    @ApiProperty({type: Number})
    @IsNotEmpty()
    phone: number;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    password: string;
}
