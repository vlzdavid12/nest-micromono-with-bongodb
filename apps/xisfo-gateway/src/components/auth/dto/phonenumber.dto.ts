import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty} from "class-validator";

export class PhonenumberDto {
    @ApiProperty()
    @IsNotEmpty()
    phone: string;
}
