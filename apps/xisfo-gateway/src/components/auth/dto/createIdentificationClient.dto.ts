import {IsNotEmpty, IsString} from "class-validator";

export class CreateClientIdentificationDto {

    @IsNotEmpty()
    @IsString()
    identification_number: string;

    @IsNotEmpty()
    @IsString()
    expedition_date: string;

    @IsString()
    client?: string;

    @IsString()
    @IsNotEmpty()
    document_type: string;

    @IsString()
    @IsNotEmpty()
    country_document: string
}
