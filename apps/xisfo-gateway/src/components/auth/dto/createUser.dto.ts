import {
    IsArray,
    IsBoolean,
    IsEmail, IsEmpty,
    IsNotEmpty,
    IsOptional,
    IsString,
    MinLength
} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateUserDto {

    @ApiProperty()
    @IsString()
    avatar: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    full_name: string;

    @ApiProperty()
    @IsString()
    @MinLength(3)
    @IsNotEmpty()
    first_name: string;

    @ApiProperty()
    @IsString()
    @MinLength(3)
    @IsNotEmpty()
    second_name: string;

    @ApiProperty()
    @IsString()
    @MinLength(3)
    @IsNotEmpty()
    last_name: string;

    @ApiProperty()
    @IsString()
    @MinLength(3)
    @IsNotEmpty()
    second_last_name: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    password: string

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    phone: string

    @ApiProperty()
    @IsEmail()
    email: string

    @ApiProperty()
    @IsString()
    address: string

    @ApiProperty()
    @IsString()
    country: string

    @ApiProperty()
    @IsString()
    country_document: string

    @ApiProperty()
    @IsString()
    date_birth: string

    @ApiProperty()
    @IsString()
    date_expired: string

    @ApiProperty()
    @IsString()
    number_cc: string

    @ApiProperty()
    @IsString()
    gender_id: string

    @ApiProperty()
    @IsBoolean()
    with_holding_tax: boolean

    @ApiProperty()
    @IsString()
    client_rate: string

    @ApiProperty()
    @IsBoolean()
    kyc: boolean

    @ApiProperty()
    @IsArray()
    @IsNotEmpty()
    roles: string[];

    @ApiProperty()
    @IsArray()
    permissions: string[];

    @ApiProperty()
    @IsBoolean()
    @IsNotEmpty()
    isActive: boolean;

    @ApiProperty()
    @IsBoolean()
    @IsNotEmpty()
    isClient: boolean;

    @ApiProperty()
    @IsArray()
    data_extra: any[];

    @ApiProperty()
    @IsString()
    client_type_id: string

    @ApiProperty()
    @IsString()
    document_type_id: string

    @ApiProperty()
    @IsString()
    @IsOptional()
    client_company: string

    @ApiProperty()
    @IsString()
    registration_status: string

    @ApiProperty()
    @IsBoolean()
    data_policy: boolean
}
