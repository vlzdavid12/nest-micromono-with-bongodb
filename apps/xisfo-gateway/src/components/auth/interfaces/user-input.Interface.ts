import {ValidRoles} from "./valid-roles";
import {ValidPermission} from "./valid-permission";

export interface UserInputInterface{
    avatar: string,
    full_name: string,
    password: string,
    email: string,
    phone: string,
    roles: ValidRoles[],
    kyc: Boolean,
    permissions: ValidPermission[],
    isActive: boolean,
    isClient: boolean,
    data_extra: any[];
    profile_id?: object;
}
