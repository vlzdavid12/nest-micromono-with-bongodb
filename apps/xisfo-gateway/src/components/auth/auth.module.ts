import {Module} from '@nestjs/common';

import {MongooseModule} from "@nestjs/mongoose";
import {ConfigModule, ConfigService} from "@nestjs/config";
import {PassportModule} from "@nestjs/passport";
import {JwtModule} from "@nestjs/jwt";
import {JwtStrategy} from './strategy/jwt.strategy';
import {ClientsXisfoModules} from '../microservices/clients';
import {UserEntity, UserSchema} from "./entities/user.entity";
import {AuthController} from "./auth.controller";
import {AuthService} from "./auth.service";
import {RegistrationStatusEntity, RegistrationStatusSchema} from "./entities/registration-status.entity";
import {ClientTypeEntity, ClientTypeSchema} from "./entities/client-type.entity";

@Module({
    imports: [ClientsXisfoModules,
        MongooseModule.forFeature([{
            name: UserEntity.name,
            schema: UserSchema
        }]),
        MongooseModule.forFeature([{
            name: RegistrationStatusEntity.name,
            schema: RegistrationStatusSchema
        }]),
        MongooseModule.forFeature([{
            name: ClientTypeEntity.name,
            schema: ClientTypeSchema
        }]),

        ConfigModule,

        PassportModule.register({defaultStrategy: 'jwt'}),

        JwtModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: (configService: ConfigService) => {
                return {
                    secret: configService.get('JWT_SECRET'),
                    signOptions: {
                        expiresIn: '2h'
                    }
                }
            }
        }),


    ],
    providers: [AuthService, JwtStrategy],
    exports: [JwtStrategy, PassportModule, JwtModule],
    controllers: [AuthController],

})
export class AuthModule {
}
