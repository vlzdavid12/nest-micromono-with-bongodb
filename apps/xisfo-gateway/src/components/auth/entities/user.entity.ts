import {Prop, Schema, SchemaFactory} from "@nestjs/mongoose";
import {Document, now} from "mongoose";


@Schema({collection: 'users'})
export class UserEntity extends Document {
    @Prop()
    avatar: string;

    @Prop({required: true})
    full_name: string;

    @Prop({required: true})
    password: string

    @Prop({
        required: true,
        unique: false
    })
    email: string

    @Prop({
        required: true,
        unique: true
    })
    phone: number

    @Prop({ required: true, default: false })
    kyc: boolean;


    @Prop({required: true, default: ['user']})
    roles: string[]

    @Prop({required: true})
    permissions: string[]

    @Prop()
    isActive: boolean;

    @Prop()
    isClient: boolean;

    @Prop({required: false})
    data_extra: any[]

    @Prop({default: now()})
    createdAt: Date;

    @Prop({default: now()})
    updatedAt: Date;

    @Prop()
    profile_id: string;


}

export const UserSchema = SchemaFactory.createForClass(UserEntity);
