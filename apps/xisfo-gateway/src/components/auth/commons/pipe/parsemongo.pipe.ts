import {ArgumentMetadata, BadRequestException, Injectable, PipeTransform} from '@nestjs/common';
import { isValidObjectId } from "mongoose";

@Injectable()
export class ParseMongoPipe implements PipeTransform {
  transform(value: string, metadata: ArgumentMetadata) {

    if(!isValidObjectId(value)){
      throw new BadRequestException("Error ID inValid");
    }

    return value.toLowerCase();
  }
}
