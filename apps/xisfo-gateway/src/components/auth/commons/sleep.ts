export class SleepHelpers {

    public static sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

}
