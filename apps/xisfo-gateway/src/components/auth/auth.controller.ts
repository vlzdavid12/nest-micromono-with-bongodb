import {
  Body,
  Controller,
  Get,
  Headers,
  HttpException,
  HttpStatus,
  Param, Patch,
  Post,
  Redirect,
  UseGuards
} from '@nestjs/common';
import {ApiTags} from "@nestjs/swagger";
import {CreateUserDto} from "./dto/createUser.dto";
import {AuthService} from "./auth.service";
import {LoginUserDto} from "./dto/loginUser.dto";
import {Auth} from "./decorators/auth.decorator";
import {ValidPermission, ValidRoles} from "./interfaces";
import {AuthGuard} from "@nestjs/passport";
import {PhonenumberDto} from "./dto/phonenumber.dto";
import {PasswordDto} from "./dto/password.dto";
import {CreateEmployeeResourceDto} from "../employee/employee-resource/dto/create-employee-resource.dto";
import {LoginEmployeeDto} from "./dto/loginEmployeeDto";
import {EmailDto} from "./dto/email.dto";


@ApiTags('Auth')
@Controller('auth')
export class AuthController {

  constructor(private readonly authService: AuthService) {}

  @Auth( [ValidRoles.admin, ValidRoles.user], [ValidPermission.read])
  @Get()
  handleCreateEmployee() {
    console.log('Authorization')
  }

  @Post('create-client')
  createUser(@Body() userDto: CreateUserDto){
    return this.authService.createClient(userDto);
  }

  @Post('create-employee')
  createEmployee(@Body() createEmployeeResourceDto: CreateEmployeeResourceDto){
    return this.authService.createEmployee(createEmployeeResourceDto);
  }

  @Post('login-client')
  loginUser(@Body() loginUserDto: LoginUserDto){
    return this.authService.login(loginUserDto);
  }

  @Post('login-employee')
  loginEmployee(@Body() loginEmployeeDto: LoginEmployeeDto){
    return this.authService.loginEmployee(loginEmployeeDto);
  }

  @Get('check-status')
  @UseGuards(AuthGuard('jwt'))
  checkAuthStatus(
      @Headers('Authorization') auth : string,
  ) {
    return this.authService.checkAuthStatus( auth );
  }

  @Patch('update-password-client')
  @UseGuards(AuthGuard('jwt'))
  updatePassword(
      @Headers('Authorization') auth : string,
      @Body() password: PasswordDto
  ){
    return this.authService.updatePassWord(auth, password);
  }


  @Post('recovery-password-client')
  sendEmailRecoveryPassword(@Body() phone: PhonenumberDto){
    // @ts-ignore
    return this.authService.sendRecoveryPassword(phone);
  }

  @Post('recovery-password-employee')
  sendEmailRecoveryPasswordEmployee(@Body() email: EmailDto){
    // @ts-ignore
    return this.authService.sendRecoveryPasswordEmployee(email);
  }

  @Get('confirm-account/:id')
  @Redirect(`${process.env.URI_API_FROND}client/`, 302)
  async confirmAccount(@Param() id: string){
    try {
      // @ts-ignore
      await this.authService.confirmAccountService(id);
      return { url: `${process.env.URI_API_FROND}client/` };
    }catch (error){
      throw new HttpException('Error', HttpStatus.BAD_REQUEST);
    }
  }

  @Get('confirm-account-employee/:id')
  @Redirect(`${process.env.URI_API_FROND}employee/`, 302)
  async confirmAccountEmployee(@Param() id: string){
    try {
      // @ts-ignore
      await this.authService.confirmAccountService(id);
      return { url: `${process.env.URI_API_FROND}employee/` };
    }catch (error){
      throw new HttpException('Error', HttpStatus.BAD_REQUEST);
    }
  }


  @Get('phone/:phone')
  phoneSearchExist(@Param() phone: PhonenumberDto){
    // @ts-ignore
    return this.authService.searchPhoneUser(phone);
  }


  @Get('email/:email')
  emailSearchExist(@Param() email: EmailDto){
    // @ts-ignore
    return this.authService.searchEmailUser(email);
  }


}
