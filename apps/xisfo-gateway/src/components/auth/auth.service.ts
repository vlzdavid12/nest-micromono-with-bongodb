import {
    BadRequestException,
    Inject,
    Injectable,
    InternalServerErrorException,
    Logger, NotFoundException,
    UnauthorizedException
} from '@nestjs/common';
import {Model} from "mongoose";
import {ClientProxy} from "@nestjs/microservices";
import {InjectModel} from "@nestjs/mongoose";

import {UserEntity} from "./entities/user.entity";
import {CreateUserDto} from "./dto/createUser.dto";
import * as bcrypt from "bcrypt";

import {JwtService} from "@nestjs/jwt";
import {JwtPayload} from "./interfaces/jwt-payload.interface";
import {LoginUserDto} from "./dto/loginUser.dto";
import {ConfigService} from "@nestjs/config";
import {MailerService} from "@nestjs-modules/mailer";
import {RegistrationStatusEntity} from "./entities/registration-status.entity";
import {ClientTypeEntity} from "./entities/client-type.entity";
import {ValidPermission, ValidRoles} from "./interfaces";
import {PasswordDto} from "./dto/password.dto";
import {CreateEmployeeResourceDto} from "../employee/employee-resource/dto/create-employee-resource.dto";
import {UserInputInterface} from "./interfaces/user-input.Interface";
import {LoginEmployeeDto} from "./dto/loginEmployeeDto";
import {InputEmployeeRequest} from "./interfaces/employee-input.interface";
import * as mongoose from "mongoose";
import {CreateClientDto} from "../client/client/dto/create-client.dto";
import {CreateClientIdentificationDto} from "./dto/createIdentificationClient.dto";

@Injectable()
export class AuthService {
    logger: any = new Logger();

    constructor(@Inject("XISFO_CLIENTS")
                private readonly client: ClientProxy,
                @InjectModel(UserEntity.name)
                private readonly userModel: Model<UserEntity>,
                @InjectModel(RegistrationStatusEntity.name)
                private readonly registrationStatus: Model<RegistrationStatusEntity>,
                @InjectModel(ClientTypeEntity.name)
                private readonly clientStatus: Model<ClientTypeEntity>,
                private readonly jwtService: JwtService,
                private readonly configService: ConfigService,
                private readonly mailerService: MailerService
    ) {
    }

    async createEmployee(employee: CreateEmployeeResourceDto) {
        let phoneExist = await this.userModel.findOne({phone: employee.phone});
        let emailExist = await this.userModel.findOne({email: employee.email});

        if (emailExist) {
            return {msg: "No se puede registrar, comunicate con el administrador."};
        }

        if (phoneExist) {
            return {msg: "No se puede registrar con este número telefónico"};
        }

        //Generate New ID
        let newID =  new mongoose.Types.ObjectId();

        const dataEmployeeCreate = {
            first_name: employee.first_name,
            second_name: employee.second_name,
            last_name: employee.last_name,
            second_last_name: employee.second_last_name,
            email: employee.email,
            phone: employee.phone,
            birthday: employee.birthday,
            childrens: employee.childrens,
            civil_status_id: employee.civil_status_id,
            contract_type_id: employee.contract_type_id,
            gender_id: employee.gender_id,
            position_id: employee.position_id,
            headquarter_id: employee.headquarter_id,
            health_provider_id: employee.health_provider_id,
            occupational_risk_manager_id: employee.occupational_risk_manager_id,
            sociodemographic_profile_id: employee.sociodemographic_profile_id,
            pension_fund_id: employee.pension_fund_id,
            education_level_id: employee.education_level_id,
            profesions: employee.profesions,
            identification_number: employee.identification_number,
            expedition_date: employee.expedition_date,
            expedition_city: employee.expedition_city,
            document_type_id: employee.document_type_id
          }

        // Create Employee
        this.client.emit('createEmployee', dataEmployeeCreate);

        let authDB: UserInputInterface = {
            avatar: employee.avatar,
            full_name: employee.full_name,
            password: bcrypt.hashSync(employee.password, 10),
            email: employee.email,
            phone: employee.phone,
            roles: [ValidRoles.user],
            kyc: false,
            permissions: [ValidPermission.read, ValidPermission.write, ValidPermission.delete],
            isActive: true,
            isClient: false,
            data_extra: employee.data_extra,
        }

        authDB.profile_id = newID

        // Create User Auth
        return await this.createUserAuth(authDB);
    }


    async createClient(client: CreateUserDto) {
        try {

            let phoneExist = await this.userModel.findOne({phone: client.phone});
            if (phoneExist) {
                return {msg: "No se puede registrar con este número telefónico"};
            }

            // Generate New ID
            const newID =  new mongoose.Types.ObjectId();

            // Create Client
            let clientDB: CreateClientDto = {
                first_name: client.first_name,
                second_name: client.second_name,
                last_name: client.last_name,
                second_last_name: client.second_last_name,
                email: client.email,
                country: client.country,
                phone: client.phone,
                address: client.address,
                client_rate: client.client_rate || '0.05',
                with_holding_tax: client.with_holding_tax || false,
                old_client: false,
                client_type: client.client_type_id,
                registration_status: client.registration_status,
                client_company_id: client.client_company || null,
                data_policy: client.data_policy
            }

            // Create Document
            let documentDB: CreateClientIdentificationDto = {
                country_document: client.country_document,
                expedition_date: client.date_expired,
                identification_number: client.number_cc,
                document_type: client.document_type_id
            }

            // Create User
            let authDB: UserInputInterface = {
                avatar: client.avatar,
                full_name: client.full_name,
                password: bcrypt.hashSync(client.password, 10),
                email: client.email,
                phone: client.phone,
                roles: [ValidRoles.user],
                kyc: client.kyc,
                permissions: [ValidPermission.read, ValidPermission.write, ValidPermission.delete],
                isActive: client.isActive || false,
                isClient: true,
                data_extra: client.data_extra,
                profile_id: newID
            }

            this.client.emit('createClient', {clientDB, documentDB});
            return await this.createUserAuth(authDB);

        } catch (error) {
            this.handleDbErrors(error);
        }

    }

    async createUserAuth(user: UserInputInterface) {

        const request = await this.userModel.create(user)

        //Email Send Verify Account
        if (request) {
            if (request.isClient) {
                await this.verifyAccountClient(request._id, user.phone, user.full_name);
            } else {
                await this.verifyAccountEmployee(request._id, user.phone, user.full_name);
            }
        }

        return {
            request,
            token: this.getJwtToken({id: request._id})
        };
    }

    async login(loginUserDto: LoginUserDto) {
        const {password, phone} = loginUserDto;

        let user = await this.userModel.findOne({phone: phone});

        if (!user)
            throw new UnauthorizedException("Credentials are not valid");
        if (!bcrypt.compareSync(password, user.password))
            throw  new UnauthorizedException("Credentials are not valid");
        user.password = "";
        return {
            user,
            token: this.getJwtToken({id: user._id})
        }
    }

    async loginEmployee(loginEmployeeDto: LoginEmployeeDto) {
        const {password, email} = loginEmployeeDto;

        let user = await this.userModel.findOne({email});

        if (!user)
            throw new UnauthorizedException("Credentials are not valid");
        if (!bcrypt.compareSync(password, user.password))
            throw  new UnauthorizedException("Credentials are not valid");
        user.password = "";
        return {
            user,
            token: this.getJwtToken({id: user._id})
        }

    }

    async checkAuthStatus(auth: any) {
        let jwtToken = auth.split(' ');
        const token = jwtToken[1];
        try {
            const verify = this.jwtService.verify(token, this.configService.get('JWT_SECRET'));
            if (verify) {
                let request = await this.userModel.findOne({_id: verify.id});
                return {
                    request,
                    token
                };
            }
        } catch (error) {
            this.handleDbErrors(error);
        }
    }

    async updatePassWord(auth: any, pass: PasswordDto) {
        let jwtToken = auth.split(' ');
        const token = jwtToken[1];
        const verify = this.jwtService.verify(token, this.configService.get('JWT_SECRET'));
        if (verify) {
            try {
                let userDB = await this.userModel.findOne({_id: verify.id});
                userDB.password = bcrypt.hashSync(pass['password'], 10);
                await userDB.updateOne(userDB);
                return userDB;
            } catch (error) {
                this.handleDbErrors(error)
            }
        } else {
            throw new UnauthorizedException("Not authorization update password.");
        }


    }

    async sendRecoveryPassword({phone}: { phone: string }) {
        let myUser = await this.userModel.findOne({phone: phone});
        if (myUser) {
            let _idToken = this.getJwtToken({id: myUser._id});
            try {
                await this.mailerService
                    .sendMail({
                        to: 'info@xisfo.co', // list of receivers
                        from: 'vlzdavid12@outlook.co', // sender address
                        subject: 'Xisfo Recuperar Contraseña', // Subject line
                        template: process.cwd() + '/apps/xisfo-gateway/src/templates/recovery_pass',
                        context: {
                            full_name: myUser.full_name,
                            url_token: `${process.env.URI_API_FROND}api/auth2/recovery-password/${_idToken}`,
                        }
                    });
                return {msg: 'Send message successfully'};
            } catch (e) {
                return {msg: 'Error send message'}
            }
        } else {
            throw new NotFoundException('Not found number phone...');
        }
    }

    async sendRecoveryPasswordEmployee({email}: { email: string }) {
        let myUser = await this.userModel.findOne({email: email});
        if (myUser) {
            let _idToken = this.getJwtToken({id: myUser._id});
            try {
                await this.mailerService
                    .sendMail({
                        to: 'info@xisfo.co', // list of receivers
                        from: 'vlzdavid12@outlook.co', // sender address
                        subject: 'Xisfo Recuperar Contraseña', // Subject line
                        template: process.cwd() + '/apps/xisfo-gateway/src/templates/recovery_pass',
                        context: {
                            full_name: myUser.full_name,
                            url_token: `${process.env.URI_API_FROND}api/auth/recovery-password-employee/${_idToken}`,
                        }
                    });
                return {msg: 'Send message successfully'};
            } catch (e) {
                return {msg: 'Error send message'}
            }
        } else {
            throw new NotFoundException('Not found email ...');
        }
    }

    async searchPhoneUser({phone}: { phone: string }) {

            let user = await this.userModel.findOne({phone: phone});
        
            if(user != null){
                return {
                    _id: user.id,
                    avatar: user.avatar
                };
            }else{
                return null
            }

    }

    async searchEmailUser({email}: { email: string }) {
        try {
            let user = await this.userModel.findOne({email: email});
            return {
                _id: user.id,
                avatar: user.avatar
            }
        } catch (error) {
            return null;
        }
    }


    async verifyAccountEmployee(_id: string, phone: string, full_name: string) {
        let _idToken = this.getJwtToken({id: _id.toString()});
        try {
            await this.mailerService
                .sendMail({
                    to: 'vlzdavid@outlook.com', // list of receivers
                    from: 'vlzdavid12@outlook.com', // sender address
                    subject: 'Xisfo Confirmar Cuenta', // Subject line
                    template: process.cwd() + '/apps/xisfo-gateway/src/templates/confirm_account',
                    context: {
                        full_name: full_name.toUpperCase(),
                        url_token: `${process.env.URI_API}api/auth/confirm-account-employee/` + _idToken,
                    }
                });
        } catch (e) {
            return {msg: 'Error not send email'}
        }
    }

    async verifyAccountClient(_id: string, phone: string, full_name: string) {
        let _idToken = this.getJwtToken({id: _id.toString()});
        try {
            await this.mailerService
                .sendMail({
                    to: 'vlzdavid@outlook.com', // list of receivers
                    from: 'vlzdavid12@outlook.com', // sender address
                    subject: 'Xisfo Confirmar Cuenta', // Subject line
                    template: process.cwd() + '/apps/xisfo-gateway/src/templates/confirm_account',
                    context: {
                        full_name: full_name.toUpperCase(),
                        url_token: `${process.env.URI_API}api/auth/confirm-account/` + _idToken,
                    }
                });
        } catch (e) {
            return {msg: 'Error not send email'}
        }
    }


    async confirmAccountService({id}: { id: string }) {
        const verify = this.jwtService.verify(id, this.configService.get('JWT_SECRET'));
        if (verify) {
            let userDB = await this.userModel.findOne({_id: verify.id});
            userDB.isActive = true;
            await userDB.updateOne(userDB);
            return;
        }

        throw new UnauthorizedException();

    }


    private getJwtToken(payload: JwtPayload) {
        return this.jwtService.sign(payload);
    }

    private handleDbErrors(error: any) {
        if (error.code === "23505") {
            throw new BadRequestException(error.detail)
        }
        throw  new InternalServerErrorException();
    }


}
