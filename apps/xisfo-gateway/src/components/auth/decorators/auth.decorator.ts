import { applyDecorators, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserRolePermissionGuard } from '../guards/user-role.guard';
import { ValidRoles,  ValidPermission } from '../interfaces';
import {PermissionProtected, RoleProtected} from './role-protected.decorator';


export function Auth( roles: ValidRoles[], permission?: ValidPermission[]) {
  return applyDecorators(
    RoleProtected(...roles),
    PermissionProtected(...permission),
    UseGuards( AuthGuard(), UserRolePermissionGuard ),
  );

}
