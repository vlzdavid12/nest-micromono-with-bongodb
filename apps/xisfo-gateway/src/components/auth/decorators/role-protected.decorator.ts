import { SetMetadata } from '@nestjs/common';
import {ValidPermission, ValidRoles} from '../interfaces';

export const META_ROLES = 'roles';
export const META_PERMISSION = 'permission';

export const RoleProtected = (...args: ValidRoles[] ) => {
    return SetMetadata( META_ROLES , args);
}

export const PermissionProtected = (...args: ValidPermission[] ) => {
    return SetMetadata( META_PERMISSION , args);
}
