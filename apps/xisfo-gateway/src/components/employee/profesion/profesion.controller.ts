import { Controller, Get } from '@nestjs/common';
import { ProfesionService } from './profesion.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Profesion')
@Controller('profesion')
export class ProfesionController {
  constructor(private readonly profesionService: ProfesionService) {}

  @Get()
  findAll() {
    return this.profesionService.findAll();
  }
}
