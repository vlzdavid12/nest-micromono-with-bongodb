import {Controller, Get, Post, Body, Patch, Param, Delete, Query} from '@nestjs/common';
import { EmployeeResourceService } from './employee-resource.service';
import { UpdateEmployeeResourceDto } from './dto/update-employee-resource.dto';
import {ApiTags} from "@nestjs/swagger";
import {PaginationDto} from "../../../commons/paginationDto";
import {PaginationDtoSearch} from "../../../commons/paginationDto.filter";
import {PaginationDtoAdvancedEmployeesFilter} from "../../../commons/filters/paginationDtoAdvancedEmployee.filter";
import { CreateEmployeeRequestDto } from './dto/create-employee-request.dto';

@ApiTags('Employee')
@Controller('employee')
export class EmployeeResourceController {
  constructor(private readonly employeeResourceService: EmployeeResourceService) {}

  @Post()
  create(@Body() createEmployeeResourceDto: CreateEmployeeRequestDto) {
    return this.employeeResourceService.create(createEmployeeResourceDto);
  }

  @Get()
  findAll(@Query() paginationDto: PaginationDto ) {
    return this.employeeResourceService.findAll(paginationDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.employeeResourceService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateEmployeeResourceDto: UpdateEmployeeResourceDto) {
    return this.employeeResourceService.update(id, updateEmployeeResourceDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.employeeResourceService.remove(id);
  }

  @Get('document/:no')
  findOneDocument(@Param('no') no: string) {
    return this.employeeResourceService.findOneByDocument(no);
  }

  @Get('filter/search')
  filterEmployee(@Query() paginationDtoSearch: PaginationDtoSearch) {
    return this.employeeResourceService.filterEmployee(paginationDtoSearch);
  }

  @Get('filter/advanced/search')
  filterAdvanced(@Query() paginationFilterAdvanced: PaginationDtoAdvancedEmployeesFilter) {
    return this.employeeResourceService.filterEmployeeAdvanced(paginationFilterAdvanced);
  }
}
