export interface EmployeeInterface {
    results: EmployeeData
}

export interface EmployeeData {
    first_name: string;
    second_name: string;
    last_name: string;
    second_last_name: string;
    email: string;
    phone: string;
    birthday: Date;
    childrens: string
    civil_status_id: string;
    contract_type_id: string;
    gender_id: string;
    position_id: string;
    headquarter_id: string;
    health_provider_id: string;
    occupational_risk_manager_id: string;
    sociodemographic_profile_id: string;
    pension_fund_id: string;
    education_level_id: string;
    profesions: string[];
}
