import {
    IsArray,
    IsBoolean,
    IsEmail,
    IsNotEmpty,
    IsOptional,
    IsString
} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateEmployeeResourceDto {
    @ApiProperty()
    @IsString()
    avatar: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    full_name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    first_name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    second_name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    last_name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    second_last_name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    document_type_id: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    expedition_city: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    identification_number: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    expedition_date: Date;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    birthday: Date;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    password: string

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    phone: string

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    childrens: string

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    @IsEmail()
    email: string


    @ApiProperty()
    @IsArray()
    roles: string[];

    @ApiProperty()
    @IsArray()
    permissions: string[];

    @ApiProperty()
    @IsNotEmpty()
    @IsBoolean()
    isClient: boolean;


    @ApiProperty()
    @IsArray()
    @IsOptional()
    profesions: string[];

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    civil_status_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    contract_type_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    gender_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    position_id: string;

    @ApiProperty()
    @IsString()
    headquarter_id: string;

    @ApiProperty()
    @IsString()
    health_provider_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    occupational_risk_manager_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    sociodemographic_profile_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    pension_fund_id: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    education_level_id: string;

    @ApiProperty()
    @IsArray()
    data_extra: any[];


}
