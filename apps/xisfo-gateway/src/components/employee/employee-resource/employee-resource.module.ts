import { Module } from '@nestjs/common';
import { EmployeeResourceService } from './employee-resource.service';
import { EmployeeResourceController } from './employee-resource.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [ClientsXisfoModules],
  controllers: [EmployeeResourceController],
  providers: [EmployeeResourceService]
})
export class EmployeeResourceModule { }
