import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { UpdateEmployeeResourceDto } from './dto/update-employee-resource.dto';
import {PaginationDto} from "../../../commons/paginationDto";
import {PaginationDtoSearch} from "../../../commons/paginationDto.filter";
import {PaginationDtoAdvancedEmployeesFilter} from "../../../commons/filters/paginationDtoAdvancedEmployee.filter";
import { CreateEmployeeRequestDto } from './dto/create-employee-request.dto';

@Injectable()
export class EmployeeResourceService {

  constructor(
    @Inject('XISFO_CLIENTS') private xisfoClient: ClientProxy
  ) { }

  create(createEmployeeResourceDto: CreateEmployeeRequestDto) { 
    return this.xisfoClient.send('createEmployee', createEmployeeResourceDto);
  }

  findAll(paginationDto: PaginationDto) {
    return this.xisfoClient.send('findAllEmployee', paginationDto);
  }

  findOne(id: string) {
    return this.xisfoClient.send('findOneEmployee', id);
  }

  update(id: string, updateEmployeeResourceDto: UpdateEmployeeResourceDto) {
    return this.xisfoClient.send('updateEmployee', { id, ...updateEmployeeResourceDto });
  }

  remove(id: string) {
    return this.xisfoClient.send('removeEmployee', id);
  }

  findOneByDocument(no: string){
    return this.xisfoClient.send('documentValidate', no);
  }

  filterEmployee(paginationDtoSearch: PaginationDtoSearch) {
    return this.xisfoClient.send('filterEmployee', paginationDtoSearch);
  }

  filterEmployeeAdvanced(searchAdvanced: PaginationDtoAdvancedEmployeesFilter){
    return this.xisfoClient.send('filterAdvancedEmployee', searchAdvanced )
  }
}
