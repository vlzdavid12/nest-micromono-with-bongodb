import { Controller, Get } from '@nestjs/common';
import { StrataService } from './strata.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags("Strata")
@Controller('strata')
export class StrataController {
  constructor(private readonly strataService: StrataService) { }

  @Get()
  findAll() {
    return this.strataService.findAll();
  }
}
