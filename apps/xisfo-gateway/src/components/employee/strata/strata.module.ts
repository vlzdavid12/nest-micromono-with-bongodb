import { Module } from '@nestjs/common';
import { StrataService } from './strata.service';
import { StrataController } from './strata.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports:[ClientsXisfoModules],
  controllers: [StrataController],
  providers: [StrataService]
})
export class StrataModule {}
