import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { EducationLevelService } from './education-level.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Education Level')
@Controller('education-level')
export class EducationLevelController {
  constructor(private readonly educationLevelService: EducationLevelService) { }

  @Get()
  findAll() {
    return this.educationLevelService.findAll();
  }
}
