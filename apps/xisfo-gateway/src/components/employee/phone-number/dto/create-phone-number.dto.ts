import { IsNotEmpty, IsString } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreatePhoneNumberDto {

    @ApiProperty()
    @IsString()
    phone_number: string

    @ApiProperty()
    @IsNotEmpty()
    employee: string

}
