import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreatePhoneNumberDto } from './dto/create-phone-number.dto';
import { UpdatePhoneNumberDto } from './dto/update-phone-number.dto';

@Injectable()
export class PhoneNumberService {

  constructor(
    @Inject('XISFO_CLIENTS') private xisfoClient: ClientProxy
  ) { }

  create(createPhoneNumberDto: CreatePhoneNumberDto) {
    return this.xisfoClient.send('createPhoneNumber', createPhoneNumberDto);
  }

  findAll() {
    return this.xisfoClient.send('findAllPhoneNumber', '');
  }

  findOne(id: string) {
    return this.xisfoClient.send('findOnePhoneNumber', id);
  }

  update(id: string, updatePhoneNumberDto: UpdatePhoneNumberDto) {
    return this.xisfoClient.send('updatePhoneNumber', { id, ...updatePhoneNumberDto });
  }

  remove(id: string) {
    return this.xisfoClient.send('removePhoneNumber', id);
  }
}
