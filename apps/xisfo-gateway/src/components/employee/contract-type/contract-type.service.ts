import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateContractTypeDto } from './dto/create-contract-type.dto';
import { UpdateContractTypeDto } from './dto/update-contract-type.dto';
import {PaginationDto} from "../../../commons/paginationDto";

@Injectable()
export class ContractTypeService {

  constructor(@Inject('XISFO_CLIENTS') private clientXisfo: ClientProxy) { }

  create(createContractTypeDto: CreateContractTypeDto) {
    return this.clientXisfo.send('createContractType', createContractTypeDto);
  }

  findAll(paginationDto: PaginationDto) {
    return this.clientXisfo.send('findAllContractType', paginationDto);
  }

  findOne(id: number) {
    return `This action returns a #${id} contractType`;
  }

  update(id: number, updateContractTypeDto: UpdateContractTypeDto) {
    return `This action updates a #${id} contractType`;
  }

  remove(id: number) {
    return `This action removes a #${id} contractType`;
  }
}
