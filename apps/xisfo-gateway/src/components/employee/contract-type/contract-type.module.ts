import { Module } from '@nestjs/common';
import { ContractTypeService } from './contract-type.service';
import { ContractTypeController } from './contract-type.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [
    ClientsXisfoModules
  ],
  controllers: [ContractTypeController],
  providers: [ContractTypeService]
})
export class ContractTypeModule { }
