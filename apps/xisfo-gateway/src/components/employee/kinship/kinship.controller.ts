import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { KinshipService } from './kinship.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Kinship')
@Controller('kinship')
export class KinshipController {
  constructor(private readonly kinshipService: KinshipService) {}

  @Get()
  findAll() {
    return this.kinshipService.findAll();
  }
}
