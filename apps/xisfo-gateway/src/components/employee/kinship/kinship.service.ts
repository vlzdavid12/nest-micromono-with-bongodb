import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class KinshipService {

  constructor(@Inject('XISFO_CLIENTS') private clientXisfo: ClientProxy) { }

  findAll() {
    return this.clientXisfo.send('findAllKinship', '');
  }
}
