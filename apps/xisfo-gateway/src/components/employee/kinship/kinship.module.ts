import { Module } from '@nestjs/common';
import { KinshipService } from './kinship.service';
import { KinshipController } from './kinship.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [ClientsXisfoModules],
  controllers: [KinshipController],
  providers: [KinshipService]
})
export class KinshipModule { }
