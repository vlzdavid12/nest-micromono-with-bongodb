import { PartialType } from '@nestjs/mapped-types';
import { CreateEmployeeIdentificationDto } from './create-employee-identification.dto';

export class UpdateEmployeeIdentificationDto extends PartialType(CreateEmployeeIdentificationDto) {}
