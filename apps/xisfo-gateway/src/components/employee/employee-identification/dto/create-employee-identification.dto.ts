import { IsNotEmpty } from "class-validator"
import {ApiProperty} from "@nestjs/swagger";

export class CreateEmployeeIdentificationDto {
    @ApiProperty()
    @IsNotEmpty()
    identification_number: string

    @ApiProperty()
    @IsNotEmpty()
    expedition_date: Date

    @ApiProperty()
    @IsNotEmpty()
    expedition_city: string

    @ApiProperty()
    @IsNotEmpty()
    document_type: string

    @ApiProperty()
    @IsNotEmpty()
    employee: string
}
