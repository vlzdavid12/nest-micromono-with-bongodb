import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { EmployeeIdentificationService } from './employee-identification.service';
import { CreateEmployeeIdentificationDto } from './dto/create-employee-identification.dto';
import { UpdateEmployeeIdentificationDto } from './dto/update-employee-identification.dto';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Employee Identification')
@Controller('employee-identification')
export class EmployeeIdentificationController {
  constructor(private readonly employeeIdentificationService: EmployeeIdentificationService) { }

  @Post()
  create(@Body() createEmployeeIdentificationDto: CreateEmployeeIdentificationDto) {
    return this.employeeIdentificationService.create(createEmployeeIdentificationDto);
  }

  @Get()
  findAll() {
    return this.employeeIdentificationService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.employeeIdentificationService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateEmployeeIdentificationDto: UpdateEmployeeIdentificationDto) {
    return this.employeeIdentificationService.update(id, updateEmployeeIdentificationDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.employeeIdentificationService.remove(id);
  }
}
