import { Module } from '@nestjs/common';
import { EmployeeIdentificationService } from './employee-identification.service';
import { EmployeeIdentificationController } from './employee-identification.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports:[ClientsXisfoModules],
  controllers: [EmployeeIdentificationController],
  providers: [EmployeeIdentificationService]
})
export class EmployeeIdentificationModule {}
