import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateEmployeeIdentificationDto } from './dto/create-employee-identification.dto';
import { UpdateEmployeeIdentificationDto } from './dto/update-employee-identification.dto';

@Injectable()
export class EmployeeIdentificationService {

  constructor(
    @Inject('XISFO_CLIENTS') private xisfoClient: ClientProxy
  ) { }

  create(createEmployeeIdentificationDto: CreateEmployeeIdentificationDto) {
    return this.xisfoClient.send('createEmployeeIdentification', createEmployeeIdentificationDto);
  }

  findAll() {
    return this.xisfoClient.send('findAllEmployeeIdentification', '');
  }

  findOne(id: string) {
    return this.xisfoClient.send('findOneEmployeeIdentification', id);
  }

  update(id: string, updateEmployeeIdentificationDto: UpdateEmployeeIdentificationDto) {
    return this.xisfoClient.send('updateEmployeeIdentification', { id, ...updateEmployeeIdentificationDto });
  }

  remove(id: string) {
    return this.xisfoClient.send('removeEmployeeIdentification', id);
  }
}
