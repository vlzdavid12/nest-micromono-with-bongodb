import { Module } from '@nestjs/common';
import { PensionFundService } from './pension-fund.service';
import { PensionFundController } from './pension-fund.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports:[ClientsXisfoModules],
  controllers: [PensionFundController],
  providers: [PensionFundService]
})
export class PensionFundModule {}
