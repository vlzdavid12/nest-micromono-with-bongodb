import { Controller, Get } from '@nestjs/common';
import { PensionFundService } from './pension-fund.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Pension Fund')
@Controller('pension-fund')
export class PensionFundController {
  constructor(private readonly pensionFundService: PensionFundService) {}

  @Get()
  findAll() {
    return this.pensionFundService.findAll();
  }
}
