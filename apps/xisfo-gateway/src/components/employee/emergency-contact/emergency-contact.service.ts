import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateEmergencyContactDto } from './dto/create-emergency-contact.dto';
import { UpdateEmergencyContactDto } from './dto/update-emergency-contact.dto';

@Injectable()
export class EmergencyContactService {

  constructor(
    @Inject('XISFO_CLIENTS') private xisfoClient: ClientProxy
  ) { }

  create(createEmergencyContactDto: CreateEmergencyContactDto) {
    return this.xisfoClient.send('createEmergencyContact', createEmergencyContactDto);
  }

  findAll() {
    return this.xisfoClient.send('findAllEmergencyContact', '');
  }

  findOne(id: string) {
    return this.xisfoClient.send('findOneEmergencyContact', id);
  }

  update(id: string, updateEmergencyContactDto: UpdateEmergencyContactDto) {
    return this.xisfoClient.send('updateEmergencyContact', { id, ...updateEmergencyContactDto });
  }

  remove(id: string) {
    return this.xisfoClient.send('removeEmergencyContact', id);
  }
}
