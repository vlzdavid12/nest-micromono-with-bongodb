import { IsNotEmpty, IsNotEmptyObject } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateEmergencyContactDto {

    @ApiProperty()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsNotEmpty()
    phone_number: number;

    @ApiProperty()
    @IsNotEmptyObject()
    kinship: object;

    @ApiProperty()
    @IsNotEmptyObject()
    employee: object
}
