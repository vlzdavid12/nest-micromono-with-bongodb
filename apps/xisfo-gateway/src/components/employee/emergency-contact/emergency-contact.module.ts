import { Module } from '@nestjs/common';
import { EmergencyContactService } from './emergency-contact.service';
import { EmergencyContactController } from './emergency-contact.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [ClientsXisfoModules],
  controllers: [EmergencyContactController],
  providers: [EmergencyContactService]
})
export class EmergencyContactModule {}
