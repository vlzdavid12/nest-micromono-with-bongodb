import { Module } from '@nestjs/common';
import { HeadquarterService } from './headquarter.service';
import { HeadquarterController } from './headquarter.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [ClientsXisfoModules],
  controllers: [HeadquarterController],
  providers: [HeadquarterService]
})
export class HeadquarterModule { }
