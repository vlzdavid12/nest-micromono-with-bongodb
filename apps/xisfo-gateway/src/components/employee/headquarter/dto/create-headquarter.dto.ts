import { IsNotEmpty, IsNotEmptyObject, Length, Min } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateHeadquarterDto {

    @ApiProperty()
    @Length(3, 100)
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsNotEmpty()
    city: string
}
