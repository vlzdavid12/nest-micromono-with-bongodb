import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateHeadquarterDto } from './dto/create-headquarter.dto';
import { UpdateHeadquarterDto } from './dto/update-headquarter.dto';
import {PaginationDto} from "../../../commons/paginationDto";

@Injectable()
export class HeadquarterService {

  constructor(
    @Inject('XISFO_CLIENTS') private xisfoClient: ClientProxy
  ) { }

  create(createHeadquarterDto: CreateHeadquarterDto) {
    return this.xisfoClient.send('createHeadquarter', createHeadquarterDto);
  }

  findAll(paginationDto: PaginationDto) {
    return this.xisfoClient.send('findAllHeadquarter', paginationDto);
  }

  findOne(id: string) {
    return this.xisfoClient.send('findOneHeadquarter', id);
  }

  update(id: string, updateHeadquarterDto: UpdateHeadquarterDto) {
    return this.xisfoClient.send('updateHeadquarter', { id, ...updateHeadquarterDto });
  }

  remove(id: string) {
    return this.xisfoClient.send('removeHeadquarter', id);
  }
}
