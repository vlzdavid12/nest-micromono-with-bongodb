import { IsEmail, IsNotEmpty, IsNotEmptyObject } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateEmailAddressDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    email_address: string

    @ApiProperty()
    @IsNotEmpty()
    employee: string
}
