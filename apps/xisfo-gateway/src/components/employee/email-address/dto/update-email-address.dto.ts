import { PartialType } from '@nestjs/mapped-types';
import { CreateEmailAddressDto } from './create-email-address.dto';

export class UpdateEmailAddressDto extends PartialType(CreateEmailAddressDto) {}
