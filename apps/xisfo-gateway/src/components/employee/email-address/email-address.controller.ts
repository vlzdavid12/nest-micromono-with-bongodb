import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { EmailAddressService } from './email-address.service';
import { CreateEmailAddressDto } from './dto/create-email-address.dto';
import { UpdateEmailAddressDto } from './dto/update-email-address.dto';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Email Address')
@Controller('email-address')
export class EmailAddressController {
  constructor(private readonly emailAddressService: EmailAddressService) {}

  @Post()
  create(@Body() createEmailAddressDto: CreateEmailAddressDto) {
    return this.emailAddressService.create(createEmailAddressDto);
  }

  @Get()
  findAll() {
    return this.emailAddressService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.emailAddressService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateEmailAddressDto: UpdateEmailAddressDto) {
    return this.emailAddressService.update(id, updateEmailAddressDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.emailAddressService.remove(id);
  }
}
