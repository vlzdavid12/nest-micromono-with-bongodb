import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateEmailAddressDto } from './dto/create-email-address.dto';
import { UpdateEmailAddressDto } from './dto/update-email-address.dto';

@Injectable()
export class EmailAddressService {

  constructor(
    @Inject('XISFO_CLIENTS') private xisfoClient: ClientProxy
  ) { }

  create(createEmailAddressDto: CreateEmailAddressDto) {
    return this.xisfoClient.send('createEmailAddress', createEmailAddressDto);
  }

  findAll() {
    return this.xisfoClient.send('findAllEmailAddress', '');
  }

  findOne(id: string) {
    return this.xisfoClient.send('findOneEmailAddress', id);
  }

  update(id: string, updateEmailAddressDto: UpdateEmailAddressDto) {
    return this.xisfoClient.send('updateEmailAddress', { id, ...updateEmailAddressDto });
  }

  remove(id: string) {
    return this.xisfoClient.send('removeEmailAddress', id);
  }
}
