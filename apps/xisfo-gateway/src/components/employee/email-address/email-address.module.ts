import { Module } from '@nestjs/common';
import { EmailAddressService } from './email-address.service';
import { EmailAddressController } from './email-address.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [ClientsXisfoModules],
  controllers: [EmailAddressController],
  providers: [EmailAddressService]
})
export class EmailAddressModule { }
