import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class CivilStatusService {
  constructor(@Inject('XISFO_CLIENTS') private clientXisfo: ClientProxy) { }

  findAll() {
    return this.clientXisfo.send('findAllCivilStatus', '');
  }
}
