import { Module } from '@nestjs/common';
import { CivilStatusService } from './civil-status.service';
import { CivilStatusController } from './civil-status.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [ClientsXisfoModules],
  controllers: [CivilStatusController],
  providers: [CivilStatusService]
})
export class CivilStatusModule { }
