import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CivilStatusService } from './civil-status.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Civil Status')
@Controller('civil-status')
export class CivilStatusController {
  constructor(private readonly civilStatusService: CivilStatusService) {}

  @Get()
  findAll() {
    return this.civilStatusService.findAll();
  }
}
