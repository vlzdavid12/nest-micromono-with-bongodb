import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class GenderService {

  constructor(
    @Inject('XISFO_CLIENTS') private xisfoClients: ClientProxy
  ){}

  findAll() {
    return this.xisfoClients.send('findAllGender', '');
  }
}
