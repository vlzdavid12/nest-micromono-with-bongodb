import { Module } from '@nestjs/common';
import { PositionService } from './position.service';
import { PositionController } from './position.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports:[ClientsXisfoModules],
  controllers: [PositionController],
  providers: [PositionService]
})
export class PositionModule {}
