import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { HousingTypeService } from './housing-type.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Housting Type')
@Controller('housing-type')
export class HousingTypeController {
  constructor(private readonly housingTypeService: HousingTypeService) {}

  @Get()
  findAll() {
    return this.housingTypeService.findAll();
  }
}
