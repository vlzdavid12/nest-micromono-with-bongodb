import { Module } from '@nestjs/common';
import { HousingTypeService } from './housing-type.service';
import { HousingTypeController } from './housing-type.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [ClientsXisfoModules],
  controllers: [HousingTypeController],
  providers: [HousingTypeService]
})
export class HousingTypeModule {}
