import { Module } from '@nestjs/common';
import { ContractTypeModule } from './contract-type/contract-type.module';
import { KinshipModule } from './kinship/kinship.module';
import { CivilStatusModule } from './civil-status/civil-status.module';
import { GenderModule } from './gender/gender.module';
import { HealthProviderModule } from './health-provider/health-provider.module';
import { OccupationalRiskManagerModule } from './occupational-risk-manager/occupational-risk-manager.module';
import { PensionFundModule } from './pension-fund/pension-fund.module';
import { EducationLevelModule } from './education-level/education-level.module';
import { ProfesionModule } from './profesion/profesion.module';
import { PositionModule } from './position/position.module';
import { HousingTypeModule } from './housing-type/housing-type.module';
import { StrataModule } from './strata/strata.module';
import { HeadquarterModule } from './headquarter/headquarter.module';
import { EmergencyContactModule } from './emergency-contact/emergency-contact.module';
import { EmployeeResourceModule } from './employee-resource/employee-resource.module';
import { EmailAddressModule } from './email-address/email-address.module';
import { PhoneNumberModule } from './phone-number/phone-number.module';
import { SociodemographicProfileModule } from './sociodemographic-profile/sociodemographic-profile.module';
import { EmployeeIdentificationModule } from './employee-identification/employee-identification.module';

@Module({
  imports: [
    ContractTypeModule,
    KinshipModule,
    CivilStatusModule,
    GenderModule,
    HealthProviderModule,
    OccupationalRiskManagerModule,
    PensionFundModule,
    EducationLevelModule,
    ProfesionModule,
    PositionModule,
    HousingTypeModule,
    StrataModule,
    HeadquarterModule,
    EmergencyContactModule,
    EmployeeResourceModule,
    EmailAddressModule,
    PhoneNumberModule,
    SociodemographicProfileModule,
    EmployeeIdentificationModule,
  ],
  exports: [
    KinshipModule,
    ContractTypeModule,
    CivilStatusModule,
    GenderModule,
    HealthProviderModule,
    OccupationalRiskManagerModule,
    PensionFundModule,
    ProfesionModule,
    PositionModule,
    HousingTypeModule,
    StrataModule,
    HeadquarterModule,
    EmergencyContactModule,
    EmployeeResourceModule,
  ]
})
export class EmployeeModule { }
