import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { HealthProviderService } from './health-provider.service';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Health Provider')
@Controller('health-provider')
export class HealthProviderController {
  constructor(private readonly healthProviderService: HealthProviderService) {}


  @Get()
  findAll() {
    return this.healthProviderService.findAll();
  }
}
