import { Module } from '@nestjs/common';
import { HealthProviderService } from './health-provider.service';
import { HealthProviderController } from './health-provider.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [ClientsXisfoModules],
  controllers: [HealthProviderController],
  providers: [HealthProviderService]
})
export class HealthProviderModule {}
