import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { OccupationalRiskManagerService } from './occupational-risk-manager.service';
import {ApiTags} from "@nestjs/swagger";


@ApiTags('Occupational Risk Manager')
@Controller('occupational-risk-manager')
export class OccupationalRiskManagerController {
  constructor(private readonly occupationalRiskManagerService: OccupationalRiskManagerService) {}

  @Get()
  findAll() {
    return this.occupationalRiskManagerService.findAll();
  }
}
