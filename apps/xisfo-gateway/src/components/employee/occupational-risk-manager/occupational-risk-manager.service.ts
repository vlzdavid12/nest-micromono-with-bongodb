import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
@Injectable()
export class OccupationalRiskManagerService {

  constructor(
    @Inject('XISFO_CLIENTS') private xisfoClient: ClientProxy
  ){}

  findAll() {
    return this.xisfoClient.send('findAllOccupationalRiskManager', '');
  }
}
