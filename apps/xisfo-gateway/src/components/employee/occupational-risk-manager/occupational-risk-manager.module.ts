import { Module } from '@nestjs/common';
import { OccupationalRiskManagerService } from './occupational-risk-manager.service';
import { OccupationalRiskManagerController } from './occupational-risk-manager.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [ClientsXisfoModules],
  controllers: [OccupationalRiskManagerController],
  providers: [OccupationalRiskManagerService]
})
export class OccupationalRiskManagerModule {}
