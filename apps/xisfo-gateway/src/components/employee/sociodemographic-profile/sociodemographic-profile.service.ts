import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateSociodemographicProfileDto } from './dto/create-sociodemographic-profile.dto';
import { UpdateSociodemographicProfileDto } from './dto/update-sociodemographic-profile.dto';

@Injectable()
export class SociodemographicProfileService {

  constructor(
    @Inject('XISFO_CLIENTS') private xisfoClient: ClientProxy
  ) { }

  create(createSociodemographicProfileDto: CreateSociodemographicProfileDto) {
    return this.xisfoClient.send('createSociodemographicProfile', createSociodemographicProfileDto);
  }

  findAll() {
    return this.xisfoClient.send('findAllSociodemographicProfile', '');
  }

  findOne(id: string) {
    return this.xisfoClient.send('findOneSociodemographicProfile', id);
  }

  update(id: string, updateSociodemographicProfileDto: UpdateSociodemographicProfileDto) {
    return this.xisfoClient.send('updateSociodemographicProfile', { id, ...updateSociodemographicProfileDto });
  }

  remove(id: string) {
    return this.xisfoClient.send('removeSociodemographicProfile', id);
  }
}
