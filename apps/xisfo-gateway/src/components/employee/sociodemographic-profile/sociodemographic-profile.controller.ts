import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { SociodemographicProfileService } from './sociodemographic-profile.service';
import { CreateSociodemographicProfileDto } from './dto/create-sociodemographic-profile.dto';
import { UpdateSociodemographicProfileDto } from './dto/update-sociodemographic-profile.dto';
import {ApiTags} from "@nestjs/swagger";

@ApiTags('SocioDemographic Profile')
@Controller('sociodemographic-profile')
export class SociodemographicProfileController {
  constructor(private readonly sociodemographicProfileService: SociodemographicProfileService) {}

  @Post()
  create(@Body() createSociodemographicProfileDto: CreateSociodemographicProfileDto) {
    return this.sociodemographicProfileService.create(createSociodemographicProfileDto);
  }

  @Get()
  findAll() {
    return this.sociodemographicProfileService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.sociodemographicProfileService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSociodemographicProfileDto: UpdateSociodemographicProfileDto) {
    return this.sociodemographicProfileService.update(id, updateSociodemographicProfileDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.sociodemographicProfileService.remove(id);
  }
}
