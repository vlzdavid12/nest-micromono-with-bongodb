import { IsNotEmpty, IsNumber, IsString, Length } from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateSociodemographicProfileDto {

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    antiquity: number;

    @ApiProperty()
    @IsNotEmpty()
    @Length(5, 100)
    address: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    housing_type: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    strata: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    city: string;
}
