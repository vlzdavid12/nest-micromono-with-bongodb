import { PartialType } from '@nestjs/mapped-types';
import { CreateSociodemographicProfileDto } from './create-sociodemographic-profile.dto';

export class UpdateSociodemographicProfileDto extends PartialType(CreateSociodemographicProfileDto) {}
