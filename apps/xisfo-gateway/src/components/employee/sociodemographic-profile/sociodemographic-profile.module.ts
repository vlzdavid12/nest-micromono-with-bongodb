import { Module } from '@nestjs/common';
import { SociodemographicProfileService } from './sociodemographic-profile.service';
import { SociodemographicProfileController } from './sociodemographic-profile.controller';
import { ClientsXisfoModules } from '../../microservices/clients';

@Module({
  imports: [ClientsXisfoModules],
  controllers: [SociodemographicProfileController],
  providers: [SociodemographicProfileService]
})
export class SociodemographicProfileModule {}
