import { Module } from '@nestjs/common';
import { AuthModule } from "./auth/auth.module";
import { GeneralModule } from './general/general.module';
import { EmployeeModule } from './employee/employee.module';
import { ClientModule } from './client/client.module';
import { PlatformModule } from './platform/platform.module';
import { PaymentModule } from './payment/payment.module';
import { WalletModule } from './wallets/wallet/wallet.module';
import { TransferModule } from './wallets/transfer/transfer.module';
import { SellTokenModule } from './token-sale/sell-token/sell-token.module';
import { WithdrawalCostModule } from './wallets/withdrawal-cost/withdrawal-cost.module';

@Module({
    imports: [
        AuthModule,
        GeneralModule,
        EmployeeModule,
        ClientModule,
        PlatformModule,
        PaymentModule,
        WalletModule,
        TransferModule,
        SellTokenModule,
        WithdrawalCostModule,
    ],
    exports: [
        GeneralModule,
        EmployeeModule,
        AuthModule,
        ClientModule,
        PlatformModule,
        PaymentModule,
        WalletModule,
        TransferModule
    ],
})
export class ComponentsModule { }