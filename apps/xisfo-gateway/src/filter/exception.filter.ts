import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp(),
    response = ctx.getResponse();

    let error = exception.getResponse();

    return response.status(error['statusCode']).json({
        statusCode: error['statusCode'],
        createdBy: error['error'],
        errors: error['message'],
    });
  }
}
