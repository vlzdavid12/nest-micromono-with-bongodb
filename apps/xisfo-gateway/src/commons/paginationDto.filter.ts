import {ApiProperty} from "@nestjs/swagger";
import {IsNumber, IsOptional, Min} from "class-validator";
import {Type} from "class-transformer";


export class PaginationDtoSearch {
    @ApiProperty({default: "", required: false})
    @IsOptional()
    search?: string

    @ApiProperty({ enum: ['ASC', 'DESC'], default: "ASC"})
    @IsOptional()
    orderBy?: string

    @ApiProperty({default: 10})
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    @Min(1)
    limit?: number;

    @ApiProperty({default: 1})
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    @Min(0)
    offset?: number;



}
