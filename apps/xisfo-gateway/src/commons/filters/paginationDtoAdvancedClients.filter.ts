import {ApiProperty} from "@nestjs/swagger";
import {IsNumber, IsOptional, Min} from "class-validator";
import {Type} from "class-transformer";

export class PaginationDtoAdvancedClientsFilter {

    @ApiProperty({enum: ['natural', 'juridico'], default: '', required: false})
    @IsOptional()
    client_type?: string

    @ApiProperty({enum: ['moneda_extranjera', 'moneda_local'], default: '', required: false})
    @IsOptional()
    registration_statuses?: string

    // Pagination
    @ApiProperty({ enum: ['ASC', 'DESC'], default: "ASC"})
    @IsOptional()
    orderBy?: string

    @ApiProperty({default: 10})
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    @Min(1)
    limit?: number;

    @ApiProperty({default: 1})
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    @Min(0)
    offset?: number;


}
