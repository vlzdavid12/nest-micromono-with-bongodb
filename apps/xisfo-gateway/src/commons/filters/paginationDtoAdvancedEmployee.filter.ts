import {ApiProperty} from "@nestjs/swagger";
import {IsNumber, IsOptional, Min} from "class-validator";
import {Type} from "class-transformer";

export class PaginationDtoAdvancedEmployeesFilter {

    @ApiProperty({enum: ['soltero', 'casado', 'divorsiado', 'union libre'], default: '', required: false})
    @IsOptional()
    civil_status_id?: string

    @ApiProperty({enum: ['Indefinido', 'Definido', 'Obra o labor', 'Prestacion Servicios'], default: '', required: false})
    @IsOptional()
    contract_type_id?: string

    @ApiProperty({enum: ['masculino', 'femenino', 'otro'], default: '', required: false})
    @IsOptional()
    gender_id?: string

    @ApiProperty({enum: ['Asesor', 'CEO', 'Creador Contenido', 'Director/a', 'Desarrollador/a'], default: '', required: false})
    @IsOptional()
    position_id?: string

    @ApiProperty({enum: ['Salud Total', 'Eps Sanitas', 'Nueva Eps', 'Sura', 'Otro'], default: '', required: false})
    @IsOptional()
    health_provider_id: string

    @ApiProperty({enum: ['AXA COLPATRIA S.A.', 'COLMENA SEGUROS', 'COMPAÑÍA DE SEGUROS DE VIDA AURORA S.A.', 'LA EQUIDAD SEGUROS DE VIDA', 'LIBERTY SEGUROS DE VIDA S.A.', 'SEGUROS SURA', 'OTRO'], default: '', required: false})
    @IsOptional()
    occupational_risk_manager_id?: string

    @ApiProperty({enum: ['Arrendada', 'Propia'], default: '', required: false})
    @IsOptional()
    housing_type?: string

    @ApiProperty({enum: ['Porvenir', 'Colpensiones', 'Colfondos', 'Protección', 'Zurich', 'Otro'], default: '', required: false})
    @IsOptional()
    pension_fund_id?: string

    @ApiProperty({enum: ['Bachiller', 'Técnico', 'Tecnologíco', 'Profesional'], default: '', required: false})
    @IsOptional()
    education_level_id?: string

    // Pagination
    @ApiProperty({ enum: ['ASC', 'DESC'], default: "ASC"})
    @IsOptional()
    orderBy?: string

    @ApiProperty({default: 10})
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    @Min(1)
    limit?: number;

    @ApiProperty({default: 1})
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    @Min(0)
    offset?: number;
}
