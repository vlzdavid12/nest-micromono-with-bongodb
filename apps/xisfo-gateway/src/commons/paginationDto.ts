import { IsNumber, IsOptional, Min } from 'class-validator';
import {ApiProperty} from "@nestjs/swagger";
import {Type} from "class-transformer";


export class PaginationDto {

    @ApiProperty({ enum: ['ASC', 'DESC'], default: "ASC"})
    @IsOptional()
    orderBy?: string

    @ApiProperty({default: 10})
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    @Min(1)
    limit?: number;

    @ApiProperty({default: 1})
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    @Min(0)
    offset?: number;



}
