import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {ComponentsModule} from './components/components.module';
import {ConfigModule} from "@nestjs/config";
import {MailerModule} from "@nestjs-modules/mailer";
import {HandlebarsAdapter} from "@nestjs-modules/mailer/dist/adapters/handlebars.adapter";

@Module({
    imports: [
        ConfigModule.forRoot(),
        MongooseModule.forRoot(process.env.DB_CONECTION),
        ComponentsModule,
        MailerModule.forRoot({
            // transport: 'smtps://user@example.com:topsecret@smtp.example.com',
            // or
            transport: {
                host: process.env.SMTP_GMAIL,
                port: 2525,
                secure: false,
                auth: {
                    user: process.env.USER_GMAIL,
                    pass: process.env.PASS_GMAIL,
                },
            },
            defaults: {
                from: '"Info Xisfo" <info@xisfo.co>',
            },
            template: {
                adapter: new HandlebarsAdapter(), // or new PugAdapter() or new EjsAdapter()
                options: {
                    strict: true,
                },
            },
        }),
    ],
})
export class AppModule {
}
