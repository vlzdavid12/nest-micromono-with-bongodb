import { ConflictException } from '@nestjs/common';


export const fileFilter = function (req, file, callback) {
    if (!file.originalname.match(/\.(jpg|jpeg|png|webp)$/)) {
      return callback(new ConflictException('The format is invalid'), false)
    }
    callback(null, true)
  }