export const fileNamer = function (req, file, callback) {
    callback(null, generateFilename(file));
}

function generateFilename(file) {
  return `${file.originalname}`;
}
  